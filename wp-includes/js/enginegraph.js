function switchToUserScreen() {
    //    alert (typeof $('#renn_droid') + ' ' + typeof map);
    $('#renn_droid').fadeOut('slow', function() {
        //        if(typeof map != "undefined") {
        //            delete map;
        //            map = null;
        //        }
        $('#user_main').fadeIn('slow', function() {
            });
    //        $('#renn_droid').empty();
    //        exit();
    });
//    $('#trip_1_race').html('');
}
function initPage(tripId) {
    if(typeof map == 'undefined') {
        $.get('/Ajax/Android/'+tripId+'/laps?csv=0&trip_id='+tripId, function(data) {
            $('#renn_droid').html(data);
        }); 
    } else {
        drawChart(0, 0, tripId, 'laps');
    }
    $('#user_main').fadeOut('slow', function() {
        // Animation complete
        $('#renn_droid').fadeIn('slow', function() {
            //            google.maps.event.trigger(map, 'resize');
            });
    });

}
function lapDetailFormatX(x) {
    d = Math.round(x - xOffset);
    return convertUnit('x', d);
}
function updatePage(data) {
    console.log(data);
    let obj = null;
    if (typeof data === 'string' || data instanceof String)
    {
        obj = jQuery.parseJSON(data);
    }
    else
    {
        obj = data;
    }

    for (var prop in obj.update) {
        if (obj.update.hasOwnProperty(prop)) { 
            // or if (Object.prototype.hasOwnProperty.call(obj,prop)) for safety...
            if (jQuery('#' + prop).is('input:text')) {
                jQuery('#' + prop).val(obj.update[prop]);
            } else {
                jQuery('#' + prop).html(obj.update[prop]);
            }
        }
    }
}

function initToolTips(className) {
    $('.' + className).hover(
        function() {
            this.tip = this.title;
            $(this).append(
                '<div class="toolTipWrapper">'
                +this.tip
                +'</div>'
                );
            this.title = "";
            this.width = $(this).width();
            $(this).find('.toolTipWrapper').css({
                left:this.width-22
            })
            $('.toolTipWrapper').fadeIn(300);
        },
        function() {
            $('.toolTipWrapper').fadeOut(100);
            $(this).children().remove();
            this.title = this.tip;
        });
}

//var colors;
var dashstyle = [
'solid', 
'shortdash',
'longdash',
];


function drawMap(min, max, tripId, lapview, lapzoom) {
    var url = '/Ajax/Android/0';
    if(typeof lapview != "undefined") {
        url = url + '/laps';
    }
    if (typeof lapzoom != "undefined") {
        url=url + '/lapzoom';
    }
    url = url + '?csv=1';
    if(max != 0) {
        url = url + '&min=' + min + '&max=' + max;
    }
    if(tripId != 0) {
        url = url + '&trip_id=' + tripId;
    }
    url += '&clr=' + $("#map_switches input[type='radio']:checked").val();
    
    $.post(url, $('#frmLap').serialize(), function(data){
        if(typeof data['error'] != 'undefined') {
            alert(data['error']);
            return;
        }
        data = $.parseJSON(data);
        mapData = data['Map'];
									
        
        var i = 0;
        deleteOverlays();
        $.each(mapData, function(key, md){
            setMarkers(md, '#FF0000');
            i++;
            if(i > colorList.length) {
                i = 0;
            }
        });
        google.maps.event.trigger(map, 'resize');
    });  
}
function fbIframe(url) {
    
}
function drawChart(min, max, tripId, lapview, lapzoom, rotx, roty, rotz) {

    var url = '/Ajax/Android/0';
    if(typeof lapview != "undefined") {
        url = url + '/laps';
    }
    if (typeof lapzoom != "undefined") {
        if(lapzoom != 0 ) {
            url=url + '/lapzoom';
        }
    }

    
    url = url + '?csv=1';
    if(max != 0) {
        url = url + '&min=' + min + '&max=' + max;
    }
    if(tripId != 0) {
        url = url + '&trip_id=' + tripId;
    }
    if (typeof rotx != "undefined") {
        url += '&rotx=' + rotx
    }
    if (typeof roty != "undefined") {
        url += '&roty=' + roty
    }
    if (typeof rotz != "undefined") {
        url += '&rotz=' + rotz
    }

    //    var visibleSeries = [];
    url += '&clr=' + $("#map_switches input[type='radio']:checked").val();
    var inp='&inp=';
    var sep = '';
    $("#frmSelectInputChannel input[type='radio']:checked").each(function(){
        inp += sep + $(this).val();
        sep = ',';
    });
    if(sep == ',') {
        url += inp;
    }
    var ser = '&ser=';
    sep = '';
    $("#frmSelectSeries input[type='checkbox']:checked").each(function(){
        ser += sep + $(this).val();
        sep = ',';
    });
    if(sep == ',') {
        url += ser;
        delete consoleHdrHtml;
    }
    //    console.log($('#frmLap').serialize());
    chart1.options.series = [];
    if($('#purl').length){
        var s = '';
        if($('#host').length) {
            s = 'http://' + $('#host').html();
        }
        var suffix = $('#frmLap').serialize();
        suffix = decodeURIComponent((suffix + '').replace(/\+/g, '%20'));
        s += url + '&' + suffix;
        urlArr = s.split('&');
        // array_unique
        urlArr = $.grep(urlArr, function(v, k){
            return $.inArray(v ,urlArr) === k;
        });
        // implode
        s = urlArr.join('&');
        
        s= s.replace('/Ajax/Android/0/laps','/Android/browse');
        s= s.replace('csv=1&','');
        s= s.replace('&posted=1','');
        $('#purl').html( '<a href="' + s + '" >Permalink</a>');
        if($('#fuckb'.length)) {
            var fuckb = $('#fuckb');
            s = encodeURI(s);
            s = (s + '').toString();
            // urlencode
            s = encodeURIComponent(s).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
            fuckb.html(
                '<iframe src="https://www.facebook.com/plugins/comments.php?href=' + s + '"scrolling="no" frameborder="0" style="border:none; width:700px; height:160px"></iframe>'    
                );
        }
        
    }
    $.post(url, $('#frmLap').serialize(), function(data){
        data = $.parseJSON(data);
        if(typeof data['error'] != 'undefined') {
            alert(data['error']);
            return;
        }
        numOfLaps = data['numOfLaps'];
        mapData = data['Map'];
        data = data['Graph'];
        numOfSeries = Object.keys(data).length / numOfLaps;
        showOnMap.marker = false;
									
        usedColors = [];
        var axleNum = 0;
        var lapIdx;
        var selectedUnit = '';
        chart1.options['yAxis'] = [];
        chart1.options.series = [];
        
        var i = 0;
        xOffset = 0;
        var foundAxis = [];
        var oldLap;
        var lapCnt = 0;
        try {
            $.each(data, function(a, b) {
                serie = b['serie'];
                if(typeof oldLap == "undefined") {
                    oldLap = this.lap;
                } else {
                    if(oldLap != this.lap) {
                        lapCnt++;
                        oldLap = this.lap;
                    }
                }
                serieName = b['serieName'];
                tag = b['tag'];
                lapUid = b['lap'];
                lapNum = parseInt(b['lapNum'], 10);
                lapIdx = serie['lapIdx'];
                factor = serie['factor'];
                lapStartTime = serie['lapStartTime'];
                selectedUnit = serie['selectedUnit'];
                lapTime = serie['lapTime'];
                if(xOffset == 0) {
                    xOffset = serie['lapOffset'];
                    offsetInLap = serie['offsetInLap'];
                }
                var vis = true;
                serieTypes[a] = serie['unit'];
                axleNum = $.inArray(serie['tag'], foundAxis);
                if(axleNum == -1) {
                    chart1.options['yAxis'][i] = serie['axisInfo'];
                    foundAxis.push(serie['tag']);
                    axleNum = i;
                }
                color = serie['axisInfo']['labels']['style']['color'];
                chart1.options.series[i] = {
                    name: a,
                    yAxis: axleNum,
                    lineWidth: 0.5,
                    marker: {
                        enabled: false
                    },
                    visible: vis,
                    selectedUnit : selectedUnit,
                    color: color,
                    dashStyle : dashstyle[lapCnt],
                    lapOffset: offsetInLap,
                    lapStartTime: lapStartTime,
                    lapTime: lapTime,
                    lapNum: lapNum,
                    lapIdx: lapIdx,
                    lapUid: lapUid,
                    serieName: serieName,
                    tag: tag,
                    data: []
                };
                usedColors.push(color);
                $.each(b['data'], function(c,d)  {
                    chart1.options.series[i].data.push({
                        name: d[2],
                        x: parseFloat(d[0]),
                        y: factor * parseFloat(d[1])
                    });
                });
                i++;
            });
        } catch (e) {
        //            alert('exception catched');
        }

        i = 0;
        //        alert(typeof chart1);
        chart1 = new Highcharts.Chart(chart1.options);
        //        map = null;
        deleteOverlays();
        $.each(mapData, function(key, md){
            setMarkers(md, '#FF0000');
            i++;
            if(i > colorList.length) {
                i = 0;
            }
        });
        google.maps.event.trigger(map, 'resize');
        initConsole();
    });
}

function convertUnit(serieName, value) {
    unitGroup = serieTypes[serieName];
    //    alert (unitGroup);
    var unit = units[unitGroup];
    //    alert(unit);
    switch(unit) {
        case 'km/h':
        case 'kmh':
            value = Math.round(value * 3.6 * 100) / 100;
            break;
        case 'm/h':
        case 'mph':
            value = Math.round(value * 2.25 * 100) / 100;
            break;
        case 'hp':
            value = Math.round(value * 1.36 * 100) / 100;
            break;
        case 'mile':
            value = Math.round(value * 0.000621371 * 1000) /1000;
            unit = 'm';
            break;
        case 'km':
            value = Math.round(value * 0.001 * 1000) /1000;
            break;
    }
    return value.toString() + unit;
}

function formatLabel(p) {
    return "<b>" + p.series.name + ": </b>" + p.y + ' ' + p.series.unit  + "<br/>" +
    "<b>time:</b>" + p.point.name + "<br/>" +
    "<b>Distance: </b>" +  convertUnit('x', Math.round(p.x));
}

//function seriesInterface() {
//    str = '';
//    var headers = [];
//    var h = [];
//    var checked = 0;
////    var a = [];
//////    debugger;
////    if(typeof $_GET['ser'] != "undefined") {
////        var s = ($_GET['ser'] + '').toString();
////        a = s.split(',');
////    }
//    $.each(chart1.options.series, function(serieNum, serie){
//        if($.inArray(serie.tag, h) == -1) {
//            h.push(serie.tag);   
//            headers.push({
//                serieName: serie.serieName,
//                tag: serie.tag,
//                checked: 1
//            }
//            );
//        }
//    });
//    $.each(availableSeries, function(dummy, serie) {
//        //    $.each($_GET, function(k,v){
//        //        console.log(k);
//        //        console.log(v);
//        //    });
//        checked = 0;
////        debugger;
////        if($.inArray(serie.tag, a) != -1) {
////            checked = 1;
////        }
//        if($.inArray(serie.tag, h) == -1) {
//            headers.push({
//                serieName: serie.serieName,
//                tag: serie.tag,
//                checked: checked
//            }
//            );
//        }
//    });
//    var cl = '';
//    $.each(headers, function(i, header){
//        console.log(header);
//        if(header.checked !== 0) {
//            cl = 'checked="checked"';
//        } else {
//            cl = '';
//        }
//        str += '<tr><td>' + header.serieName + '</td><td><input type="checkbox" ' + cl + ' name="series" value="' + header.tag + '"/>' + '</td></tr>';
//    });
//    return str;
//}


function consoleFormat(x) {
    var fastLapStart;
    var fastLapTime;
    var fastLapNum;
    var distance = chart1.chartWidth;

    var mouseX, mouseY, pX, pY, d;
    mouseX = mMouseX;
    mouseY = mMouseY;

    
    $.each(chart1.options.series, function(key, serie){
        if(typeof fastLapTime == "undefined") {
            fastLapTime = serie.lapTime;
            fastLapStart = serie.lapStartTime;
            fastLapNum = serie.lapNum;
        } else {
            if(serie.lapTime < fastLapTime) {
                fastLapTime = serie.lapTime;
                fastLapStart = serie.lapStartTime;
                fastLapNum = serie.lapNum;
            } 
        }
        id = '#' + serie.tag + '_' + serie.lapUid;
        idx = searchArray(x, serie.data, 'x', true);
        val = serie.data[idx].y; // Math.round(100 * serie.data[idx].y) / 100;

        d = 1000;
        try {
            pX = chart1.series[key]['data'][idx].plotX;
            pY = chart1.series[key]['data'][idx].plotY;
            d = mouseY - pY;
        } catch (e) {
            if (e instanceof TypeError) {

            }
        }
        if(Math.abs(d) < 10) {
            $(id).html('<span style="color: white">' + val + '</span>');
            showOnMap(serie, idx);
        //            showOnMap(chart1.options.series, fastLapNum, fastLapStart, idx);
        } else {
            $(id).html(val);
        }
        
    });
    
    return;
}
function lineDistance( x1, y1, x2, y2 )    {
    var xs = 0;
    var ys = 0;
     
    xs = x2 - x1;
    xs = xs * xs;
     
    ys = y2 - y1;
    ys = ys * ys;
    return Math.sqrt( xs + ys );
}

function confirm(dialogText, okFunc, cancelFunc, dialogTitle) {
    $('#confirm_delete').html(dialogText);
    $('#confirm_delete').dialog({
        dialogClass: "eg_fields",
        draggable: false,
        modal: true,
        resizable: false,
        width: 'auto',
        title: dialogTitle || 'Confirm',
        minHeight: 75,
        buttons: {
            OK: function () {
                if (typeof (okFunc) == 'function') {
                    setTimeout(okFunc, 50);
                }
                $(this).dialog('destroy');
            },
            Cancel: function () {
                if (typeof (cancelFunc) == 'function') {
                    setTimeout(cancelFunc, 50);
                }
                $(this).dialog('destroy');
            }
        }
    });
}

function openCompare(tripId) {
    $.get('/Ajax/Lap/0/browselijst/?sel=cmp&o=html&trip_id=' + tripId, function(data) {
        $('#compare').html(data);
    })
}

function addToCompare(tripId) {
    $.get('/Ajax/Lap/0/add_cmp_lap?o=html&lap_id=' + $('#trip_id_' + tripId).val() + '&trip_id=' + tripId, function(data) {
        if(data.indexOf('duplicate') != -1) {
            alert('failed, duplicate record');
        } else {
            $('#sidebar_cmp_laps').html(data);
        }
    });
}

function delFromCompare(lapId) {
    //    $.get('Android/?sel=del_cmp_lap&o=html&lap_id=' + lapId, function(data) {
    $.get('/Ajax/Lap/0/del_cmp_lap?o=html&lap_id=' + lapId, function(data) {
        $('#lap_' + lapId).fadeOut('slow', function() {
            // Animation complete
            });

    });
}

function showDetail(tripId, ok) {
    $.get('/Ajax/Trip/' + tripId + '/detail/?o=html&ok=' + ok, function(data) {
        $('#trip_data').html(data);
        $('#trip_detail').fadeIn('slow', function() {
            // Animation complete
            });
    });        
}
//function showOnMap(elems, fastLapNum, fastLapStart, idx) {
function showOnMap(serie, idx) {
    var mapIcons = [];
    var i = 0;

    i = 0;
    //    var fastlapLatLon;
    //    $.each(elems, function (dummy, el){
    if(typeof serie == "undefined"){
        return;
    }

    var mapRef;
    lNum = serie.lapNum;
    $.each(mapData, function(i, data){
        if(data.lap == lNum) {
            mapRef = data;
            return false;
        }
    });
    //                found = mapRef.data[lapIdx];
    if(typeof mapRef.data[idx] != "undefined"){
        found = mapRef.data[idx];
    } 
    if(typeof found =="undefined") {
        return;
    }
        
    if(typeof showOnMap.marker == "boolean") {
        showOnMap.marker = [];
    }
        
    if(typeof showOnMap.marker[lNum] != "object") {
        // lap_icon
        mapIcons[lNum] = new google.maps.MarkerImage(
            '/img/lwt_map_icons/brown/'+lNum+'.png',
            null,
            null
            );
        showOnMap.marker[lNum] = new google.maps.Marker({
            position: new google.maps.LatLng(found['lat'], found['lon']),
            map: map,
            icon: mapIcons[lNum],
            visible: true
        //                title: 'hoi'
        } );
        positionMarkers.push(showOnMap.marker[lNum]);
    } else {
        showOnMap.marker[lNum].setPosition( new google.maps.LatLng(found['lat'], found['lon']));
    }
    
//    console.log(found);
//        i++;
//    });
}

function searchArray(needle, haystack, field, index) {
    var high = haystack.length - 1;
    var low = 0;
 
    while (low <= high) {
        mid = parseInt((low + high) / 2);
        element = haystack[mid][field];
        if (element > needle) {
            high = mid - 1;
        } else if (element < needle) {
            low = mid + 1;
        } else {
            if(typeof index == "undefined") {
                return haystack[mid];
            } else {
                return mid;
            }
        }
    }
    if(typeof index == "undefined") {
        return haystack[low];
    } else {
        return mid;
    }
}

function goBack() {
    if(zoomHistory.length < 2) {
        return;
    }
    var l = zoomHistory.length -1;
    l--;
    if( typeof zoomHistory[l] == "undefinded") {
        return;
    }
    var beforeLast = zoomHistory[l];
    drawChart(beforeLast['x'], beforeLast['y'], beforeLast['tripId'], true, true);
    zoomHistory.pop();
    if(zoomHistory.length < 2) {
        $('#back_button').attr('class', 'button_grey');
    }
}

function refreshCurrentZoom(b, rotx, roty, rotz) {
    if(typeof rotx == "undefined") {
        var rotx = 0;
    }
    if(typeof roty == "undefined") {
        var roty = 0;
    }
    if(typeof rotz == "undefined") {
        var rotz = 0;
    }
    if(zoomHistory.length < 2) {
        if(typeof b == "undefined") 
            drawMap(0, 0, 0, true);
        else
            drawChart(0, 0, 0, true, false, rotx, roty, rotz);
        return;
    }
    var l = zoomHistory.length -1;
    if(typeof zoomHistory[l] == "undefinded") {
        return;
    }
    var last = zoomHistory[l];
    if(typeof b == "undefined") {
        drawMap(last['x'], last['y'], last['tripId'], true, true, rotx, roty, rotz);
    }else{
        drawChart(last['x'], last['y'], last['tripId'], true, true);
    }
    
}

function showSeries(serieNum, flagOn) {
    while(typeof chart1.series[serieNum] != "undefined") {
        var serie = chart1.series[serieNum];
        if(flagOn) {
            serie.show();
        } else {
            serie.hide();
        }
        serieNum += 5;
    }
}

function switchSerie(serieNum, id) {
    //    var show;
    var color = '';
    var found = false;
    $.each(chart1.series, function(num, serie){
        if(!found) {
            if(num != serieNum) {
                return;
            }
        }
        if(color != '' && color != serie.color) {
            return;
        }
        if(serie.visible) {
            serie.hide();
            $('#' + id).addClass('button_inact');
        } else {
            serie.show();
            $('#' + id).removeClass('button_inact');            
        }
        color = serie.color;
        found = true;
    });
}

function closestPoint(latLon, haystack, idx) {
    if(typeof latLon == "undefined") {
        return haystack[haystack.length - 1];
    }
    lat1 = latLon[0];
    lon1 = latLon[1];
    
    var prevDist = 99999;
    var newDist =  99990;
    var a;
    var idx_a = idx;
    while(newDist < prevDist) {
        prevDist = newDist;
        if(typeof haystack[idx_a] == "undefined") {
            a = prevDist;
            break;
        }
        lon2 = haystack[idx_a].lon;
        lat2 = haystack[idx_a].lat;
        newDist = haversine(lat1, lon1, lat2, lon2);
        if(newDist > prevDist) {
            a = prevDist;
            break;
        }
        idx_a++;
    }
    prevDist = 99999;
    newDist =  99990;
    var b;
    var idx_b = idx;
    while(newDist < prevDist) {
        prevDist = newDist;
        if(typeof haystack[idx_b] == "undefined") {
            b = prevDist;
            break;
        }
        lon2 = haystack[idx_b].lon;
        lat2 = haystack[idx_b].lat;
        newDist = haversine(lat1, lon1, lat2, lon2);
        if(newDist > prevDist) {
            b = prevDist;
            break;
        }
        idx_b--;
    }
    if(a < b) {
        return haystack[idx_a];
    } else {
        return haystack[idx_b];
    }
}

function initConsole() {
    html = '<table><tr><th></th>';
    headerNames = [];
    dataHtml = '<tr>';
    //    lapCnt = 0;
    serieCnt = 0;
    lapCnt = 0;
    lapUid = 0;
    $.each(chart1.options.series, function(key, serie){
        if(serie.lapUid != lapUid) {
            lapUid = serie.lapUid;
            lapNum = serie.lapNum;
            dataHtml += '</tr><tr>';
            dataHtml += '<td>' + lapNum + '</td>';
            lapCnt++;
        }
        if($.inArray(serie.tag, headerNames) == -1) {
            headerNames.push(serie.tag);
            html += '<th>' + serie.serieName + ' ' + serie.selectedUnit + '</th>';
        }
        if(serieCnt == numOfSeries) {
            serieCnt = 0;
        //            lapCnt++;
        }
        serieCnt++;
        dataHtml += '<td id="' + serie.tag +'_'+lapUid +'"></td>';
    });
    if(lapCnt == 2) {
        dataHtml += '<tr><td>time slip</td><td colspan="100" id="time_diff">--</td></tr>';
    }
    dataHtml += '</tr>';
    html += '</tr>' + dataHtml + '</table>';
    
    $('#console').html(html);
}

function haversine(lat1, lon1, lat2, lon2) {
    var R = 6371; // km
    var dLat = (lat2-lat1) * 0.0174532925;
    var dLon = (lon2-lon1) * 0.0174532925;
    lat1 *=   0.0174532925;
    lat2 *=   0.0174532925;

    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c;
    return d;
}

//var output = document.getElementById('output');  
function assert( outcome, description ) {  
    
    var o = $('#output');
    
    //    var li = document.createElement('li');  
    var s = outcome ? 'pass' : 'fail';  
    o.html(o.html() + '<br/>' + s + ': ' + description);
//    li.appendChild( document.createTextNode( description ) );  
//    output.appendChild(li);  
}
