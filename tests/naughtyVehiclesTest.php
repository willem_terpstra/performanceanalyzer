<?php

/**
 * Created by PhpStorm.
 * User: Gebruiker
 * Date: 12-11-2016
 * Time: 09:24
 */
class naughtyVehiclesTest extends TestBase
{


    public function setUp() : void
    {
        parent::setUp();
        $this->getDataFromProd([
            'Vehicle' => ['id' => 78],
            'Chassis' => ['id' => 78],
            'Driver' => ['id' => 1],
            'Engine' => ['id' => 741],
            'Merken' => ['id' =>28],
            'Modellen' => ['id' =>365],
            'PrimaryTransmission' => ['id' => 200],
            'SecundaryTransmission' => ['id' => 22],
            'Transmission' => ['id' => 152],
            'Run' => ['id' => 17941],
            'Dyno' => ['run_id' => 17941]
        ]);
        $c = new Chart();
        $c->setCurrent('acceleration');
    }

    public function testVehicles() {
        Users::loginByEmail('guest@motorevents.nl');
        $r = new Request();
        $request = $r->getRoute('/vehicle/view/seeley-cb750');
        $instance = new $request['controller']([
            'action' => $request['action'],
            'options' => Get::$getVars
        ]);

        $content = $r->dispatch($instance, $request);
//        $hasModel = strpos($content, '/vehicle/view/bimota-db5-s-2009') !== false;
//        $this->assertTrue($hasModel, 'url /vehicle/view/bimota-db5-s-2009 not found in testdata');
    }


}