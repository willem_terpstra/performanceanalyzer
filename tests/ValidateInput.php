<?php

/**
 * Created by PhpStorm.
 * User: Gebruiker
 * Date: 12-11-2016
 * Time: 09:24
 */
class ValidateInput extends TestBase
{
    
    const VEH_ID = 78;
    


    public function setUp() : void
    {
        parent::setUp();
        Users::loginByEmail('willem@motorevents.nl');

        $this->getDataFromProd([
            'Vehicle' => ['id' => self::VEH_ID],
            'Chassis' => ['id' => 78],
            'Driver' => ['id' => 1],
            'Engine' => ['id' => 741],
            'Merken' => ['id' =>28],
            'Modellen' => ['id' =>365],
            'PrimaryTransmission' => ['id' => 200],
            'SecundaryTransmission' => ['id' => 22],
            'Transmission' => ['id' => 152],
            'Run' => ['id' => 17941],
            'Dyno' => ['run_id' => 17941]
        ]);
    }

    /**
     * @return Run
     */
    public function getRun()
    {
        $veh = new Vehicle();
        $veh->find(['select' => ['id' => self::VEH_ID]]);
        $veh->first();

        $run = new Run();
        $run->find(['select' => ['id' => $veh->getRunId()]]);
        $run->first();
        return $run;
    }

    public function testRunNoStartRpm()
    {
        $c = new Chart();

        Users::loginByEmail('willem@motorevents.nl');
        $run = $this->getRun();
        $run->setStartRpm(0);
        $run->save();

        foreach ([Chart::POWER, Chart::ACCELERATION, Chart::POWER | Chart::ACCELERATION, 0] as $flag) {
            $run->setValidation($flag);
            $run->save();
            $c->setCurrent('power');
            try {
                $run->validate();
            } catch (VehicleException $e) {
                self::assertEquals('Start rpm cannot be empty', $e->getMessage());
            }
            $validation = $run->getValidation();
            switch ($flag) {
            case Chart::POWER:
                self::assertEquals(0, $validation);
                break;
            case Chart::ACCELERATION:
                self::assertEquals(Chart::ACCELERATION, $validation);
                break;
            case Chart::POWER | Chart::ACCELERATION:
                self::assertEquals(Chart::ACCELERATION, $validation);
                break;
            case 0:
                self::assertEquals(0, $validation);
                break;
            }

            $c->setCurrent('acceleration');
            $run->setValidation($flag);
            $run->save();
            try {
                $run->validate();
            } catch (VehicleException $e) {
                self::assertEquals('Start rpm cannot be empty', $e->getMessage());
            }
            $validation = $run->getValidation();
            switch ($flag) {
            case Chart::POWER:
                self::assertEquals(Chart::POWER, $validation);
                break;
            case Chart::ACCELERATION:
                self::assertEquals(0, $validation);
                break;
            case Chart::POWER | Chart::ACCELERATION:
                self::assertEquals(Chart::POWER, $validation);
                break;
            case 0:
                self::assertEquals(0, $validation);
                break;
            }
        }
    }


    public function testRunWrongStartRpm()
    {
        $c = new Chart();
        $run = $this->getRun();
        $run->setStartRpm(7000);
        $run->save();

        $c->setCurrent('power');
//        $this->expectException('VehicleException');
        try {
            $run->validate();
        } catch (VehicleException $e) {
            self::assertEquals('start rpm should be lower then rpm at max power, start rpm should be lower then rpm at max torque', $e->getMessage());
        } catch (Exception $e) {
            sleep(0);
        }

        $c->setCurrent('acceleration');
//        $this->expectException('VehicleException');
        try {
            $run->validate();
        } catch (VehicleException $e) {
            self::assertEquals('start rpm should be lower then rpm at max power, start rpm should be lower then rpm at max torque', $e->getMessage());
        }
    }


    public function testRunWrongKwRNmpm()
    {
        $c = new Chart();
        $run = $this->getRun();
        $run->setStartRpm(700);
        $run->setKwRpm(3500);
        $run->setNmRpm(4000);
        $run->save();
        $c->setCurrent('power');
        $this->expectException('VehicleException');
        $run->validate();
    }

    public function testRunOk()
    {
        $c = new Chart();
        $run = $this->getRun();
        $run->setStartRpm(700);
        $run->setKwRpm(6000);
        $run->setNmRpm(4500);
        $run->save();
        $c->setCurrent('power');
        $run->validate();
        self::assertEquals(Chart::$types['Power'], $run->getValidation() & Chart::$types['Power']);
        $c->setCurrent('acceleration');
        $run->validate();
        self::assertEquals(Chart::$types['Power'] | Chart::$types['Acceleration'], $run->getValidation());
    }

    /**
     * @return Driver
     */
    public function getDriver()
    {
        $veh = new Vehicle();
        $veh->find(['select' => ['id' => self::VEH_ID]]);
        $veh->first();

        $driver = new Driver();
        $driver->find(['select' => ['id' => $veh->getDriverId()]]);
        $driver->first();
        return $driver;
    }

    public function testDriverWeight()
    {
        $c = new Chart();
        $driver = $this->getDriver();
        $driver->setWeight(25);
        $driver->save();
        $c->setCurrent('acceleration');
        try {
//            self::setExpectedException('EntityException');
            $driver->validate();
        } catch (VehicleException $e) {
            self::assertEquals('Driver weight must be between ' . Driver::MIN_WEIGHT . ' and ' . Driver::MAX_WEIGHT, $e->getMessage());
        }

        $driver->setWeight(225);
        $driver->save();
        try {
//            self::setExpectedException('EntityException');
            $driver->validate();
        } catch (VehicleException $e) {
            self::assertEquals('Driver weight must be between ' . Driver::MIN_WEIGHT . ' and ' . Driver::MAX_WEIGHT, $e->getMessage());
        }

        $driver->setWeight(80);
        $driver->save();
        $driver->validate();
        self::assertEquals(Chart::$types['Acceleration'], $driver->getValidation() & Chart::$types['Acceleration']);
    }

    /**
     * @return Chassis
     */
    public function getChassis()
    {
        $veh = new Vehicle();
        $veh->find(['select' => ['id' => self::VEH_ID]]);
        $veh->first();

        $chassis = new Chassis();
        $chassis->find(['select' => ['id' => $veh->getChassisId()]]);
        $chassis->first();
        return $chassis;
    }

    public function testChassisWeight()
    {
        $c = new Chart();
        $c->setCurrent('acceleration');
        $chassis = $this->getChassis();
        $chassis->setGewicht(Chassis::MIN_WEIGHT - 1);
        $chassis->save();
        try {
//            self::setExpectedException('VehicleException');
            $chassis->validate();
        } catch (VehicleException $e) {
            self::assertEquals('Chassis weight must be between ' . Chassis::MIN_WEIGHT . ' and ' . Chassis::MAX_WEIGHT, $e->getMessage());
        }

        $chassis->setGewicht(80);
        $chassis->save();
        $chassis->validate();
        self::assertEquals(Chart::$types['Acceleration'], $chassis->getValidation() & Chart::$types['Acceleration']);
    }

    /**
     * @return Transmission
     */
    public function getTransmission()
    {
        $veh = new Vehicle();
        $veh->find(['select' => ['id' => self::VEH_ID]]);
        $veh->first();

        $transmission = new Transmission();
        $transmission->find(['select' => ['id' => $veh->getTransmissionId()]]);
        $transmission->first();
        return $transmission;
    }

    public function testTransmission()
    {
        $c = new Chart();
        $c->setCurrent('acceleration');
        $transmission = $this->getTransmission();
        $g1t1 = $transmission->getVersnG1T1();
        $g1t2 = $transmission->getVersnG1T2();
        $transmission->setVersnG1T1($g1t2);
        $transmission->setVersnG1T2($g1t1);
        $transmission->save();
        try {
//            self::setExpectedException('VehicleException');
            $transmission->validate();
        } catch (VehicleException $e) {
            self::assertEquals('Ratio of gear 2 cannot be smaller then the ratio of gear 1', $e->getMessage());
        }

        $transmission->setVersnG1T1($g1t1);
        $transmission->setVersnG1T2($g1t2);
        $transmission->save();
        $transmission->validate();
        self::assertEquals(Chart::$types['Acceleration'], $transmission->getValidation() & Chart::$types['Acceleration']);
    }

    public function getPrimaryTransmission()
    {
        $veh = new Vehicle();
        $veh->find(['select' => ['id' => self::VEH_ID]]);
        $veh->first();

        $primaryTransmission = new PrimaryTransmission();
        $primaryTransmission->find(['select' => ['id' => $veh->getPrimaryTransmissionId()]]);
        $primaryTransmission->first();
        return $primaryTransmission;
    }

    public function testPrimaryTransmission()
    {
        $c = new Chart();
        $c->setCurrent('acceleration');
        $primaryTransmission = $this->getPrimaryTransmission();
        $t1 = $primaryTransmission->getPrimT1();
        $t2 = $primaryTransmission->getPrimT2();
        $primaryTransmission->setPrimT1($t2);
        $primaryTransmission->setPrimT2($t1);
        $primaryTransmission->save();
        try {
            self::setExpectedException('VehicleException');
            $primaryTransmission->validate();
        } catch (VehicleException $e) {
            self::assertEquals('Crankside gear must be smaller or equal to clutch gear', $e->getMessage());
        }
        $primaryTransmission->setPrimT1($t1);
        $primaryTransmission->setPrimT2($t2);
        $primaryTransmission->save();
        $primaryTransmission->validate();
        self::assertEquals(Chart::$types['Acceleration'], $primaryTransmission->getValidation() & Chart::$types['Acceleration']);
    }

    /**
     * @return SecundaryTransmission
     */
    public function getSecundaryTransmission()
    {
        $veh = new Vehicle();
        $veh->find(['select' => ['id' => self::VEH_ID]]);
        $veh->first();

        $secundaryTransmission = new SecundaryTransmission();
        $secundaryTransmission->find(['select' => ['id' => $veh->getSecundaryTransmissionId()]]);
        $secundaryTransmission->first();
        return $secundaryTransmission;
    }

    public function testSecundaryTransmission()
    {
        $c = new Chart();
        $c->setCurrent('acceleration');
        $secundaryTransmission = $this->getSecundaryTransmission();
        $t1 = $secundaryTransmission->getFinalT1();
        $t2 = $secundaryTransmission->getfinalT2();
        $secundaryTransmission->setFinalT1($t2);
        $secundaryTransmission->setfinalT2($t1);
        $secundaryTransmission->save();
        try {
//            self::setExpectedException('VehicleException');
            $secundaryTransmission->validate();
        } catch (VehicleException $e) {
            self::assertEquals('Gearbox sprocket must be smaller or equal to rear wheel sprocket', $e->getMessage());
        }
        $secundaryTransmission->setFinalT1($t1);
        $secundaryTransmission->setFinalT2($t2);
        $secundaryTransmission->save();
        $secundaryTransmission->validate();
        self::assertEquals(Chart::$types['Acceleration'], $secundaryTransmission->getValidation() & Chart::$types['Acceleration']);
    }

    public function getVehicle()
    {
        $veh = new Vehicle();
        $veh->find(['select' => ['id' => self::VEH_ID]]);
        $veh->first();

        return $veh;
    }

    public function testVehicle()
    {
        $c = new Chart();
        $c->setCurrent('acceleration');
        $veh = $this->getVehicle();
//        $veh->Run->validate();
        $veh->validate();
    }

}
