<?php

/**
 * Created by PhpStorm.
 * User: Gebruiker
 * Date: 12-11-2016
 * Time: 09:27
 */
class TestBase extends PHPUnit\DbUnit\TestCase
{
    public $postData = [];
    public $getData = [];
    public $testSlug = '';

    protected $config, $testConfig;

    public function getConnection()
    {
        $config = $this->testConfig;

        $conn = mysqli_connect($config->host, $config->user, $config->pwd, $config->db);
        dbTable::setConnection($conn);
        $dbh = new PDO('mysql:host=localhost;dbname=' . $config->db, $config->user, $config->pwd);
        return $this->createDefaultDBConnection($dbh);
    }

    public function getDataSet()
    {
        return $this->createMySQLXMLDataSet($_SERVER['DOCUMENT_ROOT'] . '/../tests/fixtures/users.xml');
    }

    protected function getDataFromProd($classes)
    {
        foreach ($classes as $class => $restrict) {
            $instance = new $class();
            $instance->createTable();
            $instance->exportRecords($restrict, $this->config->mysql, $this->testConfig->mysql);
        }
    }

    protected function setUp() : void
    {
        $root = $_SERVER['DOCUMENT_ROOT'] = getcwd() . DIRECTORY_SEPARATOR . 'v01-enginegraph';
        $_SERVER['HTTP_ACCEPT'] = 'application/json';
        require_once $root . DIRECTORY_SEPARATOR . 'autoload.php';
        $lang = 'nl';
//        require_once('MVC/lang/lang_' . $lang . ".php");
        if (!$config = simplexml_load_file($_SERVER['DOCUMENT_ROOT'] . '/MVC/config/config.xml')) {
            trigger_error('Error reading XML file', E_USER_ERROR);
        }
        $this->config = $config->prod;
        $this->testConfig = $config->test;
        $_POST = $this->postData;
        $_GET = $this->getData;
        $p = new Post();
        $g = new Get();
//        $_SESSION['sessionId'] = Session::id($this->testSlug, (string)$config->test->session_id);
        $_SESSION['lang'] = $lang;
        if (isset($_SESSION['danger'])) {
            unset($_SESSION['danger']);
        }
        if (isset($_SESSION['warning'])) {
            unset($_SESSION['warning']);
        }
        $dataSet = $this->getConnection();
        $this->getDataSet();
    }
}