<?php

// Usage:
// [path_to_phpunit.bat]\phpunit [path_to_this]\[this_file]

class EntityTest extends TestBase
{

//    private $config, $testConfig;
    public $testSlug = 'bimota-db5-s-2009';

    public function setPostData()
    {
        $this->postData = array(
            'Run' =>
                array(
                    'Dyno' =>
                        array(
                            5215708 =>
                                array(
                                    'nm' => '23',
                                    'kw' => '3.6',
                                ),
                            5215709 =>
                                array(
                                    'nm' => '28.1',
                                    'kw' => '5.2',
                                ),
                            5215710 =>
                                array(
                                    'nm' => '32.7',
                                    'kw' => '6.8',
                                ),
                            5215711 =>
                                array(
                                    'nm' => '37.3',
                                    'kw' => '8.8',
                                ),
                            5215712 =>
                                array(
                                    'nm' => '42.2',
                                    'kw' => '11',
                                ),
                            5215713 =>
                                array(
                                    'nm' => '47.6',
                                    'kw' => '13.7',
                                ),
                            5215714 =>
                                array(
                                    'nm' => '52.9',
                                    'kw' => '16.6',
                                ),
                            5215715 =>
                                array(
                                    'nm' => '58',
                                    'kw' => '19.7',
                                ),
                            5215716 =>
                                array(
                                    'nm' => '62.7',
                                    'kw' => '23',
                                ),
                            5215717 =>
                                array(
                                    'nm' => '67',
                                    'kw' => '26.3',
                                ),
                            5215718 =>
                                array(
                                    'nm' => '71.9',
                                    'kw' => '30.1',
                                ),
                            5215719 =>
                                array(
                                    'nm' => '78.2',
                                    'kw' => '34.8',
                                ),
                            5215720 =>
                                array(
                                    'nm' => '85.7',
                                    'kw' => '40.3',
                                ),
                            5215721 =>
                                array(
                                    'nm' => '92.8',
                                    'kw' => '46.2',
                                ),
                            5215722 =>
                                array(
                                    'nm' => '98.7',
                                    'kw' => '51.7',
                                ),
                            5215723 =>
                                array(
                                    'nm' => '102.3',
                                    'kw' => '56.2',
                                ),
                            5215724 =>
                                array(
                                    'nm' => '103',
                                    'kw' => '59.3',
                                ),
                            5215725 =>
                                array(
                                    'nm' => '121.5',
                                    'kw' => '61.1',
                                ),
                            5215726 =>
                                array(
                                    'nm' => '110',
                                    'kw' => '62.8',
                                ),
                            5215727 =>
                                array(
                                    'nm' => '98.5',
                                    'kw' => '64.5',
                                ),
                            5215728 =>
                                array(
                                    'nm' => '97.1',
                                    'kw' => '66',
                                ),
                            5215729 =>
                                array(
                                    'nm' => '95.6',
                                    'kw' => '67.5',
                                ),
                            5215730 =>
                                array(
                                    'nm' => '94.1',
                                    'kw' => '69',
                                ),
                            5215731 =>
                                array(
                                    'nm' => '89.7',
                                    'kw' => '68',
                                ),
                            5215732 =>
                                array(
                                    'nm' => '82.1',
                                    'kw' => '64.5',
                                ),
                        ),
                    'id' => '24644',
                    'start_rpm' => 1000,
                    'max_hp_rpm' => '',
                    'max_nm_rpm' => '',
                ),
            'Chassis' =>
                array(
                    'gewicht' => '',
                    'fuel_tank_capacity' => '16',
                    'bandmaat_aangedr_wiel' => '180',
                    'band_aspect_ratio' => '55',
                    'tyre_pressure_fr' => '2.25',
                    'tyre_pressure_rr' => '2.25',
                    'wielmaat_aangedr_wiel' => '17',
                    'frontal_area' => '0.5028',
                    'cx' => '0.4',
                ),
            'Driver' =>
                array(
                    'weight' => '80',
                ),
//            'Engine' =>
//                array(
//                    'engine_sprocket_t' => '23',
//                ),
            'Transmission' =>
                array(
//                    'input_sprocket_t' => '46',
                    'num_gears' => '6',
                    'layshaft_reduction' => '',
                    'layshaft_g1' => '1',
                    'layshaft_g2' => '1',
                    'versn_g1_t1' => '37',
                    'versn_g1_t2' => '15',
                    'ratio_g1' => '',
                    'versn_g2_t1' => '34',
                    'versn_g2_t2' => '17',
                    'ratio_g2' => '',
                    'versn_g3_t1' => '31',
                    'versn_g3_t2' => '18',
                    'ratio_g3' => '',
                    'versn_g4_t1' => '37',
                    'versn_g4_t2' => '24',
                    'ratio_g4' => '',
                    'versn_g5_t1' => '34',
                    'versn_g5_t2' => '24',
                    'ratio_g5' => '',
                    'versn_g6_t1' => '28',
                    'versn_g6_t2' => '21',
                    'ratio_g6' => '',
                    'versn_g7_t1' => '',
                    'versn_g7_t2' => '',
                    'ratio_g7' => '',
                ),
            'SecundaryTransmission' =>
                array(
                    'final_t1' => '23',
                    'final_t2' => '46',
                    'final_reduction' => '0',
                )
        );
    }

    public function setGetData()
    {
        $this->getData = ['c' => 'power'];
    }

    protected function setUp() : void
    {
        parent::setUp();
        $this->setPostData();
        $this->setGetData();
        $_SESSION['error'] = [];
        $_SESSION['warning'] = [];


        $classes = [
            'Users' => [
                'id' => [1, 695]
            ],
            'Vehicle' => [
                'id' => 8134,

            ],
            'Merken' => [
                'id' => [18, 150]
            ],
            'Modellen' => [
                'id' => [1938, 4322]
            ],
            'Run' => [
                'id' => 24644
            ],
            'Driver' => [
                'rbac_group_id' => [1, 695]
            ],
            'Dyno' => [
                'run_id' => 24644
            ],
            'Clutch' => [
                'id' => 6630
            ],
            'Chassis' => [
                'id' => 8151
            ],
            'Engine' => [
                'id' => 7382
            ],
            'Transmission' => [
                'id' => 6786
            ],
            'PrimaryTransmission' => [
                'id' => 4345
            ],
            'SecundaryTransmission' => [
                'id' => 6660
            ],
            'ToepassingsGebied' => [
                'id' => 1
            ],
            'RbacPrivileges' => [
                'rbac_group_id' => [1, 695]
            ]
        ];

//        $this->getDataFromProd($classes);
        Authorization::login('willem', 'saab99');
    }


    public function testVehicleClone()
    {
        $vehicle = new Vehicle();
    }

    public function testView()
    {
        $this->assertEquals(Unit::imperial(), false, 'Unit is imperial');

        $vehicleController = new VehicleController();
        Post::$postVars = $this->postData;
        $result = $vehicleController->view(['slug' => $this->testSlug]);
        $errPos = strpos($result, 'error:');
        $this->assertFalse($errPos, $result);

        // check if rbac_group of original run has not changed
        $runId = $this->postData['Run']['id'];
        $row = Entity::$db->query('select * from run where id=' . $runId)->fetch(PDO::FETCH_ASSOC);
        $this->assertEquals($row['rbac_group_id'], 1, ' run with id ' . $runId . ' changed from rbac_group_id 1 to ' . $row['rbac_group_id']);

        // check if run_id from original vehicle has not changed
        $vehicle = Entity::$db->query('select * from vehicle where slug="' . $this->testSlug . '"')->fetch(PDO::FETCH_ASSOC);
        $this->assertEquals($vehicle['run_id'], $this->postData['Run']['id'], ' vehicle with slug "' . $this->testSlug . '" changed from run_id ' . $this->postData['Run']['id'] . ' to ' . $vehicle['run_id']);

        // is a single sandbox created
        $row = Entity::$db->query('select count(*)  as new_run_cnt from run where session_id = "' . $_SESSION['sessionId'] . '"')->fetch(PDO::FETCH_ASSOC);
        $this->assertEquals(1, $row['new_run_cnt'], 'none or more then 1 run found with session_id ' . $_SESSION['sessionId']);

        // sandbox should have the same number of records as the original run
        $row = Entity::$db->query('select id  as new_run_id from run where session_id = "' . $_SESSION['sessionId'] . '"')->fetch(PDO::FETCH_ASSOC);
        $newRunId = $row['new_run_id'];
        $row = Entity::$db->query('select count(*) as dyno_cnt from dyno where run_id=' . $newRunId)->fetch(PDO::FETCH_ASSOC);
        $this->assertEquals(
            $row['dyno_cnt'], count($this->postData['Run']['Dyno'])
            , ' wrong dyno count, found ' . $row['dyno_cnt']
            . ' records, expected '
            . count($this->postData['Run']['Dyno'])
            . ' records.'
        );

        // there should be 2 vehicles
        $vehicleInstances = $vehicleController->getView()->getEntities();
        $this->assertEquals(2, count($vehicleInstances));

        $vehicleInstances[1]->Run->setData($this->postData['Run']);
        $dynoData = $vehicleInstances[1]->Run->Dyno;

        $postData = $this->checkDynoVsPost($dynoData, $this->postData);

        // change postdata
        foreach ($postData['Run']['Dyno'] as &$dyno) {
            $dyno['nm'] += 1;
        }

        // post again
        Post::$postVars = $postData;
        $result = $vehicleController->view(['slug' => $this->testSlug]);

        $errPos = strpos($result, 'error:');
        $this->assertFalse($errPos, $result);

        $vehicleInstances = $vehicleController->getView()->getEntities();
        $dynoData = $vehicleInstances[1]->Run->Dyno;

        // check if changes are committed
        $this->checkDynoVsPost($dynoData, $postData);


        // View without Dyno data
        Post::$postVars = [];
        Entity::$db->query('delete from dyno where run_id=' . $runId);
        $result = $vehicleController->view(['slug' => $this->testSlug]);

        $errPos = (bool)strpos($result, T::$_['ERROR_IN_RUN']);
        $this->assertTrue($errPos);

    }

    /**
     * @return boolean
     */
    public function testEdit()
    {

        Users::loginByEmail('willem@motorevents.nl');
        RbacPrivileges::addDefault($_SESSION['RbacGroupId']);
        $this->assertEquals(Unit::imperial(), false, 'Unit is imperial');

        $vehicleController = new VehicleController();
        Post::$postVars = $this->postData;
        $result = $vehicleController->edit(['slug' => $this->testSlug]);

        if (array_key_exists('error', $_SESSION)) {
            self::assertEmpty($_SESSION['error'], implode("\n", $_SESSION['error']));
        }

        $errPos = strpos($result, 'error:');
        $this->assertFalse($errPos, $result);

        $this->postData['Driver']['weight'] = 100;
        Post::$postVars = $this->postData;
        $result = $vehicleController->edit(['slug' => $this->testSlug]);

        $newWeight = Entity::$db->query('select weight from driver where id=1')->fetch(PDO::FETCH_ASSOC)['weight'];
        self::assertEquals(100, $newWeight, 'Weight not updated while saving');

        $vehicleInstances = $vehicleController->getView()->getEntities();
        $vehicleInstances[0]->Run->setData($this->postData['Run']);
        $dynoData = $vehicleInstances[0]->Run->Dyno;

        $postData = $this->checkDynoVsPost($dynoData, $this->postData);

        // change postdata
        foreach ($postData['Run']['Dyno'] as &$dyno) {
            $dyno['nm'] += 1;
        }

        // post again
        Post::$postVars = $postData;
        $vehicleController->view(['slug' => $this->testSlug]);
        $vehicleInstances = $vehicleController->getView()->getEntities();
        $dynoData = $vehicleInstances[1]->Run->Dyno;

        // check if changes are committed
        $this->checkDynoVsPost($dynoData, $postData);

//        // make driver invalid and post again
//        $postData['Driver']['weight'] = 20;
//        $vehicleInstances[0]->createNonExistingChildren();
//        Post::$postVars = $postData;
//        $vehicleController->save(['slug' => $this->testSlug]);


        return $result;
    }

    /**
     * @return boolean
     */
    public function testAdd()
    {

        Users::loginByEmail('willem@motorevents.nl');
        RbacPrivileges::addDefault($_SESSION['RbacGroupId']);
        $this->assertEquals(Unit::imperial(), false, 'Unit is imperial');

        $vehicleController = new VehicleController();
        $result = $vehicleController->add(['slug' => null]);

        if (array_key_exists('danger', $_SESSION)) {
            self::assertEquals([], $_SESSION['danger'], implode("\n", $_SESSION['danger']));
        }
        $chart = new Chart();

        $chart->setCurrent('acceleration');
        $errPos = strpos($result, 'error:');
        $this->assertFalse($errPos, $result);

        Post::$postVars = [
            'Vehicle' =>
                array(
                    'merk' => 'suzuki',
                    'model' => 'gs 750',
                    'jaar' => '1977',
                )
        ];

        $vehicleController->add(['slug' => null]);

        $parentId = $vehicleController->getView()->getEntity()->id;

        Post::$postVars = [
            'Chassis' =>
                array(
                    'parent_id' => $parentId,
                    'gewicht' => '100',
                    'fuel_content' => '10',
                    'bandmaat_aangedr_wiel' => '100',
                    'band_aspect_ratio' => '90',
                    'tyre_pressure_fr' => '2',
                    'tyre_pressure_rr' => '2',
                    'wielmaat_aangedr_wiel' => '19',
                    'frontal_area' => '0.45',
                    'cx' => '0.45',
                ),];
        $result = $vehicleController->add(['slug' => null]);

        Post::$postVars = [
            'Driver' =>
                array(
                    'parent_id' => $parentId,
                    'weight' => '78',
                ),];


        $result = $vehicleController->add(['slug' => null]);

        Post::$postVars = [
            'Transmission' =>
                array(

                    'parent_id' => $parentId,
                    'num_gears' => '7',
                    'layshaft_g1' => '1',
                    'layshaft_g2' => '1',
                    'layshaft_reduction' => '1',
                    'versn_g1_t1' => '1',
                    'versn_g1_t2' => '1',
                    'ratio_g1' => '1',
                    'versn_g2_t1' => '1',
                    'versn_g2_t2' => '1',
                    'ratio_g2' => '1',
                    'versn_g3_t1' => '1',
                    'versn_g3_t2' => '1',
                    'ratio_g3' => '1',
                    'versn_g4_t1' => '1',
                    'versn_g4_t2' => '1',
                    'ratio_g4' => '1',
                    'versn_g5_t1' => '1',
                    'versn_g5_t2' => '1',
                    'ratio_g5' => '1',
                    'versn_g6_t1' => '1',
                    'versn_g6_t2' => '1',
                    'ratio_g6' => '1',
                    'versn_g7_t1' => '1',
                    'versn_g7_t2' => '1',
                    'ratio_g7' => '1',
                )
        ];

        $vehicleController->add(['slug' => null]);

        Post::$postVars = [
            'Engine' =>
                [
                    'parent_id' => $parentId,
                ]
        ];

        $result = $vehicleController->add(['slug' => null]);

        Post::$postVars = [
            'SecundaryTransmission' =>
                [
                    'parent_id' => $parentId,
                    'final_t1' => 11,
                    'final_t2' => 111,
                    'final_reduction' => 111,
                ]
        ];

        $vehicleController->add(['slug' => null]);
        Post::$postVars = [
            'PrimaryTransmission' =>
                [
                    'parent_id' => $parentId,
                    'prim_t1' => '28.0',
                    'prim_t2' => '46.0',
//                    'final_reduction' => 111,
                ]
        ];
        $vehicleController->add(['slug' => null]);

        Post::$postVars = [
            'Run' => [
                'parent_id' => $parentId,
                'name' => 'fred',
                'dyno_start_rpm' => 2000,
                'dyno_end_rpm' => 6500,
                'kw' => '50',
                'kw_rpm' => '6000',
                'nm' => '50',
                'nm_rpm' => '5500',
                'start_rpm' => '1500',
                'nm_start_rpm' => '12',
                'end_rpm' => '7250',
                'nm_end_rpm' => '20'
            ]
        ];
        $result = $vehicleController->add(['slug' => null]);

        $vehicle = $vehicleController->getView()->getEntities()[0];

        self::assertEquals(Chart::ACCELERATION, $vehicle->getValidation(), 'Validation failed');

        return $result;
    }

    /**
     *
     * @param Dyno $dynoData from vehicle
     * @param type $postData posted
     * @return array postdata for next run (with changed dyno id's)
     */
    private function checkDynoVsPost($dynoData, $postData)
    {
        /* @var $dynoData Dyno */
        $dynoData->first();
        $newPostData = [];
        foreach ($postData['Run']['Dyno'] as $dyno) {
            $this->assertEquals($dyno['nm'], $dynoData->nm);
            $newPostData[$dynoData->id] = $dyno;
            $dynoData->getNext();
        }
        $postData['Run']['Dyno'] = $newPostData;
        return $postData;
    }

}
