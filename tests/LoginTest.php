<?php

/**
 * Created by PhpStorm.
 * User: Gebruiker
 * Date: 17-11-2016
 * Time: 19:35
 */

require_once  'TestBase.php';
class LoginTest extends TestBase
{

    function testNotLoggedIn()
    {
        $b = Authorization::isLoggedIn();
        self::assertFalse($b, 'No user should be logged in');
    }

    function testLogin() {
        Authorization::login('willem', 'saab99');
        $b = Authorization::isLoggedIn();
        self::assertTrue($b, 'User should be logged in');

    }


    // type: json
    // action: gplus_login
    // code: 4/i_EQFf5S143jlhAhWMPMKOYpH6CfWkwj0dvErum5ffg
}