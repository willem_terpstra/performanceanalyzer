<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
get_header();
?>

<div id="primary">
	<div id="content" role="main">
		<h1 class="entry-title"><?= ucwords($merk) ?></h1>
		<dl>
			<?php
//list terms in a given taxonomy using wp_list_categories  (also useful as a widget)
$orderby = 'name';
$show_count = 0; // 1 for yes, 0 for no
$pad_counts = 0; // 1 for yes, 0 for no
$hierarchical = 1; // 1 for yes, 0 for no
$taxonomy = 'category';
$title = '';

$args = array(
  'orderby' => $orderby,
  'show_count' => $show_count,
  'pad_counts' => $pad_counts,
  'hierarchical' => $hierarchical,
  'taxonomy' => $taxonomy,
  'title_li' => $title
);
?>
<ul>
<?php
wp_list_categories($args);
?>
</ul>
			
			<?php
			foreach ($list as $model) {
				$slug = str_replace(' ', '-', strtolower($model->merk . '-' . $model->model . '-' . $model->jaar));
				$p = str_replace('@', 'hp@', $model->max_hp_rpm);
				$p .= 'rpm';
				$t = str_replace('@', 'nm@', $model->max_nm_rpm);
				$t .= 'rpm';
				$title = ucwords($model->merk . ' ' . $model->model . ' ' . $model->jaar);
				?><dt><a href="/models/<?= $slug ?>"><?= $title ?></a></dt>
				<dd>Performance <?= $p ?>, <?= $t ?></dd>
				<?php
			}
			?>
		</dl>

	</div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>