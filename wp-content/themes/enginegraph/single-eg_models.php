<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
//session_start();
$u = 'metric';
if (isset($_SESSION) && isset($_SESSION['units'])) {
    $u = $_SESSION['units'];
}
$metric = (bool)($u == 'metric');
$imperial = (bool)($u == 'imperial');


get_header();
$page_id = 147;  // ahum
$page = get_post($page_id);
?>

<script type="text/javascript">
    jQuery(document).ready(function () {
        var metric = <?= empty($metric) ? '0' : '1' ?>;
        var imperial = <?= empty($imperial) ? '0' : '1' ?>;
        if (metric) {
            jQuery('.i').css('display', 'none');
            jQuery('.m').css('display', 'inline');
        }
        if (imperial) {
            jQuery('.i').css('display', 'inline');
            jQuery('.m').css('display', 'none');
        }

        jQuery(".i").on("change paste keyup", function() {
            var self = jQuery(this);
            var value = self.attr('value');
            var target =  jQuery('.m #' + self.attr('id'));
            var targetUnit = target.attr('custom-unit');
            if (typeof targetUnit !== "undefined") {
                value = convertUnits(self.attr('custom-unit'), targetUnit, value);
                target.attr('value', value);
            }
        });

        jQuery('#units').change(function () {
            var str = '';
            jQuery('select option:selected').each(function () {
                str += jQuery(this).text() + '';
                str = str.trim();
            });
            var ival;
            var mval;
            var units = new Array();
            var name;
            if (str == 'metric') {
                jQuery('.i').css('display', 'none');
//                jQuery('.i').each(function () {
//                    jQuery(this).prop('disabled', true);
//                    if (jQuery(this).attr('custom-unit')) {
//                        name = jQuery(this).attr('name');
//                        obj = new Object();
//                        obj.name = name;
//                        obj.ival = jQuery(this).val();
//                        obj.unit_i = jQuery(this).attr('custom-unit');
//                        units.push(obj);
//                    }
//                });
                jQuery('.m').css('display', 'inline');
                jQuery('.m').each(function () {
                    if (jQuery(this).attr('custom-unit')) {
                        name = jQuery(this).attr('name');
                        for (i = 0; i < units.length; i++) {
                            if (units[i].name == name) {
                                var unit_m = jQuery(this).attr('custom-unit');
                                mval = convertUnits(units[i].unit_i, unit_m, units[i].ival);
                                jQuery(this).val(mval);
//                                jQuery(this).prop('disabled', false);
                            }
                        }
                        ;
                    }
                });

                jQuery.get('/wp-admin/admin-ajax.php/?action=metric', function (data) {
                    setSessVehicle('_sndb1', currentDiagramId);
                });
            }
            if (str == 'imperial') {
                jQuery('.m').css('display', 'none');
//                jQuery('.m').each(function () {
//                    jQuery(this).prop('disabled', true);
//                    if (jQuery(this).attr('custom-unit')) {
//                        name = jQuery(this).attr('name');
//                        obj = new Object();
//                        obj.name = name;
//                        obj.mval = jQuery(this).val();
//                        obj.unit_m = jQuery(this).attr('custom-unit');
//                        units.push(obj);
//                    }
//                });
                jQuery('.i').css('display', 'inline');
                jQuery('.i').each(function () {
                    if (jQuery(this).attr('custom-unit')) {
                        name = jQuery(this).attr('name');
                        for (i = 0; i < units.length; i++) {
                            if (units[i].name == name) {
                                var unit_i = jQuery(this).attr('custom-unit');
                                ival = convertUnits(units[i].unit_m, unit_i, units[i].mval);
                                jQuery(this).val(ival);
                                jQuery(this).prop('disabled', false);
                            }
                        }
                        ;
                    }
                });
                jQuery.get('/wp-admin/admin-ajax.php/?action=imperial', function (data) {
                    setSessVehicle('_sndb1', currentDiagramId);
                });
            }

        });
    });
    function convertUnits(fromUnit, toUnit, value) {

        switch (fromUnit) {
            case 'kg':
                switch (toUnit) {
                    case 'lb':
                        return round2Dec(2.205 * value);
                }
                break;
            case 'lb':
                switch (toUnit) {
                    case 'kg':
                        return round2Dec(0.4535 * value);
                }
                break;
            case 'nm':
                switch (toUnit) {
                    case 'lbft' :
                        a = value.split('@');
                        return round2Dec(0.7376 * a[0]) + '@' + a[1];
                }
                break;
            case 'lbft':
                switch (toUnit) {
                    case 'nm' :
                        a = value.split('@');
                        return round2Dec(1.356 * a[0]) + '@' + a[1];
                }
                break;
            case 'm2':
                switch (toUnit) {
                    case 'ft2' :
                        return round2Dec(10.7639 * value);
                }
                break;
            case 'ft2' :
                switch (toUnit) {
                    case 'm2':
                        return round2Dec(0.0929 * value);
                }
                break;
        }

    }
    function round2Dec(value) {
        return parseFloat(value).toFixed(2);
    }
</script>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=407703575906676";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<div id="primary">
    <div id="content" role="main">

        <h1 class="entry-title"><?= strtoupper("$merk $model"); ?></h1>
	<?php
    // echo $page->post_content;
    ?>
        <?php
        $args = array(
            'post_type' => 'eg_models',
            'p' => $postId ?? null
        );
        query_posts($args);
        ?>
        <?php while (have_posts()) :
            the_post(); ?>
            <nav id="nav-single">
                <h3 class="assistive-text"><?php _e('Post navigation', 'twentyeleven'); ?></h3>
                <span> Units:</span>
                <select id="units">
                    <?php
                    switch ($u) {
                    case 'metric':
                        $m = 'selected="selected"';
                        $i = '';
                        break;
                    case 'imperial':
                        $i = 'selected="selected"';
                        $m = '';
                        break;
                    }
                    ?>
                    <option <?= $m ?> value="metric">metric</option>
                    <option <?= $i ?> value="imperial">imperial</option>
                </select>
            </nav><!-- #nav-single -->

            <?php get_template_part('eg_models', 'single'); ?>

            <?php comments_template('', true); ?>

        <?php endwhile; // end of the loop.    ?>
        <?php wp_reset_query(); ?>
        <div class="fb-comments"
             data - href="<?= $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"
             data - width="470"
             data - num - posts="10">
        </div>
    </div><!-- #content -->
</div>
<!-- #primary -->
<?php get_footer(); ?>
