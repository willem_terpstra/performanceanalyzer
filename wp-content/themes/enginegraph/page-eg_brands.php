<?php
/**
 * Template Name: Brands Template
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
get_header();

global $post;
$termId = 0;
$cat = get_term_by('name', 'Motorcyle brands', 'category');
if (is_object($cat))
{
    $termId = $cat->term_id;
}
$args = array(
    'orderby' => 'name',
    'order' => 'ASC',
    'show_count' => 1,
    'title_li' => '',
    'number' => 100,
    'child_of' => (int) $termId,
    'walker' => new EngineGraphWalkerCategory()
);

?>
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=407703575906676";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="primary">
    <div id="content" role="main">
        <?= $post->post_content; ?>
        <table>
            <tr>
                <td>
                    <strong>Select a brand:</strong>
                    <ul>
                        <?php wp_list_categories($args); ?>
                    </ul>

                </td>
                <td >
                    <div style="padding-left:20px">
                        <strong >&nbsp;&nbsp;&nbsp;Sample image:</strong><br/>
                        <a href="/buell/1125cr-cafe-racer-2009/" >
                            <img src="/img/frontpg-sample.png" />
                        </a>
                    </div>
                </td>
            </tr>
        </table>

        <div class="fb-comments" data-href="http://motorcycleperformanceanalyzer.com" data-width="470" data-num-posts="10"></div>
    </div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>