<?php
/* Template Name: Login */
$error = '';
if (($_POST)) {
    try {

        EgInterface::selectEgDb();
        EgInterface::_register($_POST['email'], $_POST['password']);
    } catch (Exception $e) {
        $error = $e->getMessage();
    }
}

get_header(); ?>
<form name="login" id="login" method="POST" action="/register">
    <div class="container-fluid">
        <div class="container">
            <div class="col-md-6">
                <h3 class="dark-grey">Registration</h3>

                <div class="form-group col-lg-6">
                    <label>Email Address</label>
                    <input type="" name="email" class="form-control" id="" value="">
                </div>

                <div class="form-group col-lg-6">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control" id="" value="">
                </div>

            </div>

            <div class="col-md-6">
                <button type="submit" class="btn btn-dark btn-primary">Register</button>
            </div>
            <?php
            if ($error):
                ?>
                <br/>
                <div class="alert alert-danger" role="alert">
                    <?= $error ?>
                </div>
                <?php
            endif;
            ?>

        </div>
    </div>
</form>
<br/>
<?php get_footer(); ?>
