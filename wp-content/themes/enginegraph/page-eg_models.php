<?php
/**
 * Template Name: Models Template
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
get_header();
$page_id = 128;
$page = get_page($page_id);
?>
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=407703575906676";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="primary">
    <div id="content" role="main">

        <?= 'Select one of the next <strong>' . ucfirst($merk) . '</strong> models:' ?><br />
        <ul>
            <?php
            $args = array(
                'numberposts' => 500,
                'offset' => 0,
                'category' => $categoryId,
                'orderby' => 'name',
                'order' => 'ASC',
                'post_type' => 'eg_models',
                'post_status' => 'publish');
            global $post;
//		$args = array('numberposts' => 5, 'offset' => 1, 'category' => 1);
            $myposts = get_posts($args);
            foreach ($myposts as $post) : setup_postdata($post);
                ?>
                <?php
                $slug = str_replace($merk, '', $post->post_name);
                if (strpos($slug, '-') === 0) {
                    $slug = substr($slug, 1);
                }
                if ($slug[strlen($slug) - 1] != '/') {
                    $slug .= '/';
                }
                ?>
                <li><a href="/<?= $merk; ?>/<?= $slug; ?>"><?php the_title(); ?></a></li>
            <?php endforeach; ?>
        </ul>

        <div class="fb-comments" data-href="<?= $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>" data-width="470" data-num-posts="10"></div>
    </div><!-- #content -->
</div><!-- #primary -->
<?php
global $in_facebook;
if (!$in_facebook) {
    get_sidebar();
}
?>
<?php get_footer(); ?>