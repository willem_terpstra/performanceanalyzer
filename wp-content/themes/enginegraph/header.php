<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?><!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width"/>
    <title><?php
        /*
         * Print the <title> tag based on what is being viewed.
         */
        global $post, $page, $paged, $merk, $model;


        wp_title('|', true, 'right');


        // Add the blog name.
        bloginfo('name');

        // Add the blog description for the home/front page.
        $site_description = get_bloginfo('description', 'display');
        if ($site_description && (is_home() || is_front_page()))
            echo " | $site_description";

        // Add a page number if necessary:
        if ($paged >= 2 || $page >= 2)
            echo ' | ' . sprintf(__('Page %s', 'twentyeleven'), max($paged, $page));

        if (isset($post) && !empty($post)) {
            echo ' | ' . $post->post_title;
        } else {
            if (!empty($merk)) {
                if (empty($model) && $post == NULL) {
                    echo ' - Select the ' . ucfirst($merk) . '  performance analyzer';
                } else {
                    echo ' - ' . ucfirst($merk) . ' ';
                }
            }
            if (!empty($model)) {
                echo ' | Checkout the ' . ucfirst($model) . ' performance analyzer';
            }
        }
        ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11"/>
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>"/>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
    <![endif]-->
    <?php
    if (is_home() || is_front_page()) : $descrip = get_bloginfo('description');
    else :
        echo '<meta name="description" content="Checkout the online ' . ucfirst($merk) . ' ' . $model . ' performance analyzer. Graphical power, torque, acceleration and quarter mile simulations. Also perfomance, prefomance and accleration" />' . "\n";
    endif;
    ?>
    <?php
    /* We add some JavaScript to pages with the comment form
     * to support sites with threaded comments (when in use).
     */
    if (is_singular() && get_option('thread_comments'))
        wp_enqueue_script('comment-reply');

    /* Always have wp_head() just before the closing </head>
     * tag of your theme, or you will break many plugins, which
     * generally use this hook to add elements to <head> such
     * as styles, scripts, and meta tags.
     */
    wp_head();
    ?>
    <!--		<script type='text/javascript' src='/wp-includes/js/jquery/ui/jquery.ui.core.min.js?ver=1.8.20'></script>-->
    <!--		<script type='text/javascript' src='/wp-includes/js/jquery/ui/jquery.ui.widget.min.js?ver=1.8.20'></script>-->
    <!--		<script type='text/javascript' src='/wp-includes/js/jquery/ui/jquery.ui.tabs.min.js?ver=1.8.20'></script>-->

    <!--        <script src="https://cdn.bootcss.com/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_HTMLorMML"></script>-->
    <script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.js"
            integrity="sha256-9uW7yW4EwdUyWU2PHu+Ccek7+xbQpDTDS5OBP0qDrTM=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.css"
          integrity="sha256-T4bfkilI7rlQXG1R8kqn+FGhe56FhZmqmp9x75Lw4s8=" crossorigin="anonymous">
    <!--		<link rel='stylesheet' id='jquery-ui-css-css'  href='/wp-content/themes/enginegraph/css/jquery-ui/css/blitzer/custom.css' type='text/css' media='all' />		-->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-32238938-1']);
        _gaq.push(['_setDomainName', 'motorcycleperformanceanalyzer.com']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();

    </script>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed">
    <header id="branding" role="banner">

        <nav id="access" role="navigation">
            <h3 class="assistive-text"><?php _e('Main menu', 'twentyeleven'); ?></h3>
            <?php /*  Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff. */ ?>
            <div class="skip-link"><a class="assistive-text" href="#content"
                                      title="<?php esc_attr_e('Skip to primary content', 'twentyeleven'); ?>"><?php _e('Skip to primary content', 'twentyeleven'); ?></a>
            </div>
            <div class="skip-link"><a class="assistive-text" href="#secondary"
                                      title="<?php esc_attr_e('Skip to secondary content', 'twentyeleven'); ?>"><?php _e('Skip to secondary content', 'twentyeleven'); ?></a>
            </div>
            <?php /* Our navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu. The menu assiged to the primary position is the one used. If none is assigned, the menu with the lowest ID is used. */ ?>
            <?php wp_nav_menu(array('theme_location' => 'primary')); ?>
        </nav><!-- #access -->
    </header><!-- #branding -->

    <div id="main">

<?php
?>