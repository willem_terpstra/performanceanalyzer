<?php
/*
  Plugin Name: EngineGraph
  Plugin URI: http://man/
  Description: Soap wrapper for enginegraph
  Version: 1.0
  Author: Willem Terpstra
  Author URI: http://enginegraph.com
  License: GPL
 */
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

add_action('init', 'start_enginegraph');
$interface = NULL;
$enabled = '';
$merk = '';
$model = '';

$client = NULL;

require_once 'MVC/interface.php';

function start_enginegraph()
{
    if (!isset($_SESSION)) {
        if (defined('KEEP_SESSION')) {
            session_id(KEEP_SESSION);
        }
        session_start();
        if (!isset($_SESSION['eg_session_id'])) {
            login();
        }
    }
    wp_enqueue_script('jquery');
    wp_enqueue_script('jsapi', 'https://www.google.com/jsapi');
    wp_enqueue_script('enginegraph.js', '/wp-includes/js/enginegraph.js');
    return true;
}

function login($user = 'wpuser', $password = 'choewoep', $loginBy = Authorization::LOGIN_BY_USERID)
{
    $sessionId = NULL;
    if ($_SESSION['eg_session_id'] ?? null && $_SESSION['eg_session_id'] !== 'false') {
        $sessionId = $_SESSION['eg_session_id'];
        EgInterface::set_session($sessionId);
    }
    EgInterface::login($user, $password, $loginBy);
//    if ($user == 'wpuser') {
    $_SESSION['units'] = 'metric';
//    } elseif ($user == 'impuser') {
//        $_SESSION['units'] = 'imperial';
//    }
    if ($sessionId !== 'false') {
        $_SESSION['eg_session_id'] = $sessionId;
    }
}

function get_enginegraph_interface($vehicleId, $form)
{
    global $interface;
    global $enabled;
    if (!session_id()) {
        session_start();
    }
    if (!$interface) {
        $interface = EgInterface::getDiagram(7, '_sndb1', $_SESSION['eg_session_id'], $enabled, $vehicleId, $form);
    }
    return $interface;
}

function updatePosts($brand): array
{
    if (!session_id()) {
        session_start();
    }

    $modelList = modelList($brand);

    return updateModels($modelList);
}

function updateModels($modelList): array
{
    Klog::LogInfo("models found: " . count($modelList));
    $posts = [];
    foreach ($modelList as $model) {
        $vehicle = EgInterface::getVehiclePage($model['uid']);

        $t = new TemplateEngine('/MVC/html/soap/models-content.php');
        $t->assign('vehicle', $vehicle);
        $pageData = $t->execTemplate();

        $title = $vehicle->model . ' ' . $vehicle->jaar;
        $oldTitle = $vehicle->merk . ' ' . $vehicle->model . ' ' . $vehicle->jaar;
        $postData = array(
            //			'menu_order' => [<order>] //If new post is a page, ses the order should it appear in the tabs.
            'comment_status' => 'closed', // 'closed' means no comments.
            'ping_status' => 'open', // 'closed' means pingbacks or trackbacks turned off
            //			'pinged' => [?] //?
            //			'post_author' => [<user ID>] //The user ID number of the author.
//			'post_category' => array((int)$cat->term_id), //[array(<category id>, <...>)] //Add some categories.
            'post_content' => $pageData, //The full text of the post.
            'post_date' => date("Y-m-d H:i:s"), //The time post was made.
            //			'post_date_gmt' => [Y-m-d H:i:s] //The time post was made, in GMT.
            //			'post_excerpt' => [<an excerpt>] //For all your post excerpt needs.
            'post_name' => str_replace(' ', '-', strtolower($title)), // The name (slug) for your post
//			'post_parent' => $parentPageId, //Sets the parent of the new post.
            //			'post_password' => [?] //password for post?
            'post_status' => 'publish', //Set the status of the new post.
            'post_title' => $title, //The title of your post.ar
            'post_type' => 'eg_models', // | 'page' | 'link' | 'nav_menu_item' | custom post type] //You may want to insert a regular post, page, link, a menu item or some custom post type
            //			'tags_input' => ['<tag>, <tag>, <...>'] //For tags.
            //			'to_ping' => [?] //?
            //			'tax_input' => [array( 'taxonomy_name' => array( 'term', 'term2', 'term3' ) )] // support for custom taxonomies.
            'tax_input' => array('year' => array($vehicle->jaar)), // support for custom taxonomies.
        );
        $page = get_page_by_title($title, OBJECT, 'eg_models');
        if (empty($page)) {
            $page = get_page_by_title($oldTitle, OBJECT, 'eg_models');
        }
        $postId = NULL;
        if (!empty($page)) {
            $postId = $page->ID;
        }
        Klog::LogInfo("postId: $postId title: $title");
        echo "postId: $postId title: $title<br/>";
        if (!empty($postId)) {
            $postData = array_merge(array('ID' => $postId), $postData);
        }

//        wp_delete_post($postId);

        $postId = wp_insert_post($postData);
        $cat = get_term_by('name', $vehicle->merk, 'category');
        if (!$cat)
        {
            $catName = $vehicle->merk;
            //create the main category
            wp_insert_term(
                // the name of the category
                $vehicle->merk,
                // the taxonomy, which in this case if category (don't change)
                'category',
                [
                    // what to use in the url for term archive
                    'slug' => str_replace(' ', '-', strtolower($vehicle->merk)),
                ]);
        }
        $cat = get_term_by('name', $vehicle->merk, 'category');
        wp_set_object_terms($postId, array((int)$cat->term_id), 'category');
        add_post_meta($postId, 'year', $vehicle->jaar);

        $posts[] = get_post($postId);

    }

    return $posts;
}

function clearUpdatedModels()
{
    EgInterface::clearUpdatedVehicleList();
}


add_action('wp_ajax_nopriv_metric', 'metric');
add_action('wp_ajax_metric', 'metric');

function metric()
{
    if (!session_id()) {
        session_start();
    }
    $_SESSION['units'] = 'metric';
//    login('wpuser', 'choewoep', Authorization::LOGIN_BY_USERID);
}

add_action('wp_ajax_nopriv_imperial', 'imperial');
add_action('wp_ajax_imperial', 'imperial');

function imperial()
{
    if (!session_id()) {
        session_start();
    }
//    login('impuser', 'choewoep', Authorization::LOGIN_BY_USERID);
    $_SESSION['units'] = 'imperial';
}

/* Shorttag handling
  ---------------------------------------------------- */

function useVehicle($vehicleId)
{
    if (!session_id()) {
        session_start();
    }
    EgInterface::useVehicle($vehicleId, $_SESSION['eg_session_id']);
}

function enableFields($enabledFields)
{
    global $enabled;
    $enabled = $enabledFields;
}

function brandList()
{
    if (!session_id()) {
        session_start();
    }
    return EgInterface::getBrandList('motor');
}

function modelList($brand)
{
    if (!session_id()) {
        session_start();
    }
    return EgInterface::getVehicleList('motor', $brand);
}

function updatedModelList()
{
    return EgInterface::getUpdatedVehicleList();
}

/* Taxonomy
  ---------------------------------------------------- */


add_action("admin_init", "eg_register_model_admin_init");
add_action("init", "eg_register_model_init");

function eg_register_model_init()
{
    // Fire this during init
    register_post_type('eg_models', array(
        'label' => __('Models'),
        'singular_label' => __('Model'),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'query_var' => false,
        '_builtin' => false,
        'taxonomies' => array('category'),
        'supports' => array('title', 'editor', 'author', 'post-formats')
    ));
}

function eg_register_model_admin_init()
{
    // Fire this during init
    register_post_type('eg_models', array(
        'label' => __('Models'),
        'singular_label' => __('Model'),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'query_var' => false,
        '_builtin' => false,
        'rewrite' => array('slug' => 'models'),
        'taxonomies' => array('category'),
        'supports' => array('title', 'editor', 'author', 'post-formats')
    ));
}

add_action("admin_init", "eg_meta_box");

function eg_meta_box()
{
    add_meta_box("eg-model-meta", "Year", "eg_model_meta", "eg_models", "normal", "low");
}

function eg_model_meta()
{
    global $post;
    $year = get_post_meta($post->ID, 'year', true);
    echo '<input type="text" id="eg_model_year" name="eg_model_year" value="' . $year . '" size="4" />';
}

add_action('save_post', 'save_postdata');

function save_postdata($post_id)
{
    // verify if this is an auto save routine.
    // If it is our form has not been submitted, so we do not want to do anything
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;

    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
    # if ( !wp_verify_nonce( $_POST['myplugin_noncename'], plugin_basename( __FILE__ ) ) )
    #     return;
    // Check permissions
    if ($_POST['post_type'] ?? false != 'eg_models') {
        return;
    }

    // OK, we're authenticated: we need to find and save the data

    $year = $_POST['eg_model_year'];
    update_post_meta($post_id, 'year', $year);
}

/* Custom routing
  ----------------------------------------------------------------- */
add_action('send_headers', 'site_router');

function site_router()
{
    global $merk, $model;
    $bits = explode("/", $_SERVER['REQUEST_URI']);
    $model = '';
    $merk = $bits[1];
    if (isset($bits[2])) {
        $model = $bits[2];
    }

    $cat = get_term_by('slug', $merk, 'category');
    if (!empty($cat)) {
        if (empty($model)) {
            $categoryId = $cat->term_id;
            include TEMPLATEPATH . '/page-eg_models.php';
        } else {
            global $wpdb;
            $slug = "$model";
            $postId = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name = '" . $wpdb->prepare($slug) . "' LIMIT 1");
            if (empty($postId)) {
                return;
            }
            $model = ucwords(str_replace('-', ' ', $model));
            include TEMPLATEPATH . '/single-eg_models.php';
//            $s = ob_get_contents();
        }
    } elseif ($merk === 'user-login') {
        include TEMPLATEPATH . '/page-login.php';
    } elseif ($merk === 'register') {
        include TEMPLATEPATH . '/page-register.php';
    } else {
        return;
    }

}

/* Custom link building
  ---------------------------------------------------- */

function custom_post_link($post_link, $id = 0)
{
    $post = get_post($id);

    if (!is_object($post) || $post->post_type != 'eg_models') {
        return $post_link;
    }
    $client = 'misc';
    $terms = wp_get_object_terms($post->ID, 'category');
    if ($terms) {
        $client = $terms[0]->slug;
    }
    $post_link = str_replace('eg_models', $client, $post_link);

    return $post_link;
}

add_filter('post_type_link', 'custom_post_link', 1, 3);

/* Helpers ---------------------------------------------------- */

function getCogs($ratio)
{
    $min_r = 1;
    $a = 0;
    $b = 0;
    for ($i = 10; $i < 100; $i++) {
        $p = $i * $ratio;
        $q = round($p);
        if ($q > $p) {
            $r = $q - $p;
        } else {
            $r = $p - $q;
        }
        if ($r < $min_r) {
            $a = $i;
            $b = $p;
            $min_r = $r;
        }
    }
    return array($a, round($b));
}

function nmAtRpmTolbAtrpm($str)
{
    $a = explode('@', $str);
    $a[0] = round($a[0] * 0.737, 2);
    return implode('@', $a);
}


function modelsByBrand(array $brand): array
{
    $brandName = $brand['naam'];
    $args = [
        'numberposts' => 500,
        'offset' => 0,
        'category_name' => $brandName,
        'orderby' => 'name',
        'order' => 'ASC',
        'post_type' => 'eg_models',
        'post_status' => 'publish'
    ];
    $posts = get_posts($args);
    $found = [];
    foreach ($posts as $post) {
        $found[] = $post;
    }
    return $found;
}


function setPostStatus(array $posts, string $newStatus): void
{
    foreach ($posts as $post) {
        $post->post_status = $newStatus;
        wp_update_post($post);
        $found[] = $post;
    }

}

add_action('admin_menu', 'update_brand_menu');

function update_brand_menu()
{
    add_menu_page('Update Brands Page', 'Update Brands', 'manage_options', 'update-brands-slug', 'update_brand_admin_page');

}

function update_brand_admin_page()
{

    // This function creates the output for the admin page.
    // It also checks the value of the $_POST variable to see whether
    // there has been a form submission.
    // update-brand
    // The check_admin_referer is a WordPress function that does some security
    // checking and is recommended good practice.

    // General check for user permissions.
    if (!current_user_can('manage_options')) {
        wp_die(__('You do not have sufficient pilchards to access this page.'));
    }

    // Start building the page()
    $brands = brandList();

    echo '<div class="wrap">';

    echo '<h2>Update brands</h2>';


    foreach ($brands as $brand) {
        $brandName = $brand['naam'];
        if (isset($_POST['submit'])
            && ($_POST['submit'] === 'Update ' . $brandName || $_POST['submit'] === 'Update All')
        ) //            && check_admin_referer('test_' . $brandName.  '_clicked'))
        {
            echo 'Updating: ' . $brandName . '<br/>';
            // the button has been pressed AND we've passed the security check
            $oldPosts = modelsByBrand($brand);

            $old = [];
            foreach ($oldPosts as $post) {
                $old[$post->post_name] = $post->post_status;
            }


            setPostStatus($oldPosts, 'pending');

            $newPosts = updatePosts($brand);

            $new = [];
            foreach ($newPosts as $post) {
                $new[$post->post_name] = $post->post_status;
            }

            $out = [];
            foreach ($old as $oldName => $oldPost) {

                if (!array_key_exists($oldName, $new)) {
                    $out[] = 'Made pending: ' . $oldName;
                } elseif ($old[$oldName] !== $new[$oldName]) {
                    $out[] = $oldName . ' status changed: ' . $old[$oldName] . ' => ' . $new[$oldName];
                }
            }

            // TODO: added posts

            echo implode('<br/>', $out);

        }
    }

    echo '<form action="options-general.php?page=update-brands-slug" method="post">';

    // this is a WordPress security feature - see: https://codex.wordpress.org/WordPress_Nonces
    global $egConn;

    foreach ($brands as $brand) {
        $brandName = $brand['naam'];
//        wp_nonce_field('test_' . $brandName . '_clicked');
        echo '<input type="hidden" value="true" name="button_' . $brandName . '" />';
        submit_button('Update ' . $brandName);
    }
    echo '<input type="hidden" value="true" name="button_all' . '" />';
    submit_button('Update All');
    echo '</form>';

    echo '</div>';

}

remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');


