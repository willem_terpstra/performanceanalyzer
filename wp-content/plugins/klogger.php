<?php
/*
  Plugin Name: Klogger
  Plugin URI: http://wordpress/
  Description: A simple hello world wordpress plugin
  Version: 1.0
  Author: Willem Terpstra
  Author URI: http://enginegraph.com
  License: GPL
 */

class KLogger {

    const DEBUG = 1; // Most Verbose
    const INFO = 2; // ...
    const WARN = 3; // ...
    const ERROR = 4; // ...
    const FATAL = 5; // Least Verbose
    const OFF = 6; // Nothing at all.
    const LOG_OPEN = 1;
    const OPEN_FAILED = 2;
    const LOG_CLOSED = 3;
    const LRECL = 255;

    /* Public members: Not so much of an example of encapsulation, but that's okay. */

    public $Log_Status = Klogger::LOG_CLOSED;
    public $DateFormat = "Y-m-d G:i:s";
    public $MessageQueue;
    private $log_file;
    private $priority = Klogger::INFO;
    private $file_handle;

    public function __construct($filepath = 'log.txt', $priority = LOG_ALERT) {
        $this->priority = $priority;
    }

    public function LogInfo($line) {
        $this->Log($line, LOG_INFO);
    }

    public function LogDebug($line) {
        $this->Log($line, LOG_DEBUG);
    }

    public function LogWarn($line) {
        $this->Log($line, LOG_WARNING);
    }

    public function LogError($line) {
        $this->Log($line, LOG_ERROR);
    }

    public function LogFatal($line) {
        $this->Log($line, Klogger::FATAL);
    }

    public function Log($line, $priority) {
//        if ($priority >= $this->priority) {
            syslog($priority, $line);
//        }
    }

    /**
     *
     * @deprecated
     * @param $line
     *
     */
    public function WriteFreeFormLine($line) {
        syslog(LOG_ERR, $line);
    }

}


class Klog {

    private static $klogger = NULL;

    function __construct() {
        if (self::$klogger === NULL) {
            self::$klogger = new Klogger('log.txt', Klogger::INFO);
        }
//		return self::$klogger->$name($arguments);
    }

    public static function LogInfo($line) {
        if (self::$klogger === NULL) {
            self::$klogger = new Klogger('log.txt', Klogger::INFO);
        }
        return self::$klogger->LogInfo($line);
    }

    public static function LogDebug($line) {
        return self::$klogger->LogDebug($line);
    }

    public static function LogWarn($line) {
        return self::$klogger->LogWarn($line);
    }

    public static function LogError($line) {
        return self::$klogger->LogError($line);
    }

    public static function LogFatal($line) {
        return self::$klogger->LogFatal($line);
    }

    public static function Log($line, $priority) {
        self::$klogger->Log($line, $priority);
    }

    public static function WriteFreeFormLine($line) {
        return self::$klogger->WriteFreeFormLine($line);
    }

    public static function readLog($file = 'log.txt', $lines = 100) {
        //global $fsize;
        $handle = fopen($file, 'r');
        $length = $lines * Klogger::LRECL;
        $filesize = filesize($file);
        $start = filesize($file) - $length;
        if (fseek($handle, $start) != -1) {
            $data = fread($handle, $length);
        }
        fclose($handle);
        return nl2br($data);
    }
}

add_action('init', 'start_klogger');
add_action('admin_init', 'start_klogger');
function start_klogger() {
    new Klog();
//	Klog::LogInfo('klog started');
}

?>
