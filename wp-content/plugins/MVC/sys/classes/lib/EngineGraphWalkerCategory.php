<?php
class EngineGraphWalkerCategory extends Walker_Category {

	function start_el(&$output, $category, $depth = 0, $args = [], $id = 0) {
		parent::start_el($output, $category, $depth, $args);
		$output = str_replace(['category/','motorcycle-brands/'], '', $output);
	}

}
?>
