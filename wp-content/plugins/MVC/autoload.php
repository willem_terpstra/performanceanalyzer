<?php

class AutoLoadInfo
{

    static $includePaths = NULL;
    const EXTERNAL_SRC_PATH = '/var/www/html/v01-enginegraph';

    public static function getIncludePaths()
    {
        if (!empty(self::$includePaths)) {
            return self::$includePaths;
        }
        if (isset($_GET['XDEBUG_SESSION_START'])) {
            $path = dirname(realpath(__FILE__));
        } else {
            $path = $_SERVER['DOCUMENT_ROOT'];
        }

        $extDir = self::EXTERNAL_SRC_PATH;
        $path .= '/wp-content/plugins';
        $includePaths = array(
            'EngineGraphWalkerCategory' => "$path/MVC/sys/classes/lib/",

            'AccelerationDiagram' => "{$extDir}/MVC/entity/custom/",
            'AccelerationDiagramView' => "{$extDir}/MVC/views/custom/",
            'AccelerationDistanceDiagram' => "{$extDir}/MVC/entity/custom/",
            'AccelerationDistanceDiagramView' => "{$extDir}/MVC/views/custom/",
            'AccelerationLeadDiagram' => "{$extDir}/MVC/entity/custom/",
            'AccelerationLeadDiagramView' => "{$extDir}/MVC/views/custom/",
            'AccelerationTimeDiagram' => "{$extDir}/MVC/entity/custom/",
            'Ajax' => "{$extDir}/MVC/classes/lib/",
            'Authorization' => "{$extDir}/MVC/classes/lib/",
            'Cache' => "{$extDir}/MVC/entity/",
            'Chassis' => "{$extDir}/MVC/entity/",
            'Clutch' => "{$extDir}/MVC/entity/",
            'Controller' => "{$extDir}/MVC/classes/lib/",
            'CubicSpline' => "{$extDir}/MVC/classes/lib/",
            'dbTable' => "{$extDir}/MVC/classes/lib/",
            'dbTableException' => "{$extDir}/MVC/classes/Exceptions/",
            'Diagram' => "{$extDir}/MVC/entity/",
            'DiagramController' => "{$extDir}/MVC/controllers/",
            'DiagramR' => "{$extDir}/MVC/entity/",
            'DiagramView' => "{$extDir}/MVC/views/",
            'Driver' => "{$extDir}/MVC/entity/",
            'DropdownAble' => "{$extDir}/MVC/interfaces/",
            'Dyno' => "{$extDir}/MVC/entity/",
            'Engine' => "{$extDir}/MVC/entity/",
            'Get' => "{$extDir}/MVC/classes/lib/",
            'GraphDescriptor' => "{$extDir}/MVC/classes/lib/",
            'HttpRequest' => "{$extDir}/MVC/classes/lib/",
            'Klog' => "{$extDir}/MVC/classes/lib/",
            'Klogger' => "{$extDir}/MVC/classes/lib/",
            'Languages' => "{$extDir}/MVC/entity/",
            'M' => "{$extDir}/MVC/classes/lib/",
            'Merken' => "{$extDir}/MVC/entity/",
            'Message' => "{$extDir}/MVC/classes/lib/",
            'PhpR' => "{$extDir}/MVC/classes/lib/",
            'Post' => "{$extDir}/MVC/classes/lib/",
            'PowerTorqDiagramView' => "{$extDir}/MVC/views/custom/",
            'PowerTorqDiagram' => "{$extDir}/MVC/entity/custom/",
            'R' => "{$extDir}/MVC/entity/",
            'Rcalc' => "{$extDir}/MVC/interfaces/",
            'RException' => "{$extDir}/MVC/classes/Exceptions/",
            'Run' => "{$extDir}/MVC/entity/",
            'Sandbox' => "{$extDir}/MVC/classes/lib/",
            'SecundaryTransmission' => "{$extDir}/MVC/entity/",
            'SoapDiagramView' => "{$extDir}/MVC/views/custom/",
            'SoapVehicle' => "{$extDir}/MVC/entity/custom/",
            'Strings' => "{$extDir}/MVC/entity/",
            'TorqDiagramView' => "{$extDir}/MVC/views/custom/",
            'Transmission' => "{$extDir}/MVC/entity/",
            'SecurityException' => "{$extDir}/MVC/classes/Exceptions/",
            'TemplateException' => "{$extDir}/MVC/classes/Exceptions/",
            'TemplateEngine' => "{$extDir}/MVC/classes/lib/",
            'Unit' => "{$extDir}/MVC/entity/",
            'UnitGroup' => "{$extDir}/MVC/entity/",
            'Url' => "{$extDir}/MVC/classes/lib/",
            'Users' => "{$extDir}/MVC/entity/",
            'Vehicle' => "{$extDir}/MVC/entity/",
            'VehicleController' => "{$extDir}/MVC/controllers/",
            'VehicleDiagram' => "{$extDir}/MVC/entity/",
            'VehicleException' => "{$extDir}/MVC/classes/Exceptions/",
            'View' => "{$extDir}/MVC/classes/lib/",
            'VwDyno' => "{$extDir}/MVC/entity/",
            'VwLogin' => "{$extDir}/MVC/entity/",
            'VwVehiclePageInfo' => "{$extDir}/MVC/entity/",
            '_Cache' => "{$extDir}/MVC/entity/generated/",
            '_Chassis' => "{$extDir}/MVC/entity/generated/",
            '_Clutch' => "{$extDir}/MVC/entity/generated/",
            '_Diagram' => "{$extDir}/MVC/entity/generated/",
            '_DiagramR' => "{$extDir}/MVC/entity/generated/",
            '_DiagramView' => "{$extDir}/MVC/views/generated/",
            '_Driver' => "{$extDir}/MVC/entity/generated/",
            '_Dyno' => "{$extDir}/MVC/entity/generated/",
            '_Engine' => "{$extDir}/MVC/entity/generated/",
            '_Languages' => "{$extDir}/MVC/entity/generated/",
            '_Merken' => "{$extDir}/MVC/entity/generated/",
            '_R' => "{$extDir}/MVC/entity/generated/",
            '_Run' => "{$extDir}/MVC/entity/generated/",
            '_SecundaryTransmission' => "{$extDir}/MVC/entity/generated/",
            '_Strings' => "{$extDir}/MVC/entity/generated/",
            '_Transmission' => "{$extDir}/MVC/entity/generated/",
            '_Unit' => "{$extDir}/MVC/entity/generated/",
            '_UnitGroup' => "{$extDir}/MVC/entity/generated/",
            '_Users' => "{$extDir}/MVC/entity/generated/",
            '_Vehicle' => "{$extDir}/MVC/entity/generated/",
            '_VehicleDiagram' => "{$extDir}/MVC/entity/generated/",
            '_VwDyno' => "{$extDir}/MVC/entity/generated/",
            '_VwLogin' => "{$extDir}/MVC/entity/generated/",
            '_VwVehiclePageInfo' => "{$extDir}/MVC/entity/generated/",
        );
        self::$includePaths = $includePaths;
        return $includePaths;
    }

}

spl_autoload_register(
    function ($className) {
        $includePaths = AutoLoadInfo::getIncludePaths();
        if (isset($includePaths[$className])) {
            $fileName = $className . '.php';
            $ipath = $includePaths[$className];
#echo $ipath . '<br/>';
            if (file_exists($ipath . $fileName)) {
                require_once $ipath . $fileName;
            }
        }
    }
);
