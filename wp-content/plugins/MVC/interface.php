<?php

require_once 'autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/globals.php';

$soap = true;
if (!$config = simplexml_load_string(file_get_contents(AutoLoadInfo::EXTERNAL_SRC_PATH . '/MVC/config/config.xml'))) {
    trigger_error('Error reading XML file', E_USER_ERROR);
}
$layout = $config->layout->blog;
Strings::init(AutoLoadInfo::EXTERNAL_SRC_PATH .'/');

$egConn = null;

class EgInterface {

    static $db = NULL;
    static $extDb = NULL;

    static public function selectEgDb() {
        if(empty(self::$extDb)) {
            global $config;
            self::$extDb = $config->mysql->db;
        }
        global $egConn, $wpdb;
        if (!$egConn)
        {
            $egConn = $wpdb->dbh;
        }
        mysqli_select_db($egConn, self::$extDb);
        dbTable::setConnection($egConn);
    }

    static public function selectWpDb() {
        if(empty(self::$db)) {
            global $config;
            self::$db = $config->mysql_wp->db;
        }
        global $egConn;
        mysqli_select_db($egConn, self::$db);
    }

    public static function __callStatic($name, $arguments) {
        $f = 'self::_' . $name;
        self::selectEgDb();
        $r = call_user_func_array($f, $arguments);
        self::selectWpDb();
        return $r;
    }

    static public function _login($userId, $passwd, $loginBy = Authorization::LOGIN_BY_USERID) {
//        if ($session_id) {
//            self::set_session($session_id);
//        }
        $authorisation = new Authorization();
//	Klog::LogInfo(session_id());
        if ($authorisation->login($userId, $passwd, $loginBy)) {
            global $layout, $config;
            Klog::LogInfo("logged in: $userId session_id: " . session_id());
            $environment = 'blog';
            $layout = $config->layout->$environment;
            $_SESSION['layout'] = $environment;
            return true;
//            return session_id();
        } else {
            Klog::LogInfo("NOT logged in: $userId session_id: " . session_id());
            return false;
        }
    }

    static public function _register ($email, $password)
    {
            $email = filter_var($email, FILTER_FLAG_EMAIL_UNICODE);
            if(!$email)
            {
                throw new Exception('Not a valid email address');
            }

            $u = new Users();
            global $config;
            $u->setPassw(
                  $password
            );
            $u->setEmail($email);
            $u->save();
    }


    static public function _getDiagram($diagram_id, $session_tag, $session_id, $enabled_fields, $vehicle_id, $form) {
        self::set_session($session_id);
        self::_useVehicle($vehicle_id, $session_id);
        $view = new SoapDiagramView($diagram_id);
        $view->setEnabled($enabled_fields);
        KLog::LogInfo('Get diagram for: ' . $session_tag . ' in session: ' . $session_id . " form: $form");
        $html = $view->browseDetail($session_tag, $form);
        return $html;
    }

    static public function _useVehicle($uid, $session_id) {
        self::set_session($session_id);
        $d = new SoapVehicle($uid);
        $result = $d->useVehicle($uid);
        return $result;
    }

    static public function getVehicle($tag, $session_id) {
        set_session($session_id);
        $d = new SoapVehicle();
        $result = $d->getVehicle($tag);
        return $result;
    }

    static public function _setVehicle($settingsObj, $session_id) {
        self::set_session($session_id);
        $gears = array();

        $settings = [];
        // use only metric units
        foreach(get_object_vars($settingsObj) as $name => $value) {
            if (strpos($name, 'm_') === 0)
            {
                $name = ltrim($name, 'm');
                $name = ltrim($name, '_');
            }

            $settings[$name] = $value;

        }
        $settings['uid'] = $settingsObj->uid;
        $settings['session_tag'] = $settingsObj->session_tag;

        for ($i = 1; $i <= 6; $i++) {
            if (isset($settings["use_g$i"])) {
                $gears[] = $settings["use_g$i"];
            }
        }
//	Klog::LogInfo("use gears: " . var_export($gears, true));
        if (!empty($gears)) {
            AccelerationDiagram::useGears($gears);
        }
        $d = new SoapVehicle();
        foreach ($settings as $k => $v) {
            if ($v == NULL) {
                unset($settings[$k]);
            }
        }
//		Klog::LogInfo(var_export($settings, true));
        $d->setSettings($settings);
        $result = $d->getSoapOut();
        return $result;
    }

    static public function set_session($session_id) {
//		session_write_close();
//		session_id($session_id);
//		session_start();
        Unit::init();
        R::init();
        new Strings();
        $environment = NULL;
        if (isset($_SESSION['layout'])) {
            $environment = $_SESSION['layout'];
        } else {
            $environment = 'blog';
        }
        global $layout;
        global $config;
        $layout = $config->layout->$environment;
    }

    static public function _getChartData($vis, $session_id) {
        self::set_session($session_id);
        $result = Url::createFromVis($vis);
        new Get($result['url']);
        new Strings();
        $d = new Diagram();
        $dc = new DiagramController();
        $data = $dc->run('DiagramView', $result['diagramId'], Controller::IMAGE);
        $d->setDescription($data);
        $result = $d->getSoapOut();
        return $result;
    }

    static public function _getBrandList($vehicle_type_id = 'motor') {
        $merken = new Merken();
        if ($vehicle_type_id == 'motor') {
            $sql = "SELECT
                    merken.uid
                    , merken.naam as naam
                    , merken.merken_id
                FROM
                    vehicle
                    INNER JOIN merken 
                        ON (vehicle.merken_id = merken.uid)
                    INNER JOIN vehicle_type 
                        ON (vehicle.vehicle_type_id = vehicle_type.uid)
                    INNER JOIN vehicle_diagram 
                        ON (vehicle_diagram.vehicle_id = vehicle.uid)
                    INNER JOIN diagram 
                        ON (vehicle_diagram.diagram_id = diagram.uid)
                WHERE (vehicle_type.naam = ?
                    AND diagram.name = ?)
                GROUP BY merken.uid
                ORDER BY merken.naam";
            $output = array();

            foreach ($merken->lijst($sql, ['motor', 'acceleration']) as $merk) {
                $output[] = $merk->getSoapOut();
            }
            return $output;
        } else {
            return 'cars not supported';
        }
    }

    static public function _getVehicleList($vehicle_type_id, $merk) {
        $vehicles = new VwVehiclePageInfo();
        if ($vehicle_type_id == 'motor') {
            $sql = "SELECT * FROM vw_vehicle_page_info WHERE merk = ?";
            $output = array();
            foreach ($vehicles->lijst($sql, [$merk['naam']]) as $vehicle) {
                $output[] = $vehicle->getSoapOut();
            }
            return $output;
        } else {
            return 'cars not supported';
        }
    }

    static public function _getUpdatedVehicleList() {
        $vehicles = new VwVehiclePageInfo();
        $sql = "SELECT * FROM vw_vehicle_page_info WHERE updated=1";
        $output = array();
        foreach ($vehicles->lijst($sql) as $vehicle) {
            $output[] = $vehicle->getSoapOut();
        }
        return $output;
    }

    static public function _clearUpdatedVehicleList() {
        self::_login('willem', 'saab99');
        $vehicles = new Vehicle();
        $sql = "SELECT * FROM vehicle WHERE updated = ?";
        foreach ($vehicles->lijst($sql, [ 1 ]) as $vehicle) {
            $vehicle->setUpdated(0);
            $vehicle->saveOrUpdate();
        }
        self::_login('wpuser', 'choewoep');
        return;
    }

    static public function _getVehiclePage($uid) {
        $vwVehiclePageInfo = new VwVehiclePageInfo($uid);
        return (object) $vwVehiclePageInfo->getSoapOut();
    }

}

