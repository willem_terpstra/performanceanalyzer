<?php

require '../wp-content/plugins/MVC/interface.php';
if (!$config = simplexml_load_string(file_get_contents(AutoLoadInfo::EXTERNAL_SRC_PATH . '/MVC/config/config.xml'))) {
    trigger_error('Error reading XML file', E_USER_ERROR);
}

date_default_timezone_set('Europe/Paris');

$rootPage = $config->root_page;
$egConn = mysqli_connect("wordpress", "root", "password");

if (defined('KEEP_SESSION')) {
    session_id(KEEP_SESSION);
}

session_start();

header('Content-type: application/json');

try {
    switch ($_GET['action']) {
    case 'chart_data':
        chart_data();
        break;
    case 'set_vehicle':
        set_vehicle();
        break;
    }
} catch (Throwable $e) {

    $code = $e->getCode();
    if (0 === $code)
    {
        $code = 500;
    }

    header($_SERVER['SERVER_PROTOCOL'] . ' ' . $code . ' ' . httpCode($code) . ' ' . $e->getMessage());

    echo json_encode([
            'error' => $e->getMessage(),
            'backtrace' => $e->getTrace()
        ],
        JSON_PRETTY_PRINT
    );
}
//finally {
//    file_put_contents('files.txt', var_export(get_included_files(), true), FILE_APPEND);
//}

function chart_data()
{
    $msgs = [];
    foreach (['tq', 'tqx'] as $param) {
        if (!array_key_exists($param, $_GET)) {
            $msgs[] = 'Missing parameter: ' . $param;
        }
    }

    if (!empty($msgs)) {
        throw new Exception(implode("\n", $msgs), 417);
    }

    $q = 'tq=' . $_GET['tq'];
    $tqx = urldecode($_GET['tqx']);
    if (preg_match('/reqId:(\d+)/', $tqx, $regs)) {
        $reqId = $regs[1];
    }

    $data = EgInterface::getChartData($q, $_SESSION['eg_session_id']);
    $data = str_replace('%reqId%', $reqId, $data['description']);
    echo $data;
//    exit;
}

function set_vehicle()
{
    $veh = new stdClass();
    foreach ($_POST as $k => $v) {
        if ($k == 'max_lbft_rpm') {
            $k = 'max_nm_rpm';
            $t = explode('@', $v);
            if (isset($t[0]) && isset($t[1])) {
                if (is_numeric($t[0]) && is_numeric($t[1])) {
                    $nmTorq = (float)$t[0] * 1.356;
                    $rpm = (int)$t[1];
                    $v = "$nmTorq@$rpm";
                }
            }
        }
        $veh->$k = $v;
    }

    $veh->session_tag = isset($_GET['t']) ? $_GET['t'] : $veh->tag;
    unset($veh->tag);
    $data = EgInterface::setVehicle($veh, $_SESSION['eg_session_id']);
    echo json_encode($data['json'], JSON_PRETTY_PRINT);
//    exit;
}

function httpCode(int $statusCode)
{
    static $status_codes = null;

    if ($status_codes === null) {
        $status_codes = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            102 => 'Processing',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            207 => 'Multi-Status',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            422 => 'Unprocessable Entity',
            423 => 'Locked',
            424 => 'Failed Dependency',
            426 => 'Upgrade Required',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported',
            506 => 'Variant Also Negotiates',
            507 => 'Insufficient Storage',
            509 => 'Bandwidth Limit Exceeded',
            510 => 'Not Extended'
        );
    }
    if (array_key_exists($statusCode, $status_codes)) {
        return $status_codes[$statusCode];
    } else {
        return '(code does not exist)';
    }
}

/**
 * @param $args
 * @param $code
 * @return mixed
 */
function createFunction($args, $code) {
    static $i = 0;
    if (empty($args))
    {
        $args = [];
    }
    $i++;
    $name = "f_$i";
    $variables = implode(', ', array_keys($args));
    $values = array_values($args);
    $function = "function $name($variables) { $code }";
    eval($function);
    return $name(...$values);
}

