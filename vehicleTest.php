#!php-7.2.0/php.exe
<?php
/**
#!php-7.1.12/php.exe
 * Created by PhpStorm.
 * User: Gebruiker
 * Date: 05/08/2018
 * Time: 16:49
 */

require_once 'wp-content/plugins/MVC/autoload.php';

//$host = 'http://motorcycleperformanceanalyzer.com/';
$host = 'http://man';


$requests = [
    [
        'url' => $host . '/cagiva/raptor-650-2007/'
    ],
//    [
//        'url' => $host . '/ajax/?action=chart_data&cpotv=&tq=select_*_from_diagram_where_diagram__id=7_and_vehicle__id=7336_order_by_rpm_asc&tqx=reqId%3A0'
//    ],
    [
        'url' => $host . '/ajax/?action=set_vehicle&t=_sndb1',
        'get' => [
//            'action' => 'set_vehicle',
//            't' => '_sndb1'
        ],
        'post' => [
            'm_tag' => '_sndb1',
            'm_driver_weight' => '176.40',
            'm_gewicht' => '396.90',
            'm_max_hp_rpm' => '69.1@9000',
            'm_max_nm_rpm' => '46.84@7400',
            'm_engine_sprocket_t' => '34',
            'm_input_sprocket_t' => '71',
            'm_final_t1' => '15',
            'm_final_t2' => '45',
            'm_bandmaat_aangedr_wiel' => '160',
            'm_band_aspect_ratio' => '60',
            'm_wielmaat_aangedr_wiel' => '17',
            'm_cx' => '0.5',
            'm_frontal_area' => '5.38',
            'uid' => '7336',
            'session_tag' => '_sndb1',
        ]
    ],
    [
            'url' => 'http://man/wp-admin/admin-ajax.php/?action=imperial'
    ],
    [
        'url' => $host . '/ajax/?action=chart_data&cpotv=&tq=select_*_from_diagram_where_diagram__id=7_and_vehicle__id=7336_order_by_rpm_asc&tqx=reqId%3A0'
    ]
//    [
//        'url' => $host . '/ajax',
//        'get' => [
//            'action' => 'set_vehicle',
//            't' => '_sndb1'
//        ],
//        'post' => [
//            'tag' => '_sndb1',
//            'driver_weight' => '176.40',
//            'gewicht' => '396.90',
//            'max_hp_rpm' => '69.1@9000',
//            'max_nm_rpm' => '46.84@7400',
//            'engine_sprocket_t' => '34',
//            'input_sprocket_t' => '71',
//            'final_t1' => '15',
//            'final_t2' => '45',
//            'use_g1' => '1',
//            'use_g2' => '2',
//            'use_g3' => '3',
//            'use_g4' => '4',
//            'use_g5' => '5',
//            'use_g6' => '6',
//            'bandmaat_aangedr_wiel' => '160',
//            'band_aspect_ratio' => '60',
//            'wielmaat_aangedr_wiel' => '17',
//            'cx' => '0.5',
//            'frontal_area' => '5.38',
//            'uid' => '7336',
//        ]
//    ]

];

$httpRequest = new HttpRequest();
foreach ($requests as $request) {
   echo $httpRequest->run($request);
}