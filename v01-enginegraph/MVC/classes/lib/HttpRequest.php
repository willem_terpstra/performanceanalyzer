<?php

/**
 * Created by PhpStorm.
 * User: Gebruiker
 * Date: 05/08/2018
 * Time: 16:33
 */

Class HttpRequest
{

    private function get($url, $get) {
        $ch = curl_init();

        $params = '';
        if (!empty($get))
        {
            $params = '?' . http_build_query($get);
        }
        echo $url . $params . "\n";
        curl_setopt($ch, CURLOPT_URL, $url . $params);
        curl_setopt($ch, CURLOPT_POST, 0);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close($ch);
        return $server_output;

    }

    private function post(string $url, array $get, array $post)
    {
        $ch = curl_init();

        $params = '';
        if (!empty($get))
        {
            $params = '?' . http_build_query($get);
        }
        echo $url . $params . "\n";

        curl_setopt($ch, CURLOPT_URL, $url . $params);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            $post
        );

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close($ch);
        return $server_output;
    }

    public function run(array $args) {
        if(count($args) === 1) {
            return $this->get($args['url'], []);
        }
        if(count($args) === 2) {
            return $this->get($args['url'], $args['get']);
        }

        return $this->post($args['url'], $args['get'], $args['post']);
    }
}
