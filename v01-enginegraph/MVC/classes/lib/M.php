<?php

class M
{

    public static function sums($results)
    {
        $f = 0;
        $a = [];
        if (empty($results[0])) {
            return NULL;
        }
        foreach ($results[0] as $r) {
            $f += $r;
            $a[] = $f;
        }
        return $a;
    }

    public static function total($results)
    {
        $ret = array_sum($results[0]);
        return $ret;
    }

    public static function firstValue($results)
    {
        $a = array_values(array_filter($results[0], function($v ) {
            return !empty($v);
        }));
        if (!empty($a)) {
            return $a[0];
        }
        return NULL;
    }

    public static function rpmFromZero($results)
    {
        $rpmInterval = DiagramController::getSelectedVehicle()->getDynoIntervalRpm();
        if (empty($rpmInterval)) {
            throw new Exception(Strings::show('rpm_interval_empty'));
        }
        $maxRpm = DiagramController::getSelectedVehicle()->getMaxRpm();
        $a = [];
        $b = array_flip($results[0]);
        for ($i = 0; !isset($b[$i]); $i += $rpmInterval) {
            if ($i > $maxRpm) {
                break;
            }
            $a[] = $i;
        }
        $results = array_merge($a, $results[0]);
        return $results;
    }

    public static function nmFromZero($results)
    {
        $rpmInterval = DiagramController::getSelectedVehicle()->getDynoIntervalRpm();
        $maxRpm = DiagramController::getSelectedVehicle()->getMaxRpm();
        $rpm = $results[0];
        $nm = $results[1];
        $a = [];
//		$b = array_flip($results[0]);
        for ($i = 0; !in_array($i, $rpm); $i += $rpmInterval) {
            if ($i > $maxRpm) {
                break;
            }
            $a[] = 0.0;
        }
        $results = array_merge($a, $nm);
        return $results;
    }

//	static $totalWeight = 0;
//	
//	
//	public static function ri_disc_oi($mass, $or, $ir) {
//		$ri = 0.5 * $mass * (pow($or, 2) + pow($ir, 2));
//		return $ri;
//	}
//
//	public static function calculateFlywheel($outerDiameter, $innerDiameter, $mass, $vLow, $vHigh, $radSecStart, $radSecEnd) {
//		$cdri = self::ri_disc_oi($mass, $outerDiameter / 2, $innerDiameter / 2);
//		$del_e1 = self::del_e($mass, $cdri, $vLow, $vHigh, $radSecStart, $radSecEnd, $re );
//		$sum_e = 0;
//		$sum_e = $sum_e + $del_e1;
//		$cemass1 = 2 * $sum_e / ($vHigh*$vHigh);
//		return $cemass1;
//		
//	}
//
//	/**
//	 * computes constant equivalent mass for flywheel calculator given
//	 * 
//	 * @param type $m static mass
//	 * @param type $i rotational inertia
//	 * @param type $vl velocity low
//	 * @param type $vh velocity high
//	 * @param type $wl omega low starting rad/s
//	 * @param type $wh omega high end rad/s
//	 * @param type $re recovery fraction (1 = full recovery, 0 = no recovery)
//	 * @return type float
//	 */
//	public static function del_e($m, $i, $vl, $vh, $wl, $wh, $re) {
//		$del_e = 0.5 * $m * ($vh * $vh - $vl * $vl) + 0.5 * $i * ($wh * $wh - $wl * $wl) * (1 - $re);
//		return $del_e;
//	}
}

?>
