<?php

class CubicSpline
{

    const EQALXES = -2;  /* two x-coordinates are equal */

    static $klo = -1;
    static $khi = -1;

    /*
      spline constructs a cubic spline given a set of x and y values, through
      these values.

     */

    static function spline($x, $y, $n, $yp1, $ypn, &$y2)
    {
        $u = [];
        if ($yp1 > 0.99e30) {
            $y2[0] = $u[0] = 0.0;
        } else {
            $y2[0] = -0.5;
            $u[0] = (3.0 / ($x[1] - $x[0])) * (($y[1] - $y[0]) / ($x[1] - $x[0]) - $yp1);
        }
        for ($i = 1; $i <= $n - 2; $i++) {
            $sig = ($x[$i] - $x[$i - 1]) / ($x[$i + 1] - $x[$i - 1]);
            $p = $sig * $y2[$i - 1] + 2.0;
            $y2[$i] = ($sig - 1.0) / $p;
            $u[$i] = ($y[$i + 1] - $y[$i]) / ($x[$i + 1] - $x[$i]) - ($y[$i] - $y[$i - 1]) / ($x[$i] - $x[$i - 1]);
            $u[$i] = (6.0 * $u[$i] / ($x[$i + 1] - $x[$i - 1]) - $sig * $u[$i - 1]) / $p;
        }
        if ($ypn > 0.99e30) {
            $qn = $un = 0.0;
        } else {
            $qn = 0.5;
            $un = (3.0 / ($x[$n - 1] - $x[$n - 2])) * ($ypn - ($y[$n - 1] - $y[$n - 2]) / ($x[$n - 1] - $x[$n - 2]));
        }
        $y2[$n - 1] = ($un - $qn * $u[$n - 2]) / ($qn * $y2[$n - 2] + 1.0);
        for ($k = $n - 2; $k >= 0; $k--) {
            $y2[$k] = $y2[$k] * $y2[$k + 1] + $u[$k];
        }
    }

    /*
      splint uses the cubic spline generated with spline to interpolate values
      in the XY  table.

     */

    static function splint($xa, $ya, $y2a, $n, $x, &$y)
    {
        $r = 0;
        if (self::$klo < 0) {
            self::$klo = 0;
            self::$khi = $n - 1;
        } else {
            if ($x < $xa[self::$klo]) {
                self::$klo = 0;
            }
            if ($x > $xa[self::$khi]) {
                self::$khi = $n - 1;
            }
        }
        while (self::$khi - self::$klo > 1) {
            $k = (self::$khi + self::$klo) >> 1;
            if ($xa[$k] > $x) {
                self::$khi = $k;
            } else {
                self::$klo = $k;
            }
        }
        $h = $xa[self::$khi] - $xa[self::$klo];
        if ($h == 0.0) {
            $y = NULL;
            $r = self::EQALXES;
        } else {
            $a = ($xa[self::$khi] - $x) / $h;
            $b = ($x - $xa[self::$klo]) / $h;
            $y = $a * $ya[self::$klo] + $b * $ya[self::$khi] + (($a * $a * $a - $a) * $y2a[self::$klo] +
                    ($b * $b * $b - $b) * $y2a[self::$khi]) * ($h * $h) / 6.0;
        }
        return ($r);
    }

    /*

      #>            spline1.dc2

      Function:     spline1

      Purpose:      1D cubic spline interpolation.

      Category:     MATH

      File:         spline.c

      Author:       P.R. Roelfsema

      Use:          INTEGER SPLINE1( XI   ,               Input   real( > NIN )
      YI   ,               Input   real( > NIN )
      NIN  ,               Input   integer
      XO   ,               Input   real( > NOUT )
      YI   ,               Output  real( > NOUT )
      NOUT ,               Output  integer

      SPLINE1 returns error codes:
      >0 - number of undefined values in YI.
      0 - no problems.
      -1 - not enough memory to create internal tables.
      -2 - the input array has two equal x-coordinates or
      value in XO outside range of input coordinates >
      interpolation problems.
      XI      Array containing input x-coordinates.
      YI      Array containing input y-coordinates.
      NIN     Number of (XI,YI) coordinate pairs.
      XO      Array containing x-coordinates for which y-coordinates
      are to be interpolated.
      YO      Array containing interpolated y-coordinates.
      If ALL YI are undefined, ALL YO are set to undefined.
      NOUT    Number of x-coordinates for which interpolation
      is wanted.

      Description:

      The interpolation is based on the cubic spline interpolation
      routines described in "Numerical recipes in C". For the interpolation
      data points in YI wich are undefined are skipped. Thus if undefined
      values are present, this routine will interpolate across them.
      XI data must be in increasing order. The number of undefined values
      in YI is returned.

      Updates:      Jan 29, 1991: PRR, Creation date
      Aug  7, 1991: MV,  Reset of 'klo' in 'spline1_c'
      Documentation about XI.
      Aug  9, 1993: PRR, Set YO to undefined when all YI undefined.
      #<

      Fortran to C interface:

      @ integer function spline1( real , real , integer , real , real , integer )

     */


    public static function spline1($xi, $yi, $nin, $xo, &$yo, $nout)
    {
#   float yp1 , ypn , *y2 , *xii , *yii , blank , alpha ;
#   fint  error , n , m , nblank , niin ;
//   setfblank_c( &blank ) ;
        $nblank = 0;
        $blank = NULL;
        for ($n = 0; $n < $nin; $n++)
            if ($yi[$n] == $blank)
                $nblank++;

        $niin = $nin - $nblank;
        if ($nblank == 0) {
            $xii = $xi;
            $yii = $yi;
        } else if ($nblank == $nin) {
            for ($n = 0; $n < $nout; $n++)
                $yo[$n] = $blank;
            return ($nblank);
        } else {
            $xii = [];
            $yii = [];
//      if ( !xii || !yii ) return( NOMEMORY ) ;
            for ($n = 0, $m = 0; ($n < $nin) && ($m < $niin); $n++) {
                if ($yi[$n] != $blank) {
                    $xii[$m] = $xi[$n];
                    $yii[$m] = $yi[$n];
                    $m += 1;
                }
            }
        }

        $y2 = [];

        $alpha = 1.0;
        $yp1 = 3e30 * $alpha;
        $ypn = 3e30 * $alpha;

        self::spline($xii, $yii, $niin, $yp1, $ypn, $y2);

        self::$klo = -1; /* Reset 'klo' before table interpolation */
        for ($n = 0; $n < $nout; $n++) {
            self::splint($xii, $yii, $y2, $niin, $xo[$n], $yo[$n]);
        }

        return ($nblank);
    }

    public function interpolate($xIn, $interval)
    {
        ksort($xIn, SORT_NUMERIC);
        $xIn = array_filter($xIn, function($i) {
            return !empty($i);
        });
        $xi = array_keys($xIn);
        $yi = array_values($xIn);

        $nin = count($xi);
        $start = $xi[0];
        $end = $xi[count($xi) - 1];


        $xo = [];
        for ($i = $start; $i <= $end; $i += $interval) {
            $xo[] = $i;
        }
        $yo = [];
        $nout = count($xo);

        self::spline1($xi, $yi, $nin, $xo, $yo, $nout);

        $rows = [];
        for ($i = 0; $i < $nout; $i++) {
            $rows[] = [
                'x' => $xo[$i],
                'y' => $yo[$i]
            ];
        }
        return $rows;
    }

}

//$yo = array();
//CubicSpline::spline1(
//		array (1000, 6000, 8000),
//		array( 4, 60, 80), 
//		3, 
//		array( 2000, 3000, 4000, 5000, 7000),
//		$yo, 
//		5);
//var_export($yo);
?>
