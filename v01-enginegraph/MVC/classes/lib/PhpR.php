<?php

class PhpR
{

    static $cache = [];

    public static function run($formula, $debugFlag = false, $rInstance = NULL)
    {

        if (Diagram::CACHE) {
            $crc = crc32($formula);
            if (isset(self::$cache[$crc])) {
                return self::$cache[$crc];
            }
        }
        // ^ => pow function
        if (strpos($formula, '^') !== false) {
            $formula = preg_replace('/([\d|.]+)\s*\^\s*([\d|.]+)/', 'pow($1,$2)', $formula);
        }
        $result = [];
        // arrays
        preg_match_all('/\[[\s|\-+[\d|.|N|U|L]+\]/', $formula, $result, PREG_PATTERN_ORDER);
        $resultArray = [];
        $values = [];
        $summations = [];
        $f = NULL;
        $i = 0;

        foreach ($result[0] as $r) {
            $summations[] = $r;
            $res = str_replace(['[', ']'], '', $r);
            $res = preg_replace('/\s+/', ' ', $res);
            $res = trim($res);
            $res = explode(' ', $res);
            $values[] = $res;
        }
        $numOfElements = 0;
        if (isset($values[0])) {
            $numOfElements = count($values[0]);
        }
        $functionRules = [];
        for ($i = 0; $i < $numOfElements; $i++) {
            if (empty($f)) {
                $f = $formula;
            }
            foreach ($summations as $index => $summation) {
                if (strpos($formula, $summation) !== false) {
                    if (isset($values[$index][$i])) {
                        $f = str_replace($summation, $values[$index][$i], $f);
                    } else {
                        continue 2;
                    }
                }
            }
            // ^ => pow
            if (strpos($f, '^') !== false) {
                $f = preg_replace('/([\d|.]+)\s*\^\s*([\d|.]+)/', 'pow($1,$2)', $f);
            }
            if (strpos($f, '[') !== false || empty($f)) {
                throw new RException('Error: parsing ' . empty($formula) ? ' empty string' : $formula, RException::PARSE_FORMULA_FAILED);
            }
            $functionRules[] = '$ret[] = ' . $f . ';';
            $f = NULL;
        }
        if (!empty($functionRules)) {
            $resultArray = createFunction('', '$ret = array(); ' . implode(' ', $functionRules) . ' return $ret;' );
//            $s = 'function func() { $ret = array(); ' . implode(' ', $functionRules) . ' return $ret; }';
//            eval ($s);
//            $resultArray = func();
        }
        if (empty($resultArray)) {
            // simpele formule
            // ^ => pow
            if (strpos($formula, '^') !== false) {
                $formula = preg_replace('/([\d|.]+)\s*\^\s*([\d|.]+)/', 'pow($1,$2)', $formula);
            }
            if (strpos($formula, '[') !== false || empty($formula)) {
                throw new RException('Error: parsing ' . empty($formula) ? ' empty string' : $formula, RException::PARSE_FORMULA_FAILED);
            }
//            $f = create_function('', 'return ' . $formula . ';');
            $value = createFunction([],'return ' . $formula . ';' );
            $resultArray[] = $value;
//            if (is_string($f)) {
//                $value = $f();
//                $resultArray[] = $value;
//            } else {
//                throw new RException('invalid function: ' . $formula, RException::PARSE_FORMULA_FAILED);
//            }
        }
        if (Diagram::CACHE) {
            self::$cache[$crc] = $resultArray;
        }

        if ($debugFlag && !empty($rInstance)) {
            Klog::LogInfo("name: {$rInstance->getName()}");
            Klog::LogInfo("content: {$rInstance->getContent()}");
            Klog::LogInfo("formula: " . self::formatFormula($formula));
            Klog::LogInfo("result: " . implode("\n", $resultArray));
        }
        return $resultArray;
    }

    private static function formatFormula($formula)
    {
        foreach (['?', ':', '<', '>', '+', '-', '*', '/', '&&', '||'] as $c) {
            $formula = str_replace($c, $c . "\n", $formula);
        }
        return $formula;
    }

    static function lop($value, $max = 10)
    {
        if ($value > $max) {
            return $max + (rand(0, 50) / 100) - 0.0675;
        }
        return $value;
    }

}

?>