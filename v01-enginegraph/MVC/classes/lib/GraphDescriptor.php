<?php

class GraphDescriptor
{

    private $messages = [];
    private $y_unit;
    private $x_unit;
    private $y_unit_name;
    private $x_unit_name;
    private $lineColor;
    private $data;
    private $misc_data;
    private $legend;
    private $title;
    private $rName;
    private $description;

    public function getData()
    {
        return $this->data;
    }

    public function getMiscData()
    {
        return $this->misc_data;
    }

    public function getDataY()
    {
        return $this->data;
    }

    public function getDataX()
    {
        return array_keys($this->data);
    }

    /**
     * @deprecated use getYUnit
     * @return type string
     */
    public function getUnit()
    {
        return $this->y_unit;
    }

    public function getYUnit()
    {
        return $this->y_unit;
    }

    public function getYUnitName()
    {
        return $this->y_unit_name;
    }

    public function setYUnitName($yUnitName)
    {
        $this->y_unit_name = $yUnitName;
    }

    public function getXUnitName()
    {
        return $this->x_unit_name;
    }

    public function setXUnitName($xUnitName)
    {
        $this->x_unit_name = $xUnitName;
    }

    public function getXUnit()
    {
        return $this->x_unit;
    }

    public function setMessages($messages)
    {
        if (!is_array($messages)) {
            $messages = (array)$messages;
        }
        $this->messages = array_merge($this->messages, $messages);
    }

    public function getMessages()
    {
        return $this->messages;
    }

    public function getLineColor()
    {
        return $this->lineColor;
    }

    public function getLegend()
    {
        return $this->legend;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function setMiscData($data)
    {
        $this->misc_data = $data;
    }

    public function setYUnit($unit)
    {
        $this->y_unit = $unit;
    }

    public function setXUnit($unit)
    {
        $this->x_unit = $unit;
    }

    public function setLineColor($lineColor)
    {
        $this->lineColor = $lineColor;
    }

    public function setLegend($legend)
    {
        $this->legend = $legend;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function setRName($rName)
    {
        $this->rName = $rName;
    }

    public function getRName()
    {
        return $this->rName;
    }

    public function setDescription($desc)
    {
        $this->description = $desc;
    }

    public function getDescription()
    {
        return $this->description;
    }

    /**
     * linear interpolation
     * @param null $yUnit
     */
    public function lerp($yUnit = NULL)
    {
        if (!empty($yUnit)) {
            $this->y_unit_name = $yUnit;
        }
        $newMisc = [];
        $keys = array_keys($this->data);
        foreach ($this->misc_data as $unit => $values) {
            if (is_array($values) && (count($values) == count($keys))) {
                if ($unit != $this->y_unit_name) {
                    $newMisc[$unit] = array_combine($keys, $values);
                }
            }
        }
        $this->misc_data = $newMisc;

        $newData = [];
        $newMisc = [];
        foreach ($this->data as $key => $value) {
            $xHigh = $key;
            $yHigh = $value;
            $x = floor($xHigh);
            if (!isset($xLow)) {
                $xLow = $xHigh;
                $yLow = $yHigh;
                continue;
            }
            $xd = $xHigh - $xLow;
            $yd = $yHigh - $yLow;
            $d = $x - $xLow;
            if ($d <= 0) {
                continue;
            }
            $r = $d / $xd;
            $y = $yLow + ($yd * $r);
            $newData[(string)$x] = (string)$y;
            if (!empty($this->misc_data)) {
                foreach ($this->misc_data as $name => $misc) {
                    if (count($misc) == count($this->data)) {
                        if (!isset($newMisc[$name])) {
                            $newMisc[$name] = [];
                        }
                        $miscYLow = $misc[$xLow];
                        $miscYHigh = $misc[$xHigh];
                        $yd = $miscYHigh - $miscYLow;
                        $interpolatedMiscY = $miscYLow + ($yd * $r);
                        $newMisc[$name][(string)$x] = $interpolatedMiscY;
                    }
                }
            }
            $xLow = $xHigh;
            $yLow = $yHigh;
        }
        $this->data = $newData;
        $this->misc_data = $newMisc;
        return;
    }

}

?>
