<?php

class Controller
{
    // Possible commands
    const ADD = "ADD";
    const UPDATE = "UPDATE";
    const SAVENEW = "SAVENEW";
    const COPY = "COPY";
    const EDITLIJST = "EDITLIJST";
    const BROWSELIJST = "BROWSELIJST";
    const SELF_REFERRING_LIST = "SELF_REFERRING_LIST";
    const BROWSE = "BROWSE";
    const DETAIL = "DETAIL";
    const REMOVE = "REMOVE";
    const LOAD = "LOAD";
    const CONFIRM = "CONFIRM";
    const THANKS = "THANKS";
    const SEARCH = "SEARCH";
    const CANCEL = "CANCEL";
    const IMAGE = "IMAGE";
    const COMPARE = "COMPARE";
    const CALCULATE = 'CALCULATE';

    const LOGIN = "LOGIN";
    const LOGOUT = "LOGOUT";

    private $atomicCommands = [self::COPY, self::REMOVE, self::THANKS];
    protected $lastAction = NULL;
    protected $view = NULL;
    public static $rawCommand;

    public function __construct()
    {
    }

    public function run($view, $uid, $action)
    {
        $post = Post::$postVars;
        $this->lastAction = $action;

        if (is_null($view)) {
            throw new ControllerException ("Cannot run controller without view", ControllerException::NO_VIEW);
        }

        if (is_null($uid)) {
            // POST
            if (isset($post['uid'])) {
                if (is_numeric($post['uid'])) {
                    $uid = (int)$post['uid'];
                }
            }
        }

        if (array_key_exists('act', $post)) {
            switch ($post['act']) {
            case self::UPDATE :
            case self::REMOVE:
                $action = $post['act'];
                break;
            }
        }


        if (in_array($action, $this->atomicCommands)) {
            if (empty($uid)) {
                throw new Exception('uid mag niet leeg zijn voor de volgende commando\'s: ' . var_export($this->atomicCommands, true));
            }
        }
        $action = strtoupper($action);
        if (is_string($view)) {
            $checkUser = false;

            $view = new $view($uid);
        }


        $this->view = $view;
        try {
            $r = $this->checkAuthorisation($action, $view);
            if ($r !== true) {
                return $r;
            }
        } catch (SecurityException $e) {
            return $e->getMessage();
        }
        switch (strtoupper($action)) {
        case self::EDITLIJST:
            $content = $view->lijst();
            break;
        case self::BROWSELIJST:
            $content = $view->browseLijst();
            break;
        case self::ADD:
            if (method_exists($view, 'add')) {
                $content = $view->add();
            } else {
                $content = $view->detail($uid);
            }
            break;
        case self::DETAIL:
            $content = $view->detail($uid);
            break;
        case self::BROWSE:
            $content = $view->browseDetail($uid);
            break;
        case self::SAVENEW:
        case self::UPDATE:
            $validated = $view->validate($post);
            if (Controller::isJson()) {
                if ($validated) {
                    Controller::toJson($view->save($uid));
                } else {
                    $errorDiv = $view->getErrorDivId();
                    Controller::toJson(['update' =>
                            [$errorDiv => implode('<br/>', $view->getMessagesAsStrings())]
                        ]
                    );
                }
                break;
            } else {
                if ($validated) {
                    $content = $view->save($uid);
                } else {
                    $content = $view->detail($uid);
                }
            }
            break;
        case self::REMOVE    :
            $content = $view->remove($uid);
            break;
        case self::CANCEL:

            $content = $view->quit();
            break;
        case self::COPY        :
            $content = $view->copy($uid);
            break;
        case self::THANKS    :
            $content = $view->showThankPage();
            break;
        case self::CONFIRM:
            $content = $view->confirm();
            break;
        case self::SELF_REFERRING_LIST :
            $content = $view->selfReferringList();
            break;
        case self::COMPARE:
            $content = $view->compare();
            break;
        case self::LOGOUT:
            $content = $view->logOut();
            break;
        default:
            throw new ControllerException ("Cannot run controller with action: [$action]", ControllerException::ACTION_NOT_FOUND);
        }
        if (!is_string($content) && $content !== false) {
            return Strings::show('requested_page_not_found');
        }

        return $content;

    }

    protected function checkAuthorisation($action, $view)
    {
        if (!Authorization::ActionAllowed($action, $view)) {
            $t = new TemplateEngine('MVC/html/custom/security_error.html');
            return $t->execTemplate();
        }
        return true;
    }

    protected function save($view, $post)
    {
        $validated = $view->validate($post);
        if ($validated) {
            $content = $view->save($uid);
        } else {
            $content = $view->detail($uid);
        }
        return $content;
    }

    public static function isJson()
    {
        if (isset(Get::$getVars['o'])) {
            if (Get::$getVars['o'] == 'json') {
                return true;
            }
        }
        return false;
    }

    public static function toJson($data)
    {
        $output = json_encode($data);
        echo $output;
        exit;
    }

}

?>