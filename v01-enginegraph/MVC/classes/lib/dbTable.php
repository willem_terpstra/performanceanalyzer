<?php

abstract class dbTable implements Iterator, Countable, ArrayAccess
{

    const EXACT_MATCH = 1;
    const NON_EXACT_MATCH = 2;
    const DATE_MATCH = 3;
    const CHECK_USER = true;
    const IGNORE_NON_EXISTING_PROPERTIES = 4;
    const ERROR_NON_EXISTING_PROPERTIES = 5;
    const LOAD_FROM_DATABASE = 6;
    const LOAD_FROM_EXTERN = 7;

    protected $errNo;
    private $errMsg;
    protected $pk = null;
    protected $fk = null;
    protected $hasFks = null;
    private $messages = [];
    protected static $meta = [];
    private $last_query = null;
    protected $is_view = false;
    static $instance_id;
    // iterator
    private $record;
    private $collection;
    private $pointer = 0;
    protected $numRows = 0;

    static protected $conn;

//	abstract protected function getModifiedMap();
    abstract protected function getTableName();

    // flags

    const CAMELCASE = 1;
    // errors
    const NO_DEFAULT_VALUE = 1364;

    public function __construct($str = null)
    {
        self::$instance_id++;
        if (!is_null($str) && trim($str) != '') {
            if (ctype_digit($str)) {
                $this->load($str);
            }
        }
        // language
        $this->loadMeta();

    }

    public static function setConnection($conn)
    {
        self::$conn = $conn;
    }

    private function loadMeta()
    {
        // load meta data
        if (!array_key_exists($this->tableName, self::$meta)) {
            $sql = "SELECT * FROM meta_fields_settings WHERE table_name = ?";
            $rs = $this->execSql($sql, [$this->tableName]);
            if ($this->numRows > 0) {
                while ($row = mysqli_fetch_assoc($rs)) {
                    self::$meta[$row['table_name']][$row['field_name']][$row['setting']] = $row['value'];
                    self::$meta[$row['table_name']][$row['field_name']]['validation'] = $row['validation'];
                }
            } else {
                self::$meta[$this->tableName] = NULL;
            }
        }
    }

    public function setMessage($m)
    {
        if ($m instanceof Message) {
            $this->messages[] = $m;
        } else {
            $this->messages[] = new Message($m, Message::ERROR, __CLASS__);
        }
    }

    public function setMessages($msg, $severity = Message::ERROR, $className = __CLASS__)
    {
        if (is_array($msg)) {
            foreach ($msg as $m) {
                if ($m instanceof Message) {
                    $this->messages[] = $m;
                } else {
                    $this->messages[] = new Message($m, Message::ERROR, $className);
                }
            }
        } else {
            if ($msg instanceof Message) {
                $this->messages[] = $msg;
            } else {
                $this->messages[] = new Message($msg, $severity, $className);
            }
        }
    }

    public function getMessages($unique = true)
    {
        if (!$unique) {
            return $this->messages;
        }
        $messages = [];
        $a = [];
        foreach ($this->messages as $m) {
            $s = $m->getMessage();
            if (!in_array($m->getMessage(), $a)) {
                $messages[] = $m;
                $a[] = $m->getMessage();
            }
        }
        return $messages;
    }

    public function getHighestErrorLevel()
    {
        $level = Message::MESSAGE_LVL;
        foreach ($this->messages as $m) {
            if ($m->getErrorLevel() > $level) {
                $level = $m->getErrorLevel();
            }
        }
        return $level;
    }

    public function clearMessages()
    {
        $this->messages = [];
    }

    public function getErrNo()
    {
        $errNo = $this->errNo;
        $this->errNo = 0;
        return $errNo;
    }

    public function compare(dbTable $compareTo, $compareFields)
    {
        $result = [];
        foreach ($compareFields as $compareField => $compareValue) {
            $func = 'get' . camelCase($compareField);
            $b = $compareTo->$func();
            $a = $this->$func();
            if($a instanceof self && $b instanceof self ) {
                $result = array_merge($a->compare($b, $compareValue));
                continue;
            }
            if (gettype($a) == 'double' && gettype($b) == 'double') {
                $a = round($a, 2);
                $b = round($b, 2);
            }

            if ($a != $b) {
                if ($unit = (string) $this->getFieldUnit($compareField)) {
                    Unit::convert($unit, $a);
                    Unit::convert($unit, $b);
                    $unit = ' ' . $unit;
                }
                $result[$compareField] = [Strings::show($compareField), $a . $unit, '=>', $b . $unit];
            }
        }
        return $result;
    }

    private function isDate($field)
    {
        if (!isset($this->fieldsInfo[$field])) {
            return false;
        }
        return $this->fieldsInfo[$field]['Type'] == 'date';
    }

    private function isText($field)
    {
        if (!isset($this->fieldsInfo[$field])) {
            return false;
        }
        return $this->fieldsInfo[$field]['Type'] == 'text';
    }

    private function isInt($field)
    {
        if (!isset($this->fieldsInfo[$field])) {
            return false;
        }
        if (strpos($this->fieldsInfo[$field]['Type'], 'int') !== false) {
            return true;
        }
        return false;
    }

    public function getFieldUnit($field): ?string
    {
        if (isset($this->fieldsInfo[$field]['Extra']['unit'])) {
            return $this->fieldsInfo[$field]['Extra']['unit'];
        }
        return null;
    }

    private function dateNlToMysql($date)
    {
        $date = str_replace(['/', ' '], '-', $date);
        $date = explode('-', $date);
        $date = $date[2] . '-' . $date[1] . '-' . $date[0];
        return $date;
    }

    public function clear()
    {
        foreach ($this->getModifiedPropertys() as $prop) {
            $this->$prop = NULL;
        }
    }

    public function fill(&$row, $ignoreNonExistingProperies = self::ERROR_NON_EXISTING_PROPERTIES, $loadSource = self::LOAD_FROM_EXTERN)
    {
        foreach ($row as $field => $value) {
            if ($this->isDate($field) && !empty($value)) {
                $this->$field = date('d-m-Y', strtotime($value));
            } else {
                try {
                    if(is_array($value)) {
                        if (property_exists($this, $field)) {
                            $func = 'get' . ucfirst(self::cc2u($field));
                            $child = $this->$func();
                            if ($child instanceof dbTable) {
                                foreach($value as $childField => $childValue)
                                {
                                    $child->$childField = $childValue;
                                    $child->dbModified[$childField] = true;
                                }
                                continue;
                            }
                        }
                    }
                    else
                    {
                        $this->$field = $value;
                    }
//					$b = (strpos($field, '_2_') !== false);
                    if (strpos($field, '_2_') !== false) {
                        $a = explode('_2_', $field);
                        $className = $this->camelCase(str_replace('_id', '', $a[0]));
                        $instance = new $className($value);
                        $f = 'get' . $this->camelCase($a[1]);
                        $value = $instance->$f();
                        unset($row[$field]);
                        $field = $a[1];
                        $row[$field] = $value;
                    }
                    if ($loadSource == self::LOAD_FROM_EXTERN) {
                        if ($this->hasProperty($field)) {
                            $this->dbModified[$field] = true;
                        }
                    }
                } catch (dbTableException $e) {
                    if ($e->getCode() == dbTableException::PROPERTY_DOES_NOT_EXIST) {
                        if ($ignoreNonExistingProperies == self::IGNORE_NON_EXISTING_PROPERTIES) {
                            continue;
                        }
                        // try set function
                        $func = 'set' . $this->camelCase($field);
                        $this->$func($value);
                    } else {
                        throw new dbTableException($e->getMessage(), $e->getCode());
                    }
                }
            }
        }
    }

    public function getModifiedMap()
    {
        return $this->dbModified;
    }

    public static function asArray($lijst)
    {
        $arr = [];
        foreach ($lijst as $l) {
            $arr[] = $l;
        }
        return $arr;
    }

    public function setField($field, $value)
    {
        $this->$field = $value;
    }

    /**
     * @param $name
     * @param $value
     * @return mixed
     * @throws dbTableException
     */
    public function __set($name, $value)
    {
        throw new dbTableException('cannot set property ' . $name . ', does not exist in: ' . $this->getTableName(), dbTableException::PROPERTY_DOES_NOT_EXIST);
    }

    /**
     * @param $name
     * @return mixed
     * @throws Exception
     */
    public function __get($name)
    {
        throw new Exception('cannot get property ' . $name . ', does not exist in: ' . $this->getTableName());
    }

    public function getCollectResult(): string
    {

        return $this->collect_result ?? '';
    }

    /**
     * @param $name
     * @param $args
     * @return $this|mixed|null
     * @throws dbTableException
     */
    public function __call($name, $args)
    {
        if (strpos($name, 'set') === 0) {
            $function = 'set';
            $property = substr($name, 3);
        }
        if (strpos($name, 'get') === 0) {
            $function = 'get';
            $property = substr($name, 3);
        }
        if (strpos($name, 'loadBy') === 0) {
            $function = 'loadBy';
            $property = substr($name, 6);
        }
        if (strpos($property, '@') !== false) {
            throw new dbTableException('Property: ' . $property . ' does not exist', dbTableException::PROPERTY_DOES_NOT_EXIST);
        }
        if (strpos($property, '_') === false) {
            $property = cc2u($property);

        }
        if (property_exists($this, $property)) {

            switch ($function) {
            case 'get' :
                return $this->$property;
            case 'set':
                if ($args && $this->$property !== $args[0]) {
                    if (!array_key_exists($property, $this->dbModified)) {
                        Logger::write('Property ' . $name . ' not found in modifiedMap for table ' . $this->getTableName(), Logger::ERROR);
                        throw new Exception('PropertyNotFound');
                    }
//						Klog::LogInfo("Setting $property to ". $args[0]);
                    $this->dbModified[$property] = true;
                    $this->$property = $args[0];
                }
                return null;
            case 'loadBy':
                $property = cc2u(substr($name, 6));
                if (!$this->hasProperty($property)) {
                    throw new Exception('PropertyNotFound');
                }
                $ids = $args[0];
                if (!is_array($args[0])) {
                    $ids = explode(',', $args[0]);
                }
                $sql = 'SELECT * FROM ' . $this->getTableName() . ' WHERE ' . $property . ' IN (' . $this->createPlaceholders($ids) . ')';
                $l = $this->lijst($sql, $ids);
                if (count($l) == 1) {
                    return $this->first();
                } elseif (count($l) == 0) {
                    throw new dbTableException('RecordNotFound', dbTableException::LOAD_FAILED_EXCEPTION);
                } else {
                    return $l;
                }
            }
        }

        throw new dbTableException('Non existing method ' . $name . ' called for non existing property ' . $property . ' in table ' . $this->getTableName(), dbTableException::PROPERTY_DOES_NOT_EXIST);
    }

    function setData($row)
    {
        if (is_array($row)) {
            foreach ($row as $field => $data) {
                $this->$field = $data;
            }
        }
    }

    function setNumRows($rows)
    {
        $this->numRows = $rows;
    }

    function current(): mixed
    {
        $row = mysqli_fetch_array($this->collection, MYSQLI_ASSOC);
        $class = $this->getClassName();
        $r = new $class();
        $r->setData($row);
        $r->setNumRows($this->numRows);
        return $r;
    }

    function next(): void
    {
        ++$this->pointer;
        if ($this->valid()) {
            mysqli_data_seek($this->collection, $this->pointer);
        }
    }

    function first(): mixed
    {
        $this->rewind();
        return $this->current();
    }

    function key(): mixed
    {
        return null;
    }

    function valid(): bool
    {
        return ($this->pointer < $this->numRows);
    }

    function rewind(): void
    {
        if ($this->numRows > 0) {
            mysqli_data_seek($this->collection, 0);
        }
        $this->pointer = 0;
    }

    function offsetExists($offset): bool
    {
        if ($offset < 0) {
            return false;
        }

        if (!is_numeric($offset)) {
            return array_key_exists($offset, $this->dbModifiedMap);
        }
        return property_exists($this, $offset);
    }

    function offsetGet($offset): mixed
    {
        if (!is_numeric($offset)) {
            $func = 'get' . $this->camelCase($offset);
            return $this->$func();
        }
        return false;
    }

    function offsetSet($index, $newValue): void
    {
        if (!is_numeric($index)) {
            $func = 'set' . $this->camelCase($index);
            $this->$func($newValue);
        }
    }

    function offsetUnset($index): void
    {
        throw new Exception('This implementation of ArrayAccess cannot unset values');
    }

    public function saveOrUpdate()
    {
        $uid = $this->getUid();
        if (empty($uid)) {
            $this->save();
        } else {
            if (property_exists($this, 'users_id')) {
                // opnieuw ophalen anders kun je elk users_id posten
                $className = get_class($this);
                $instance = new $className($this->uid);
                if ($instance->getUsersId() && // no owner
                    $instance->getUsersId() !== Authorization::getCurrentUser()->getUid()
                ) {
                    throw new SecurityException('NotAllowed', SecurityException::WRONG_USER);
                }
                unset($instance);
            }
            $this->update();
        }
    }

    public function fillEmptyString()
    {
        $fieldList = $this->getModifiedMap();
        foreach ($fieldList as $field => $value) {
            $this->$field = '';
        }
    }

    private function prepareText($value)
    {
        $value = str_replace('\\r\\n', "\n", $value);
        $value = preg_replace('/(\\\\+)/', '', $value);
        $value = str_replace(['<p>', '</p>'], '', $value);
        return $value;
    }

    protected function save($autoIncrement = true)
    {
        $sql = "INSERT IGNORE INTO {$this->getTableName()} ";
        if (!empty($this->uid)) {
            throw new dbTableException('Record allready exists: ' . var_export($this, true), dbTableException::DUPLICATE_ENTRY);
        }

        $fieldNames = [];
        $values = [];
        $newUid = false;

        foreach ($this->getModifiedMap() as $field => $modified) {
            if ($field == $this->getPK() && empty($this->$field)) {
                if (!$autoIncrement) {
                    $newUid = $this->getMaxPk() + 1;
                    $this->$field = $newUid;
                } else { // use auto_increment
                    continue;
                }
            }

            if ($modified) {
                $value = $this->$field;
                if ($value && $this->isDate($field)) {
                    $value = $this->dateNlToMysql($value);
                }
                $fieldNames[] = $field;
                $values[] = $value;
            }
        }
        $fieldNames = implode(',', $fieldNames);
        $sql .= "($fieldNames) VALUES (" . $this->createPlaceholders($values) . ") ";
        $this->execSql($sql, $values);

        if ($newUid) {
            $this->setUid($newUid);
        } else {
            $this->setUid(mysqli_insert_id(self::$conn));
        }

        return;
    }


    protected function update()
    {
        $sql = "UPDATE {$this->getTableName()} SET ";

        $updates = $values = [];

        foreach ($this->getModifiedMap() as $field => $modified) {
            if ($modified) {
                $value = $this->$field;
                if ($this->isDate($field)) {
                    if ($value) {
                        $value = $this->dateNlToMysql($value);
                    }
                }

                $updates[] = "$field = ?";
                $values[] = $value;
            }
        }
        if (count($updates) == 0) {
            return;
        }
        $updates = implode(', ', $updates);

        $sql .= $updates;
        $sql .= " WHERE	{$this->getPK()}= ?";
        $values[] = $this->getUid();
        $this->execSql($sql, $values);
    }


    public function remove()
    {
        $this->execSql("DELETE FROM {$this->getTableName()} WHERE {$this->getPK()}='{$this->getUid()}'");
    }

    public function execSql(string $sql, array $args)
    {

        $stmt = mysqli_prepare(self::$conn, $sql);
        if (false === $stmt) {
            throw new dbTableException('Error in sql: ' . $sql, dbTableException::SQL_ERROR);

        }

        $params = [];
        $types = array_reduce($args, function ($string, $arg) use (&$params) {
            $params[] = $arg;

            if (is_float($arg)) {
                $string .= 'd';
            } elseif (is_int($arg)) {
                $string .= 'i';
            } elseif (is_string($arg)) {
                $string .= 's';
            } else {
                $string .= 'b';
            }

            return $string;
        }, '');

        $result = null;
        $bStmtExecuted = $stmt->execute($params);
        if ($bStmtExecuted) {
            $result = $stmt->get_result();
        }


        $this->last_query = $sql;
        $this->errNo = mysqli_errno(self::$conn);
        $this->errMsg = mysqli_error(self::$conn);

        if (null === $result) {
            throw new dbTableException('Failure executing sql', dbTableException::DUPLICATE_ENTRY);
        }

        if (is_resource($result)) {
            $this->numRows = $result->num_rows;
        } else {
            $this->numRows = $stmt->affected_rows;
        }

        $stmt->close();

        if ($this->errNo !== 0) {
            throw new dbTableException("{$this->errNo}: {$this->errMsg} <br/><pre>$sql</pre>", $this->errNo);
        }
        return $result;
    }

    /* load method */
    public function load($id, $checkUser = false)
    {
        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE " . $this->getPK() . "= ? LIMIT 1";
        $rs = $this->execSql($sql, [$id]);
        if ($this->numRows == 0) {
            throw new dbTableException("load $id failed: $sql", dbTableException::LOAD_FAILED_EXCEPTION);
        }
        $row = mysqli_fetch_assoc($rs);
        $this->fill($row, self::ERROR_NON_EXISTING_PROPERTIES, self::LOAD_FROM_DATABASE);
    }

    public function checkOwner()
    {
        $user = new VwUsers();
        $user = $user->loadByUseridMd($_SESSION['mdUserid']);
        return ($this->getUsersId() == $user->getUid());
    }

    /**
     * geeft instance terug waarvan de property $propertyName de waarde propertyValue heeft
     *
     * @param string $propertyName
     * @param string $propertyValue
     * @return false indien niet gevonden, anders true
     */
    public function loadByProperty($propertyName, $propertyValue)
    {
        $sql = "SELECT uid FROM {$this->getTableName()} WHERE $propertyName='$propertyValue'";

        $rs = $this->execSql($sql);
        if (mysqli_num_rows($rs) == 1) {
            $row = mysqli_fetch_assoc($rs);
            $uid = $row['uid'];
            $this->load($uid);
            return true;
        }
        return false;
    }

    protected function getPK()
    {
        if ($this->is_view) {
            return 'uid';
        }
        if (!empty($this->pk)) {
            return $this->pk;
        } else {
            throw new Exception("<strong>Primary key not defined for table (view must set is_view, viewname must start with vw_): " . $this->getTableName() . "</strong>");
        }
    }

    private function createPlaceholders(array $args)
    {
        return ltrim(str_repeat(', ?', count($args)), ',');
    }

    /**
     *
     * @param null $param
     * @param array $args
     * @return $this
     */
    public function lijst($param = null, array $args = [])
    {
        $this->collection = NULL;
        if (is_null($param)) {
            $sql = "SELECT * FROM " . $this->getTableName() . " ";
            $rs = $this->execSql($sql, []);
        } elseif (is_array($param)) {
            $sql = "SELECT * FROM " . $this->getTableName() . " WHERE " . $this->getPK() . " IN(" . $this->createPlaceholders($param) . ')';
            $rs = $this->execSql($sql, $param);
        } elseif (is_string($param)) {
            $rs = $this->execSql($param, $args);
        }
        $this->numRows = $rs->num_rows;
        $this->collection = $rs;

        return $this;
    }

    public function getLabel($field)
    {
        $lang = Languages::currIso();
        if (isset(self::$meta[$this->getTableName()][$field]['unit_group_id'])) {
            if (isset(self::$meta[$this->getTableName()][$field]['label_' . $lang])) {
                $s = self::$meta[$this->getTableName()][$field]['label_' . $lang];
            } else {
                $s = '';
            }
            $u = Unit::$units[$this->getUnitGroup($field)];
            return strtolower("$s ($u)");
        } elseif (isset(self::$meta[$this->getTableName()][$field]['label_' . $lang])) {
            return strtolower(self::$meta[$this->getTableName()][$field]['label_' . $lang]);
        } else {
            return strtolower(Strings::show($field));
        }
    }

    private function isHidden($field)
    {
        if (isset(self::$meta[$this->getTableName()][$field]['hidden'])) {
            return self::$meta[$this->getTableName()][$field]['hidden'];
        } else {
            return false;
        }
    }

    private function isObliged($field)
    {
        if (isset(self::$meta[$this->getTableName()])) {
            if (array_key_exists($field, self::$meta[$this->getTableName()])) {
                if (array_key_exists('obliged', self::$meta[$this->getTableName()][$field])) {
                    return self::$meta[$this->getTableName()][$field]['obliged'];
                }
            }
        }
        return false;
    }

    public function getChildren($orderBy = null)
    {
        $sqlOrderBy = '';
        if (!is_null($orderBy)) {
            $sqlOrderBy = " ORDER BY $orderBy";
        }
        $sql = 'SELECT * FROM ' . $this->getTableName() . ' WHERE ' . $this->getTableName() . '_id = ' . $this->getUid() . $sqlOrderBy;
        $l = $this->lijst($sql);
        $l = $this->asArray($l);
        foreach ($l as $instance) {
            $instance->getChildren();
        }
        return $l;
    }

    public function getHtmlType($field)
    {
        if ($this->isHidden($field)) {
            return 'hidden';
        } elseif (isset(self::$meta[$this->getTableName()][$field]['type'])) {
            return self::$meta[$this->getTableName()][$field]['type'];
        } else {
            return 'text';
        }
    }

    public function getUnitGroup($field)
    {
//$m = self::$meta;
        if (!isset(self::$meta[$this->getTableName()])) {
            $this->loadMeta();
        }
        if (isset(self::$meta[$this->getTableName()][$field]['unit_group_id'])) {
            $unitGroup = new UnitGroup(self::$meta[$this->getTableName()][$field]['unit_group_id']);
            return $unitGroup->getName();
        }
        return NULL;
    }

    public function getHtmlSpecialAttributes($field)
    {
        $type = $this->getHtmlType($field);
        switch ($type) {
        case 'checkbox';
            if ($this->$field == '1') {
                return ' checked="checked" ';
            }
        case 'datum':
            return ' size="10" ';
        case 'time':
            return ' size="5" ';
        default:
            return '';
        }
    }

    private function getMaxPk()
    {
        $sql = "SELECT MAX({$this->getPK()}) FROM {$this->getTableName()}";

        $rs = mysqli_query($sql);
        $row = mysqli_fetch_array($rs);
        return $row[0];
    }

    public function getForeignKey($fieldName)
    {
        $this->getForeignKeys();
        if ($this->hasFks) {
            foreach ($this->fk as $fk) {
                if ($fieldName == $fk["FK"]) {
                    return $fk;
                }
            }
        }
        return null;
    }

    public function isSelfReferring()
    {
        $this->getForeignKeys();
        if ($this->hasFks) {
            foreach ($this->fk as $fk) {
                if ($fk['SELF_REFERRING']) {
                    return true;
                }
            }
        }
        return false;
    }

    private function getForeignKeys()
    {
        if (!is_null($this->hasFks)) {
            return $this->fk;
        }

        if ($this->is_view) {
            $this->hasFks = false;
            return null;
        }
        $sql = "SHOW CREATE TABLE `{$this->getTableName()}`";

        $rs = mysqli_query(self::$conn, $sql);
        $row = mysqli_fetch_array($rs);
        $data = $row['Create Table'];
        // split up:
        // InnoDB free: 11264 kB; (`ckr_info_id`) REFER `swol/ckr_info`(`id`)
//		$data = str_replace ("`","",$data); 
        $arr = explode("CONSTRAINT", $data);
        array_shift($arr); // first element is not a constraint, remove
        $fkCount = 0;
        $fk = null;
        $this->hasFks = false;
        foreach ($arr as $el) {
            $c = explode("`", $el);
            $fk[$fkCount]['FK'] = $c[3];
            $fk[$fkCount]['REF'] = "{$c[5]}.{$c[7]}";
            $fk[$fkCount]['CLASS'] = $this->camelCase($c[5]);
            $fk[$fkCount]['SELF_REFERRING'] = false;
            if ($c[5] == $this->getTableName()) {
                $fk[$fkCount]['SELF_REFERRING'] = true;
#				Logger::write(strtoupper($this->getTableName()) . ' = SELF REFERING<br/>');					
            }
            $fkCount++;
            $this->hasFks = true;
        }
        $this->fk = $fk;
        if (is_null($this->hasFks)) {
            $this->hasFks = false;
        }
        return null;
    }

    static public function getTables($camelCase = false)
    {
        $tables = [];

        $q = mysqli_query(self::$conn, "SHOW TABLES");
        while ($r = mysqli_fetch_array($q)) {

            if ($camelCase) { // to get class names
                $tables[] = self::camelCase($r[0]);
            } else {
                $tables[] = $r[0];
            }
        }
        return $tables;
    }

    static public function exists($tbl)
    {
        $tables = self::getTables();
        if (in_array($tbl, $tables)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    static public function cc2u($name)
    {
        return cc2u($name);
    }

    static public function camelCase($name)
    {
        return camelCase($name);
    }

    public function getField(string $sql, array $args)
    {
        $rs = $this->execSql($sql, $args);
        $rs->data_seek(0);
        $row = $rs->fetch_array();
        return $row[0];
    }

    public function count(): int
    {
        if (empty($this->numRows)) {
            return 0;
        }
        return $this->numRows;
    }

    public function getNumRows(): int
    {
        return $this->count();
    }

    public function getModifiedPropertys()
    {
        $modified = [];
        foreach ($this->getModifiedMap() as $prop => $value) {
            if ($value == true) {
                $modified[$prop] = true;
            }
        }
        return $modified;
    }

    public function getProperties()
    {
        $properties = [];
        foreach ($this->getModifiedMap() as $prop => $value) {
            if ($value == true) {
                $properties[$prop] = true;
            }
        }
        return $properties;
    }

    public static function copyFields($source, &$target, $sourcePk = 'uid')
    {
        $mapA = $source->getModifiedMap(); // alle variablelen
        $mapB = $target->getModifiedMap();
        $map = array_intersect_key($mapB, $mapA);

        unset($map[$sourcePk]); // niet de pk
        foreach ($map as $field => $dummy) { // namen  in var $field
            $get = 'get' . camelCase($field);  // naam getter
            $set = 'set' . camelCase($field);  // naam setter
            // kopieren:
            $target->$set($source->$get());
        }
    }

    public static function replaceEmptyFields($source, &$target, $sourcePk = 'uid')
    {
        $mapA = $source->getModifiedMap(); // alle variablelen CursusDatum
        $mapB = $target->getModifiedMap();
        $map = array_intersect_key($mapB, $mapA);

        unset($map[$sourcePk]); // niet de pk
        foreach ($map as $field => $dummy) { // namen cursus datum velden in var $field
            $get = 'get' . camelCase($field);  // naam getter cursusDatums
            $set = 'set' . camelCase($field);  // naam setter aanmelden entity
            // kopieren:
            $targetValue = $target->$get();
            if (empty($targetValue)) {
                $target->$set($source->$get());
            }
        }
    }

    // TODO: in progress
    static public function columnSort($unsorted, $column)
    {
        $sorted = $unsorted;
        $size = $sorted->getNumRows() - 1;

        for ($i = 0; $i < $size; $i++) {
            for ($j = 0; $j < $size - $i; $j++) {
                if ($sorted[$j][$column] > $sorted[$j + 1][$column]) {
                    $tmp = $sorted[$j];
                    $sorted[$j] = $sorted[$j + 1];
                    $sorted[$j + 1] = $tmp;
                }
            }
        }
        return $sorted;
    }

    public function validate($post)
    {
        foreach ($this->fieldsInfo as $field => $descr) {
            $func = 'validate' . camelCase($field);
            if (method_exists($this, $func)) {
                $this->$func($post);
                continue;
            }
            $obliged = $this->isObliged($field);
            if ($obliged !== false && (isset($post[$field]) && empty($post[$field]))) {
                $this->setMessage(new Message($obliged, Message::ERROR, $this->getClassName()));
                continue;
            }

            $fieldName = $field;
            $type = $descr['Type'];
            if (preg_match('/(?P<type>\w+)?\(/s', $type, $match)) {
                $name = $match['type'];
            } elseif (preg_match('/(?P<type>\w+)/s', $type, $match)) {
                $name = $match['type'];
            }
            if (preg_match('/.*?\((?P<param>.*)?\)/s', $type, $match)) {
                $param = $match['param'];
            } else {
                $param = "";
            }
            if (!empty(self::$meta[$this->getTableName()][$field]['validation'])) {
                if (array_key_exists('label_nl', self::$meta[$this->getTableName()][$field])) {
                    $fieldName = self::$meta[$this->getTableName()][$field]['label_nl'];
                }
                switch (self::$meta[$this->getTableName()][$field]['validation']) {
                case 'telefoon':
                    $regexp = '/\d{10}/';
                    break;
                case 'mobiel':
                    $regexp = '/06\d{8}/';
                    break;
                case 'postcode':
                    $regexp = '/\d{4}[a-z|A-Z]{2}/';
                    break;
                case 'datum_nl':
                    $regexp = '/\d{1,2}-\d{1,2}-\d{4}/';
                    break;
                case 'email':
                    $regexp = '/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i';
                    break;
                }
            } else {
                switch ($name) {
                case 'int':
                case 'tinyint':
                    $regexp = '/\d*/';
                    break;
                case 'float':
                    $regexp = '/[\d|\.]+/';
                    break;
                case 'char':
                case 'varchar':
                    $regexp = '/.{0,' . $param . '}/';
                    break;
                case 'date':
                    $regexp = '%[0-3]*[0-9][/|-][0-1]*[0-9][/|-](20|19)[0-9][0-9]%';
                    break;
                default:
                    $regexp = NULL;
                }
            }
            if ($regexp === NULL) {
                continue;
            }
            if (!empty($post[$field])) {
                if (array_key_exists($field, Post::$postVars)) {
                    preg_match($regexp, $post[$field], $regs);
                    if ($regs[0] != $post[$field]) {
                        $this->setMessage("'{$post[$field]}' " . Strings::show('is_not_correct') . ' ' . strtolower(Strings::show($fieldName)) . ' ' . Strings::show('value') . '.');
                    }
                }
            }
        }
        $messages = $this->getMessages();
        return (empty($messages));
    }

    private function createWheres($from)
    {

        $dates = [];
        $wheres = [];
        $searchDateField = NULL;
        if ($this->getPostedStartdateField() == $this->getDatefieldToSearchOn() ||
            $this->getPostedEnddateField() == $this->getDatefieldToSearchOn()
        ) {
            throw new Exception('fout: $this->getPostedEnddateField() == $this->getDatefieldToSearchOn()');
        }
        if (array_key_exists($this->getDatefieldToSearchOn(), $from)) {
            $searchDateField = $from[$this->getDatefieldToSearchOn()];
//			unset($from[$this->getDatefieldToSearchOn()]);
        }

        foreach ($from as $field => $value) {
            $match = NULL;
            if (array_key_exists($field, $this->dbModified) && !empty($value)) {
                $dateSearch = ($field == $this->getPostedStartdateField() || $field == $this->getPostedEnddateField());
                // int value: exact match
                if (is_numeric($value)) {
                    if ($dateSearch) {
                        continue;
                    }
                    if ((int)$value == $value) {
                        $match = self::EXACT_MATCH;
                    }
                }
                if ($match == NULL) {
                    if ($field == $this->getPostedStartdateField() || $field == $this->getPostedEnddateField()) {
                        if (preg_match('/\A\d+-\d+-\d+\Z/', $value)) {
                            // date
                            $date = preg_replace('/(\d+)-(\d+)-(\d+)/', '$3-$2-$1', $value);
                            $dates[$field] = $date;
                            $match = self::DATE_MATCH;
                            continue;
                        }
                    }
                }
                if ($dateSearch) {
                    $this->setMessage('datum formaat is dd-mm-jjjj');
                    continue;
                }

                if ($match == NULL) {
                    // some string
                    $value = mysqli_real_escape_string($value);
                    $match = self::NON_EXACT_MATCH;
                }
                switch ($match) {
                case self::EXACT_MATCH:
                    $wheres[] = "$field = '$value'";
                    break;
                case self::NON_EXACT_MATCH:
                    $wheres[] = "$field LIKE '%$value%'";
                    break;
                }
            }
        }

        if (!empty($dates)) {
            if (array_key_exists($this->getPostedStartdateField(), $dates) && array_key_exists($this->getPostedEnddateField(), $dates)) {
                if (!empty($searchDateField)) {
                    $w = [];
                    $w[] = "$searchDateField BETWEEN '{$dates[$this->getPostedStartdateField()]}' AND '{$dates[$this->getPostedEnddateField()]}'";
                    $wheres = array_merge($w, $wheres);
                }
            } else {
                if (array_key_exists($this->getPostedStartdateField(), $dates) || array_key_exists($this->getPostedEnddateField(), $dates)) {
                    $f1 = $dates[$this->getPostedStartdateField()];
                    $f2 = $dates[$this->getPostedEnddateField()];

                    if (!empty($f1)) {
                        $date = $f1;
                        $operator = '>=';
                    } else {
                        $date = $f2;
                        $operator = '<=';
                    }

                    if (!empty($searchDateField)) {
                        $w = [];
                        $w[] = "$searchDateField $operator '$date'";
                        $wheres = array_merge($w, $wheres);
                    }
                }
            }
        }

        return $wheres;
    }

    public function getSearchSql($from)
    {
        $wheres = $this->createWheres($from);
        $sql = 'SELECT * FROM ' . $this->getTableName() . ' WHERE 1=1 ';
        if (!empty($wheres)) {
            $wheres = ' AND ' . implode(' AND ', $wheres);
        } else {
            $wheres = '';
        }
        $sql = "$sql $wheres";

        return $sql;
    }

    public function hasProperty($propertyName)
    {
        return property_exists($this, $propertyName);
    }

    public function debug($field)
    {
        $s = "\n** instance: " . self::$instance_id . " ****************\n";
        $d = $this;
        $d->first();
        foreach ($d as $e) {
            $s .= $e->$field . "\n";
        }
        $d->first();
        $s .= "\n******************\n";
        return $s;
    }

    public function getAsTable()
    {
        $f = [];
        foreach ($this->fieldsInfo as $field => $value) {
            $f[$field] = $this->$field;
        }
        return $f;
    }

    public static function getReferencedTableName($dbName, $tableName, $columnName)
    {
        $sql = "SELECT referenced_table_name FROM information_schema.KEY_COLUMN_USAGE WHERE referenced_table_schema='$dbName' AND table_name = '$tableName' AND column_name='$columnName'";
        $rs = mysqli_query(self::$conn, $sql);
        $ret = '';
        if (mysqli_num_rows($rs) === 1) {
            $row = mysqli_fetch_array($rs);
            $ret = $row['referenced_table_name'];
        }
        mysqli_select_db(self::$conn, $dbName);
        return $ret;
    }

}

if (!function_exists('camelCase')) {

    function camelCase($name)
    {
        $arr = explode("_", $name);
        $result = "";
        foreach ($arr as $part) {
            $result .= ucfirst($part);
        }
        $result = ucfirst($result);
        return $result;
    }

}
if (!function_exists('cc2u')) {

    function cc2u($name)
    {
        return ltrim(strtolower(preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '_$0', $name)), '_');
    }

}

class dbTableI extends dbTable
{

    protected $tableName = 'dbTableI';

    protected function getTableName()
    {
        return $this->tableName;
    }

}

function sqlGet($sql)
{
    static $db = NULL;
    if (empty($db)) {
        $db = new dbTableI();
    }
    $r = NULL;
    $vars = get_defined_vars();
    $r = mysqli_fetch_assoc($db->execSql($sql));
    if (!is_array($r)) {
        Klog::LogInfo(__FILE__ . ' ' . __LINE__ . 'sql error: ' . $sql);
        throw new dbTableException('Fatal Error (logged)', 10003);
    }
    extract($r);
    $newVars = get_defined_vars();
    unset($newVars['vars']);
    $diff = [];
    foreach ($newVars as $key => $value) {
        if (!array_key_exists($key, $vars)) {
            $diff[$key] = $value;
        }
    }
    if (count($diff) == 1) {
        return reset($diff);
    } else {
        return $diff;
    }
}
