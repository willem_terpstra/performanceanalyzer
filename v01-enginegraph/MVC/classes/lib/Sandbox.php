<?php

class Sandbox implements Countable
{

    private static $sandbox = [];

    function __construct()
    {
        if (isset($_SESSION['sandbox'])) {
            self::$sandbox = unserialize($_SESSION['sandbox']);
        }
    }

    public function count(): int
    {
        return count(self::$sandbox);
    }

    /**
     *
     * @param string $tag
     * @return Vehicle
     */
    public static function get($tag)
    {
        if (!array_key_exists('sandbox', $_SESSION)) {
            return NULL;
        }
        return self::$sandbox[$tag];
    }

    public static function getAll()
    {
        return self::$sandbox;
    }

    public static function set(Vehicle $vehicle)
    {

        $sessionTag = $vehicle->getSessionTag();
        $uid = $vehicle->getUid();
        if (empty($uid)) {
            throw new SandboxException('Cannot set vehicle without uid');
        }

        if (empty($sessionTag)) {
            $i = 0;
            for (; ;) {
                $i++;
                $tag = "_sndb$i";
                if (array_key_exists($tag, self::$sandbox)) {
                    continue;
                }
                $sessionTag = $tag;
                break;
            }
            $vehicle->setSessionTag($sessionTag);
        }
        self::$sandbox[$sessionTag] = $vehicle;
        Klog::LogInfo('Setting vehicle ' . $vehicle->getUid() . ' with tag ' . $sessionTag);
        self::serialize();
        return $vehicle;
    }

    public static function remove($tag)
    {
        unset(self::$sandbox[$tag]);
        self::serialize();
    }

    public static function removeAll()
    {
        foreach (self::$sandbox as $tag => $v) {
            self::remove($tag);
        }
    }

    private static function serialize()
    {
        $ser = serialize(self::$sandbox);
        $_SESSION['sandbox'] = $ser;
        if ($ser != $_SESSION['sandbox']) {
            throw new SandboxException('SessionSaveFailed');
        }
    }

}

new Sandbox();

