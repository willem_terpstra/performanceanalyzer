<?php

class Url
{

    public static function createFromVis($tq)
    {
        $query = urldecode($tq);
        $query = str_replace('__', '&nbsp;', $query);
        $query = str_replace('_', ' ', $query);
        $query = str_replace('&nbsp;', '_', $query);

        $query = strtolower($query);
        $selectRun = '';
        if (preg_match('/\s+from\s+diagram\s+/', $query)) {
            if (preg_match('/diagram_id\s*=\s*(\d+)/', $query, $regs)) {
                $diagramId = $regs[1];
            } else {
                $result = "";
            }
            if (preg_match('/vehicle_id\s*=\s*(\d+)/', $query, $regs)) {
                $vehicleId = $regs[1];
            } else {
                $result = "";
            }
            if (preg_match('/run_id\s*=\s*(\d+)/', $query, $regs)) {
                $runId = $regs[1];
            } else {
                $result = "";
            }
            if (isset($runId)) {
                $selectRun = "&run_id=$runId";
            }
        } else {
            echo 'Invalid query<br/>';
            exit;
        }
        $url = 'http://' . $_SERVER['HTTP_HOST'] . "/Diagram/image/$diagramId?vehicle_id=$vehicleId" . $selectRun . "&o=json";
        return [
            'diagramId' => $diagramId,
            'url' => $url
        ];
    }

}

?>
