<?php

class Authorization
{
    const LOGIN_BY_EMAIL = 1;
    const LOGIN_BY_USERID = 2;
    static private $checked = false;
    static private $user;

    /**
     * @deprecated
     * @param $requiredAuth
     * @return bool
     */
    public static function Allowed($requiredAuth)
    {
        return self::getAuthLevel() >= $requiredAuth;
    }

    public static function ActionAllowed($action, /* ..View */
                                         $view)
    {
        self::$checked = true;
        $currLevel = self::getAuthLevel();
        $vwClassAuth = new VwClassAuth();
        $instance = $view->getInstance();
        $vwClassAuth = $vwClassAuth->loadByName(get_class($instance));
        // check on failure
        $uid = $vwClassAuth->getUid();
        if (empty($uid)) {
            throw new SecurityException('No security defined for: ' . get_class($instance));
        }
        $securityArr = [
            Controller::BROWSE => 'getBrowse',
            Controller::BROWSELIJST => 'getBrowse',
            Controller::CANCEL => 'getBrowse',
            Controller::COMPARE => 'getBrowse',
            Controller::CONFIRM => 'getBrowse',
            Controller::ADD => 'getUpdate',
            Controller::SAVENEW => 'getUpdate',
            Controller::DETAIL => 'getUpdate',
            Controller::EDITLIJST => 'getUpdate',
            Controller::IMAGE => 'getBrowse',
            Controller::LOGOUT => 'getBrowse',
            Controller::UPDATE => 'getUpdate',
            Controller::SEARCH => 'getBrowse',
            Controller::REMOVE => 'getUpdate',
            VehicleController::FIND_DIAGRAMS => 'getBrowse',

        ];
        if (!array_key_exists($action, $securityArr)) {
            throw new SecurityException('Security not defined for action: ' . $action . ', class: ' . get_class($instance), SecurityException::SECURITY_NOT_DEFINED . ' (instance of: ' . get_class($instance) . ')');
        }
        $authFunc = $securityArr[$action] . 'Auth';
        $auth = $vwClassAuth->$authFunc();
        $groupFunc = $securityArr[$action] . 'Group';
        $group = $vwClassAuth->$groupFunc();

        // Niet toegankelijk
        if ($auth == Auth::NOBODY) {
            throw new SecurityException('Deze pagina is niet toegankelijk', SecurityException::SECURITY_NOBODY);
        }
//		// Add mag alleen als er geen instance geladen is
//		if($action == Controller::ADD) {
//			$uid = $instance->getUid();
//			if(!empty($uid)) {
//				return false;
//			}
//		}		
        $authFlag = false;
        switch ($auth) {
        case Auth::GAST:
            if (in_array($currLevel, [Auth::GAST, Auth::USER, Auth::ADMIN, Auth::SUPER])) {
                $authFlag = true;
            }
            break;
        case Auth::USER:
            if (in_array($currLevel, [Auth::USER, Auth::ADMIN, Auth::SUPER])) {
                $authFlag = true;
            }
            break;
        case Auth::ADMIN:
            if (in_array($currLevel, [Auth::ADMIN, Auth::SUPER])) {
                $authFlag = true;
            }
            break;
        default:
            throw new Exception("Security not implemented for action: $action, authorisation $auth and class " . get_class($instance));
        }
        if ($group == Groups::OWNER && $authFlag == true) {
            // Admin mag tot admin-owner
            if (in_array($currLevel, [Auth::ADMIN, Auth::SUPER]) && in_array($auth, [Auth::GAST, Auth::USER])) {
                return $authFlag;
            }
            // je moet eigenaar zijn om deze actie uit te mogen voeren
            if (empty($uid)) {
                // nieuw object is nog van niemand
                return $authFlag;
            }
            if (empty($_SESSION)) {
                return $authFlag;    // geen controle op gebruiker mogelijk
            }
            if (!isset($_SESSION['user_id'])) {
                return $authFlag;    // geen controle op gebruiker mogelijk
            }
            if (empty($_SESSION['user_id'])) {
                return $authFlag;    // geen controle op gebruiker mogelijk
            }
            try {
                $usersId = $instance->getUsersId();
            } catch (dbTableException $e) {
                if ($e->getCode() == dbTableException::PROPERTY_DOES_NOT_EXIST) {
                    throw new SecurityException('Class ' . $instance->getClassName() . ' does not have a user reference, ownership cannot be determined.');
                }
                throw new Exception($e);
            }
            if ($usersId == $_SESSION['user_id']) {
                $authFlag = true;
            } else {
                $authFlag = false;    // verkeerde gebruiker
            }
        }
        return $authFlag;
    }


    public function login($login, $passwd, $loginBy = self::LOGIN_BY_EMAIL)
    {
        $auth = new Users();

        try {
            switch ($loginBy) {
            case self::LOGIN_BY_EMAIL:
                $auth = $auth->loadByEmail($login);
                break;

            case self::LOGIN_BY_USERID:
                $auth = $auth->loadByUserid($login);
                break;
            }
        } catch (dbTableException $e) {
            if ($e->getMessage() === 'RecordNotFound') {
                header('Location: /register');
                exit;
            }
            throw new dbTableException($e);
        }

        $pwd = $auth->getPassw();
        if ($pwd !== $passwd) {
            throw new Exception('Login failed.');
        }
        $_SESSION['mdUserid'] = md5($login);
        $_SESSION['mdPasswd'] = md5($passwd);
        $user = new Users($auth->getUid());
        Unit::setUnits();
        Languages::setLanguage($user->getLanguagesId());

    }

    public static function isLoggedIn()
    {
        return !empty($_SESSION['mdUserid']);
    }

    public static function logOut()
    {
        unset($_SESSION['mdUserid']);
        unset($_SESSION['mdPasswd']);
        self::$user = NULL;
        header('Location: /');
    }

    public static function setRequiredAuth($level)
    {
        self::$requiredAuth = $level;
    }

    public static function getAuthLevel()
    {
        if (empty($_SESSION['mdUserid'])) {
            return Auth::GAST;
        }
        if (empty($_SESSION['mdPasswd'])) {
            return Auth::GAST;
        }
        $vwLogin = new VwLogin();
        $vwLogin->loadByUseridMd($_SESSION['mdUserid']);
        $vwLogin = $vwLogin->first();
        if ($vwLogin->getPasswMd() == $_SESSION['mdPasswd']) {

            return $vwLogin->getAuthId();
        }
        return Auth::GAST;
    }

    /**
     *
     * @return Users
     */
    public static function getCurrentUser()
    {
        if (!empty(self::$user)) {
            return self::$user;
        }
        if (empty($_SESSION['mdUserid'])) {
            return NULL;
        }
        $vwLogin = new VwLogin();
        $vwLogin->loadByUseridMd($_SESSION['mdUserid']);
        $vwLogin = $vwLogin->first();
        $uid = $vwLogin->getUid();
        self::$user = new Users($uid);
        return self::$user;
    }

    static public function generatePassword($length = 8)
    {
        // start with a blank password
        $password = "";

        // define possible characters
        $possible = "0123456789bcdfghjkmnpqrstvwxyz";

        // set up a counter
        $i = 0;

        // add random characters to $password until $length is reached
        while ($i < $length) {
            // pick a random character from the possible ones
            $char = substr($possible, mt_rand(0, strlen($possible) - 1), 1);

            // we don't want this character if it's already in the password
            if (!strstr($password, $char)) {
                $password .= $char;
                $i++;
            }
        }

        // done!
        return $password;
    }

    public static function getChecked()
    {
        return self::$checked;
    }
}

?>