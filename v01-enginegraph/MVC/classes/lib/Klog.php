<?php

class Klog
{

    private static $klogger = NULL;

    function __construct()
    {
        if (self::$klogger === NULL) {
            self::$klogger = new Klogger('log.txt', Klogger::INFO);
        }
//		return self::$klogger->$name($arguments);
    }

    public static function LogInfo($line)
    {
        if (self::$klogger === NULL) {
            self::$klogger = new Klogger('log.txt', Klogger::INFO);
        }
        return self::$klogger->LogInfo($line);
    }

    public static function LogDebug($line)
    {
        return self::$klogger->LogDebug($line);
    }

    public static function LogWarn($line)
    {
        return self::$klogger->LogWarn($line);
    }

    public static function LogError($line)
    {
        return self::$klogger->LogError($line);
    }

    public static function LogFatal($line)
    {
        return self::$klogger->LogFatal($line);
    }

    public static function Log($line, $priority)
    {
        self::$klogger->Log($line, $priority);
    }

    public static function WriteFreeFormLine($line)
    {
        return self::$klogger->WriteFreeFormLine($line);
    }

    public static function readLog($file = 'log.txt', $lines = 100)
    {
        //global $fsize;
        $handle = fopen($file, 'r');
        $length = $lines * Klogger::LRECL;
        $start = filesize($file) - $length;
        if (fseek($handle, $start) != -1) {
            $data = fread($handle, $length);
        }
        fclose($handle);
        return nl2br($data);
    }

}

?>
