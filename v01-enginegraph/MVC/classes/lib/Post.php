<?php

class Post implements ArrayAccess
{

    public static $postVars = [];

    function __construct()
    {
        if (!empty(self::$postVars)) {
            return;
        }
        if (isset($_POST)) {
            self::$postVars = $this->cleanData($_POST);
            unset ($_POST);
        }
    }

    private function cleanData($data)
    {
        if (is_array($data)) {
            $return = [];
            foreach ($data as $key => $value) {
                $key = str_replace('_AT_', '@', $key);
                $return[$key] = $this->cleanData($value);
            }
            return $return;
        }

        return $data;
    }


    public function offsetExists($offset): bool
    {
        return isset(self::$postVars[$offset]);
    }

    public function offsetSet($offset, $value): void
    {
        self::$postVars[$offset] = $value;
    }

    public function offsetGet($offset): mixed
    {
        if (!array_key_exists($offset, self::$postVars)) {
            return NULL;
        }
        return self::$postVars[$offset];
    }

    public function offsetUnset($offset): void
    {
        unset(self::$postVars[$offset]);
    }

}


new Post();

?>