<?php

class Ajax
{
    private static $allowedClasses = [
        'VehicleController',
        'UsersController',
        'RunController',
        'DiagramController',
//		'DynoController',
        'EngineController',
        'ChassisController',
        'VwEngineMerkenController',
        'VwMerkenController',
        'TransmissionController',
        'VwTransmissionMerkenController'
    ];

    private static $command;

    public static function getCommand()
    {
        return self::$command;
    }

    public static function setInstance($handle, $instance)
    {
        if (!array_key_exists('instances', $_SESSION)) {
            $_SESSION['instances'] = [];
        }
        $_SESSION['instances'][$handle] = serialize($instance);
    }

    public static function getInstance($handle)
    {
        if (!array_key_exists('instances', $_SESSION)) {
            return NULL;
        }
        if (!array_key_exists($handle, $_SESSION['instances'])) {
            return NULL;
        }
        return unserialize($_SESSION['instances'][$handle]);
    }

    public static function clear()
    {
        $_SESSION['instances'] = [];
    }

    public static function run($request)
    {
        $controller = $request[2] . 'Controller';
        $view = $request[2] . 'View';
        if (!class_exists($controller)) {
            echo 'error calling ' . $controller;
            return '';
        }
        self::$command = $request[4];
        if (in_array($controller, self::$allowedClasses)) {
            $uid = $request[3];
            if (!empty($uid)) {
                if (!is_numeric($uid)) {
                    echo 'error calling ajax with: ' . $uid;
                    return '';
                }
            }
            $controller = new $controller();
            try {
                echo $controller->run($view, $uid, '');
            } catch (Exception $e) {
                return json_encode(
                    ['error' => $e->getMessage()]
                );
            }
        } else {
            echo 'Not allowed';
        }
        return '';
    }
}

?>