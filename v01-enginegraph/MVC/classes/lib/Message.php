<?php

class Message
{
    const MESSAGE = 'MESSAGE';
    const WARNING = 'WARNING';
    const ERROR = 'ERROR';
    const FATAL = 'FATAL';

    const MESSAGE_LVL = 0;
    const WARNING_LVL = 1;
    const ERROR_LVL = 2;
    const FATAL_LVL = 3;

    private $severity;
    private $errorLvl;
    private $message;
    private $className;

    public function __construct($message, $code = Message::ERROR, $className = __CLASS__)
    {
        $this->set($message, $code, $className);
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getSeverity()
    {
        return $this->severity;
    }

    public function getErrorLevel()
    {
        return $this->errorLvl;
    }

    private function set($message, $code, $className)
    {
        $this->message = $message;
        $this->severity = $code;
        $this->className = $className;
        switch ($code) {
        case Message::MESSAGE:
            $this->errorLvl = Message::MESSAGE_LVL;
            break;
        case Message::WARNING:
            $this->errorLvl = Message::WARNING_LVL;
            break;
        case Message::ERROR:
            $this->errorLvl = Message::ERROR_LVL;
            break;
        case Message::FATAL:
            $this->errorLvl = Message::FATAL_LVL;
            break;
        }
    }

    public function __toString()
    {
        return $this->severity . ': ' . Strings::show($this->message) . ' (' . Strings::show($this->className) . ')';
    }
}

?>
