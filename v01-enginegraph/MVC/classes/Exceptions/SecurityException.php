<?php

class SecurityException extends Exception
{
    const SECURITY_NOT_DEFINED = 1;
    const SECURITY_NOBODY = 2;
    const WRONG_USER = 3;
}