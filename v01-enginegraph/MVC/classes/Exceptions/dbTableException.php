<?php

class dbTableException extends Exception
{
    const DUPLICATE_ENTRY = 1062;
    const CANNOT_UPDATE_PARENT_ROW = 1452;

    const LOAD_FAILED_EXCEPTION = 10000;
    const DATA_INCOMPLETE = 10001;
    const PROPERTY_DOES_NOT_EXIST = 10002;
    const SQL_ERROR = 10003;
}

