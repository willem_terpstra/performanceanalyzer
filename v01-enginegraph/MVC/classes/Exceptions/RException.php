<?php

class RException extends Exception
{
    const CANNOT_SOLVE_VARIABLE = 1;
    const DIVISION_BY_ZERO = 2;
    const NO_RUN_SELECTED = 3;
    const FORMULA_NOT_FOUND = 4;
    const PARSE_FORMULA_FAILED = 5;
    const DEACTIVATED = 6;
    const PROPERTY_NOT_FOUND = 7;

    private $params;

    private $msg = NULL;

    function __construct($message, $code, $params = NULL)
    {
        parent::__construct($message, $code);
        $this->params = $params;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function PropertyNotFound($property)
    {
        if ($property == 'nm') {
            $this->msg[] = 'RunHasNoData';
        } else {
            $this->msg[] = 'PropertyNotFound: ' . $property;
        }
        $this->message = implode("<br/>", $this->msg);
    }
}

?>