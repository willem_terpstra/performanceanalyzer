<?php

class SoapVehicle extends Vehicle
{

    private $tag;
    private $settings = NULL;
    private $result = NULL;
    private static $soapFields = [
        'session_tag' => NULL,
        'uid' => ['minOccurs' => 0],
        'run_id' => ['minOccurs' => 0],
        'gewicht' => ['minOccurs' => 0],
        'distance_diagram_distance' => ['minOccurs' => 0],
        'launch_rpm' => ['minOccurs' => 0],
        'model' => ['minOccurs' => 0],
        'jaar' => ['minOccurs' => 0],
        // Performance
        'max_hp_rpm_org' => ['minOccurs' => 0],
        'max_nm_rpm_org' => ['minOccurs' => 0],
        'max_hp_rpm' => ['minOccurs' => 0],
        'max_nm_rpm' => ['minOccurs' => 0],
        // Engine
        'engine_sprocket_t' => ['minOccurs' => 0],
        'flywheel_mass' => ['minOccurs' => 0],
        'flywheel_inner_diameter' => ['minOccurs' => 0],
        'flywheel_outer_diameter' => ['minOccurs' => 0],
        'shift_rpm' => ['minOccurs' => 0],
        // Transmission
        'input_sprocket_t' => ['minOccurs' => 0],
        'use_g1' => ['minOccurs' => 0],
        'use_g2' => ['minOccurs' => 0],
        'use_g3' => ['minOccurs' => 0],
        'use_g4' => ['minOccurs' => 0],
        'use_g5' => ['minOccurs' => 0],
        'use_g6' => ['minOccurs' => 0],
        // SecundaryTransmission
        'final_t1' => ['minOccurs' => 0],
        'final_t2' => ['minOccurs' => 0],
        'final_reduction' => ['minOccurs' => 0],
        // Chassis
        'wielmaat_aangedr_wiel' => ['minOccurs' => 0],
        'bandmaat_aangedr_wiel' => ['minOccurs' => 0],
        'band_aspect_ratio' => ['minOccurs' => 0],
        'cx' => ['minOccurs' => 0],
        'frontal_area' => ['minOccurs' => 0],
        // Driver
        'driver_weight' => ['minOccurs' => 0],
    ];
    public static $declarations = [];
    public static $return = [
        'uid' => ['name' => 'uid', 'type' => 'xsd:string'],
        'tag' => ['name' => 'tag', 'type' => 'xsd:string'],
        'json' => ['name' => 'json', 'type' => 'xsd:string'],
//		
    ];

    function __construct()
    {
        if (empty(self::$declarations)) {
            foreach (self::$soapFields as $field => $params) {
                self::$declarations[$field] = ['name' => $field, 'type' => 'xsd:string'];
                if (is_array($params)) {
                    self::$declarations[$field] = array_merge(self::$declarations[$field], $params);
                }
            }
        }
    }

    public function useVehicle($uid)
    {
        AccelerationDiagram::useGears([]);
        $vehicle = new Vehicle($uid);
        $vehicle->setSessionTag('_sndb1');
        $vehicle = Sandbox::set($vehicle);
        unset($vehicle);
        $vehicle = new Vehicle($uid);
        $vehicle->setSessionTag('_sndb2');
        $vehicle = Sandbox::set($vehicle); // twice, one for compare
        return '_sndb1';
    }

    public function getVehicle($tag)
    {
        $vehicle = Sandbox::get($tag);
        $this->copyFields($vehicle, $this);
        $this->setSessionTag($tag);
        $ret = [];
        foreach (self::$soapFields as $field) {
            $func = 'get' . $this->camelCase($field);
            $ret[$field] = $this->$func();
        }
        return $ret;
    }

    public function setSettings($settings)
    {
        $this->settings = $settings;
        $vehicle = Sandbox::get($settings['session_tag']);
        if (empty($settings['uid'])) {
            throw new Exception('settings uid is empty');
        }
        if ($vehicle->getUid() != $settings['uid']) {
            Klog::LogInfo('Changing vehicle from ' . $vehicle->getUid() . ' to ' . $settings['uid']);
            // got another one
            Sandbox::removeAll();
            AccelerationDiagram::useGears(NULL);
            $this->useVehicle($settings['uid']);
            $vehicle = Sandbox::get($settings['session_tag']);
        }
        $ratio = $settings['input_sprocket_t'] / $settings['engine_sprocket_t'];
        $settings['engine_sprocket_t'] = 1;
        $settings['input_sprocket_t'] = round($ratio, 4);

        $this->tag = $settings['session_tag'];
        unset($settings['session_tag']);

        $maxHpRpm = $settings['max_hp_rpm'];
        $maxNmRpm = $settings['max_nm_rpm'];

        unset($settings['max_hp_rpm']); // handled by createRun
        unset($settings['max_nm_rpm']); // handled by createRun

        $this->result = VehicleController::setSessVeh($this->tag, $settings);
        $this->createRun($this->tag, $maxHpRpm, $maxNmRpm);
    }

    private function addResult($msg, $oldVal = '', $newVal = '', $error = false)
    {
        if ($error) {
            $this->result['errors'][] = $msg;
        } else {
            $t = new TemplateEngine('MVC/html/soap/bootstrap/table.html');
            $t->assign('data', [
                [
                    $msg,
                    $oldVal,
                    ' => ',
                    $newVal
                ]
            ]);
            $t->assign('showLabels', false);
            $res = $t->execTemplate();

            $t = new TemplateEngine('MVC/html/soap/bootstrap/info.html');
            $t->assign('str', $res);

            $this->result['update']['frmSessVeh_' . $this->tag . ' div[name=sessVehResult]'][] = $t->execTemplate();
        }

    }

    protected function createRun($tag, $maxHpRpm, $maxNmRpm)
    {
        $vehicle = Sandbox::get($tag);
        $run = new Run();
        $run = $run->lijst(
            'SELECT * FROM run WHERE session_id= ? AND vehicle_id= ? AND session_tag= ?',
            [
                session_id(),
                $vehicle->getUid(),
                $tag
            ]
        );
        if (count($run) > 0) {
            $run = $run->first();
        } else {
            $run = $vehicle->getRun(NULL, true);
        }

        if ($run->getMaxNmRpm() != $maxNmRpm || $run->getMaxHpRpm() != $maxHpRpm) {
            if ($run->getMaxNmRpm() != $maxNmRpm) {
                $this->addResult(Strings::show('max_nm_rpm'), $run->getMaxNmRpm(), $maxNmRpm);
            }
            if ($run->getMaxHpRpm() != $maxHpRpm) {
                $this->addResult(Strings::show('max_hp_rpm'), $run->getMaxHpRpm(), $maxHpRpm);
            }
            try {
                $run = $run->getRunFromStrings($vehicle, $maxHpRpm, $maxNmRpm);
                $run->setSessionTag();

            } catch (RunException $e) {
                $s = '';
                switch ($e->getMessage()) {
                case 'InvalidPower':
                    $s = Strings::show('error') . ':' . $maxHpRpm . ' Hp@rpm<br/>';
                    break;
                case 'InvalidTorq':
                    $s = Strings::show('error') . ':' . $maxNmRpm . ' Nm@rpm<br/>';
                    break;
                }
                $this->addResult($s . Strings::show($e->getMessage()), '', '', true);
                return;
            }
            $vehicle->setSelectedRun($run);
            Sandbox::set($vehicle);
        }
    }

    public function getSoapOut()
    {
        $tag = '';
        if (array_key_exists('tag', $this->settings)) {
            $tag = $this->settings['tag'];
        }
        return [
            'uid' => $this->uid,
            'tag' => $tag,
            'json' => $this->result,
        ];
    }

}

new SoapVehicle();
?>