<?php

class PowerTorqDiagram extends Diagram
{

    public function useFormula(R $r, Vehicle $vehicle)
    {
        return true;
    }

    public function validateException($params)
    {
        return false;
    }

    public function validateInput($instance)
    {
        $errors = parent::validateInput($instance);
        $this->updateVehicleDiagram($instance, $errors);
        return $errors;
    }

}

?>
