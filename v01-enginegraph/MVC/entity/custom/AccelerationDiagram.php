<?php

class AccelerationDiagram extends Diagram
{

//	private static $useGears = array();

    public static function useGears($gears)
    {
        if (empty($gears)) {
            unset($_SESSION['useGears']);
        } else {
            $_SESSION['useGears'] = $gears;
        }
    }

    public function useFormula(R $r, Vehicle $vehicle)
    {
        $g = $vehicle->getAantalVersn();
        $f = $r->getName();


        if (preg_match('/\w+_g(\d+)/', $f, $regs)) {
            $i = $regs[1];
            if (!empty($_SESSION['useGears'])) {
                return in_array($i, $_SESSION['useGears']) && $i <= $g;
            }
            return $i <= $g;
        }
        return true;
    }

    public function validateException($params)
    {
        return false;
    }

    public function validateInput($instance)
    {

        $errors = parent::validateInput($instance);

        $gewicht = $instance->getGewicht();
        if (empty($gewicht)) {
            $errors[] = new Message('GewichtEmpty', Message::WARNING, __CLASS__);
        }
        $transmissionId = $instance->getTransmissionId();
        if (empty($transmissionId)) {
            $errors[] = new Message('TransmissionEmpty', Message::WARNING, __CLASS__);
        }
        $chassisId = $instance->getChassisId();
        if (empty($chassisId)) {
            $errors[] = new Message('ChassisEmpty', Message::WARNING, __CLASS__);
        }
        $primaireReductie = $instance->getVerhPrimair();
        if (empty($primaireReductie)) {
            $errors[] = new Message('PrimaireReductieEmpty', Message::WARNING, __CLASS__);
        }
        $secundaireReductie = $instance->getSecundaryTransmissionId();
        if (empty($secundaireReductie)) {
            $errors[] = new Message('SecundaireReductieEmpty', Message::WARNING, __CLASS__);
        }
        $wielmaat = $instance->getWielmaatAangedrWiel();
        if (empty($wielmaat)) {
            $errors[] = new Message('WielmaatEmpty', Message::WARNING, __CLASS__);
        }
        $bandmaat = $instance->getBandmaatAangedrWiel();
        if (empty($bandmaat)) {
            $errors[] = new Message('BandmaatEmpty', Message::WARNING, __CLASS__);
        }
        $bandRatio = $instance->getBandAspectRatio();
        if (empty($bandRatio)) {
            $errors[] = new Message('BandAspectRatioEmpty', Message::WARNING, __CLASS__);
        }
        $cx = $instance->getCx();
        if (empty($cx)) {
            $errors[] = new Message('CxIsEmpty', Message::WARNING, __CLASS__);
        }
        $frontalArea = $instance->getFrontalArea();
        if (empty($frontalArea)) {
            $errors[] = new Message('FrontalAreaIsEmpty', Message::WARNING, __CLASS__);
        }
        $aantalVersn = $instance->getAantalVersn();
        if (empty($aantalVersn)) {
            $errors[] = new Message('AantVersnEmpty', Message::WARNING, __CLASS__);
        }
        for ($i = 1; $i <= $aantalVersn; $i++) {
            $f = "getVerhVersn$i";
            $r = $instance->$f();
            if (empty($r)) {
                $errors[] = new Message("VerhVersn$i" . 'Empty', Message::WARNING, __CLASS__);
            }
        }
        $finalReduction = $instance->getFinalReduction();
        if (empty($finalReduction)) {
            $errors[] = new Message('finalReductionIsEmpty', Message::WARNING, __CLASS__);
        }
        foreach ($errors as $error) {
            Klog::LogInfo($error->getMessage());
        }
        $errors = array_merge($errors, parent::validateInput($instance));
        $this->updateVehicleDiagram($instance, $errors);
        return $errors;
    }

    public function setVehicle(Vehicle $vehicle)
    {
        parent::setVehicle($vehicle);
        AccelerationTimeDiagram::resetMeasurement();
        AccelerationDistanceDiagram::resetMeasurement();
    }

}

?>