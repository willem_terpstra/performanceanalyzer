<?php

class AccelerationTimeDiagram extends AccelerationDiagram
{
    private static $evolvedTime = 0;
    private static $highSpeed = [];
    private static $oldG = 0;


    public static function resetMeasurement()
    {
        self::$evolvedTime = 0;
        self::$highSpeed = [];
        self::$oldG = 0;
    }

    static public function inGraph($rpm, $shiftRpm, $speed, $nextSpeed, $gear)
    {
        if ($rpm <= $shiftRpm - 1) {
            self::$highSpeed[$gear] = $speed;
        } else {
            return false;
        }
        if ($gear == 1) {
            return true;
        } else {
            if (self::$highSpeed[$gear - 1] < $nextSpeed) {
                return true;
            } else {
                return false;
            }
        }
    }

}


