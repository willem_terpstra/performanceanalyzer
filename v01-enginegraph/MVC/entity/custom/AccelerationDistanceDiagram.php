<?php

class AccelerationDistanceDiagram extends AccelerationDiagram
{
    private static $oldG = 0;
    private static $evolvedDistance = 0;

    public static function resetMeasurement()
    {
        self::$evolvedDistance = 0;
        self::$oldG = 0;
    }

    static function getEvolvedDistance($l, $rpm, $shiftRpm, $speed, $nextSpeed, $gear, $distance)
    {
        if (!AccelerationTimeDiagram::inGraph($rpm, $shiftRpm, $speed, $nextSpeed, $gear)) {
            return NULL;
        }
        if (self::$evolvedDistance > ($distance * 1000)) {    // km to m
            return false;
        }
        if ($gear != self::$oldG) {
            self::$oldG = $gear;
            self::$evolvedDistance += 0.15 * ($speed / 3.6);
            return self::$evolvedDistance;
        }
        self::$evolvedDistance += $l;
//echo "gear: $gear distance: ". self::$evolvedDistance .'<br/>';
        return self::$evolvedDistance;
    }

    static function getQuarterMileTime($results)
    {
        for ($i = 0; $i < 14; $i += 2) {
            if (isset($results[$i]) && is_array($results[$i])) {
                $d = $results[$i + 1];
                $j = -1;
                foreach ($d as $dist) {
                    $j++;
                    if ($dist > 402) {
                        $r = $results[$i][(string)$j];    // qm time
                        $a = [];
                        $a[] = $r;
                        return $a;
                    }
                }

            }
        }
        return NULL;
    }

}

?>
