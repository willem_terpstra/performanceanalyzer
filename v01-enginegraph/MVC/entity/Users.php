<?php


class Users extends _Users implements DropdownAble
{


    /* Add here your code */

//    public function getUnits()
//    {
//        $sql = "SELECT users.uid, unit.naam AS unit_naam, unit_group.name AS unit_group_name
//                FROM
//                    users_unit
//                    INNER JOIN users
//                        ON (users_unit.users_id = users.uid)
//                    INNER JOIN unit
//                        ON (users_unit.unit_id = unit.uid)
//                    INNER JOIN unit_group
//                        ON (unit.unit_group_id = unit_group.uid)
//                WHERE  users.uid= ?";
//        $rs = $this->execSql($sql, [627]);
////        $rs = $this->execSql($sql, [$this->getUid()]);
//        $userUnits = [];
//        while ($row = mysqli_fetch_array($rs, MYSQLI_ASSOC)) {
//            $userUnits[$row['unit_group_name']] = $row['unit_naam'];
//        }
//        return $userUnits;
//    }

//    public function saveOrUpdate()
//    {
//        parent::saveOrUpdate();
//        $unitGroups = new VwUnitUnitGroup();
//        $unitGroups = $unitGroups->lijst('SELECT * FROM vw_unit_unit_group WHERE conversion=1');
//        $ugCount = count($unitGroups);
//        $sql = "SELECT	users.uid, unit.naam AS unit_naam, unit_group.name AS unit_group_name
//                FROM
//                    users_unit
//                    INNER JOIN users
//                        ON (users_unit.users_id = users.uid)
//                    INNER JOIN unit
//                        ON (users_unit.unit_id = unit.uid)
//                    INNER JOIN unit_group
//                        ON (unit.unit_group_id = unit_group.uid)
//                WHERE users.uid={$this->getUid()}";
//        $rs = $this->execSql($sql);
//        if (mysql_num_rows($rs) < $ugCount) {
//            $userUnits = [];
//            while ($row = mysqli_fetch_assoc($rs)) {
//                $userUnits[$row['group_name']] = $row['naam'];
//            }
//            foreach ($unitGroups as $unitGroup) {
//                if (!array_key_exists($unitGroup->getGroupname(), $userUnits)) {
//                    $userUnit = new UsersUnit();
//                    $userUnit->setUsersId($this->getUid());
//                    $userUnit->setUnitId($unitGroup->getUnitId());
//                    $userUnit->saveOrUpdate();
//                }
//            }
//        }
//    }

    public function getValue()
    {
        return $this->getVoornaam() . ' ' . $this->getAchternaam();
    }

    public function getDropdownNullOption()
    {
        return NULL;
    }

    public function getDropdownDefaultValue()
    {
        return NULL;
    }

    public function setPassw($password) {
        global $config;
        $password = $config->salt . $password;
        $this->passw = hash('sha256', $password);
    }

    public function getIntValueMethod()
    {
        return 'getUid';
    }

    public function getUserByCompleteName($firstName, $lastName)
    {
        $sql = "SELECT * FROM users WHERE voornaam = '$firstName' AND achternaam = '$lastName'";
        foreach ($this->lijst($sql) as $user) {
            return $user;
        }
        return NULL;
    }

    /* system functions */

    public function getClassName()
    {
        return "Users";
    }

    public function getUid()
    {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id)
    {
        $idName = $this->getPK();
        $this->$idName = $id;
    }


}

?>