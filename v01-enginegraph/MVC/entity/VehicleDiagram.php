<?php


class VehicleDiagram extends _VehicleDiagram
{

    public function getDiagram($uid = null, $getObject = false)
    {
        return $this->diagram_id;
    }

    static public function validateInstance(&$instance)
    {
        // update VehicleDiagram
        $diagrams = new Diagram();
        $diagrams = $diagrams->lijst();
        $errors = [];
        foreach ($diagrams as $diagram) {
            $diagramName = $diagram->camelCase($diagram->getName()) . 'Diagram';
            $diagram = new $diagramName($diagram->getUid());
            $instance->setMessages($diagram->validateInput($instance));

//			$errors = array_merge($errors, $this->getDiagramErrors($instance, $diagram));
        }
    }

    static public function generate()
    {
        $vehicles = new Vehicle();
        $vehicles = $vehicles->lijst();
        $usable = [];
        $diagram = new Diagram();

        foreach ($diagram->lijst() as $d) {
            $diagramName = $d->camelCase($d->getName()) . 'Diagram';
            $diagramInstance = new $diagramName($d->getUid());

            foreach ($vehicles as $vehicle) {
                $diagramInstance->validateInput($vehicle);
            }
        }
    }

    public function getVehicle($uid = NULL)
    {
        if (empty($uid)) {
            $uid = $this->getVehicleId();
        }
        $Vehicle = new Vehicle($uid);
        return $Vehicle;
    }

    /* system functions */

    public function getClassName()
    {
        return "VehicleDiagram";
    }

    public function getUid()
    {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id)
    {
        $idName = $this->getPK();
        $this->$idName = $id;
    }


}

?>