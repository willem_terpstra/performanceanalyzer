<?php

class Clutch extends _Clutch
{

//	public $area;
////	public $springpressure;
//	
    public function getArea()
    {
        return $this->plates * (((M_PI_4) * pow($this->outerdiameter, 2)) - ((M_PI_4) * pow($this->innerdiameter, 2)));
    }

    public function getSpringpressure()
    {
        return $this->springs * $this->springpressure;
    }

//	public function torque($springPressure) {
//		if($springPressure === NULL) {
//			$springPressure = $this->getSpringPressure();
//		}
//		return $springPressure * $this->area() * $this->fcoeff;
//	}


    static function getTorque($result)
    {
        // %nm%, %rpm%, %max_nm_rpm%, %max_nm%
        $nmArr = $result[0];
        $rpmArr = $result[1];
        $maxNmRpm = $result[2][0];
//		$maxNmRpm = 4000;
        $maxNm = $result[3][0];
        $t = array_combine($rpmArr, $nmArr);
        $c = 0;
        foreach ($rpmArr as $rpm) {
            if ($rpm > $maxNmRpm) {
                break;
            }
            $c++;
        }
        $nmStart = 0.8 * $maxNm;
        $nmStep = ($maxNm - $nmStart) / $c;
        $nm2 = $nmStart;
        $ot = [];
        foreach ($t as $rpm => $nm) {
            if ($rpm < $maxNmRpm) {
                $nm2 += $nmStep;
            } else {
                $nm2 = $nm;
            }
            $ot[] = $nm2;
        }
        return $ot;
    }

// static function getTorque($result) {
//		// result:
//		// %snelheid 1e versn%, 
//		// %max_nm%, 
//		// %max_nm_rad_sec%, 
//		// %verh_primair%, 
//		// (%verh_secundair% * (%wheel_circumref%/1000*60)) / 0.104719755		
//		// %rpm%
//		// %nm%
//
//		$acc = array();
//
//		$wc = 0;
//		$speed = $result[0];
//		$maxNm = $result[1][0];
//		$we = $maxNmRadSec = $result[2][0];
//		$primaryReduction = $result[3][0];
//		$endReduction = $result[4][0];
//		$rpm = $result[5];
//		$nm = $result[6];
//		$clutch = DiagramController::getSelectedVehicle()->getClutch();
//		$maxUsedClutchTorq = $maxNm / $primaryReduction;
//
//		$t = 0;
//		while ($t < $maxNm) {
//			$t = $nm[$i];
//			$i++;
//		}
//		$aangrijpingspunt = $speed[$i];
//		$rotSpeedDiff = $we - $wc;
//		$step = $rotSpeedDiff / 25;
//		$wCounter = $wc;
//		$area = $clutch->getArea();
//		$fcoeff = $clutch->getFcoeff();
//		$areaCoeff = $clutch->getArea() * $clutch->getFcoeff();
//		$t = array();
//		while ($wCounter < $we) {
//			$torq = 0.5 * $areaCoeff * ($we - $wCounter);
//			$torq = $torq > $maxNm ? $maxNm : $torq;
//			$wCounter += $step;
//		}
//
//		for (;;) {
//
//			break;
//		}
//	}

    /* system functions */

    public function getClassName()
    {
        return "Clutch";
    }

    public function getUid()
    {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id)
    {
        $idName = $this->getPK();
        $this->$idName = $id;
    }

}

?>