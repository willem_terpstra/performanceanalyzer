<?php


class SecundaryTransmission extends _SecundaryTransmission
{

    function __call($name, $args)
    {
        if (strpos($name, 'setFinalT') !== false) {
            preg_match_all('/setFinalT(\d)/', $name, $result, PREG_PATTERN_ORDER);
            $t = $result[1][0];
            $prop = "final_t{$t}";
            $this->$prop = $args[0];
            $this->dbModified[$prop] = true;
        } elseif (strpos($name, 'getFinalT') !== false) {
            preg_match_all('/getFinalT(\d)/', $name, $result, PREG_PATTERN_ORDER);
            $t = $result[1][0];
            $prop = "final_t{$t}";
            return $this->$prop;
        } else {
            return parent::__call($name, $args);
        }
        return null;
    }

    function __clone()
    {
        $this->setUid(NULL);
        $this->setUsersId(Authorization::getCurrentUser()->getUid());
        $this->setName($this->getName() . ' ' . Strings::show('copy'));
        foreach ($this->dbModified as &$property) {
            $property = true;
        }
    }

    public function getName()
    {
        if (empty($this->name)) {
            if (!empty($this->final_t1) && !empty($this->final_t2)) {
                return $this->final_t1 . '/' . $this->final_t2;
            } else {
                return 'Unititled';
            }
        } else {
            return parent::getName();
        }
    }

    public function getValue()
    {
        return $this->getName();
    }

    public function save($autoIncrement = true)
    {
        if (!empty($this->final_t1) && !empty ($this->final_t2)) {
            $this->setFinalReduction($this->final_t1 / $this->final_t2);
        }
        parent::save($autoIncrement);
    }

    public function update()
    {
        if (!empty($this->final_t1) && !empty ($this->final_t2)) {
            $this->setFinalReduction($this->final_t1 / $this->final_t2);
        }
        parent::update();
    }

    public function calcReduction()
    {
        if (!empty($this->final_t1) && !empty ($this->final_t2)) {
            return $this->final_t1 / $this->final_t2;
        }
        if (!empty ($this->final_reduction)) {
            return $this->final_reduction;
        }
        return 1;
    }

    public function validateFinalT1($post)
    {
        if (is_numeric($post['final_t1']) && is_numeric($post['final_t2'])) {
            return true;
        } elseif (is_numeric($post['final_reduction'])) {
            return true;
        }
        $this->setMessages('invalid_values_secundary_reduction', Message::ERROR, 'secundary_reduction');
        return null;
    }


    /* system functions */

    public function getClassName()
    {
        return "SecundaryTransmission";
    }

    public function getUid()
    {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id)
    {
        $idName = $this->getPK();
        $this->$idName = $id;
    }


}

?>