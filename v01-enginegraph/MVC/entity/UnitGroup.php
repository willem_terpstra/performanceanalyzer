<?php


class UnitGroup extends _UnitGroup
{

    const    POWER = 1;
    const    TORQ = 2;
    const    SPEED = 3;
    const    VOLUME = 4;
    const    LENGTH = 5;
    const    REVOLUTIONS = 6;
    const    FORCE = 7;
    const    PRESSURE = 8;
    const    FLOW = 9;
    const    MISC = 10;
    const    ACCELERATION = 11;
    const    EFFICIENCY = 12;
    const    DISTANCE = 13;
    const    WEIGHT = 14;
    const    SURFACE = 15;

    static $unitGroups = [];

    static function isDefined($name)
    {
        if (empty(self::$unitGroups)) {
            $unitGroup = new UnitGroup();
            foreach ($unitGroup->lijst() as $group) {
                self::$unitGroups[] = $group->getName();
            }
        }
        return in_array($name, self::$unitGroups);
    }

    public function getUnits()
    {
        $sql = 'SELECT * FROM unit WHERE unit_group_id=' . $this->uid;
        $units = new Unit();
        $units = $units->lijst($sql);
        return $units;
    }

    public function getValue(): string
    {
        return $this->getName();
    }

    public function getName(): string
    {
        return $this->name ?? '';
    }

    /* system functions */

    public function getClassName()
    {
        return "UnitGroup";
    }

    public function getUid()
    {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id)
    {
        $idName = $this->getPK();
        $this->$idName = $id;
    }


}

?>