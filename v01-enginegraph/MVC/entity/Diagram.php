<?php

class Diagram extends _Diagram
{

    const TORQ_ABUNDANCE = 1;
    const PMEP = 2;
    const VE = 3;
    const POWER_TORQ = 6;
    const ACCELERATION = 7;
    const ACCELERATION_TIME = 9;
    const ACCELERATION_DISTANCE = 10;
    const ACCELERATION_LEAD = 11;

    const CACHE = 1;

    /**
     *
     * @var type Vehicle
     */
    private $Vehicle;
    protected static $errors = [];

    public function validateInput($instance)
    {
        $errors = [];
        if ($instance instanceof Run) {
            $instance = $instance->getVehicle(NULL, true);
        }
        if ($instance instanceof Vehicle) {
            $r = $instance->getNumRuns();
            if ($instance->getNumRuns() == 0) {
                $errors[] = new Message('NoRuns', Message::WARNING, __CLASS__);
            } else {
                $maxRpm = 0;
                foreach ($instance->getRuns() as $run) {
                    $maxFoundRpm = $run->getMaxRpm();
                    if ($maxFoundRpm > $maxRpm) {
                        $maxRpm = $maxFoundRpm;
                    }
                }
                if (!$maxRpm) {
                    $errors[] = new Message('selectedRunHasNoData', Message::WARNING, __CLASS__);
                }
            }
        }
//		$this->updateVehicleDiagram($instance, $errors);
        return $errors;
    }

//	abstract function validateInput(Vehicle $vehicle);

    public function setVehicle(Vehicle $vehicle)
    {
        $this->Vehicle = $vehicle;
    }

    public function getVehicle()
    {
        return $this->Vehicle;
    }

    public function getUsedClassName()
    {
        return camelCase($this->getName());
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getRequiredVars()
    {
        throw new Exception('Obsolete function: ' . __CLASS__ . '::' . __FUNCTION__);
    }

    public function getImagePath(Vehicle $vehicle)
    {

        $path = '/Diagram/image/' . $this->getUid() . '?vehicle_id=' . $vehicle->getUid();
        return $path;
    }

    public function lijst($param = null, array $args = [])
    {
        if (!empty($param)) {
            return parent::lijst($param . ' ORDER BY sort_order');
        }
        return parent::lijst('SELECT * FROM ' . $this->getTableName() . ' ORDER BY sort_order');
    }

    function __call($name, $args)
    {
        try {
            $ret = parent::__call($name, $args);
            return $ret;
        } catch (dbTableException $e) {
            if ($e->getCode() == dbTableException::PROPERTY_DOES_NOT_EXIST) {

//                if (preg_match('/(?P<function>get)(?P<property>\w+)Unit/', $name, $regs)) {
//                    $property = strtolower($regs['property']);
//                    $unitGroup = $property . 'UnitId';
//                    if (array_key_exists($unitGroup, $_SESSION)) {
//                        return Unit::getUnit($property);
//                    }
//                }
                if (preg_match('/(?P<function>getMax)(?P<property>\w+)Rpm/', $name, $regs)) {
                    if (empty($this->Vehicle)) {
                        return '';
                    }
                    return $this->Vehicle->$name();
                }
                if (preg_match('/(?P<function>get)(?P<property>\w+)/', $name, $regs)) {
                    if (empty($this->Vehicle)) {
                        return '';
                    }
                    return $this->Vehicle->$name();
                }
            }
            throw new dbTableException($e);
        }
    }

    protected function updateVehicleDiagram(Vehicle $vehicle, $errors)
    {
        if (!empty($errors)) {
            $sql = 'DELETE FROM vehicle_diagram WHERE vehicle_id= ? AND diagram_id= ?';
            $this->execSql($sql, [$vehicle->getUid(), $this->getUid()]);
            return;
        }
        $vehicleDiagram = new VehicleDiagram();
        $vehicleDiagram->setVehicleId($vehicle->getUid());
        $vehicleDiagram->setDiagramId($this->uid);
        try {
            $vehicleDiagram->saveOrUpdate();
        } catch (dbTableException $e) {
            if ($e->getCode() != dbTableException::DUPLICATE_ENTRY) {
                throw new dbTableException($e);
            }
        }
    }

    /* system functions */

    public function getClassName()
    {
        return "Diagram";
    }

    public function getUid()
    {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id)
    {
        $idName = $this->getPK();
        $this->$idName = $id;
    }

}

?>