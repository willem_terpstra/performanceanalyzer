<?php

// TODO: dynojet en fos naar eigen classes

class Run extends _Run implements DropdownAble
{

    const MODULUS = 2;
    const INTERPOL_LAGRANGE = 1;
    const INTERPOL_CUBIC_SPLINES = 2;

//	const MODULUS = 999999;
    private $arrDyno = NULL;
    private $max_rpm = NULL;
    private $max_hp = NULL;

    const NM = 'trlala';

    public function __toString()
    {
        return $this->getNaam();
    }

    public function __clone()
    {
        $oldUid = $this->uid;
        $this->setUid(NULL);
        $this->setUsersId(Authorization::getCurrentUser()->getUid());
        foreach ($this->dbModified as &$property) {
            $property = true;
        }
        $this->filename = NULL;
        $this->saveOrUpdate();
        $dynoPoints = new Dyno();
        foreach ($dynoPoints->lijst('SELECT * FROM dyno WHERE run_id= ?', [$oldUid]) as $dyno) {
            $d = clone($dyno);
            $d->setRunId($this->uid);
            $d->saveOrUpdate();
        }
    }

    protected function save($autoIncrement = true)
    {
        if (empty($this->filename)) {
            $this->setFilename(NULL);
        }
        parent::save();
    }

    protected function update()
    {
        if (empty($this->filename)) {
            $this->setFilename(NULL);
        }
        parent::update();
    }

    public function getNaam() : string
    {
        return $this->naam;
    }

    public function getImportedMessage()
    {
        return $this->imported_message;
    }

    public function getOmschrijving()
    {
        return $this->omschrijving;
    }

    public function getVwDynoPoints()
    {
        $vwDyno = new VwDyno();
        $vwDyno = $vwDyno->lijst('SELECT * FROM vw_dyno WHERE run_id=' . $this->uid);
        return $vwDyno;
    }

    public function getEndRpm()
    {
        $dyno = new Dyno();
        $maxRpm = $dyno->getField('SELECT MAX(rpm) FROM dyno WHERE run_id =?', [$this->uid]);
        return $maxRpm;
    }

    public function getStartRpm()
    {
        $dyno = new Dyno();
        $minRpm = $dyno->getField('SELECT MIN(rpm) FROM dyno WHERE run_id =' . $this->uid);
        return $minRpm;
    }

//    public function getTemperature($conversion = true)
//    {
//        if ($conversion) {
//            return Unit::convert($this->temperature, Unit::getDefaultUnit('temperature'), Unit::getUnit('temperature'));
//        } else {
//            return $this->temperature;
//        }
//    }
//
//    public function getAtmPressure($conversion = true)
//    {
//        if ($conversion) {
//            return Unit::convert($this->atm_pressure, Unit::getDefaultUnit('pressure'), Unit::getUnit('pressure'));
//        } else {
//            return $this->atm_pressure;
//        }
//    }

    public function getVehicle()
    {
        return new Vehicle($this->vehicle_id);
    }

//    function __call($name, $args)
//    {
////		Klog::Loginfo(__CLASS__ . ' ' . $name . ' => ' . var_export($args, true));
////        if (preg_match('/(?P<function>getMax)(?P<property>\w+)Rpm/', $name, $regs)) {
////            $property = strtolower($regs['property']);
////            $unit = Unit::$units[$property];
////            $sql = "SELECT $unit, rpm FROM vw_dyno
////				INNER JOIN run
////					ON (vw_dyno.run_id = run.uid)
////				WHERE (run.uid ={$this->uid})
////				ORDER BY $unit DESC LIMIT 1";
////            $rs = $this->execSql($sql);
////            $row = mysql_fetch_assoc($rs);
////            return round($row[$unit], 2) . $unit . '@' . $row['rpm'] . 'rpm';
////        }
//        if (preg_match('/(?P<function>getMax)(?P<property>\w+)/', $name, $regs)) {
//            $property = strtolower($regs['property']);
//            if (UnitGroup::isDefined($property)) {
//// get corresponding unit for max function
//                $property = Unit::getUnit($property);
//            }
//            $dyno = new VwDyno();
//            $dyno = $dyno->lijst('SELECT * FROM vw_dyno WHERE run_id=' . $this->uid . "  ORDER BY $property DESC LIMIT 1");
//            $dyno = $dyno->first();
//            $func = 'get' . ucfirst(camelCase($property));
//            return $dyno->$func();
//        }
//        if (preg_match('/(?P<function>getMin)(?P<property>\w+)/', $name, $regs)) {
//            $property = strtolower($regs['property']);
//            if (UnitGroup::isDefined($property)) {
//// get corresponding unit for max function
//                $property = Unit::getUnit($property);
//            }
//            $dyno = new VwDyno();
//            $dyno = $dyno->lijst('SELECT * FROM vw_dyno WHERE run_id=' . $this->uid . "  ORDER BY $property ASC LIMIT 1");
//            $dyno = $dyno->first();
//            $func = 'get' . camelCase($property);
//            return $dyno->$func();
//        }
//        return parent::__call($name, $args);
//    }

    public function getMaxAtRpm($unit = NULL)
    {
        if (empty($unit)) {
            $unit = Unit::$units['power'];
        }
        $dyno = new VwDyno();
        foreach ($dyno->lijst('SELECT * FROM vw_dyno WHERE run_id=' . $this->uid . '  ORDER BY ' . $unit . ' DESC LIMIT 1') as $d) {
            $f = 'get' . ucfirst($unit);
            return round($d->$f(), 2) . $unit . '@' . $d->getRpm() . 'rpm';
        }
        return 0;
    }

    public function getMaxRpm()
    {
        if (empty($this->max_rpm)) {
            $this->max_rpm = $this->getEndRpm();
        }
        return $this->max_rpm;
    }

    public function getMaxHp()
    {
        if (empty($this->max_hp)) {
            $dyno = new VwDyno();
            $this->max_hp = $dyno->getField('SELECT MAX(hp) FROM vw_dyno WHERE run_id = ?', [$this->uid]);
        }
        return $this->max_hp;
    }

    public function searchProperty($property, $shiftRpm = 100000)
    {
        $allowedProps = ['rpm', 'nm', 'hp', 'kw', 'rad_sec'];
        if (in_array($property, $allowedProps)) {
            if (!empty($this->arrDyno[$property])) {
                return $this->arrDyno[$property];
            }
            $arrDyno = [];
            $sql = 'SELECT ' . implode(', ', $allowedProps) . ' FROM vw_dyno WHERE run_id= ?';
            if ($shiftRpm) {
                $sql .= ' AND rpm <= ' . $shiftRpm;
            }
            $rs = dbTable::execSql($sql, [$this->uid]);
            $arrDyno = [];
            while ($row = mysqli_fetch_array($rs)) {
                $arrDyno['rpm'][] = $row['rpm'];
                $arrDyno['nm'][] = $row['nm'];
                $arrDyno['hp'][] = $row['hp'];
//                $arrDyno['bhp'][] = $row['bhp'];
                $arrDyno['kw'][] = $row['kw'];
                $arrDyno['rad_sec'][] = $row['rad_sec'];
//                $arrDyno['watt_rad_sec'][] = $row['watt_rad_sec'];
            }
            $this->arrDyno = $arrDyno;
            if (!empty($arrDyno)) {
                return $arrDyno[$property];
            }
        }
        return false;
    }

    public function getRResult(R $r)
    {
        $resultRun = new RResultRun();
        $resultArray = [];
        foreach ($resultRun->lijst('SELECT * FROM r_result_run WHERE run_id=' . $this->uid . ' AND r_id=' . $r->getUid()) as $result) {
            $resultArray[] = $result->getValue();
        }
        if (count($resultArray) == 0) {
            return NULL;
        }
        return $resultArray;
    }

    public function saveRResult(R $r, $result)
    {
        foreach ($result as $value) {
            $resultRun = new RResultRun();
            $resultRun->setRunId($this->uid);
            $resultRun->setRId($r->getUid());
            $resultRun->setValue($value);
            $resultRun->saveOrUpdate();
        }
    }

    private function addDynojet($data)
    {
        $time_start = Datum::microtime_float();
        echo 'run: ' . $this->uid . '<br/>';

        $dynojet = new Dynojet($data);

        $humidity = $dynojet->getHumidity();
        echo 'Humidity: ' . $humidity . '<br/>';
        $this->setHumidity($humidity);

        $temp = $dynojet->getTemp();
        echo 'Temp: ' . $temp . '<br/>';
        $this->setTemperature($temp);

        $atm = $dynojet->getAtm();
        echo 'Atm: ' . $atm . '<br/>';
        $this->setAtmPressure($atm);

        $desc = $dynojet->getDescription();
        echo 'Omschrijving: ' . $desc . '<br/>';
        $this->setOmschrijving($desc);

        $this->saveOrUpdate();

//echo $dynojet->getHexData()
        $rpmFiltered = [];
        $rpmData = $dynojet->getRpmBlk();
        foreach ($rpmData as $k => $v) {
            if (!empty($prev)) {
                $diff = abs($v - $prev);
                $perc = ($v / $prev) * 100;
                if ($perc < 95 || $perc > 105 || $prev < $v) {
                    $prev = $v;
                } else {
                    $rpmFiltered[] = (1 / ($v / 1000000)) * 600;
                    $rpmTimes[] = $v / 10000000;
                }
            }
            $prev = $v;
        }
        $time_end = Datum::microtime_float();
        $time = $time_end - $time_start;
        echo "<hr/>Loop 1 completed after $time seconds<br/>";
        $rpmMin = round(min($rpmFiltered));
        $rpmMax = round(max($rpmFiltered));
        $rpmRange = $rpmMax - $rpmMin;
        $rpmCount = count($rpmFiltered);
        $rpmTimesTotal = array_sum($rpmTimes);
        echo 'Rpm time: ' . array_sum($rpmTimes) . '<br/>';
        echo 'Min rpm: ' . $rpmMin . '<br/>';
        echo 'Max rpm: ' . $rpmMax . '<br/>';
        echo 'Rpm range: ' . $rpmRange . '<br/>';
        echo 'Rpm count: ' . count($rpmFiltered) . '<br/>';


        $values = $dynojet->getDrumBlk();
//		$i = 2000;
        $j = 0;
        $prevValue = 0;
        $omtrek = 1;
        $rad = (($omtrek / 3.141) / 2) * 0.8; //  * (2.2);
        $acceleration = [];
        $torque = [];
        $drumRpm = [];
        $distance = [];
        $valueCnt = count($values);
        $sum = array_sum($values);
        $avg = $sum / $valueCnt;
        $max = max($values);
        $min = min($values);
        $prev = NULL;
        $d = 0;
        $t = 0;

        echo 'Sum: ' . $sum . '<br/>';
        echo 'Avg: ' . $avg . '<br/>';
        echo 'Max: ' . $max . '<br/>';
        echo 'Min: ' . $min . '<br/>';
        echo "Drum radius: $rad<br/>";
        $prevValue = 0;
        $drumWeight = 379;
        $valueTimes = [];
        $totalTime = 0;
        foreach ($values as $value) {
            $j++;

//			if(!($j%self::MODULUS)) {
//				continue;
//			}
            if ($j % self::MODULUS) {
                continue;
            }
            if ($value <= 2) {
                continue;
            }

            $value /= 10000000;
            if ($prevValue == 0) {
                $prevValue = $value;
                continue;
            }
            if (!empty($prevValue)) {
                $diff = abs($value - $prevValue);
                $perc = ($value / $prevValue) * 100;
                if ($perc < 95 || $perc > 105) {
                    $prevValue = $value;
                    continue;
                }
            }
            $initialSpeed = $omtrek / $prevValue;
            $endSpeed = $omtrek / $value;
            $acc = ($endSpeed - $initialSpeed) / ($value);

            if ($acc > 2 && $acc < 2000) {
//				echo "Acceleration: $acc m/s<br/>";
                $acceleration[] = $acc;
                $avgSpeed = ($endSpeed + $initialSpeed) / 2;
                $drumRpm[] = $avgSpeed / $omtrek;
//				echo "Speed: $avgSpeed m/s (". $avgSpeed*3.6 ." km/h)<br/>";

                $d += $avgSpeed * $value;
                $distance[] = $d;
//				echo "Distance: ($avgSpeed * $value) $d m<br/>";
                $t += $value;

                $force = $drumWeight * $acc;
//				echo "Force: ($drumWeight kg * $acc m/s^2) ". $force ." N<br/>";
//				$t += $value;

                $torq = $force / $rad;
//				echo "Torque: ($force N / $rad m) $torq N/m<br/>";
                $torque[] = $torq;

//				echo "Time: $t<br/>";
                $valueTimes[] = $t;
            }
            $prevValue = $value;
        }
        $time_end = Datum::microtime_float();
        $time = $time_end - $time_start;
        echo "<hr/>Loop 2 completed after $time seconds<br/>";

//		$endSpeed *= 3.6;
        echo "Time: $t<br/>";
        echo "Endspeed: $endSpeed<br/>";
        echo 'Torq count: ' . count($torque) . '<br/>';
        $rpmTimeFactor = ($t / $rpmTimesTotal);
        if (count($torque) < 20) {
            throw new Exception('NoTorq');
        }
        echo 'rpmTimeFactor: ' . $rpmTimeFactor . '<br/>';
        $totalTime = 0;
        foreach ($rpmTimes as &$rpmTime) {
            $rpmTime *= $rpmTimeFactor;
            $totalTime += $rpmTime;
            $rpmTime = $totalTime;
//echo "rpmTime: $rpmTime<br/>";
        }

        $rpm = $rpmMin;

        echo 'Start rpm: ' . $rpmMin . '<br/>';
//var_export($torque);
        $prevTorq = NULL;
        $time_end = Datum::microtime_float();
        $time = $time_end - $time_start;
        echo "<hr/>Loop 3 started after $time seconds<br/>";
        $sql = 'INSERT INTO dyno (run_id, kw, rpm) VALUES ';
        $runId = $this->uid;
        $inserts = [];
        for ($i = 0; $i < count($torque); $i++) {
            $rpm = $this->findRpmByTime($valueTimes[$i], $rpmTimes, $rpmFiltered);
            if (empty($rpm)) {
                continue;
            }
            if (empty($drumRpm[$i])) {
                continue;
            }

            $torq = ($torque[$i] * ($drumRpm[$i] / $rpm));
            if (!empty($prevTorq)) {
                $perc = ($torq / $prevTorq) * 100;
                if ($perc < 80 || $perc > 120) {
                    $prevTorq = $torq;
                    continue;
                }
            }
            $kw = ($torq * $rpm) / 9554;
            $inserts[] = "($runId, $kw, $rpm)";
            $prevTorq = $torq;
        }
        $sql .= implode(',', $inserts);
        $this->execSql($sql);
        $time_end = Datum::microtime_float();
        $time = $time_end - $time_start;

        echo "<hr/>Completed in $time seconds<br/>";
    }

    private function findRpmByTime($time, $rpmTimes, $rpmFiltered)
    {
        $i = 0;
        foreach ($rpmTimes as $rpmTime) {
//echo "time $time  rpmtime $rpmTime<br/>";
            if ($time < $rpmTime) {
//echo '<hr/>';
//				$rpmTime = $prev;
                break;
            }
            $i++;
//			$prev = $rpmTime;
        }
        $rpm = $rpmFiltered[$i];
        return $rpm;
    }

    public function addData($format, $fileName, $data)
    {
        $fileName = mysqli_real_escape_string($fileName);
        $orgData = $data;
        $sql = 'DELETE FROM dyno WHERE run_id=' . $this->uid;

        $this->execSql($sql);
        $ext = substr($fileName, strrpos($fileName, '.') + 1);
        if (preg_match('/0+\d+/', $ext)) {
//		if(in_array($ext, array('001', '002', '003', '004', '005', '006', '007', '008', '009',))) {
// Dynojet file
            return $this->addDynojet($data);
        }
        $data = explode("\n", $data);
        array_shift($data);
        $this->setImportedMessage('');
// alternatief formaat?
        if (preg_match('/^\s(\d*)\s+([\d|.]*)\s+([\d|.]*)\s+(\d*)\s+([\d|.]*)\s*([\d|.]*)\s+([\d|.]*)\s+([\d|.]*)/', $data[0], $regs)) {
            $result = $regs[0];
            $newData = "header\n";
            $o = [];
            foreach ($data as $d) {
                if (preg_match('/^\s(\d*)\s+([\d|.]*)\s+([\d|.]*)\s+(\d*)\s+([\d|.]*)\s*([\d|.]*)\s+([\d|.]*)\s+([\d|.]*)/', $d, $regs)) {

                    array_shift($regs);
                    $newData .= implode("\n", $regs) . "\n";
                } else {
                    foreach ((array)$d as $d1) {
                        if (preg_match('/"(?P<day>\d{2})-(?P<month>\d{2})-(?P<year>\d{4})\s+(?P<hour>\d{2}):(?P<minute>\d{2})",(?P<mbar>[\d|.]*)/', $d1, $regs)) {
                            $year = $regs['year'];
                            if ($year > 2030) {
                                $year -= 98; // fos...
                            }
                            $this->setDatumRun($regs['day'] . '-' . $regs['month'] . '-' . $year);
                            $this->setTijd($regs['hour'] . ':' . $regs['minute']);
                            $this->setAtmPressure($regs['mbar']);
                            continue;
                        }
                        $o[] = $d1;
                    }
                }
            }
            $this->setImportedMessage(implode('<br/>', $o));
            $this->saveOrUpdate();
            $this->addData($format, $fileName, $newData);
            return null;
        }

        $data = array_chunk($data, 8);
        $startRpm = NULL;
        $endRpm = NULL;
        $numOfRecords = 0;
        foreach ($data as $d) {
            if (count($d) == 8) {
                $dyno = new Dyno();
                $dyno->setRunId($this->uid);
                $dyno->setRpm(trim($d[0]));
                $dyno->setKw(trim($d[1]));
                $dyno->saveOrUpdate();

                $numOfRecords++;
                if (empty($startRpm)) {
                    $startRpm = trim($d[0]); // alleen eerste keer
                }
                $endRpm = trim($d[0]); // loopt mee
            } else {
                foreach ((array)$d as $d1) {
                    if (preg_match('/"(?P<day>\d{2})-(?P<month>\d{2})-(?P<year>\d{4})\s+(?P<hour>\d{2}):(?P<minute>\d{2})",(?P<mbar>[\d|.]*)/', $d1, $regs)) {
                        $year = $regs['year'];
                        if ($year > 2030) {
                            $year -= 94;
                        }
                        $this->setDatumRun($regs['day'] . '-' . $regs['month'] . '-' . $year);
                        $this->setTijd($regs['hour'] . ':' . $regs['minute']);
                        $this->setAtmPressure(1000 * $regs['mbar']);
                        continue;
                    }
                    $this->setImportedMessage($this->getImportedMessage() . '<br/>' . $d1);
                }
            }
        }
        $numOfRecords--;
        $interval = ($endRpm - $startRpm) / $numOfRecords;
        $this->setDynoStartRpm($startRpm);
        $this->setDynoEndRpm($endRpm);
        $this->setDynoIntervalRpm($interval);
        $this->setFilename($fileName);
        $this->saveOrUpdate();
        return null;
    }

//	public function setMaxHpRpm($value) {
//		$this->max_hp_rpm = $value;
//	}
//	
//	public function setMaxNmRpm($value) {
//		$this->max_nm_rpm = $value;
//	}

    public function remove()
    {
        $sql = 'DELETE FROM dyno WHERE run_id=' . $this->uid;
        $this->execSql($sql);
        return parent::remove();
    }

    public function getClassName()
    {
        return "Run";
    }

    public function getUid()
    {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id)
    {
        $idName = $this->getPK();
        $this->$idName = $id;
    }

    public function getNumDynoPoints()
    {
        $sql = 'SELECT COUNT(*) FROM vw_dyno WHERE run_id = ' . $this->uid;
        $count = dbTable::getField($sql);
        return $count;
    }

    public function interpolate($kws, $nms, $pivot)
    {
        $oldKw = [];
        $oldNm = [];

        $sql = 'SELECT rpm, kw, nm FROM vw_dyno WHERE run_id= ?';
        $rs = $this->execSql($sql, [$this->uid]);
        while ($row = mysqli_fetch_array($rs)) {
            $oldKw[$row['rpm']] = $row['kw'];
            $oldNm[$row['rpm']] = $row['nm'];
        }

        $f = function($i) {
            return !empty($i);
        };

        $kws = array_filter($kws, $f);
        $nms = array_filter($nms, $f);

        $updates = [];
        foreach ($nms as $rpm => $nm) {
            if (isset($oldNm[$rpm])) {
                if (round($oldNm[$rpm], 4) == $nm) {
                    continue;
                }
            }
            $kw = ($nm * $rpm) / 9554;
            $updates[$rpm] = round($kw, 4);
        }

        $i = 0;
        $j = count($kws);
        foreach ($kws as $rpm => $kw) {
            $i++;
            if (isset($updates[$rpm])) {
                continue;
            }
            if ($i != 1 && $i != $j) {
                if (isset($oldKw[$rpm])) {
                    if (round($oldKw[$rpm], 4) == $kw) {
                        continue;
                    }
                }
            }
            $updates[$rpm] = $kw;
        }
        if (!empty($pivot)) {
            foreach ($pivot as $rpm => $p) {
                if (!isset($updates[$rpm])) {
                    if (isset($kws[$rpm])) {
                        $updates[$rpm] = $kws[$rpm];
                    } elseif (isset($nms[$rpm])) {
                        $updates[$rpm] = ($nms[$rpm] * $rpm) / 9554;
                    }
                }
            }
        }
        if (empty($updates)) {
            $this->setMessages(Strings::show('no_changes_found'), Message::MESSAGE, __CLASS__);
            return;
        }

        switch ($this->getInterpolation()) {
        case self::INTERPOL_LAGRANGE:
            $i = new LaGrange();
            break;
        case self::INTERPOL_CUBIC_SPLINES:
            $i = new CubicSpline();
            break;
        default:
            $i = new CubicSpline();
            break;
        }
        ksort($updates, SORT_NUMERIC);
//		Klog::LogInfo('updates: ' . var_export($updates, true));

        $rows = $i->interpolate($updates, 50);
//		$this->execSql('SET UNIQUE_CHECKS=0');
        $this->execSql('DELETE FROM dyno WHERE run_id= ?', [$this->uid]);
        $runId = $this->uid;

        $values = [];
        foreach ($rows as $row) {
            $placeholders[] = '(?, ?, ?, ?)';
            $values[] = $runId;
            $values[] = $row['x'];
            $values[] = round($row['y'], 4);
            $values[] = (isset($updates[$row['x']]) ? 1 : 0);
        }

        $sql = 'INSERT INTO dyno (run_id, rpm, kw, pivot) VALUES ' . implode(', ', $placeholders);
        $this->execSql($sql, $values);

        // recalculate
        $this->setMaxNmRpm();
        $this->setMaxHpRpm();
    }

    public function copy($fromRunId)
    {
        $fromRun = new Run($fromRunId);
        $this->generate($fromRun->getMaxRpm(), $fromRun->getMaxHp(), $fromRun);
    }

    public function generate($newEndRpm, $newMaxHp, $donorRun = NULL)
    {
//	public function generate($fields, $donorRun = NULL) {
        if ($newEndRpm < 1000) {
            $this->setMessages('end rpm to low', Message::ERROR, __CLASS__);
        }
        if ($newEndRpm > 22500) {
            $this->setMessages('end rpm to high', Message::ERROR, __CLASS__);
        }
        if ($newMaxHp > 2000) {
            $this->setMessages('hp to high', Message::ERROR, __CLASS__);
        }
        if ($newMaxHp < 0.1) {
            $this->setMessages('hp to low', Message::ERROR, __CLASS__);
        }
        if (count($this->getMessages()) > 0) {
            return;
        }
        if (empty($donorRun)) {
            $donorRun = new Run(48);
        }
        $donorEndRpm = $donorRun->getEndRpm();
        $maxRpmFactor = $newEndRpm / $donorEndRpm;
        $donorMaxKw = $donorRun->getMaxKw();
        $maxKwFactor = ($newMaxHp / 1.36) / $donorMaxKw;
        $donorRunId = $donorRun->getUid();

        $this->execSql('DELETE FROM dyno WHERE run_id=' . $this->uid);
        $rs = $this->execSql("SELECT * FROM dyno WHERE run_id =$donorRunId ORDER BY rpm");
        $runId = $this->uid;
        while ($row = mysqli_fetch_assoc($rs)) {
            $inserts[] = "($runId, " . $maxKwFactor * $row['kw'] . ', ' . $maxRpmFactor * $row['rpm'] . ')';
        }
        $sql = 'INSERT INTO dyno (run_id, kw, rpm) VALUES ';
        $sql .= implode(',', $inserts);
        $this->execSql($sql);
        $this->setMessage(new Message('run_copy_success', Message::MESSAGE, 'Run'));
        VehicleDiagram::validateInstance($this);
    }

    /**
     *
     * @param Vehicle $vehicle
     * @param string $power in hp ([hp]@[rpm])
     * @param string $torq in nm ([nm]@[rpm])
     * @return Run
     * @throws Exception
     */
    public function getRunFromStrings(Vehicle $vehicle, $power, $torq)
    {
//Klog::LogInfo(var_export(debug_backtrace(), true));
        $kw = [];
        $nm = [];
        $pivot = [];
        $maxRpm = NULL;
        $kwPower = NULL;
        $kwRpm = NULL;
        $nmRpm = NULL;
        $nmTorq = NULL;

        $p = explode('@', $torq);
        if (isset($p[0]) && isset($p[1])) {
            if (is_numeric($p[0]) && is_numeric($p[1])) {
                $nmTorq = (float)$p[0];
                $nmRpm = (int)$p[1];
            }
        }
        if (empty($nmTorq) || empty($nmRpm)) {
            throw new RunException('InvalidTorq');
        }

        $p = explode('@', $power);
        if (isset($p[0]) && isset($p[1])) {
            if (is_numeric($p[0]) && is_numeric($p[1])) {
                $kwPower = (float)$p[0];
                $kwRpm = (int)$p[1];
                $kwPower /= 1.36;
                $kwTorq = ($kwPower * 9554) / $kwRpm;
                if ($kwTorq >= $nmTorq) {
//					$kwTorq = $nmTorq - 0.1;
//					$kwPower = ($kwTorq * $kwRpm) / 9554;
                    $this->setMessage(new Message("Torque is higher at max hp then at given max nm (" . round($kwTorq, 2) . "nm vs " . round($nmTorq, 2) . 'nm)', Message::WARNING));
                }
                $kw[$kwRpm] = $kwPower;
                $maxRpm = round($kwRpm * 1.1);
                $maxRpm = round($maxRpm / 50) * 50;
            }
        }
        if (empty($kwPower) || empty($kwRpm)) {
            throw new RunException('InvalidPower');
        }

        $nmPower = ($nmTorq * $nmRpm) / 9554;
        if ($nmPower > $kwPower) {
//			$torq = $kwTorq - 0.1;
            $this->setMessage(new Message("Hp is higher at max. nm then at given max hp (" . round($nmPower, 2) . "hp vs " . round($kwPower, 2) . 'hp)', Message::WARNING));
        }

        $rpmRange = [
            'start' => [
                'rpm' => $nmRpm / 4,
                'torq' => $nmTorq / 5,
            ],
            'div' => [8, 4, 2, 1.5],
        ];

        $rpmLow = round($rpmRange['start']['rpm'] / 50) * 50;
        $nmLow = $rpmRange['start']['torq'];
        $nm[$rpmLow] = $nmLow;
        foreach ($rpmRange['div'] as $div) {
            $rpm = $rpmLow + (($nmRpm - $rpmLow) / $div);
            $rpm = round($rpm / 50) * 50;
            $t = $nmLow + (($nmTorq - $nmLow) / $div);
            $nm[$rpm] = $t;
        }

        $rpmDiff = $kwRpm - $nmRpm;
        $f = $rpmDiff / 100;
        $rpmInc = 100;
        $nmDec = ($nmTorq - $kwTorq) / $f;
        $i = 0;
        for ($r = $nmRpm; $r < $kwRpm; $r += $rpmInc) {
            $r = round($r);
            $nm[$r] = $nmTorq - ($nmDec * $i);
            $i++;
        }
        $nm[$maxRpm] = $kwTorq * 0.8;
        foreach ($nm as $rpm => $n) {
            $pivot[$rpm] = 1;
        }
        unset($rpm);
        foreach ($kw as $rpm => $k) {
            $pivot[$rpm] = 1;
        }
        ksort($pivot, SORT_NUMERIC);

        $this->setVehicleId($vehicle->getUid());
        $this->setDynoStartRpm(1000);
        $this->setDynoEndRpm($maxRpm);
        $this->setDynoIntervalRpm(50);
        $this->setRunType('generated');
        $this->setNaam($vehicle->getMerk() . ' ' . $vehicle->getModel());
        $this->setMessages($this->getMessages());
        $this->setMaxHpRpm($power);
        $this->setMaxNmRpm($torq);
        $this->setSessionId(session_id());

        try {
            $this->saveOrUpdate();
        } catch (SecurityException $e) {
            if ($e->getMessage() == 'NotAllowed') {
                $run = clone ($this);
                $run->saveOrUpdate();
                $run->interpolate($kw, $nm, $pivot);
                return $run;
            }
        }

        $this->interpolate($kw, $nm, $pivot);

        return $this;
    }

    public function setMaxNmRpm($value = NULL)
    {
        $maxNmRpm = NULL;
        if (empty ($value)) {
            $sql = 'SELECT nm, rpm FROM vw_dyno WHERE run_id = ? ORDER BY nm DESC LIMIT 1';
            $rs = $this->execSql($sql, [$this->uid]);
            $row = mysqli_fetch_assoc($rs);
            $maxNmRpm = (round($row['nm'], 1) . '@' . $row['rpm']);
        } else {
            $maxNmRpm = $value;
        }
        parent::setMaxNmRpm($maxNmRpm);
    }

    public function setMaxHpRpm($value = NULL)
    {
        $maxHpRpm = NULL;
        if (empty($value)) {
            $sql = 'SELECT hp, rpm FROM vw_dyno WHERE run_id = ? ORDER BY hp DESC LIMIT 1';
            $rs = $this->execSql($sql, [$this->uid]);
            $row = mysqli_fetch_assoc($rs);
            $maxHpRpm = (round($row['hp'], 1) . '@' . $row['rpm']);
        } else {
            $maxHpRpm = $value;
        }
        parent::setMaxHpRpm($maxHpRpm);
    }

    public function getMaxHpRpm()
    {
        if (empty($this->max_hp_rpm)) {
            $this->setMaxHpRpm();
        }
        return $this->max_hp_rpm;
    }


    public function getMaxNmRpm()
    {
        if (empty($this->max_nm_rpm)) {
            $this->setMaxNmRpm();
        }
        return $this->max_nm_rpm;
    }


    public function getMerk()
    {
        $v = new Vehicle($this->getVehicleId());
        return $v->getMerk();
    }

    public function getModel()
    {
        $v = new Vehicle($this->getVehicleId());
        return $v->getModel();
    }

    public function getEngine()
    {
        if ($this->engine_id) {
            return new Engine($this->engine_id);
        }
        return NULL;
    }

    public function getDropdownDefaultValue()
    {
        return '-- select --';
    }

    public function getDropdownNullOption()
    {
        return NULL;
    }

    public function getDynoIntervalRpm()
    {
        return $this->dyno_interval_rpm;
    }

    public function getValue()
    {
        $s = strip_tags(trim($this->getNaam() . ' ' . $this->getOmschrijving() . ' ' . $this->getImportedMessage()));
        if (trim($s) == '') {
            $s = Strings::show('untitled');
        }
        return round($this->getMaxHp(), 2) . Unit::$units['power'] . ' ' . $s;
    }

    public function getIntValueMethod()
    {
        return 'getUid';
    }

}

?>