<?php


class Merken extends _Merken implements DropdownAble
{

    function getValue()
    {
        return $this->getNaam();
    }

    function getDropdownNullOption()
    {
        return '--selecteer--';
    }

    function getDropdownDefaultValue()
    {
        return NULL;
    }

    function getIntValueMethod()
    {
        return 'getUid';
    }

    /* system functions */

    public function getClassName()
    {
        return "Merken";
    }

    public function getUid()
    {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id)
    {
        $idName = $this->getPK();
        $this->$idName = $id;
    }


}

?>