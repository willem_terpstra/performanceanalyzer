<?php

class Strings extends _Strings
{

    private static $data = [];

    private static $instance;

    private static $klogger;

    public function __construct($str = null)
    {
    }

//	private static $lang = NULL;

    public static function init($path)
    {
        self::$instance = new Strings();
//        self::$klogger = new KLogger();
        if (!file_exists('translations')) {
//            self::$klogger->logWarn('translations not loaded');
            return;
        }
        $content = file_get_contents('translations');
        self::$data = unserialize($content);
    }

    public static function show($tag)
    {
        static $s = NULL;
        static $l = NULL;
        if (empty(self::$data)) {
            if (file_exists('translations')) {
//                self::$klogger->logWarn('translations not loaded');
//                return $tag;
                $content = file_get_contents('translations');
                self::$data = unserialize($content);
            }
        }
//		$d = self::$data;
        $tag = strtolower($tag);

//		if (empty($s)) {
//			$s = new Strings();
//		}
        if (empty($l)) {
            $l = Languages::currUid();
        }

        if (is_array(self::$data)) {
            if (isset(self::$data[$l][$tag])) {
                return self::$data[$l][$tag];
            }
        }

        if (empty(self::$data[$l][$tag])) {
//			$rs = mysql_query('SELECT content FROM strings WHERE tag="' . $tag . '" AND languages_id=' . Languages::currUid());
            $rs = self::$instance->execSql(
                'SELECT content FROM strings WHERE tag= ? AND languages_id= ?',
                [
                    $tag, Languages::currUid()
                ]
            );
            $translated = "[$tag]";
            if ($rs instanceof mysqli_result) {
                if (self::$instance->numRows) {
                    $row = mysqli_fetch_assoc($rs);
                    $translated = $row['content'];
                }
            }
            self::$data[$l][$tag] = $translated;
        }
        return self::$data[$l][$tag];
    }

    public static function showByLang($tag, $lang)
    {
        $tag = strtolower($tag);
        if (empty(self::$data[$lang][$tag])) {
            return "[$tag]";
        }
        return self::$data[$lang][$tag];
    }

    /* system functions */

    public function getClassName()
    {
        return "Strings";
    }

    public function getUid()
    {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id)
    {
        $idName = $this->getPK();
        $this->$idName = $id;
    }

}

?>