<?php


class VwVehiclePageInfo extends _VwVehiclePageInfo
{
    /**
     *
     * @var Vehicle $vehicle
     * @return array
     */
    public function getSoapOut()
    {
        return parent::getSoapOut();
    }

    /* Add here your code */
    public function getRun($runId)
    {
        return new Run($runId);
    }

    /* system functions */

    public function getClassName()
    {
        return "VwVehiclePageInfo";
    }

    public function getUid()
    {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id)
    {
        $idName = $this->getPK();
        $this->$idName = $id;
    }


}

?>