<?php


class VwLogin extends _VwLogin
{

    /* Add here your code */
    public static function getUidFromSession()
    {
        $vwLogin = new VwLogin();
        $vwLogin->loadByUseridMd($_SESSION['mdUserid']);
        if (count($vwLogin) == 1) {
            $vwLogin = $vwLogin->first();
            return $vwLogin->getUid();
        }
        return false;
    }

    /* system functions */

    public function getClassName()
    {
        return "VwLogin";
    }

    public function getUid()
    {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id)
    {
        $idName = $this->getPK();
        $this->$idName = $id;
    }


}

?>