<?php


class DiagramR extends _DiagramR
{

    /* Add here your code */
    public function getRId()
    {
        return $this->r_id;
    }


    /* system functions */

    public function getClassName()
    {
        return "DiagramR";
    }

    public function getUid()
    {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id)
    {
        $idName = $this->getPK();
        $this->$idName = $id;
    }


}

?>