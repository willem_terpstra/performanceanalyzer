<?php

class Unit extends _Unit implements DropdownAble
{

//    static private $defaultUnits = [];
    static public $units = [];
    static private $foundFactors = [];

    static public $map = [
        'm/s^2' => 'ft/s^2',
        'km/l' => 'mpg',
        'km' => 'mile',
        'Newton' => 'lbf',
        'm' => 'feet',
        'kg' => 'pound',
        'hp' => 'bhp',
        'kpa' => 'psi',
        'celsius' => 'fahrenheit',
        'sec' => 'sec',
        'km/h' => 'miles/h',
        'm^2' => 'ft^2',
        'nm' => 'lbft',
        'cc' => 'ci',
    ];


//    const DEFAULT_USER_ID_NL = 611; // siuser
//    const DEFAULT_USER_ID_EN = 627; // impuser
    const NO_CONVERSION = false;

    public function __construct($id = null)
    {
        parent::__construct($id);

    }

    public static function convert($unit, &$value): void
    {

        $factor = [
            'ft/s^2' => 3.28084,
            'mpg' => 6.06061,
            'mile' => 0.621371,
            'lbf' => 0.2248,
            'feet' => 3.281,
            'pound' => 2.20462,
            'bhp' => 1.02,
            'psi' => 0.145038,
            'fahrenheit' => function ($celsius) {
                return $celsius * 1.8 + 32;
            },
            'miles/h' => 0.621371,
            'ft^2' => 10.7639,
            'lbft' => 0.735294,
            'ci' => 0.0610237
        ];

        if ($_SESSION['units'] === 'imperial' && !array_key_exists($unit, $factor)) {
            if (array_key_exists($unit, self::$map)) {
                $unit = self::$map[$unit];
            } else {
                return;
            }
        }


        if(!array_key_exists($unit, $factor))
        {
            return;
        }
//        if (!array_key_exists($unit, $factor))
//        {
//            if (array_key_exists($unit, self::$units)) {
//                $unit = self::$units[$unit];
//            }
//            else
//            {
//                return;
//            }
//        }

        $calc = $factor[$unit];
        if (is_callable($calc)) {
            $value = $calc($value);
            return;
        }

        $value *= $calc;
    }

    public static function init()
    {
        self::setUnits();
    }


    public static function setUnits()
    {

        if ($_SESSION['units'] === 'metric') {
            self::$units =
                [
                    'acceleration' => 'm/s^2',
                    'consumption' => 'km/l',
                    'distance' => 'km',
                    'efficiency' => 'bsfc',
                    'flow' => 'cfm',
                    'force' => 'Newton',
                    'length' => 'm',
                    'misc' => 'factor',
                    'weight' => 'kg',
                    'power' => 'hp',
                    'pressure' => 'kpa',
                    'revolutions' => 'rpm',
                    'shortlength' => 'm',
                    'temperature' => 'celsius',
                    'time' => 'sec',
                    'speed' => 'km/h',
                    'surface' => 'm^2',
                    'torq' => 'nm',
                    'volume' => 'cc',
                ];
        } else {
            self::$units =
                [
                    'acceleration' => 'ft/s^2',
                    'consumption' => 'mpg',
                    'efficiency' => 'bsfc',
                    'flow' => 'cfm',
                    'force' => 'lbf',
                    'distance' => 'mile',
                    'length' => 'feet',
                    'misc' => '%',
                    'power' => 'hp',
                    'pressure' => 'psi',
                    'revolutions' => 'rpm',
                    'shortlength' => 'inch',
                    'speed' => 'miles/h',
                    'surface' => 'ft^2',
                    'temperature' => 'fahrenheit',
                    'time' => 'sec',
                    'torq' => 'lbft',
                    'volume' => 'ci',
                    'weight' => 'pound',
                ];
        }
    }

    public static function setUnit($group, $unit)
    {
        if ($group instanceof UnitGroup) {
            $group = $group->getName();
        }
        if ($unit instanceof Unit) {
            $unit = $unit->getNaam();
        }
//        self::$userUnits[$group] == $unit;
        self::$units[$group] = $unit;
        return Strings::show($group) . ': ' . Strings::show($unit);
    }

//    public static function getUnit($group)
//    {
//        $u = self::$units;
//        if (!isset(self::$units[$group])) {
//            self::init();
//            if (!isset(self::$units[$group])) {
//                return self::$defaultUnits[$group];
//            }
////			throw new UnitException($group);
//        };
//        return self::$units[$group];
//    }


    public static function getUnits()
    {
        return self::$units;
    }

    public static function getDefaultUnit($group)
    {
        throw new Exception('Deprecated');
//        return self::$units[$group];
    }

    public static function groupExists($group)
    {
        return isset(self::$units[$group]);
    }

    public function getValue()
    {
        return $this->getNaam();
    }

    public function getIntValueMethod()
    {
        return 'getUid';
    }

    public function getDropdownDefaultValue()
    {
        return NULL;
    }

    public function getDropdownNullOption()
    {
        return NULL;
    }

//    public function getConversion()
//    {
//        return 1;
////        return $this->conversion;
//    }


    public function loadByNaam($naam)
    {
        $unit = new Unit();
        $l = $unit->lijst("SELECT * FROM unit WHERE naam=? LIMIT 1", [$naam]);
        return $l->first();
    }

    public function getNaam()
    {
        return $this->naam;
    }

    public function getUnitGroupId()
    {
        return $this->unit_group_id;
    }

    /* system functions */

    public function getClassName()
    {
        return "Unit";
    }

    public function getUid()
    {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id)
    {
        $idName = $this->getPK();
        $this->$idName = $id;
    }

}

?>