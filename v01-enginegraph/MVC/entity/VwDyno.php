<?php


class VwDyno extends _VwDyno
{

    public static function checkUnit($unit)
    {
        return in_array($unit, ['kw', 'hp', 'nm', 'lbft', 'kgfm']);
    }

    /* Add here your code */
    public static function maxValue($runId, $unit)
    {
        $dyno = new VwDyno();
        $sql = "SELECT * FROM dyno WHERE run_id=$runId ORDER BY $unit DESC LIMIT 1";
        $dyno = $dyno->lijst($sql);
        $dyno = $dyno->first();
        $func = 'get' . ucfirst($unit);
        $pow = $dyno->$func();
        $pow = round($pow, 2);
        return $pow . '@' . $dyno->getRpm();
    }

    /* system functions */

    public function getClassName()
    {
        return "VwDyno";
    }

    public function getUid()
    {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id)
    {
        $idName = $this->getPK();
        $this->$idName = $id;
    }


}

?>