<?php

class Engine extends _Engine implements DropdownAble
{

    function __clone()
    {
        $this->setUid(NULL);
        $this->setUsersId(Authorization::getCurrentUser()->getUid());
        $this->setName($this->getName() . ' ' . Strings::show('copy'));
        foreach ($this->dbModified as &$property) {
            $property = true;
        }
    }

    public function getStandardRun()
    {
        $sql = 'SELECT * FROM engine_tuning_run WHERE engine_id=' . $this->getUid() . ' AND tuning_fase_id=' . TuningFase::STANDARD;
        $rs = $this->execSql($sql);
        if (mysqli_num_rows($rs) == 1) {
            $row = mysqli_fetch_assoc($rs);
            return $row['run_id'];
        }
        return NULL;
    }


    public function saveOrUpdate()
    {
        $bsfc = $this->calcBsfc();
        parent::setBsfc($bsfc);
        parent::saveOrUpdate();
    }

    public function getBsfc()
    {
        if (empty($this->bsfc)) {
            $this->calcBsfc();
        }
        return $this->bsfc;
    }

    public function calcBsfc()
    {
        $verbruikSnelheid = $this->getVerbruikSnelheid();
        $verbruikVolumenAantal = $this->getVerbruikVolumenAantal();
        if (empty($verbruikSnelheid) || empty($verbruikVolumenAantal)) {
            return NULL;
        }
        $vehicle = $this->getVehicle(NULL, true);
        $r = new R();
        $r = $r->loadByName('bsfc');
        $bsfc = $r->exec([
                'cx' => $vehicle->getCx(),
                'frontal_area' => $vehicle->getFrontalArea(),
                'verbruik_snelheid' => $this->getVerbruikSnelheid(),
                'verbruik_volumen_aantal' => $this->getVerbruikVolumenAantal()]
        );
        return $bsfc;
    }

    public function getDropdownDefaultValue()
    {
        return NULL;
    }

    public function getDropdownNullOption()
    {
        return NULL;
    }

    public function getIntValueMethod()
    {
        return 'getUid';
    }

    public function getValue()
    {
        return $this->getName();
    }

    public function getName()
    {
        $name = parent::getName();
        if (empty($name) || $name == 'untitled') {
            $vehicleId = $this->getVehicleId();
            if (!empty($vehicleId)) {
                $vehicle = new Vehicle($vehicleId);
                $name = $vehicle->getMerk() . ' ' . $vehicle->getModel();
                $this->setName($name);
//				$this->saveOrUpdate();
                return $name;
            }
        } else {
            return $name;
        }
        return null;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getShiftRpm()
    {
        if (empty($this->shift_rpm)) {
            return 100000;
        }
        return $this->shift_rpm;
    }

    /* system functions */

    public function getClassName()
    {
        return "Engine";
    }

    public function getUid()
    {
        return $this->uid;
    }

    public function setUid($id)
    {
        $idName = $this->getPK();
        $this->$idName = $id;
    }

}

?>