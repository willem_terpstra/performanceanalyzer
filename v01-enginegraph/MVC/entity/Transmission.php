<?php

class Transmission extends _Transmission implements DropdownAble
{

    public function setG1($t1, $t2)
    {
        if ($t2 == 1) {
            list($t2, $t1) = $this->getCogs($t1);
        }
        $this->versn_g1_t1 = $t1;
        $this->versn_g1_t2 = $t2;
        $this->dbModified['versn_g1_t1'] = true;
        $this->dbModified['versn_g1_t2'] = true;
        $this->verh_versn_1 = $t2 / $t1;
        $this->dbModified['verh_versn_1'] = true;
    }

    public function setG2($t1, $t2)
    {
        if ($t2 == 1) {
            list($t2, $t1) = $this->getCogs($t1);
        }
        $this->versn_g2_t1 = $t1;
        $this->versn_g2_t2 = $t2;
        $this->dbModified['versn_g2_t1'] = true;
        $this->dbModified['versn_g2_t2'] = true;
        $this->verh_versn_2 = $t2 / $t1;
        $this->dbModified['verh_versn_2'] = true;
    }

    public function setG3($t1, $t2)
    {
        if ($t2 == 1) {
            list($t2, $t1) = $this->getCogs($t1);
        }
        $this->versn_g3_t1 = $t1;
        $this->versn_g3_t2 = $t2;
        $this->dbModified['versn_g3_t1'] = true;
        $this->dbModified['versn_g3_t2'] = true;
        $this->verh_versn_3 = $t2 / $t1;
        $this->dbModified['verh_versn_3'] = true;
    }

    public function setG4($t1, $t2)
    {
        if ($t2 == 1) {
            list($t2, $t1) = $this->getCogs($t1);
        }
        $this->versn_g4_t1 = $t1;
        $this->versn_g4_t2 = $t2;
        $this->dbModified['versn_g4_t1'] = true;
        $this->dbModified['versn_g4_t2'] = true;
        $this->verh_versn_4 = $t2 / $t1;
        $this->dbModified['verh_versn_4'] = true;
    }

    public function setG5($t1, $t2)
    {
        if ($t2 == 1) {
            list($t2, $t1) = $this->getCogs($t1);
        }
        $this->versn_g5_t1 = $t1;
        $this->versn_g5_t2 = $t2;
        $this->dbModified['versn_g5_t1'] = true;
        $this->dbModified['versn_g5_t2'] = true;
        $this->verh_versn_5 = $t2 / $t1;
        $this->dbModified['verh_versn_5'] = true;
    }

    public function setG6($t1, $t2)
    {
        if ($t2 == 1) {
            list($t2, $t1) = $this->getCogs($t1);
        }
        $this->versn_g6_t1 = $t1;
        $this->versn_g6_t2 = $t2;
        $this->dbModified['versn_g6_t1'] = true;
        $this->dbModified['versn_g6_t2'] = true;
        $this->verh_versn_6 = $t2 / $t1;
        $this->dbModified['verh_versn_6'] = true;
    }

    function __call($name, $args)
    {
        if (strpos($name, 'setVersnG') !== false) {
            preg_match_all('/setVersnG(\d)T(\d)/', $name, $result, PREG_PATTERN_ORDER);
            $g = $result[1][0];
            $t = $result[2][0];
            $prop = "versn_g{$g}_t{$t}";
            $this->$prop = $args[0];
            $this->dbModified[$prop] = true;
        } elseif (strpos($name, 'getVersnG') !== false) {
            preg_match_all('/getVersnG(\d)T(\d)/', $name, $result, PREG_PATTERN_ORDER);
            $g = $result[1][0];
            $t = $result[2][0];
            $prop = "versn_g{$g}_t{$t}";
            return $this->$prop;
        } elseif (strpos($name, 'setLayshaftG') !== false) {
            preg_match_all('/setLayshaftG(\d)/', $name, $result, PREG_PATTERN_ORDER);
            $g = $result[1][0];
//			$t = $result[2][0];
            $prop = "layshaft_g{$g}";
            $this->$prop = $args[0];
            $this->dbModified[$prop] = true;
        } elseif (strpos($name, 'getLayshaftG') !== false) {
            preg_match_all('/getLayshaftG(\d)/', $name, $result, PREG_PATTERN_ORDER);
            $g = $result[1][0];
//			$t = $result[2][0];
            $prop = "layshaft_g{$g}";
            return $this->$prop;
        } else {
            return parent::__call($name, $args);
        }
        return null;
    }

    function __clone()
    {
        $this->setUid(NULL);
        $this->setUsersId(Authorization::getCurrentUser()->getUid());
        $this->setName($this->getName() . ' ' . Strings::show('copy'));
        foreach ($this->dbModified as &$property) {
            $property = true;
        }
    }

    public function getInputSprocketT()
    {
        return $this->input_sprocket_t;
    }

    public function getLayshaftReduction()
    {
        return $this->layshaft_reduction;
    }

    public function getVerhVersn1()
    {
        return $this->verh_versn_1;
    }

    public function getVerhVersn2()
    {
        return $this->verh_versn_2;
    }

    public function getVerhVersn3()
    {
        return $this->verh_versn_3;
    }

    public function getVerhVersn4()
    {
        return $this->verh_versn_4;
    }

    public function getVerhVersn5()
    {
        return $this->verh_versn_5;
    }

    public function getVerhVersn6()
    {
        return $this->verh_versn_6;
    }

    public function getVersnDirectDrive()
    {
        return $this->versn_direct_drive;
    }

    public function getAantalVersn()
    {
        return $this->aantal_versn;
    }

    public function getDropdownDefaultValue()
    {
        return Strings::show('--select--');
    }

    public function getDropdownNullOption()
    {
        return '--none--';
    }

    public function getIntValueMethod()
    {
        return 'getUid';
    }

    public function getValue()
    {
        return $this->getName() . ', ' . $this->getAantalVersn() . ' ' . Strings::show('num_of_gears');
    }

    public function __toString()
    {
        return $this->getValue();
    }

    /**
     * rekent een waarschijnlijk aantal tanden uit voor een gegeven ratio
     * @param double $ratio
     * @return array
     */
    static public function getCogs($ratio)
    {
        $min_r = 1;
        $a = 0;
        $b = 0;
        for ($i = 10; $i < 100; $i++) {
            $p = $i * $ratio;
            $q = round($p);
            if ($q > $p) {
                $r = $q - $p;
            } else {
                $r = $p - $q;
            }
            if ($r < $min_r) {
                $a = $i;
                $b = $p;
                $min_r = $r;
            }
        }
        return [$a, round($b)];
    }

    /* system functions */

    public function getClassName()
    {
        return "Transmission";
    }

    public function getUid()
    {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id)
    {
        $idName = $this->getPK();
        $this->$idName = $id;
    }

}

?>