<?php

class R extends _R
{

    private static $parsedBefore = [];
    private $result;
    private $offset = 0;
    private $calculatedProperties = [];
    private $parsedFormula;
    private static $cache = [];

    public function __construct($id = null)
    {
        parent::__construct($id);
        if ($this->inactive) {
            throw new RException('Function ' . $this->name . ' deactivated', RException::DEACTIVATED);
        }
    }

    public static function init()
    {
        if (empty(self::$cache)) {
            $r = new R();
            $list = $r->lijst('SELECT * FROM r WHERE inactive=?', [0]);
            foreach ($list as $l) {
                self::$cache[$l->getName()] = $l;
            }
        }
    }

    public function loadByName($name)
    {
        if (isset(self::$cache[$name])) {
            return self::$cache[$name];
        }
        throw new RException('R-formula not found: ' . $name, RException::FORMULA_NOT_FOUND);
    }

    public function getResult(Vehicle $instance)
    {
        $debug = $this->debug_flag;
        $result = [];
        $yValues = $this->_getResult($instance, $debug);
        if ($yValues === NULL) {
            throw new RException('ParseFormulaFailed', RException::PARSE_FORMULA_FAILED, [RException::PARSE_FORMULA_FAILED => $this->parsedFormula]);
        }
        if ($yValues === false) {
            return false;
        }
        $y = $yValues['result'];
        $result['yUnit'] = $this->getUnit();

        $xValueSource = $this->getXValues();
        if (empty($xValueSource)) {
            $xValueSource = 'rpm';
            $x = $instance->getRpm();
            $result['xUnit'] = $xValueSource;
        } else {
            $rX = new R();
            $rX = $rX->loadByName($xValueSource);
            $xValues = $rX->_getResult($instance, $debug);
            $x = $xValues['result'];
//			$result['misc'] = $rX->getCalculatedProperties();
            $result['xUnit'] = $rX->getUnit($rX->getUnitId());
        }
        if ($this->debug_flag) {
            // Klog::LogInfo('num x: ' . count($x) . ' num y: ' . count($y));
        }
        while (count($x) > count($y)) {
            $x = array_slice($x, 1);
        }
        while (count($y) > count($x)) {
            $y = array_slice($y, 1);
        }

        if (!is_array($x)) {
            return false;
        }
        if (!is_array($y)) {
            throw new RException('No valid y-set', RException::PARSE_FORMULA_FAILED);
        }
        $result['data'] = [];
        $c = count($x);
        for ($i = 0; $i < $c; $i++) {
            if ($x[$i] !== NULL and $y[$i] !== NULL) {
                $result['data'][(string)$x[$i]] = $y[$i];
            }
        }

//		if (!array_key_exists('misc', $result)) {
//			$result['misc'] = $this->getCalculatedProperties();
//		}

        return $result;
    }

    private function _getResult($instance, $debug = false)
    {
        if ($this->debug_flag) {
            $debug = true;
        }
        $formula = $this->parse($instance, $this->getContent(), $debug);
        if ($formula === false) {
            return false;
        }
        if ($formula === 'NULL') {
            return false;
        }
        $this->parsedFormula = $formula;

        if (!empty($this->php_function)) {
            $formulas = explode(',', $formula);
            if (empty($formulas[0]) && !empty($formula)) {
                $formulas = [];
                $formulas[] = $formula;
            }
            $results = [];
            foreach ($formulas as $f) {
                if (!empty($f)) {
                    $results[] = PhpR::run($f, $debug, $this);
                }
            }
            $func = 'return ' . $this->php_function . '($results);';
//            $func = create_function('$results', $func);
//            $result = $func($results);
            $result = createFunction(['$results' => $results], $func);
            if ($debug) {
                Klog::LogInfo($this->name . ' php_function: ' . $this->php_function . ' result: ' . var_export($result, true));
            }
        } else {
            $result = PhpR::run($formula, $debug, $this);
        }
        $this->result = [
            'formula' => $formula,
            'result' => $result,
        ];

        global $collect_result;
        global $collected_result;
//		if ($this->name == 'snelheid 2e versn') {
//			echo $this->name . '<br/>';
//		}
        if (in_array($this->name, $collect_result)) {
            if (!isset($collected_result[$this->name])) {
                $collected_result[$this->name] = [];
                $collected_result[$this->name]['data'] = $result;
                if (!empty($this->unit_id)) {
                    $collected_result[$this->name]['unit_group'] = $this->getUnit(NULL, true)->getUnitGroup();
                }
            }
        }
        return $this->result;
    }

    public function exec($params)
    {
        $formula = $this->content;
        foreach ($params as $var => $value) {
            if ($value === NULL) {
                return NULL;
            }
            $formula = str_replace('%' . $var . '%', $value, $formula);
        }
        $result = PhpR::run($formula);
        return $result[0];
    }

    private function parse(Vehicle $instance, $formula, $debug)
    {
        $vehicleUid = $instance->getUid();
        if (Diagram::CACHE) {
            $vehicleUid = $instance->getUid();
            if (!isset(self::$parsedBefore[$this->uid])) {
                self::$parsedBefore[$this->uid] = [];
                self::$parsedBefore[$this->uid]['instances'] = [];
                self::$parsedBefore[$this->uid]['instances'][$vehicleUid] = [];
                self::$parsedBefore[$this->uid]['instances'][$vehicleUid]['instance'] = $instance;
            } else {
                if (isset(self::$parsedBefore[$this->uid]['instances'][$vehicleUid]) && self::$parsedBefore[$this->uid]['instances'][$instance->getUid()]['instance'] === $instance) {
                    return self::$parsedBefore[$this->uid]['instances'][$vehicleUid]['result'];
                } else {
                    self::$parsedBefore[$this->uid]['instances'][$vehicleUid] = [];
                    self::$parsedBefore[$this->uid]['instances'][$vehicleUid]['instance'] = $instance;
                }
            }
        }
//if($this->name == 'evolved_distance_g6') {
//	$ddd = 0;
//}
        // division by zero check
        $nonz = [];
        $result = [];
        preg_match_all('#\d+/%\w+%#', $formula, $result, PREG_PATTERN_ORDER);
        foreach ($result[0] as $division) {
            if (preg_match('/%(\w+)%/', $division, $regs)) {
                $nonz[] = $regs[1];
            }
        }

        $e = NULL;
        preg_match_all('/%([\w|\s]+)%/', $formula, $result, PREG_PATTERN_ORDER);
        $properties = $result[1];
        foreach ($properties as $p) {
            $found = false;
            $orgPropName = $p;
            $value = false;
            // Special property suffixes
            $this->setOffset(0);
            if (strpos($p, '_OFFSET_') !== false) {
                if (method_exists($this, 'setOffset')) {
                    preg_match('/_OFFSET_(?P<offset>\d+)/', $p, $matches);
                    $this->setOffset($matches['offset']);
                    $p = preg_replace('/_OFFSET_\d+/', '', $p);
                }
            }
            $getMax = false;
            if (strpos($p, '_MAX') !== false) {
                $getMax = true;
                $p = preg_replace('/_MAX/', '', $p);
            }
            $getMin = false;
            if (strpos($p, '_MIN') !== false) {
                $getMin = true;
                $p = preg_replace('/_MIN/', '', $p);
            }
            if ($instance->hasProperty($p)) {
                $found = true;
                $func = 'get' . camelCase($p);
                $value = $instance->$func(Unit::NO_CONVERSION);
            } else {
                try {
                    $value = $instance->searchProperty($p);
                } catch (VehicleException $e) {
                    if ($e->getMessage() == 'NoRunSelected') {
                        throw new RException('NoRunSelected', RException::NO_RUN_SELECTED, ['instance' => $instance, 'formula' => $this]);
                    }
                }
                if (is_array($value)) {
                    $found = true;
                    if ($this->offset) {
                        $value = array_slice($value, $this->offset);
                    }
                    if ($getMax) {
                        $value = max($value);
                    }
                    if ($getMin) {
                        $value = min($value);
                    }
//					if ($getSum) {
//						$value = array_sum($value);
//					}
                    $this->calculatedProperties[$orgPropName] = $value;
//					global $collect_result;
//					global $collected_result;
//					if (in_array($orgPropName, $collect_result)) {
//						if (!isset($collected_result[$orgPropName])) {
//							$collected_result[$orgPropName] = array();
//							$collected_result[$orgPropName]['data'] = $result;
//							if (!empty($this->unit_id)) {
//								$collected_result[$orgPropName]['unit_group'] = $this->getUnit(NULL, true)->getUnitGroup();
//							}
//						}
//					}
                    if (is_array($value)) {
                        $value = '[ ' . implode(' ', $value) . ' ]';
                    }
                }
                if ($value === false) {
//if($p == 'evolved_distance_base_g6') {
//	$ddd = 0;
//}					
                    // check formula's
                    $r = new R();
                    $r = $r->loadByName($p);
                    if (count($r) != 0) {
                        $found = true;
                        $ret = $r->_getResult($instance, $debug);

                        if (is_countable($ret['result']) && count($ret['result']) > 1) {
                            if ($this->offset) {
                                $ret['result'] = array_slice($ret['result'], $this->offset);
                            }
                            if ($getMax) {
                                $ret['result'] = max($ret['result']);
                            }
                            if ($getMin) {
                                $ret['result'] = min($ret['result']);
                            }
                            // create array
                            if (is_array($ret['result'])) {
                                array_walk($ret['result'], function (&$value, $key) {
                                    $value = empty($value) ? 'NULL' : $value;
                                });

                                $s = implode(' ', $ret['result']);
                                if (trim($s) == '') {
                                    $value = 'NULL';
                                } else {
                                    $value = '[ ' . $s . ' ]';
                                }
                            } else {
                                $value = $ret['result'];
                            }
                            $this->calculatedProperties[$orgPropName] = $value;
                        } else {
                            if (is_array($ret['result'])) {
                                $value = $ret['result'][0];
                            } else {
                                $value = $ret['result'];
                            }
                            $this->calculatedProperties[$orgPropName] = $value;
                        }
                    }
                }
                if ($value === false) {
                    if (empty($e)) {
                        $e = new RException('value could not be solved:' . $orgPropName, RException::CANNOT_SOLVE_VARIABLE, ['instance' => $instance, 'formula' => $this]);
                    }
                    $e->PropertyNotFound($orgPropName);
                }
                if ($value === NULL) {
                    if (!$found) {
                        throw new RException('property not found: ' . $orgPropName, RException::PROPERTY_NOT_FOUND);
                    }
                    self::$parsedBefore[$this->uid]['instances'][$instance->getUid()]['result'] = false;
                    return false;
                }
            }

            if ($value === 0) {
                if (in_array($p, $nonz)) {
                    $e = new RException('division by zero: ' . $p, RException::DIVISION_BY_ZERO, ['instance' => $instance, 'formula' => $this]);
                }
            }
            $formula = str_replace('%' . $orgPropName . '%', $value, $formula);
        }
        if (!empty($e)) {
            throw($e);
        }
        self::$parsedBefore[$this->uid]['instances'][$vehicleUid]['result'] = $formula;
        return $formula;
    }

    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getName()
    {
        return $this->name;
    }

//    public function getUnitId()
//    {
//        return $this->unit_id;
//    }

    /* system functions */

    public function getCalculatedProperties()
    {
        return $this->calculatedProperties;
    }

    public function getClassName()
    {
        return "R";
    }

    public function getUid()
    {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id)
    {
        $idName = $this->getPK();
        $this->$idName = $id;
    }

    public function __toString()
    {
        return $this->getName();
    }

}

?>