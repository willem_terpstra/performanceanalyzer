<?php

class Languages extends _Languages implements DropdownAble
{

    const EN = 1;
    const NL = 2;

    private static $currIso;
    private static $currUid;

    function __construct($id = NULL)
    {
        if ($id == NULL) {
            if (isset($_SESSION['lang'])) {
                $id = $_SESSION['lang'];
            } else {
                $id = self::NL;
            }
        }
        parent::__construct($id);
    }

    public static function currIso()
    {
        return self::$currIso;
    }

    public static function currUid()
    {
        return self::$currUid;
    }

    /* Add here your code */

    public static function ucCurr()
    {
        return ucfirst(self::currIso());
    }

    public static function showFunc()
    {
        return 'show' . ucfirst(self::currIso());
    }

    public static function getFunc()
    {
        return 'get' . ucfirst(self::currIso());
    }

    public static function setFunc()
    {
        return 'set' . ucfirst(self::currIso());
    }

    public static function setLanguage($languageId)
    {
        $_SESSION['lang'] = $languageId;
        // re-run constructor
        $l = new Languages($languageId);
        self::$currIso = $l->getIso();
        self::$currUid = $l->getUid();
    }

    public function getDropdownDefaultValue()
    {
        return NULL;
    }

    public function getDropdownNullOption()
    {
        return NULL;
    }

    public function getValue()
    {
        return $this->getNaam();
    }

    public function getIntValueMethod()
    {
        return 'getUid';
    }

    /* system functions */

    public function getClassName()
    {
        return "Languages";
    }

    public function getUid()
    {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id)
    {
        $idName = $this->getPK();
        $this->$idName = $id;
    }

}

?>