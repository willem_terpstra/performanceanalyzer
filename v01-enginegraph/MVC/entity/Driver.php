<?php

class Driver extends _Driver
{

    public function getDriverWeight($conversion = true)
    {
//        if ($conversion) {
//            $defaultUnit = Unit::getDefaultUnit('weight');
//            $userUnit = Unit::getUnit('weight');
//            if ($defaultUnit != $userUnit) {
//                return Unit::convert($this->driver_weight, $defaultUnit, $userUnit);
//            }
//        }
        return $this->driver_weight;
    }

    public function setDriverWeight(float $weight) {
        $this->driver_weight = $weight;
    }

    /* system functions */

    public function getClassName()
    {
        return "Driver";
    }

    public function getUid()
    {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id)
    {
        $idName = $this->getPK();
        $this->$idName = $id;
    }

}

?>