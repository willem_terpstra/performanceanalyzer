<?php

class Chassis extends _Chassis
{

    function __clone()
    {
        $this->setUid(NULL);
        $this->setUsersId(Authorization::getCurrentUser()->getUid());
        $this->setModel($this->getModel() . ' ' . Strings::show('copy'));
        foreach ($this->dbModified as &$property) {
            $property = true;
        }
    }

    public function setFrontalArea($value)
    {
        $this->frontal_area = $value;
    }

//    public function getFrontalArea($conversion = true)
//    {
//        if ($conversion) {
//            $defaultUnit = Unit::getDefaultUnit('surface');
//            $userUnit = Unit::getUnit('surface');
//            if ($defaultUnit != $userUnit) {
//                return Unit::convert($this->frontal_area, $defaultUnit, $userUnit);
//            }
//        }
//        return $this->frontal_area;
//    }

    public function getBandAspectRatio()
    {
        return $this->band_aspect_ratio;
    }

    public function getBandmaatAangedrWiel()
    {
        return $this->bandmaat_aangedr_wiel;
    }

    public function getWielmaatAangedrWiel()
    {
        return $this->wielmaat_aangedr_wiel;
    }

    public function getCx()
    {
        return $this->cx;
    }

    public function getDropdownDefaultValue()
    {
        return NULL;
    }

    public function getDropdownNullOption()
    {
        return NULL;
    }

    public function getIntValueMethod()
    {
        return 'getUid';
    }

    public function getValue()
    {
        return $this->getMerken($this->merken_id) . ' ' . $this->getModel();
    }

    /* system functions */

    public function getClassName()
    {
        return "Chassis";
    }

    public function getUid()
    {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id)
    {
        $idName = $this->getPK();
        $this->$idName = $id;
    }

}

?>