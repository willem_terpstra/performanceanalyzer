<?php


class Dyno extends _Dyno
{
//	const SAE_J1349_JUN_90 = 1.18;
//	const SAE_J1349_AUG_04 = 1.176;

    function __clone()
    {
        $this->setUid(NULL);
        foreach ($this->dbModified as &$property) {
            $property = true;
        }
    }

    /*	
        public static function SEA_J1349($pd, $tc, $version = self::SAE_J1349_AUG_04) {
            $cf = $version * ((990/$pd) * sqrt(($tc + 273) / 298)) - $version;
            return $cf;
        }
    */
    public function saveOrUpdate()
    {
        try {
            parent::saveOrUpdate();
        } catch (dbTableException $e) {
            if ($e->getCode() == dbTableException::DUPLICATE_ENTRY) {
                // ignore
            } else {
                throw new dbTableException($e);
            }
        }
    }


    /* system functions */

    public function getClassName()
    {
        return "Dyno";
    }

    public function getUid()
    {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id)
    {
        $idName = $this->getPK();
        $this->$idName = $id;
    }


}

?>