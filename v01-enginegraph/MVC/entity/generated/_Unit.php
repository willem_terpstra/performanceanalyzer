<?php 
class _Unit extends dbTable    {
/* db table */
protected    $tableName = 'unit';
protected    $pk = 'uid';
protected $fieldsInfo = array(
        'uid' => array( 'Type' => "int(10) unsigned", 'Null' => 'NO'),
        'unit_group_id' => array( 'Type' => "int(10) unsigned", 'Null' => 'YES'),
        'naam' => array( 'Type' => "varchar(255)", 'Null' => 'YES'),
        'conversion' => array( 'Type' => "float", 'Null' => 'YES'),
        'type' => array( 'Type' => "set('metric','imperial')", 'Null' => 'YES'),
);

/* modification map */
protected  $dbModified = array (
'uid' => false,'unit_group_id' => false,'naam' => false,'conversion' => false,'type' => false,);

/* properties */
protected $uid;
protected $unit_group_id;
protected $naam;
protected $conversion;
protected $type;

/* soap declarations */
public static $soapDeclarations = array(
        'uid' => array('name' => 'uid', 'type' => 'xsd:string'),
        'unit_group' => array('name' => 'unit_group', 'type' => 'xsd:string'),
        'naam' => array('name' => 'naam', 'type' => 'xsd:string'),
        'conversion' => array('name' => 'conversion', 'type' => 'xsd:string'),
        'type' => array('name' => 'type', 'type' => 'xsd:string'),
);

/* constructor */
public function __construct ($id = null)
{

if ( !empty($id) )
{    $this->load($id);
}

parent::__construct($this->getTableName());
}

public function getClassName()
{    return "_Unit";
}

public function getTableName()
{    return $this->tableName;
}

public function getSoapOut()
{
return array(
        "uid" => $this->uid,
            "unit_group" => $this->getUnitGroup($this->unit_group_id),
            "naam" => $this->naam,
            "conversion" => $this->conversion,
            "type" => $this->type,
    );
}


/* FK methods */
public function getUnitGroupList ($sql = null)    {
$UnitGroup = new UnitGroup();
$list = array();
$list = $UnitGroup->lijst($sql);
return $list;
}

public function getUnitGroup ($uid = NULL, $getObject = false)    {
if(empty($uid)) {
$uid = $this->getUnitGroupId();
}
$UnitGroup = new UnitGroup($uid);
if($getObject) {
return $UnitGroup;
}
return $UnitGroup->getValue();
}

}

