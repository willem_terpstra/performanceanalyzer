<?php

class _SecundaryTransmission extends dbTable
{
    /* db table */
    protected $tableName = 'secundary_transmission';
    protected $pk = 'uid';
    protected $fieldsInfo = [
        'uid' => ['Type' => "int(10) unsigned", 'Null' => 'NO'],
        'merken_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'users_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'name' => ['Type' => "varchar(255)", 'Null' => 'YES'],
        'final_t1' => ['Type' => "float", 'Null' => 'YES'],
        'final_t2' => ['Type' => "float", 'Null' => 'YES'],
        'final_reduction' => ['Type' => "float", 'Null' => 'YES'],
        'vehicle_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'secundary_drive_type' => ['Type' => "varchar(64)", 'Null' => 'YES'],
    ];

    /* modification map */
    protected $dbModified = [

        'uid' => false,
        'merken_id' => false,
        'users_id' => false,
        'name' => false,
        'final_t1' => false,
        'final_t2' => false,
        'final_reduction' => false,
        'vehicle_id' => false,
        'secundary_drive_type' => false,];

    /* properties */
    protected $uid;
    protected $merken_id;
    protected $users_id;
    protected $name;
    protected $final_t1;
    protected $final_t2;
    protected $final_reduction;
    protected $vehicle_id;
    protected $secundary_drive_type;

    /* soap declarations */
    public static $soapDeclarations = [
        'uid' => ['name' => 'uid', 'type' => 'xsd:string'],
        'merken' => ['name' => 'merken', 'type' => 'xsd:string'],
        'users' => ['name' => 'users', 'type' => 'xsd:string'],
        'name' => ['name' => 'name', 'type' => 'xsd:string'],
        'final_t1' => ['name' => 'final_t1', 'type' => 'xsd:string'],
        'final_t2' => ['name' => 'final_t2', 'type' => 'xsd:string'],
        'final_reduction' => ['name' => 'final_reduction', 'type' => 'xsd:string'],
        'vehicle' => ['name' => 'vehicle', 'type' => 'xsd:string'],
        'secundary_drive_type' => ['name' => 'secundary_drive_type', 'type' => 'xsd:string'],

    ];

    /* constructor */
    public function __construct($id = null)
    {

        if (!empty($id)) {
            $this->load($id);
        }

        parent::__construct($this->getTableName());
    }

    public function getClassName()
    {
        return "_SecundaryTransmission";
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function getSoapOut()
    {
        return [
            "uid" => $this->uid,
            "merken" => $this->getMerken($this->merken_id),
            "users" => $this->getUsers($this->users_id),
            "name" => $this->name,
            "final_t1" => $this->final_t1,
            "final_t2" => $this->final_t2,
            "final_reduction" => $this->final_reduction,
            "vehicle" => $this->getVehicle($this->vehicle_id),
            "secundary_drive_type" => $this->secundary_drive_type,
        ];
    }


    /* FK methods */

    public function getMerkenList($sql = null)
    {
        $Merken = new Merken();
        $list = [];
        $list = $Merken->lijst($sql);
        return $list;
    }

    public function getMerken($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getMerkenId();
        }
        $Merken = new Merken($uid);
        if ($getObject) {
            return $Merken;
        }
        return $Merken->getValue();
    }

    public function getUsersList($sql = null)
    {
        $Users = new Users();
        $list = [];
        $list = $Users->lijst($sql);
        return $list;
    }

    public function getUsers($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getUsersId();
        }
        $Users = new Users($uid);
        if ($getObject) {
            return $Users;
        }
        return $Users->getValue();
    }

}

