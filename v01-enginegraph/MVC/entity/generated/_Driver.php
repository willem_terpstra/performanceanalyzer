<?php 
class _Driver extends dbTable    {
/* db table */
protected    $tableName = 'driver';
protected    $pk = 'uid';
protected $fieldsInfo = array(
          'uid' => [
                    'Type' => "int(10) unsigned",
                    'Null' => 'NO',
                    'Extra' => []
        
            ],
          'users_id' => [
                    'Type' => "int(10) unsigned",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'vehicle_id' => [
                    'Type' => "int(10) unsigned",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'driver_name' => [
                    'Type' => "varchar(255)",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'driver_weight' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => array (
  'unit' => 'kg',
)        
            ],
);

/* modification map */
protected  $dbModified = array (
'uid' => false,'users_id' => false,'vehicle_id' => false,'driver_name' => false,'driver_weight' => false,);

/* properties */
protected $uid;
protected $users_id;
protected $vehicle_id;
protected $driver_name;
protected $driver_weight;

/* soap declarations */
public static $soapDeclarations = array(
        'uid' => array('name' => 'uid', 'type' => 'xsd:string'),
        'users' => array('name' => 'users', 'type' => 'xsd:string'),
        'vehicle' => array('name' => 'vehicle', 'type' => 'xsd:string'),
        'driver_name' => array('name' => 'driver_name', 'type' => 'xsd:string'),
        'driver_weight' => array('name' => 'driver_weight', 'type' => 'xsd:string'),
);

/* constructor */
public function __construct ($id = null)
{

if ( !empty($id) )
{    $this->load($id);
}

parent::__construct($this->getTableName());
}

public function getClassName()
{    return "_Driver";
}

public function getTableName()
{    return $this->tableName;
}

public function getSoapOut()
{
return array(
        "uid" => $this->uid,
            "users" => $this->getUsers($this->users_id),
            "vehicle" => $this->getVehicle($this->vehicle_id),
            "driver_name" => $this->driver_name,
            "driver_weight" => $this->driver_weight,
    );
}


/* FK methods */

}

