<?php

class _UnitGroup extends dbTable
{
    /* db table */
    protected $tableName = 'unit_group';
    protected $pk = 'uid';
    protected $fieldsInfo = [
        'uid' => ['Type' => "int(10) unsigned", 'Null' => 'NO'],
        'name' => ['Type' => "varchar(255)", 'Null' => 'YES'],
        'user_select' => ['Type' => "int(1)", 'Null' => 'YES'],
    ];

    /* modification map */
    protected $dbModified = [

        'uid' => false,
        'name' => false,
        'user_select' => false,];

    /* properties */
    protected $uid;
    protected $name;
    protected $user_select;

    /* soap declarations */
    public static $soapDeclarations = [
        'uid' => ['name' => 'uid', 'type' => 'xsd:string'],
        'name' => ['name' => 'name', 'type' => 'xsd:string'],
        'user_select' => ['name' => 'user_select', 'type' => 'xsd:string'],

    ];

    /* constructor */
    public function __construct($id = null)
    {

        if (!empty($id)) {
            $this->load($id);
        }

        parent::__construct($this->getTableName());
    }

    public function getClassName()
    {
        return "_UnitGroup";
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function getSoapOut()
    {
        return [
            "uid" => $this->uid,
            "name" => $this->name,
            "user_select" => $this->user_select,
        ];
    }


    /* FK methods */

}

