<?php 
class _Engine extends dbTable    {
/* db table */
protected    $tableName = 'engine';
protected    $pk = 'uid';
protected $fieldsInfo = array(
          'uid' => [
                    'Type' => "int(10) unsigned",
                    'Null' => 'NO',
                    'Extra' => []
        
            ],
          'merken_id' => [
                    'Type' => "int(10) unsigned",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'vehicle_id' => [
                    'Type' => "int(10) unsigned",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'transmission_id' => [
                    'Type' => "int(10) unsigned",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'users_id' => [
                    'Type' => "int(10) unsigned",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'name' => [
                    'Type' => "varchar(255)",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'bore' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'stroke' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'compression' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'aantal_cyl' => [
                    'Type' => "int(10)",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'layout' => [
                    'Type' => "char(6)",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'inhoud' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'inh_verb_kamer' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'engine_sprocket_t' => [
                    'Type' => "int(5)",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'verbruik_volumen_aantal' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'verbruik_snelheid' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'bsfc' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'flywheel_mass' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'flywheel_inner_diameter' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'flywheel_outer_diameter' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'shift_rpm' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'principe' => [
                    'Type' => "char(6)",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'starter' => [
                    'Type' => "char(64)",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'intake_system' => [
                    'Type' => "char(64)",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'num_valves' => [
                    'Type' => "int(10)",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'cooling' => [
                    'Type' => "varchar(64)",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
);

/* modification map */
protected  $dbModified = array (
'uid' => false,'merken_id' => false,'vehicle_id' => false,'transmission_id' => false,'users_id' => false,'name' => false,'bore' => false,'stroke' => false,'compression' => false,'aantal_cyl' => false,'layout' => false,'inhoud' => false,'inh_verb_kamer' => false,'engine_sprocket_t' => false,'verbruik_volumen_aantal' => false,'verbruik_snelheid' => false,'bsfc' => false,'flywheel_mass' => false,'flywheel_inner_diameter' => false,'flywheel_outer_diameter' => false,'shift_rpm' => false,'principe' => false,'starter' => false,'intake_system' => false,'num_valves' => false,'cooling' => false,);

/* properties */
protected $uid;
protected $merken_id;
protected $vehicle_id;
protected $transmission_id;
protected $users_id;
protected $name;
protected $bore;
protected $stroke;
protected $compression;
protected $aantal_cyl;
protected $layout;
protected $inhoud;
protected $inh_verb_kamer;
protected $engine_sprocket_t;
protected $verbruik_volumen_aantal;
protected $verbruik_snelheid;
protected $bsfc;
protected $flywheel_mass;
protected $flywheel_inner_diameter;
protected $flywheel_outer_diameter;
protected $shift_rpm;
protected $principe;
protected $starter;
protected $intake_system;
protected $num_valves;
protected $cooling;

/* soap declarations */
public static $soapDeclarations = array(
        'uid' => array('name' => 'uid', 'type' => 'xsd:string'),
        'merken' => array('name' => 'merken', 'type' => 'xsd:string'),
        'vehicle' => array('name' => 'vehicle', 'type' => 'xsd:string'),
        'transmission' => array('name' => 'transmission', 'type' => 'xsd:string'),
        'users' => array('name' => 'users', 'type' => 'xsd:string'),
        'name' => array('name' => 'name', 'type' => 'xsd:string'),
        'bore' => array('name' => 'bore', 'type' => 'xsd:string'),
        'stroke' => array('name' => 'stroke', 'type' => 'xsd:string'),
        'compression' => array('name' => 'compression', 'type' => 'xsd:string'),
        'aantal_cyl' => array('name' => 'aantal_cyl', 'type' => 'xsd:string'),
        'layout' => array('name' => 'layout', 'type' => 'xsd:string'),
        'inhoud' => array('name' => 'inhoud', 'type' => 'xsd:string'),
        'inh_verb_kamer' => array('name' => 'inh_verb_kamer', 'type' => 'xsd:string'),
        'engine_sprocket_t' => array('name' => 'engine_sprocket_t', 'type' => 'xsd:string'),
        'verbruik_volumen_aantal' => array('name' => 'verbruik_volumen_aantal', 'type' => 'xsd:string'),
        'verbruik_snelheid' => array('name' => 'verbruik_snelheid', 'type' => 'xsd:string'),
        'bsfc' => array('name' => 'bsfc', 'type' => 'xsd:string'),
        'flywheel_mass' => array('name' => 'flywheel_mass', 'type' => 'xsd:string'),
        'flywheel_inner_diameter' => array('name' => 'flywheel_inner_diameter', 'type' => 'xsd:string'),
        'flywheel_outer_diameter' => array('name' => 'flywheel_outer_diameter', 'type' => 'xsd:string'),
        'shift_rpm' => array('name' => 'shift_rpm', 'type' => 'xsd:string'),
        'principe' => array('name' => 'principe', 'type' => 'xsd:string'),
        'starter' => array('name' => 'starter', 'type' => 'xsd:string'),
        'intake_system' => array('name' => 'intake_system', 'type' => 'xsd:string'),
        'num_valves' => array('name' => 'num_valves', 'type' => 'xsd:string'),
        'cooling' => array('name' => 'cooling', 'type' => 'xsd:string'),
);

/* constructor */
public function __construct ($id = null)
{

if ( !empty($id) )
{    $this->load($id);
}

parent::__construct($this->getTableName());
}

public function getClassName()
{    return "_Engine";
}

public function getTableName()
{    return $this->tableName;
}

public function getSoapOut()
{
return array(
        "uid" => $this->uid,
            "merken" => $this->getMerken($this->merken_id),
            "vehicle" => $this->getVehicle($this->vehicle_id),
            "transmission" => $this->getTransmission($this->transmission_id),
            "users" => $this->getUsers($this->users_id),
            "name" => $this->name,
            "bore" => $this->bore,
            "stroke" => $this->stroke,
            "compression" => $this->compression,
            "aantal_cyl" => $this->aantal_cyl,
            "layout" => $this->layout,
            "inhoud" => $this->inhoud,
            "inh_verb_kamer" => $this->inh_verb_kamer,
            "engine_sprocket_t" => $this->engine_sprocket_t,
            "verbruik_volumen_aantal" => $this->verbruik_volumen_aantal,
            "verbruik_snelheid" => $this->verbruik_snelheid,
            "bsfc" => $this->bsfc,
            "flywheel_mass" => $this->flywheel_mass,
            "flywheel_inner_diameter" => $this->flywheel_inner_diameter,
            "flywheel_outer_diameter" => $this->flywheel_outer_diameter,
            "shift_rpm" => $this->shift_rpm,
            "principe" => $this->principe,
            "starter" => $this->starter,
            "intake_system" => $this->intake_system,
            "num_valves" => $this->num_valves,
            "cooling" => $this->cooling,
    );
}


/* FK methods */
public function getMerkenList ($sql = null)    {
$Merken = new Merken();
$list = array();
$list = $Merken->lijst($sql);
return $list;
}

public function getMerken ($uid = NULL, $getObject = false)    {
if(empty($uid)) {
$uid = $this->getMerkenId();
}
$Merken = new Merken($uid);
if($getObject) {
return $Merken;
}
return $Merken->getValue();
}
public function getTransmissionList ($sql = null)    {
$Transmission = new Transmission();
$list = array();
$list = $Transmission->lijst($sql);
return $list;
}

public function getTransmission ($uid = NULL, $getObject = false)    {
if(empty($uid)) {
$uid = $this->getTransmissionId();
}
$Transmission = new Transmission($uid);
if($getObject) {
return $Transmission;
}
return $Transmission->getValue();
}
public function getUsersList ($sql = null)    {
$Users = new Users();
$list = array();
$list = $Users->lijst($sql);
return $list;
}

public function getUsers ($uid = NULL, $getObject = false)    {
if(empty($uid)) {
$uid = $this->getUsersId();
}
$Users = new Users($uid);
if($getObject) {
return $Users;
}
return $Users->getValue();
}

}

