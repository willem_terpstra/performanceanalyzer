<?php

class _DiagramR extends dbTable
{
    /* db table */
    protected $tableName = 'diagram_r';
    protected $pk = 'uid';
    protected $fieldsInfo = [
        'uid' => ['Type' => "int(10) unsigned", 'Null' => 'NO'],
        'diagram_id' => ['Type' => "int(10) unsigned", 'Null' => 'NO'],
        'r_id' => ['Type' => "int(10) unsigned", 'Null' => 'NO'],
        'legend' => ['Type' => "varchar(255)", 'Null' => 'YES'],
        'inactive' => ['Type' => "tinyint(1)", 'Null' => 'YES'],
        'collect_result' => ['Type' => "text", 'Null' => 'YES'],
    ];

    /* modification map */
    protected $dbModified = [

        'uid' => false,
        'diagram_id' => false,
        'r_id' => false,
        'legend' => false,
        'inactive' => false,
        'collect_result' => false,];

    /* properties */
    protected $uid;
    protected $diagram_id;
    protected $r_id;
    protected $legend;
    protected $inactive;
    protected $collect_result;

    /* soap declarations */
    public static $soapDeclarations = [
        'uid' => ['name' => 'uid', 'type' => 'xsd:string'],
        'diagram' => ['name' => 'diagram', 'type' => 'xsd:string'],
        'r' => ['name' => 'r', 'type' => 'xsd:string'],
        'legend' => ['name' => 'legend', 'type' => 'xsd:string'],
        'inactive' => ['name' => 'inactive', 'type' => 'xsd:string'],
        'collect_result' => ['name' => 'collect_result', 'type' => 'xsd:string'],

    ];

    /* constructor */
    public function __construct($id = null)
    {

        if (!empty($id)) {
            $this->load($id);
        }

        parent::__construct($this->getTableName());
    }

    public function getClassName()
    {
        return "_DiagramR";
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function getSoapOut()
    {
        return [
            "uid" => $this->uid,
            "diagram" => $this->getDiagram($this->diagram_id),
            "r" => $this->getR($this->r_id),
            "legend" => $this->legend,
            "inactive" => $this->inactive,
            "collect_result" => $this->collect_result,
        ];
    }


    /* FK methods */

    public function getDiagramList($sql = null)
    {
        $Diagram = new Diagram();
        $list = [];
        $list = $Diagram->lijst($sql);
        return $list;
    }

    public function getDiagram($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getDiagramId();
        }
        $Diagram = new Diagram($uid);
        if ($getObject) {
            return $Diagram;
        }
        return $Diagram->getValue();
    }

    public function getRList($sql = null)
    {
        $R = new R();
        $list = [];
        $list = $R->lijst($sql);
        return $list;
    }

    public function getR($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getRId();
        }
        $R = new R($uid);
        if ($getObject) {
            return $R;
        }
        return $R->getValue();
    }

}

