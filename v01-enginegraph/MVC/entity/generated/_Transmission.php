<?php

class _Transmission extends dbTable
{
    /* db table */
    protected $tableName = 'transmission';
    protected $pk = 'uid';
    protected $fieldsInfo = [
        'uid' => ['Type' => "int(10) unsigned", 'Null' => 'NO'],
        'merken_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'users_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'name' => ['Type' => "varchar(255)", 'Null' => 'YES'],
        'aantal_versn' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'layshaft_reduction' => ['Type' => "float", 'Null' => 'YES'],
        'layshaft_g1' => ['Type' => "float", 'Null' => 'YES'],
        'layshaft_g2' => ['Type' => "float", 'Null' => 'YES'],
        'versn_direct_drive' => ['Type' => "int(11)", 'Null' => 'YES'],
        'verh_versn_1' => ['Type' => "float", 'Null' => 'YES'],
        'verh_versn_2' => ['Type' => "float", 'Null' => 'YES'],
        'verh_versn_3' => ['Type' => "float", 'Null' => 'YES'],
        'verh_versn_4' => ['Type' => "float", 'Null' => 'YES'],
        'verh_versn_5' => ['Type' => "float", 'Null' => 'YES'],
        'verh_versn_6' => ['Type' => "float", 'Null' => 'YES'],
        'verh_versn_7' => ['Type' => "float", 'Null' => 'YES'],
        'versn_g1_t1' => ['Type' => "float", 'Null' => 'YES'],
        'versn_g2_t1' => ['Type' => "float", 'Null' => 'YES'],
        'versn_g3_t1' => ['Type' => "float", 'Null' => 'YES'],
        'versn_g4_t1' => ['Type' => "float", 'Null' => 'YES'],
        'versn_g5_t1' => ['Type' => "float", 'Null' => 'YES'],
        'versn_g6_t1' => ['Type' => "float", 'Null' => 'YES'],
        'versn_g7_t1' => ['Type' => "float", 'Null' => 'YES'],
        'versn_g1_t2' => ['Type' => "float", 'Null' => 'YES'],
        'versn_g2_t2' => ['Type' => "float", 'Null' => 'YES'],
        'versn_g3_t2' => ['Type' => "float", 'Null' => 'YES'],
        'versn_g4_t2' => ['Type' => "float", 'Null' => 'YES'],
        'versn_g5_t2' => ['Type' => "float", 'Null' => 'YES'],
        'versn_g6_t2' => ['Type' => "float", 'Null' => 'YES'],
        'versn_g7_t2' => ['Type' => "float", 'Null' => 'YES'],
        'input_sprocket_t' => ['Type' => "float", 'Null' => 'YES'],
        'performance_loss' => ['Type' => "float", 'Null' => 'YES'],
        'vehicle_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
    ];

    /* modification map */
    protected $dbModified = [

        'uid' => false,
        'merken_id' => false,
        'users_id' => false,
        'name' => false,
        'aantal_versn' => false,
        'layshaft_reduction' => false,
        'layshaft_g1' => false,
        'layshaft_g2' => false,
        'versn_direct_drive' => false,
        'verh_versn_1' => false,
        'verh_versn_2' => false,
        'verh_versn_3' => false,
        'verh_versn_4' => false,
        'verh_versn_5' => false,
        'verh_versn_6' => false,
        'verh_versn_7' => false,
        'versn_g1_t1' => false,
        'versn_g2_t1' => false,
        'versn_g3_t1' => false,
        'versn_g4_t1' => false,
        'versn_g5_t1' => false,
        'versn_g6_t1' => false,
        'versn_g7_t1' => false,
        'versn_g1_t2' => false,
        'versn_g2_t2' => false,
        'versn_g3_t2' => false,
        'versn_g4_t2' => false,
        'versn_g5_t2' => false,
        'versn_g6_t2' => false,
        'versn_g7_t2' => false,
        'input_sprocket_t' => false,
        'performance_loss' => false,
        'vehicle_id' => false,];

    /* properties */
    protected $uid;
    protected $merken_id;
    protected $users_id;
    protected $name;
    protected $aantal_versn;
    protected $layshaft_reduction;
    protected $layshaft_g1;
    protected $layshaft_g2;
    protected $versn_direct_drive;
    protected $verh_versn_1;
    protected $verh_versn_2;
    protected $verh_versn_3;
    protected $verh_versn_4;
    protected $verh_versn_5;
    protected $verh_versn_6;
    protected $verh_versn_7;
    protected $versn_g1_t1;
    protected $versn_g2_t1;
    protected $versn_g3_t1;
    protected $versn_g4_t1;
    protected $versn_g5_t1;
    protected $versn_g6_t1;
    protected $versn_g7_t1;
    protected $versn_g1_t2;
    protected $versn_g2_t2;
    protected $versn_g3_t2;
    protected $versn_g4_t2;
    protected $versn_g5_t2;
    protected $versn_g6_t2;
    protected $versn_g7_t2;
    protected $input_sprocket_t;
    protected $performance_loss;
    protected $vehicle_id;

    /* soap declarations */
    public static $soapDeclarations = [
        'uid' => ['name' => 'uid', 'type' => 'xsd:string'],
        'merken' => ['name' => 'merken', 'type' => 'xsd:string'],
        'users' => ['name' => 'users', 'type' => 'xsd:string'],
        'name' => ['name' => 'name', 'type' => 'xsd:string'],
        'aantal_versn' => ['name' => 'aantal_versn', 'type' => 'xsd:string'],
        'layshaft_reduction' => ['name' => 'layshaft_reduction', 'type' => 'xsd:string'],
        'layshaft_g1' => ['name' => 'layshaft_g1', 'type' => 'xsd:string'],
        'layshaft_g2' => ['name' => 'layshaft_g2', 'type' => 'xsd:string'],
        'versn_direct_drive' => ['name' => 'versn_direct_drive', 'type' => 'xsd:string'],
        'verh_versn_1' => ['name' => 'verh_versn_1', 'type' => 'xsd:string'],
        'verh_versn_2' => ['name' => 'verh_versn_2', 'type' => 'xsd:string'],
        'verh_versn_3' => ['name' => 'verh_versn_3', 'type' => 'xsd:string'],
        'verh_versn_4' => ['name' => 'verh_versn_4', 'type' => 'xsd:string'],
        'verh_versn_5' => ['name' => 'verh_versn_5', 'type' => 'xsd:string'],
        'verh_versn_6' => ['name' => 'verh_versn_6', 'type' => 'xsd:string'],
        'verh_versn_7' => ['name' => 'verh_versn_7', 'type' => 'xsd:string'],
        'versn_g1_t1' => ['name' => 'versn_g1_t1', 'type' => 'xsd:string'],
        'versn_g2_t1' => ['name' => 'versn_g2_t1', 'type' => 'xsd:string'],
        'versn_g3_t1' => ['name' => 'versn_g3_t1', 'type' => 'xsd:string'],
        'versn_g4_t1' => ['name' => 'versn_g4_t1', 'type' => 'xsd:string'],
        'versn_g5_t1' => ['name' => 'versn_g5_t1', 'type' => 'xsd:string'],
        'versn_g6_t1' => ['name' => 'versn_g6_t1', 'type' => 'xsd:string'],
        'versn_g7_t1' => ['name' => 'versn_g7_t1', 'type' => 'xsd:string'],
        'versn_g1_t2' => ['name' => 'versn_g1_t2', 'type' => 'xsd:string'],
        'versn_g2_t2' => ['name' => 'versn_g2_t2', 'type' => 'xsd:string'],
        'versn_g3_t2' => ['name' => 'versn_g3_t2', 'type' => 'xsd:string'],
        'versn_g4_t2' => ['name' => 'versn_g4_t2', 'type' => 'xsd:string'],
        'versn_g5_t2' => ['name' => 'versn_g5_t2', 'type' => 'xsd:string'],
        'versn_g6_t2' => ['name' => 'versn_g6_t2', 'type' => 'xsd:string'],
        'versn_g7_t2' => ['name' => 'versn_g7_t2', 'type' => 'xsd:string'],
        'input_sprocket_t' => ['name' => 'input_sprocket_t', 'type' => 'xsd:string'],
        'performance_loss' => ['name' => 'performance_loss', 'type' => 'xsd:string'],
        'vehicle' => ['name' => 'vehicle', 'type' => 'xsd:string'],

    ];

    /* constructor */
    public function __construct($id = null)
    {

        if (!empty($id)) {
            $this->load($id);
        }

        parent::__construct($this->getTableName());
    }

    public function getClassName()
    {
        return "_Transmission";
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function getSoapOut()
    {
        return [
            "uid" => $this->uid,
            "merken" => $this->getMerken($this->merken_id),
            "users" => $this->getUsers($this->users_id),
            "name" => $this->name,
            "aantal_versn" => $this->aantal_versn,
            "layshaft_reduction" => $this->layshaft_reduction,
            "layshaft_g1" => $this->layshaft_g1,
            "layshaft_g2" => $this->layshaft_g2,
            "versn_direct_drive" => $this->versn_direct_drive,
            "verh_versn_1" => $this->verh_versn_1,
            "verh_versn_2" => $this->verh_versn_2,
            "verh_versn_3" => $this->verh_versn_3,
            "verh_versn_4" => $this->verh_versn_4,
            "verh_versn_5" => $this->verh_versn_5,
            "verh_versn_6" => $this->verh_versn_6,
            "verh_versn_7" => $this->verh_versn_7,
            "versn_g1_t1" => $this->versn_g1_t1,
            "versn_g2_t1" => $this->versn_g2_t1,
            "versn_g3_t1" => $this->versn_g3_t1,
            "versn_g4_t1" => $this->versn_g4_t1,
            "versn_g5_t1" => $this->versn_g5_t1,
            "versn_g6_t1" => $this->versn_g6_t1,
            "versn_g7_t1" => $this->versn_g7_t1,
            "versn_g1_t2" => $this->versn_g1_t2,
            "versn_g2_t2" => $this->versn_g2_t2,
            "versn_g3_t2" => $this->versn_g3_t2,
            "versn_g4_t2" => $this->versn_g4_t2,
            "versn_g5_t2" => $this->versn_g5_t2,
            "versn_g6_t2" => $this->versn_g6_t2,
            "versn_g7_t2" => $this->versn_g7_t2,
            "input_sprocket_t" => $this->input_sprocket_t,
            "performance_loss" => $this->performance_loss,
            "vehicle" => $this->getVehicle($this->vehicle_id),
        ];
    }


    /* FK methods */

    public function getMerkenList($sql = null)
    {
        $Merken = new Merken();
        $list = [];
        $list = $Merken->lijst($sql);
        return $list;
    }

    public function getMerken($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getMerkenId();
        }
        $Merken = new Merken($uid);
        if ($getObject) {
            return $Merken;
        }
        return $Merken->getValue();
    }

    public function getUsersList($sql = null)
    {
        $Users = new Users();
        $list = [];
        $list = $Users->lijst($sql);
        return $list;
    }

    public function getUsers($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getUsersId();
        }
        $Users = new Users($uid);
        if ($getObject) {
            return $Users;
        }
        return $Users->getValue();
    }

}

