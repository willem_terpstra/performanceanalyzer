<?php

class _VehicleDiagram extends dbTable
{
    /* db table */
    protected $tableName = 'vehicle_diagram';
    protected $pk = 'uid';
    protected $fieldsInfo = [
        'uid' => ['Type' => "int(10) unsigned", 'Null' => 'NO'],
        'vehicle_id' => ['Type' => "int(10) unsigned", 'Null' => 'NO'],
        'diagram_id' => ['Type' => "int(10) unsigned", 'Null' => 'NO'],
    ];

    /* modification map */
    protected $dbModified = [

        'uid' => false,
        'vehicle_id' => false,
        'diagram_id' => false,];

    /* properties */
    protected $uid;
    protected $vehicle_id;
    protected $diagram_id;

    /* soap declarations */
    public static $soapDeclarations = [
        'uid' => ['name' => 'uid', 'type' => 'xsd:string'],
        'vehicle' => ['name' => 'vehicle', 'type' => 'xsd:string'],
        'diagram' => ['name' => 'diagram', 'type' => 'xsd:string'],

    ];

    /* constructor */
    public function __construct($id = null)
    {

        if (!empty($id)) {
            $this->load($id);
        }

        parent::__construct($this->getTableName());
    }

    public function getClassName()
    {
        return "_VehicleDiagram";
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function getSoapOut()
    {
        return [
            "uid" => $this->uid,
            "vehicle" => $this->getVehicle($this->vehicle_id),
            "diagram" => $this->getDiagram($this->diagram_id),
        ];
    }


    /* FK methods */

    public function getDiagramList($sql = null)
    {
        $Diagram = new Diagram();
        $list = [];
        $list = $Diagram->lijst($sql);
        return $list;
    }

    public function getDiagram($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getDiagramId();
        }
        $Diagram = new Diagram($uid);
        if ($getObject) {
            return $Diagram;
        }
        return $Diagram->getValue();
    }

}

