<?php

class _Languages extends dbTable
{
    /* db table */
    protected $tableName = 'languages';
    protected $pk = 'uid';
    protected $fieldsInfo = [
        'uid' => ['Type' => "int(10) unsigned", 'Null' => 'NO'],
        'naam' => ['Type' => "varchar(255)", 'Null' => 'YES'],
        'iso' => ['Type' => "char(2)", 'Null' => 'YES'],
    ];

    /* modification map */
    protected $dbModified = [

        'uid' => false,
        'naam' => false,
        'iso' => false,];

    /* properties */
    protected $uid;
    protected $naam;
    protected $iso;

    /* soap declarations */
    public static $soapDeclarations = [
        'uid' => ['name' => 'uid', 'type' => 'xsd:string'],
        'naam' => ['name' => 'naam', 'type' => 'xsd:string'],
        'iso' => ['name' => 'iso', 'type' => 'xsd:string'],

    ];

    /* constructor */
    public function __construct($id = null)
    {

        if (!empty($id)) {
            $this->load($id);
        }

        parent::__construct($this->getTableName());
    }

    public function getClassName()
    {
        return "_Languages";
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function getSoapOut()
    {
        return [
            "uid" => $this->uid,
            "naam" => $this->naam,
            "iso" => $this->iso,
        ];
    }


    /* FK methods */

}

