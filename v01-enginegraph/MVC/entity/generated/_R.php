<?php

class _R extends dbTable
{
    /* db table */
    protected $tableName = 'r';
    protected $pk = 'uid';
    protected $fieldsInfo = [
        'uid' => ['Type' => "int(10) unsigned", 'Null' => 'NO'],
        'unit_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'users_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'name' => ['Type' => "varchar(256)", 'Null' => 'YES'],
        'php_function' => ['Type' => "varchar(255)", 'Null' => 'YES'],
        'content' => ['Type' => "text", 'Null' => 'YES'],
        'x_values' => ['Type' => "text", 'Null' => 'YES'],
        'orientation' => ['Type' => "int(10)", 'Null' => 'YES'],
        'color' => ['Type' => "enum('green','red','yellow','blue','orange')", 'Null' => 'YES'],
        'used_units' => ['Type' => "text", 'Null' => 'YES'],
        'debug_flag' => ['Type' => "tinyint(1)", 'Null' => 'YES'],
        'class_name' => ['Type' => "varchar(255)", 'Null' => 'YES'],
        'inactive' => ['Type' => "tinyint(1)", 'Null' => 'YES'],
        'nocache' => ['Type' => "tinyint(1)", 'Null' => 'YES'],
    ];

    /* modification map */
    protected $dbModified = [

        'uid' => false,
        'unit_id' => false,
        'users_id' => false,
        'name' => false,
        'php_function' => false,
        'content' => false,
        'x_values' => false,
        'orientation' => false,
        'color' => false,
        'used_units' => false,
        'debug_flag' => false,
        'class_name' => false,
        'inactive' => false,
        'nocache' => false,];

    /* properties */
    protected $uid;
    protected $unit_id;
    protected $users_id;
    protected $name;
    protected $php_function;
    protected $content;
    protected $x_values;
    protected $orientation;
    protected $color;
    protected $used_units;
    protected $debug_flag;
    protected $class_name;
    protected $inactive;
    protected $nocache;

    /* soap declarations */
    public static $soapDeclarations = [
        'uid' => ['name' => 'uid', 'type' => 'xsd:string'],
        'unit' => ['name' => 'unit', 'type' => 'xsd:string'],
        'users' => ['name' => 'users', 'type' => 'xsd:string'],
        'name' => ['name' => 'name', 'type' => 'xsd:string'],
        'php_function' => ['name' => 'php_function', 'type' => 'xsd:string'],
        'content' => ['name' => 'content', 'type' => 'xsd:string'],
        'x_values' => ['name' => 'x_values', 'type' => 'xsd:string'],
        'orientation' => ['name' => 'orientation', 'type' => 'xsd:string'],
        'color' => ['name' => 'color', 'type' => 'xsd:string'],
        'used_units' => ['name' => 'used_units', 'type' => 'xsd:string'],
        'debug_flag' => ['name' => 'debug_flag', 'type' => 'xsd:string'],
        'class_name' => ['name' => 'class_name', 'type' => 'xsd:string'],
        'inactive' => ['name' => 'inactive', 'type' => 'xsd:string'],
        'nocache' => ['name' => 'nocache', 'type' => 'xsd:string'],

    ];

    /* constructor */
    public function __construct($id = null)
    {

        if (!empty($id)) {
            $this->load($id);
        }

        parent::__construct($this->getTableName());
    }

    public function getClassName()
    {
        return "_R";
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function getSoapOut()
    {
        return [
            "uid" => $this->uid,
            "unit" => $this->getUnit($this->unit_id),
            "users" => $this->getUsers($this->users_id),
            "name" => $this->name,
            "php_function" => $this->php_function,
            "content" => $this->content,
            "x_values" => $this->x_values,
            "orientation" => $this->orientation,
            "color" => $this->color,
            "used_units" => $this->used_units,
            "debug_flag" => $this->debug_flag,
            "class_name" => $this->class_name,
            "inactive" => $this->inactive,
            "nocache" => $this->nocache,
        ];
    }


    /* FK methods */

    public function getUnitList($sql = null)
    {
        $Unit = new Unit();
        $list = [];
        $list = $Unit->lijst($sql);
        return $list;
    }

    public function getUnit($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getUnitId();
        }
        $Unit = new Unit($uid);
        if ($getObject) {
            return $Unit;
        }
        return $Unit->getValue();
    }

    public function getUsersList($sql = null)
    {
        $Users = new Users();
        $list = [];
        $list = $Users->lijst($sql);
        return $list;
    }

    public function getUsers($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getUsersId();
        }
        $Users = new Users($uid);
        if ($getObject) {
            return $Users;
        }
        return $Users->getValue();
    }

}

