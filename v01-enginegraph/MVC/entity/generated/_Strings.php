<?php

class _Strings extends dbTable
{
    /* db table */
    protected $tableName = 'strings';
    protected $pk = 'uid';
    protected $fieldsInfo = [
        'uid' => ['Type' => "int(10) unsigned", 'Null' => 'NO'],
        'languages_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'tag' => ['Type' => "char(64)", 'Null' => 'YES'],
        'content' => ['Type' => "text", 'Null' => 'YES'],
    ];

    /* modification map */
    protected $dbModified = [

        'uid' => false,
        'languages_id' => false,
        'tag' => false,
        'content' => false,];

    /* properties */
    protected $uid;
    protected $languages_id;
    protected $tag;
    protected $content;

    /* soap declarations */
    public static $soapDeclarations = [
        'uid' => ['name' => 'uid', 'type' => 'xsd:string'],
        'languages' => ['name' => 'languages', 'type' => 'xsd:string'],
        'tag' => ['name' => 'tag', 'type' => 'xsd:string'],
        'content' => ['name' => 'content', 'type' => 'xsd:string'],

    ];

    /* constructor */
    public function __construct($id = null)
    {

        if (!empty($id)) {
            $this->load($id);
        }

        parent::__construct($this->getTableName());
    }

    public function getClassName()
    {
        return "_Strings";
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function getSoapOut()
    {
        return [
            "uid" => $this->uid,
            "languages" => $this->getLanguages($this->languages_id),
            "tag" => $this->tag,
            "content" => $this->content,
        ];
    }


    /* FK methods */

    public function getLanguagesList($sql = null)
    {
        $Languages = new Languages();
        $list = [];
        $list = $Languages->lijst($sql);
        return $list;
    }

    public function getLanguages($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getLanguagesId();
        }
        $Languages = new Languages($uid);
        if ($getObject) {
            return $Languages;
        }
        return $Languages->getValue();
    }

}

