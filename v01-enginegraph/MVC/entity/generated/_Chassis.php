<?php 
class _Chassis extends dbTable    {
/* db table */
protected    $tableName = 'chassis';
protected    $pk = 'uid';
protected $fieldsInfo = array(
          'uid' => [
                    'Type' => "int(10) unsigned",
                    'Null' => 'NO',
                    'Extra' => []
        
            ],
          'users_id' => [
                    'Type' => "int(10) unsigned",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'merken_id' => [
                    'Type' => "int(10) unsigned",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'vehicle_type_id' => [
                    'Type' => "int(10) unsigned",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'model' => [
                    'Type' => "varchar(255)",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'verh_secundair' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'wielmaat_aangedr_wiel' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'bandmaat_aangedr_wiel' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'band_aspect_ratio' => [
                    'Type' => "int(10) unsigned",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'cx' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'frontal_area' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'gewicht' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'vehicle_id' => [
                    'Type' => "int(10) unsigned",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'fuel_tank_capacity' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'lengte' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'front_brake_system' => [
                    'Type' => "varchar(255)",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'rear_brake_system' => [
                    'Type' => "varchar(255)",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'epos_drive' => [
                    'Type' => "enum('FF','FR','MR','RR')",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'undriven_wheel_size' => [
                    'Type' => "varchar(255)",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
);

/* modification map */
protected  $dbModified = array (
'uid' => false,'users_id' => false,'merken_id' => false,'vehicle_type_id' => false,'model' => false,'verh_secundair' => false,'wielmaat_aangedr_wiel' => false,'bandmaat_aangedr_wiel' => false,'band_aspect_ratio' => false,'cx' => false,'frontal_area' => false,'gewicht' => false,'vehicle_id' => false,'fuel_tank_capacity' => false,'lengte' => false,'front_brake_system' => false,'rear_brake_system' => false,'epos_drive' => false,'undriven_wheel_size' => false,);

/* properties */
protected $uid;
protected $users_id;
protected $merken_id;
protected $vehicle_type_id;
protected $model;
protected $verh_secundair;
protected $wielmaat_aangedr_wiel;
protected $bandmaat_aangedr_wiel;
protected $band_aspect_ratio;
protected $cx;
protected $frontal_area;
protected $gewicht;
protected $vehicle_id;
protected $fuel_tank_capacity;
protected $lengte;
protected $front_brake_system;
protected $rear_brake_system;
protected $epos_drive;
protected $undriven_wheel_size;

/* soap declarations */
public static $soapDeclarations = array(
        'uid' => array('name' => 'uid', 'type' => 'xsd:string'),
        'users' => array('name' => 'users', 'type' => 'xsd:string'),
        'merken' => array('name' => 'merken', 'type' => 'xsd:string'),
        'vehicle_type' => array('name' => 'vehicle_type', 'type' => 'xsd:string'),
        'model' => array('name' => 'model', 'type' => 'xsd:string'),
        'verh_secundair' => array('name' => 'verh_secundair', 'type' => 'xsd:string'),
        'wielmaat_aangedr_wiel' => array('name' => 'wielmaat_aangedr_wiel', 'type' => 'xsd:string'),
        'bandmaat_aangedr_wiel' => array('name' => 'bandmaat_aangedr_wiel', 'type' => 'xsd:string'),
        'band_aspect_ratio' => array('name' => 'band_aspect_ratio', 'type' => 'xsd:string'),
        'cx' => array('name' => 'cx', 'type' => 'xsd:string'),
        'frontal_area' => array('name' => 'frontal_area', 'type' => 'xsd:string'),
        'gewicht' => array('name' => 'gewicht', 'type' => 'xsd:string'),
        'vehicle' => array('name' => 'vehicle', 'type' => 'xsd:string'),
        'fuel_tank_capacity' => array('name' => 'fuel_tank_capacity', 'type' => 'xsd:string'),
        'lengte' => array('name' => 'lengte', 'type' => 'xsd:string'),
        'front_brake_system' => array('name' => 'front_brake_system', 'type' => 'xsd:string'),
        'rear_brake_system' => array('name' => 'rear_brake_system', 'type' => 'xsd:string'),
        'epos_drive' => array('name' => 'epos_drive', 'type' => 'xsd:string'),
        'undriven_wheel_size' => array('name' => 'undriven_wheel_size', 'type' => 'xsd:string'),
);

/* constructor */
public function __construct ($id = null)
{

if ( !empty($id) )
{    $this->load($id);
}

parent::__construct($this->getTableName());
}

public function getClassName()
{    return "_Chassis";
}

public function getTableName()
{    return $this->tableName;
}

public function getSoapOut()
{
return array(
        "uid" => $this->uid,
            "users" => $this->getUsers($this->users_id),
            "merken" => $this->getMerken($this->merken_id),
            "vehicle_type" => $this->getVehicleType($this->vehicle_type_id),
            "model" => $this->model,
            "verh_secundair" => $this->verh_secundair,
            "wielmaat_aangedr_wiel" => $this->wielmaat_aangedr_wiel,
            "bandmaat_aangedr_wiel" => $this->bandmaat_aangedr_wiel,
            "band_aspect_ratio" => $this->band_aspect_ratio,
            "cx" => $this->cx,
            "frontal_area" => $this->frontal_area,
            "gewicht" => $this->gewicht,
            "vehicle" => $this->getVehicle($this->vehicle_id),
            "fuel_tank_capacity" => $this->fuel_tank_capacity,
            "lengte" => $this->lengte,
            "front_brake_system" => $this->front_brake_system,
            "rear_brake_system" => $this->rear_brake_system,
            "epos_drive" => $this->epos_drive,
            "undriven_wheel_size" => $this->undriven_wheel_size,
    );
}


/* FK methods */
public function getUsersList ($sql = null)    {
$Users = new Users();
$list = array();
$list = $Users->lijst($sql);
return $list;
}

public function getUsers ($uid = NULL, $getObject = false)    {
if(empty($uid)) {
$uid = $this->getUsersId();
}
$Users = new Users($uid);
if($getObject) {
return $Users;
}
return $Users->getValue();
}
public function getMerkenList ($sql = null)    {
$Merken = new Merken();
$list = array();
$list = $Merken->lijst($sql);
return $list;
}

public function getMerken ($uid = NULL, $getObject = false)    {
if(empty($uid)) {
$uid = $this->getMerkenId();
}
$Merken = new Merken($uid);
if($getObject) {
return $Merken;
}
return $Merken->getValue();
}
public function getVehicleTypeList ($sql = null)    {
$VehicleType = new VehicleType();
$list = array();
$list = $VehicleType->lijst($sql);
return $list;
}

public function getVehicleType ($uid = NULL, $getObject = false)    {
if(empty($uid)) {
$uid = $this->getVehicleTypeId();
}
$VehicleType = new VehicleType($uid);
if($getObject) {
return $VehicleType;
}
return $VehicleType->getValue();
}

}

