<?php

class _Users extends dbTable
{
    /* db table */
    protected $tableName = 'users';
    protected $pk = 'uid';
    protected $fieldsInfo = [
        'uid' => ['Type' => "int(10) unsigned", 'Null' => 'NO'],
        'auth_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'languages_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'userid' => ['Type' => "char(32)", 'Null' => 'YES'],
        'passw' => ['Type' => "char(32)", 'Null' => 'YES'],
        'voornaam' => ['Type' => "varchar(255)", 'Null' => 'YES'],
        'achternaam' => ['Type' => "varchar(255)", 'Null' => 'YES'],
        'email' => ['Type' => "varchar(255)", 'Null' => 'YES'],
        'www' => ['Type' => "text", 'Null' => 'YES'],
        'bedrijfsnaam' => ['Type' => "varchar(255)", 'Null' => 'YES'],
    ];

    /* modification map */
    protected $dbModified = [

        'uid' => false,
        'auth_id' => false,
        'languages_id' => false,
        'userid' => false,
        'passw' => false,
        'voornaam' => false,
        'achternaam' => false,
        'email' => false,
        'www' => false,
        'bedrijfsnaam' => false,];

    /* properties */
    protected $uid;
    protected $auth_id;
    protected $languages_id;
    protected $userid;
    protected $passw;
    protected $voornaam;
    protected $achternaam;
    protected $email;
    protected $www;
    protected $bedrijfsnaam;

    /* soap declarations */
    public static $soapDeclarations = [
        'uid' => ['name' => 'uid', 'type' => 'xsd:string'],
        'auth' => ['name' => 'auth', 'type' => 'xsd:string'],
        'languages' => ['name' => 'languages', 'type' => 'xsd:string'],
        'userid' => ['name' => 'userid', 'type' => 'xsd:string'],
        'passw' => ['name' => 'passw', 'type' => 'xsd:string'],
        'voornaam' => ['name' => 'voornaam', 'type' => 'xsd:string'],
        'achternaam' => ['name' => 'achternaam', 'type' => 'xsd:string'],
        'email' => ['name' => 'email', 'type' => 'xsd:string'],
        'www' => ['name' => 'www', 'type' => 'xsd:string'],
        'bedrijfsnaam' => ['name' => 'bedrijfsnaam', 'type' => 'xsd:string'],

    ];

    /* constructor */
    public function __construct($id = null)
    {

        if (!empty($id)) {
            $this->load($id);
        }

        parent::__construct($this->getTableName());
    }

    public function getClassName()
    {
        return "_Users";
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function getSoapOut()
    {
        return [
            "uid" => $this->uid,
            "auth" => $this->getAuth($this->auth_id),
            "languages" => $this->getLanguages($this->languages_id),
            "userid" => $this->userid,
            "passw" => $this->passw,
            "voornaam" => $this->voornaam,
            "achternaam" => $this->achternaam,
            "email" => $this->email,
            "www" => $this->www,
            "bedrijfsnaam" => $this->bedrijfsnaam,
        ];
    }


    /* FK methods */

    public function getAuthList($sql = null)
    {
        $Auth = new Auth();
        $list = [];
        $list = $Auth->lijst($sql);
        return $list;
    }

    public function getAuth($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getAuthId();
        }
        $Auth = new Auth($uid);
        if ($getObject) {
            return $Auth;
        }
        return $Auth->getValue();
    }

    public function getLanguagesList($sql = null)
    {
        $Languages = new Languages();
        $list = [];
        $list = $Languages->lijst($sql);
        return $list;
    }

    public function getLanguages($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getLanguagesId();
        }
        $Languages = new Languages($uid);
        if ($getObject) {
            return $Languages;
        }
        return $Languages->getValue();
    }

}

