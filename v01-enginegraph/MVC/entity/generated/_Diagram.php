<?php

class _Diagram extends dbTable
{
    /* db table */
    protected $tableName = 'diagram';
    protected $pk = 'uid';
    protected $fieldsInfo = [
        'uid' => ['Type' => "int(10) unsigned", 'Null' => 'NO'],
        'is_active' => ['Type' => "tinyint(1)", 'Null' => 'YES'],
        'x_unit_group_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'y_unit_group_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'name' => ['Type' => "varchar(255)", 'Null' => 'YES'],
        'description' => ['Type' => "text", 'Null' => 'YES'],
        'sort_order' => ['Type' => "int(11)", 'Null' => 'YES'],
        'collect_result' => ['Type' => "text", 'Null' => 'YES'],
    ];

    /* modification map */
    protected $dbModified = [

        'uid' => false,
        'is_active' => false,
        'x_unit_group_id' => false,
        'y_unit_group_id' => false,
        'name' => false,
        'description' => false,
        'sort_order' => false,
        'collect_result' => false,];

    /* properties */
    protected $uid;
    protected $is_active;
    protected $x_unit_group_id;
    protected $y_unit_group_id;
    protected $name;
    protected $description;
    protected $sort_order;
    protected $collect_result;

    /* soap declarations */
    public static $soapDeclarations = [
        'uid' => ['name' => 'uid', 'type' => 'xsd:string'],
        'is_active' => ['name' => 'is_active', 'type' => 'xsd:string'],
        'x_unit_group' => ['name' => 'x_unit_group', 'type' => 'xsd:string'],
        'y_unit_group' => ['name' => 'y_unit_group', 'type' => 'xsd:string'],
        'name' => ['name' => 'name', 'type' => 'xsd:string'],
        'description' => ['name' => 'description', 'type' => 'xsd:string'],
        'sort_order' => ['name' => 'sort_order', 'type' => 'xsd:string'],
        'collect_result' => ['name' => 'collect_result', 'type' => 'xsd:string'],

    ];

    /* constructor */
    public function __construct($id = null)
    {

        if (!empty($id)) {
            $this->load($id);
        }

        parent::__construct($this->getTableName());
    }

    public function getClassName()
    {
        return "_Diagram";
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function getSoapOut()
    {
        return [
            "uid" => $this->uid,
            "is_active" => $this->is_active,
            "x_unit_group" => $this->getXUnitGroup($this->x_unit_group_id),
            "y_unit_group" => $this->getYUnitGroup($this->y_unit_group_id),
            "name" => $this->name,
            "description" => $this->description,
            "sort_order" => $this->sort_order,
            "collect_result" => $this->collect_result,
        ];
    }


    /* FK methods */

    public function getXUnitGroupList($sql = null)
    {
        $UnitGroup = new UnitGroup();
        $list = [];
        $list = $UnitGroup->lijst($sql);
        return $list;
    }

    public function getXUnitGroup($uid = NULL, $getObject = false): string|UnitGroup
    {
        if (empty($uid)) {
            $uid = $this->getXUnitGroupId();
        }
        $UnitGroup = new UnitGroup($uid);
        if ($getObject) {
            return $UnitGroup;
        }
        return $UnitGroup->getValue();
    }

    public function getYUnitGroupList($sql = null)
    {
        $UnitGroup = new UnitGroup();
        $list = [];
        $list = $UnitGroup->lijst($sql);
        return $list;
    }

    public function getYUnitGroup($uid = NULL, $getObject = false): string|UnitGroup
    {
        if (empty($uid)) {
            $uid = $this->getYUnitGroupId();
        }
        $UnitGroup = new UnitGroup($uid);
        if ($getObject) {
            return $UnitGroup;
        }
        return $UnitGroup->getValue();
    }

}

