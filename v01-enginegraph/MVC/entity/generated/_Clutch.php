<?php 
class _Clutch extends dbTable    {
/* db table */
protected    $tableName = 'clutch';
protected    $pk = 'uid';
protected $fieldsInfo = array(
          'uid' => [
                    'Type' => "int(10) unsigned",
                    'Null' => 'NO',
                    'Extra' => []
        
            ],
          'users_id' => [
                    'Type' => "int(10) unsigned",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'vehicle_id' => [
                    'Type' => "int(10) unsigned",
                    'Null' => 'NO',
                    'Extra' => []
        
            ],
          'name' => [
                    'Type' => "varchar(255)",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'innerdiameter' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'outerdiameter' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'plates' => [
                    'Type' => "int(11)",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'dry' => [
                    'Type' => "tinyint(1)",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'fcoeff' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'springpressure' => [
                    'Type' => "float",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
          'springs' => [
                    'Type' => "int(11)",
                    'Null' => 'YES',
                    'Extra' => []
        
            ],
);

/* modification map */
protected  $dbModified = array (
'uid' => false,'users_id' => false,'vehicle_id' => false,'name' => false,'innerdiameter' => false,'outerdiameter' => false,'plates' => false,'dry' => false,'fcoeff' => false,'springpressure' => false,'springs' => false,);

/* properties */
protected $uid;
protected $users_id;
protected $vehicle_id;
protected $name;
protected $innerdiameter;
protected $outerdiameter;
protected $plates;
protected $dry;
protected $fcoeff;
protected $springpressure;
protected $springs;

/* soap declarations */
public static $soapDeclarations = array(
        'uid' => array('name' => 'uid', 'type' => 'xsd:string'),
        'users' => array('name' => 'users', 'type' => 'xsd:string'),
        'vehicle' => array('name' => 'vehicle', 'type' => 'xsd:string'),
        'name' => array('name' => 'name', 'type' => 'xsd:string'),
        'innerdiameter' => array('name' => 'innerdiameter', 'type' => 'xsd:string'),
        'outerdiameter' => array('name' => 'outerdiameter', 'type' => 'xsd:string'),
        'plates' => array('name' => 'plates', 'type' => 'xsd:string'),
        'dry' => array('name' => 'dry', 'type' => 'xsd:string'),
        'fcoeff' => array('name' => 'fcoeff', 'type' => 'xsd:string'),
        'springpressure' => array('name' => 'springpressure', 'type' => 'xsd:string'),
        'springs' => array('name' => 'springs', 'type' => 'xsd:string'),
);

/* constructor */
public function __construct ($id = null)
{

if ( !empty($id) )
{    $this->load($id);
}

parent::__construct($this->getTableName());
}

public function getClassName()
{    return "_Clutch";
}

public function getTableName()
{    return $this->tableName;
}

public function getSoapOut()
{
return array(
        "uid" => $this->uid,
            "users" => $this->getUsers($this->users_id),
            "vehicle" => $this->getVehicle($this->vehicle_id),
            "name" => $this->name,
            "innerdiameter" => $this->innerdiameter,
            "outerdiameter" => $this->outerdiameter,
            "plates" => $this->plates,
            "dry" => $this->dry,
            "fcoeff" => $this->fcoeff,
            "springpressure" => $this->springpressure,
            "springs" => $this->springs,
    );
}


/* FK methods */
public function getUsersList ($sql = null)    {
$Users = new Users();
$list = array();
$list = $Users->lijst($sql);
return $list;
}

public function getUsers ($uid = NULL, $getObject = false)    {
if(empty($uid)) {
$uid = $this->getUsersId();
}
$Users = new Users($uid);
if($getObject) {
return $Users;
}
return $Users->getValue();
}

}

