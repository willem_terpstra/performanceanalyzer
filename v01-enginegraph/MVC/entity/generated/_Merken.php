<?php

class _Merken extends dbTable
{
    /* db table */
    protected $tableName = 'merken';
    protected $pk = 'uid';
    protected $fieldsInfo = [
        'uid' => ['Type' => "int(10) unsigned", 'Null' => 'NO'],
        'naam' => ['Type' => "varchar(255)", 'Null' => 'YES'],
        'merken_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
    ];

    /* modification map */
    protected $dbModified = [

        'uid' => false,
        'naam' => false,
        'merken_id' => false,];

    /* properties */
    protected $uid;
    protected $naam;
    protected $merken_id;

    /* soap declarations */
    public static $soapDeclarations = [
        'uid' => ['name' => 'uid', 'type' => 'xsd:string'],
        'naam' => ['name' => 'naam', 'type' => 'xsd:string'],
        'merken' => ['name' => 'merken', 'type' => 'xsd:string'],

    ];

    /* constructor */
    public function __construct($id = null)
    {

        if (!empty($id)) {
            $this->load($id);
        }

        parent::__construct($this->getTableName());
    }

    public function getClassName()
    {
        return "_Merken";
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function getSoapOut()
    {
        return [
            "uid" => $this->uid,
            "naam" => $this->naam,
            "merken" => $this->getMerken($this->merken_id),
        ];
    }


    /* FK methods */

    public function getMerkenList($sql = null)
    {
        $Merken = new Merken();
        $list = [];
        $list = $Merken->lijst($sql);
        return $list;
    }

    public function getMerken($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getMerkenId();
        }
        $Merken = new Merken($uid);
        if ($getObject) {
            return $Merken;
        }
        return $Merken->getValue();
    }

}

