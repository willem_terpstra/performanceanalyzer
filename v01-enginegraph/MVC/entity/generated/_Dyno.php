<?php

class _Dyno extends dbTable
{
    /* db table */
    protected $tableName = 'dyno';
    protected $pk = 'uid';
    protected $fieldsInfo = [
        'uid' => ['Type' => "int(10) unsigned", 'Null' => 'NO'],
        'run_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'kw' => ['Type' => "float", 'Null' => 'YES'],
        'rpm' => ['Type' => "int(11)", 'Null' => 'YES'],
        'lambda' => ['Type' => "float", 'Null' => 'YES'],
        'pivot' => ['Type' => "tinyint(1)", 'Null' => 'YES'],
    ];

    /* modification map */
    protected $dbModified = [

        'uid' => false,
        'run_id' => false,
        'kw' => false,
        'rpm' => false,
        'lambda' => false,
        'pivot' => false,];

    /* properties */
    protected $uid;
    protected $run_id;
    protected $kw;
    protected $rpm;
    protected $lambda;
    protected $pivot;

    /* soap declarations */
    public static $soapDeclarations = [
        'uid' => ['name' => 'uid', 'type' => 'xsd:string'],
        'run' => ['name' => 'run', 'type' => 'xsd:string'],
        'kw' => ['name' => 'kw', 'type' => 'xsd:string'],
        'rpm' => ['name' => 'rpm', 'type' => 'xsd:string'],
        'lambda' => ['name' => 'lambda', 'type' => 'xsd:string'],
        'pivot' => ['name' => 'pivot', 'type' => 'xsd:string'],

    ];

    /* constructor */
    public function __construct($id = null)
    {

        if (!empty($id)) {
            $this->load($id);
        }

        parent::__construct($this->getTableName());
    }

    public function getClassName()
    {
        return "_Dyno";
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function getSoapOut()
    {
        return [
            "uid" => $this->uid,
            "run" => $this->getRun($this->run_id),
            "kw" => $this->kw,
            "rpm" => $this->rpm,
            "lambda" => $this->lambda,
            "pivot" => $this->pivot,
        ];
    }


    /* FK methods */

}

