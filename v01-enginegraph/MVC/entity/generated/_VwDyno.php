<?php 
class _VwDyno extends dbTable    {
/* db table */
protected    $tableName = 'vw_dyno';
protected    $pk = '';
protected $fieldsInfo = array(
        'uid' => array( 'Type' => "int(10) unsigned", 'Null' => 'NO'),
        'run_id' => array( 'Type' => "int(10) unsigned", 'Null' => 'YES'),
        'kw' => array( 'Type' => "float", 'Null' => 'YES'),
        'rpm' => array( 'Type' => "int(11)", 'Null' => 'YES'),
        'rad_sec' => array( 'Type' => "decimal(20,9)", 'Null' => 'YES'),
        'nm' => array( 'Type' => "double", 'Null' => 'YES'),
        'hp' => array( 'Type' => "double(21,4)", 'Null' => 'YES'),
        'pivot' => array( 'Type' => "tinyint(1)", 'Null' => 'YES'),
);

/* modification map */
protected  $dbModified = array (
'uid' => false,'run_id' => false,'kw' => false,'rpm' => false,'rad_sec' => false,'nm' => false,'hp' => false,'pivot' => false,);

/* properties */
protected $uid;
protected $run_id;
protected $kw;
protected $rpm;
protected $rad_sec;
protected $nm;
protected $hp;
protected $pivot;

/* soap declarations */
public static $soapDeclarations = array(
        'uid' => array('name' => 'uid', 'type' => 'xsd:string'),
        'run' => array('name' => 'run', 'type' => 'xsd:string'),
        'kw' => array('name' => 'kw', 'type' => 'xsd:string'),
        'rpm' => array('name' => 'rpm', 'type' => 'xsd:string'),
        'rad_sec' => array('name' => 'rad_sec', 'type' => 'xsd:string'),
        'nm' => array('name' => 'nm', 'type' => 'xsd:string'),
        'hp' => array('name' => 'hp', 'type' => 'xsd:string'),
        'pivot' => array('name' => 'pivot', 'type' => 'xsd:string'),
);

/* constructor */
public function __construct ($id = null)
{
    $this->is_view = true;
    
if ( !empty($id) )
{    $this->load($id);
}

parent::__construct($this->getTableName());
}

public function getClassName()
{    return "_VwDyno";
}

public function getTableName()
{    return $this->tableName;
}

public function getSoapOut()
{
return array(
        "uid" => $this->uid,
            "run" => $this->getRun($this->run_id),
            "kw" => $this->kw,
            "rpm" => $this->rpm,
            "rad_sec" => $this->rad_sec,
            "nm" => $this->nm,
            "hp" => $this->hp,
            "pivot" => $this->pivot,
    );
}


/* FK methods */

}

