<?php

class _VwVehiclePageInfo extends dbTable
{
    /* db table */
    protected $tableName = 'vw_vehicle_page_info';
    protected $pk = '';
    protected $fieldsInfo = [
        'uid' => ['Type' => "int(10) unsigned", 'Null' => 'NO'],
        'updated' => ['Type' => "int(10)", 'Null' => 'YES'],
        'diagram' => ['Type' => "varchar(255)", 'Null' => 'YES'],
        'merk' => ['Type' => "varchar(255)", 'Null' => 'YES'],
        'model' => ['Type' => "varchar(255)", 'Null' => 'YES'],
        'jaar' => ['Type' => "int(4) unsigned", 'Null' => 'YES'],
        'gewicht' => ['Type' => "float", 'Null' => 'YES'],
        'verh_secundair' => ['Type' => "float", 'Null' => 'YES'],
        'wielmaat_aangedr_wiel' => ['Type' => "float", 'Null' => 'YES'],
        'bandmaat_aangedr_wiel' => ['Type' => "float", 'Null' => 'YES'],
        'band_aspect_ratio' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'cx' => ['Type' => "float", 'Null' => 'YES'],
        'fuel_tank_capacity' => ['Type' => "float", 'Null' => 'YES'],
        'frontal_area' => ['Type' => "float", 'Null' => 'YES'],
        'front_brake_system' => ['Type' => "varchar(255)", 'Null' => 'YES'],
        'rear_brake_system' => ['Type' => "varchar(255)", 'Null' => 'YES'],
        'epos_drive' => ['Type' => "enum('FF','FR','MR','RR')", 'Null' => 'YES'],
        'undriven_wheel_size' => ['Type' => "varchar(255)", 'Null' => 'YES'],
        'bore' => ['Type' => "float", 'Null' => 'YES'],
        'stroke' => ['Type' => "float", 'Null' => 'YES'],
        'compression' => ['Type' => "float", 'Null' => 'YES'],
        'aantal_cyl' => ['Type' => "int(10)", 'Null' => 'YES'],
        'layout' => ['Type' => "char(6)", 'Null' => 'YES'],
        'inhoud' => ['Type' => "float", 'Null' => 'YES'],
        'inh_verb_kamer' => ['Type' => "float", 'Null' => 'YES'],
        'engine_sprocket_t' => ['Type' => "int(5)", 'Null' => 'YES'],
        'principe' => ['Type' => "char(6)", 'Null' => 'YES'],
        'starter' => ['Type' => "char(64)", 'Null' => 'YES'],
        'intake_system' => ['Type' => "char(64)", 'Null' => 'YES'],
        'num_valves' => ['Type' => "int(10)", 'Null' => 'YES'],
        'cooling' => ['Type' => "varchar(64)", 'Null' => 'YES'],
        'run_id' => ['Type' => "int(10) unsigned", 'Null' => 'NO'],
        'max_hp_rpm' => ['Type' => "char(64)", 'Null' => 'YES'],
        'max_nm_rpm' => ['Type' => "char(64)", 'Null' => 'YES'],
        'final_t1' => ['Type' => "float", 'Null' => 'YES'],
        'final_t2' => ['Type' => "float", 'Null' => 'YES'],
        'final_reduction' => ['Type' => "float", 'Null' => 'YES'],
        'secundary_drive_type' => ['Type' => "varchar(64)", 'Null' => 'YES'],
        'aantal_versn' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'layshaft_reduction' => ['Type' => "float", 'Null' => 'YES'],
        'layshaft_g1' => ['Type' => "float", 'Null' => 'YES'],
        'layshaft_g2' => ['Type' => "float", 'Null' => 'YES'],
        'verh_versn_1' => ['Type' => "float", 'Null' => 'YES'],
        'verh_versn_2' => ['Type' => "float", 'Null' => 'YES'],
        'verh_versn_3' => ['Type' => "float", 'Null' => 'YES'],
        'verh_versn_4' => ['Type' => "float", 'Null' => 'YES'],
        'verh_versn_5' => ['Type' => "float", 'Null' => 'YES'],
        'verh_versn_6' => ['Type' => "float", 'Null' => 'YES'],
        'verh_versn_7' => ['Type' => "float", 'Null' => 'YES'],
        'input_sprocket_t' => ['Type' => "float", 'Null' => 'YES'],
        'driver_weight' => ['Type' => "float", 'Null' => 'YES'],
        'toepassingsgebied' => ['Type' => "char(64)", 'Null' => 'YES'],
    ];

    /* modification map */
    protected $dbModified = [

        'uid' => false,
        'updated' => false,
        'diagram' => false,
        'merk' => false,
        'model' => false,
        'jaar' => false,
        'gewicht' => false,
        'verh_secundair' => false,
        'wielmaat_aangedr_wiel' => false,
        'bandmaat_aangedr_wiel' => false,
        'band_aspect_ratio' => false,
        'cx' => false,
        'fuel_tank_capacity' => false,
        'frontal_area' => false,
        'front_brake_system' => false,
        'rear_brake_system' => false,
        'epos_drive' => false,
        'undriven_wheel_size' => false,
        'bore' => false,
        'stroke' => false,
        'compression' => false,
        'aantal_cyl' => false,
        'layout' => false,
        'inhoud' => false,
        'inh_verb_kamer' => false,
        'engine_sprocket_t' => false,
        'principe' => false,
        'starter' => false,
        'intake_system' => false,
        'num_valves' => false,
        'cooling' => false,
        'run_id' => false,
        'max_hp_rpm' => false,
        'max_nm_rpm' => false,
        'final_t1' => false,
        'final_t2' => false,
        'final_reduction' => false,
        'secundary_drive_type' => false,
        'aantal_versn' => false,
        'layshaft_reduction' => false,
        'layshaft_g1' => false,
        'layshaft_g2' => false,
        'verh_versn_1' => false,
        'verh_versn_2' => false,
        'verh_versn_3' => false,
        'verh_versn_4' => false,
        'verh_versn_5' => false,
        'verh_versn_6' => false,
        'verh_versn_7' => false,
        'input_sprocket_t' => false,
        'driver_weight' => false,
        'toepassingsgebied' => false,];

    /* properties */
    protected $uid;
    protected $updated;
    protected $diagram;
    protected $merk;
    protected $model;
    protected $jaar;
    protected $gewicht;
    protected $verh_secundair;
    protected $wielmaat_aangedr_wiel;
    protected $bandmaat_aangedr_wiel;
    protected $band_aspect_ratio;
    protected $cx;
    protected $fuel_tank_capacity;
    protected $frontal_area;
    protected $front_brake_system;
    protected $rear_brake_system;
    protected $epos_drive;
    protected $undriven_wheel_size;
    protected $bore;
    protected $stroke;
    protected $compression;
    protected $aantal_cyl;
    protected $layout;
    protected $inhoud;
    protected $inh_verb_kamer;
    protected $engine_sprocket_t;
    protected $principe;
    protected $starter;
    protected $intake_system;
    protected $num_valves;
    protected $cooling;
    protected $run_id;
    protected $max_hp_rpm;
    protected $max_nm_rpm;
    protected $final_t1;
    protected $final_t2;
    protected $final_reduction;
    protected $secundary_drive_type;
    protected $aantal_versn;
    protected $layshaft_reduction;
    protected $layshaft_g1;
    protected $layshaft_g2;
    protected $verh_versn_1;
    protected $verh_versn_2;
    protected $verh_versn_3;
    protected $verh_versn_4;
    protected $verh_versn_5;
    protected $verh_versn_6;
    protected $verh_versn_7;
    protected $input_sprocket_t;
    protected $driver_weight;
    protected $toepassingsgebied;

    /* soap declarations */
    public static $soapDeclarations = [
        'uid' => ['name' => 'uid', 'type' => 'xsd:string'],
        'updated' => ['name' => 'updated', 'type' => 'xsd:string'],
        'diagram' => ['name' => 'diagram', 'type' => 'xsd:string'],
        'merk' => ['name' => 'merk', 'type' => 'xsd:string'],
        'model' => ['name' => 'model', 'type' => 'xsd:string'],
        'jaar' => ['name' => 'jaar', 'type' => 'xsd:string'],
        'gewicht' => ['name' => 'gewicht', 'type' => 'xsd:string'],
        'verh_secundair' => ['name' => 'verh_secundair', 'type' => 'xsd:string'],
        'wielmaat_aangedr_wiel' => ['name' => 'wielmaat_aangedr_wiel', 'type' => 'xsd:string'],
        'bandmaat_aangedr_wiel' => ['name' => 'bandmaat_aangedr_wiel', 'type' => 'xsd:string'],
        'band_aspect_ratio' => ['name' => 'band_aspect_ratio', 'type' => 'xsd:string'],
        'cx' => ['name' => 'cx', 'type' => 'xsd:string'],
        'fuel_tank_capacity' => ['name' => 'fuel_tank_capacity', 'type' => 'xsd:string'],
        'frontal_area' => ['name' => 'frontal_area', 'type' => 'xsd:string'],
        'front_brake_system' => ['name' => 'front_brake_system', 'type' => 'xsd:string'],
        'rear_brake_system' => ['name' => 'rear_brake_system', 'type' => 'xsd:string'],
        'epos_drive' => ['name' => 'epos_drive', 'type' => 'xsd:string'],
        'undriven_wheel_size' => ['name' => 'undriven_wheel_size', 'type' => 'xsd:string'],
        'bore' => ['name' => 'bore', 'type' => 'xsd:string'],
        'stroke' => ['name' => 'stroke', 'type' => 'xsd:string'],
        'compression' => ['name' => 'compression', 'type' => 'xsd:string'],
        'aantal_cyl' => ['name' => 'aantal_cyl', 'type' => 'xsd:string'],
        'layout' => ['name' => 'layout', 'type' => 'xsd:string'],
        'inhoud' => ['name' => 'inhoud', 'type' => 'xsd:string'],
        'inh_verb_kamer' => ['name' => 'inh_verb_kamer', 'type' => 'xsd:string'],
        'engine_sprocket_t' => ['name' => 'engine_sprocket_t', 'type' => 'xsd:string'],
        'principe' => ['name' => 'principe', 'type' => 'xsd:string'],
        'starter' => ['name' => 'starter', 'type' => 'xsd:string'],
        'intake_system' => ['name' => 'intake_system', 'type' => 'xsd:string'],
        'num_valves' => ['name' => 'num_valves', 'type' => 'xsd:string'],
        'cooling' => ['name' => 'cooling', 'type' => 'xsd:string'],
        'run' => ['name' => 'run', 'type' => 'xsd:string'],
        'max_hp_rpm' => ['name' => 'max_hp_rpm', 'type' => 'xsd:string'],
        'max_nm_rpm' => ['name' => 'max_nm_rpm', 'type' => 'xsd:string'],
        'final_t1' => ['name' => 'final_t1', 'type' => 'xsd:string'],
        'final_t2' => ['name' => 'final_t2', 'type' => 'xsd:string'],
        'final_reduction' => ['name' => 'final_reduction', 'type' => 'xsd:string'],
        'secundary_drive_type' => ['name' => 'secundary_drive_type', 'type' => 'xsd:string'],
        'aantal_versn' => ['name' => 'aantal_versn', 'type' => 'xsd:string'],
        'layshaft_reduction' => ['name' => 'layshaft_reduction', 'type' => 'xsd:string'],
        'layshaft_g1' => ['name' => 'layshaft_g1', 'type' => 'xsd:string'],
        'layshaft_g2' => ['name' => 'layshaft_g2', 'type' => 'xsd:string'],
        'verh_versn_1' => ['name' => 'verh_versn_1', 'type' => 'xsd:string'],
        'verh_versn_2' => ['name' => 'verh_versn_2', 'type' => 'xsd:string'],
        'verh_versn_3' => ['name' => 'verh_versn_3', 'type' => 'xsd:string'],
        'verh_versn_4' => ['name' => 'verh_versn_4', 'type' => 'xsd:string'],
        'verh_versn_5' => ['name' => 'verh_versn_5', 'type' => 'xsd:string'],
        'verh_versn_6' => ['name' => 'verh_versn_6', 'type' => 'xsd:string'],
        'verh_versn_7' => ['name' => 'verh_versn_7', 'type' => 'xsd:string'],
        'input_sprocket_t' => ['name' => 'input_sprocket_t', 'type' => 'xsd:string'],
        'driver_weight' => ['name' => 'driver_weight', 'type' => 'xsd:string'],
        'toepassingsgebied' => ['name' => 'toepassingsgebied', 'type' => 'xsd:string'],

    ];

    /* constructor */
    public function __construct($id = null)
    {
        $this->is_view = true;

        if (!empty($id)) {
            $this->load($id);
        }

        parent::__construct($this->getTableName());
    }

    public function getClassName()
    {
        return "_VwVehiclePageInfo";
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function getSoapOut()
    {
        return [
            "uid" => $this->uid,
            "updated" => $this->updated,
            "diagram" => $this->diagram,
            "merk" => $this->merk,
            "model" => $this->model,
            "jaar" => $this->jaar,
            "gewicht" => $this->gewicht,
            "verh_secundair" => $this->verh_secundair,
            "wielmaat_aangedr_wiel" => $this->wielmaat_aangedr_wiel,
            "bandmaat_aangedr_wiel" => $this->bandmaat_aangedr_wiel,
            "band_aspect_ratio" => $this->band_aspect_ratio,
            "cx" => $this->cx,
            "fuel_tank_capacity" => $this->fuel_tank_capacity,
            "frontal_area" => $this->frontal_area,
            "front_brake_system" => $this->front_brake_system,
            "rear_brake_system" => $this->rear_brake_system,
            "epos_drive" => $this->epos_drive,
            "undriven_wheel_size" => $this->undriven_wheel_size,
            "bore" => $this->bore,
            "stroke" => $this->stroke,
            "compression" => $this->compression,
            "aantal_cyl" => $this->aantal_cyl,
            "layout" => $this->layout,
            "inhoud" => $this->inhoud,
            "inh_verb_kamer" => $this->inh_verb_kamer,
            "engine_sprocket_t" => $this->engine_sprocket_t,
            "principe" => $this->principe,
            "starter" => $this->starter,
            "intake_system" => $this->intake_system,
            "num_valves" => $this->num_valves,
            "cooling" => $this->cooling,
            "run" => $this->getRun($this->run_id),
            "max_hp_rpm" => $this->max_hp_rpm,
            "max_nm_rpm" => $this->max_nm_rpm,
            "final_t1" => $this->final_t1,
            "final_t2" => $this->final_t2,
            "final_reduction" => $this->final_reduction,
            "secundary_drive_type" => $this->secundary_drive_type,
            "aantal_versn" => $this->aantal_versn,
            "layshaft_reduction" => $this->layshaft_reduction,
            "layshaft_g1" => $this->layshaft_g1,
            "layshaft_g2" => $this->layshaft_g2,
            "verh_versn_1" => $this->verh_versn_1,
            "verh_versn_2" => $this->verh_versn_2,
            "verh_versn_3" => $this->verh_versn_3,
            "verh_versn_4" => $this->verh_versn_4,
            "verh_versn_5" => $this->verh_versn_5,
            "verh_versn_6" => $this->verh_versn_6,
            "verh_versn_7" => $this->verh_versn_7,
            "input_sprocket_t" => $this->input_sprocket_t,
            "driver_weight" => $this->driver_weight,
            "toepassingsgebied" => $this->toepassingsgebied,
        ];
    }


    /* FK methods */

}

