<?php

class _Vehicle extends dbTable
{
    /* db table */
    protected $tableName = 'vehicle';
    protected $pk = 'uid';
    protected $fieldsInfo = [
        'uid' => ['Type' => "int(10) unsigned", 'Null' => 'NO'],
        'chassis_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'clutch_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'driver_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'engine_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'merken_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'run_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'secundary_transmission_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'transmission_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'toepassings_gebied_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'users_id' => ['Type' => "int(10) unsigned", 'Null' => 'NO'],
        'vehicle_type_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'merk' => ['Type' => "varchar(255)", 'Null' => 'YES'],
        'model' => ['Type' => "varchar(255)", 'Null' => 'YES'],
        'gewicht' => ['Type' => "float", 'Null' => 'YES'],
        'legend' => ['Type' => "varchar(255)", 'Null' => 'YES'],
        'distance_diagram_distance' => ['Type' => "float", 'Null' => 'YES'],
        'launch_rpm' => ['Type' => "int(11)", 'Null' => 'YES'],
        'jaar' => ['Type' => "int(4) unsigned", 'Null' => 'YES'],
        'top_speed' => ['Type' => "float", 'Null' => 'YES'],
        'extern_source' => ['Type' => "char(32)", 'Null' => 'YES'],
        'external_identifier' => ['Type' => "int(10)", 'Null' => 'YES'],
        'updated' => ['Type' => "int(10)", 'Null' => 'YES'],
    ];

    /* modification map */
    protected $dbModified = [

        'uid' => false,
        'chassis_id' => false,
        'clutch_id' => false,
        'driver_id' => false,
        'engine_id' => false,
        'merken_id' => false,
        'run_id' => false,
        'secundary_transmission_id' => false,
        'transmission_id' => false,
        'toepassings_gebied_id' => false,
        'users_id' => false,
        'vehicle_type_id' => false,
        'merk' => false,
        'model' => false,
        'gewicht' => false,
        'legend' => false,
        'distance_diagram_distance' => false,
        'launch_rpm' => false,
        'jaar' => false,
        'top_speed' => false,
        'extern_source' => false,
        'external_identifier' => false,
        'updated' => false,];

    /* properties */
    protected $uid;
    protected $chassis_id;
    protected $clutch_id;
    protected $driver_id;
    protected $engine_id;
    protected $merken_id;
    protected $run_id;
    protected $secundary_transmission_id;
    protected $transmission_id;
    protected $toepassings_gebied_id;
    protected $users_id;
    protected $vehicle_type_id;
    protected $merk;
    protected $model;
    protected $gewicht;
    protected $legend;
    protected $distance_diagram_distance;
    protected $launch_rpm;
    protected $jaar;
    protected $top_speed;
    protected $extern_source;
    protected $external_identifier;
    protected $updated;

    /* soap declarations */
    public static $soapDeclarations = [
        'uid' => ['name' => 'uid', 'type' => 'xsd:string'],
        'chassis' => ['name' => 'chassis', 'type' => 'xsd:string'],
        'clutch' => ['name' => 'clutch', 'type' => 'xsd:string'],
        'driver' => ['name' => 'driver', 'type' => 'xsd:string'],
        'engine' => ['name' => 'engine', 'type' => 'xsd:string'],
        'merken' => ['name' => 'merken', 'type' => 'xsd:string'],
        'run' => ['name' => 'run', 'type' => 'xsd:string'],
        'secundary_transmission' => ['name' => 'secundary_transmission', 'type' => 'xsd:string'],
        'transmission' => ['name' => 'transmission', 'type' => 'xsd:string'],
        'toepassings_gebied' => ['name' => 'toepassings_gebied', 'type' => 'xsd:string'],
        'users' => ['name' => 'users', 'type' => 'xsd:string'],
        'vehicle_type' => ['name' => 'vehicle_type', 'type' => 'xsd:string'],
        'merk' => ['name' => 'merk', 'type' => 'xsd:string'],
        'model' => ['name' => 'model', 'type' => 'xsd:string'],
        'gewicht' => ['name' => 'gewicht', 'type' => 'xsd:string'],
        'legend' => ['name' => 'legend', 'type' => 'xsd:string'],
        'distance_diagram_distance' => ['name' => 'distance_diagram_distance', 'type' => 'xsd:string'],
        'launch_rpm' => ['name' => 'launch_rpm', 'type' => 'xsd:string'],
        'jaar' => ['name' => 'jaar', 'type' => 'xsd:string'],
        'top_speed' => ['name' => 'top_speed', 'type' => 'xsd:string'],
        'extern_source' => ['name' => 'extern_source', 'type' => 'xsd:string'],
        'external_identifier' => ['name' => 'external_identifier', 'type' => 'xsd:string'],
        'updated' => ['name' => 'updated', 'type' => 'xsd:string'],

    ];

    /* constructor */
    public function __construct($id = null)
    {

        if (!empty($id)) {
            $this->load($id);
        }

        parent::__construct($this->getTableName());
    }

    public function getClassName()
    {
        return "_Vehicle";
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function getSoapOut()
    {
        return [
            "uid" => $this->uid,
            "chassis" => $this->getChassis($this->chassis_id),
            "clutch" => $this->getClutch($this->clutch_id),
            "driver" => $this->getDriver($this->driver_id),
            "engine" => $this->getEngine($this->engine_id),
            "merken" => $this->getMerken($this->merken_id),
            "run" => $this->getRun($this->run_id),
            "secundary_transmission" => $this->getSecundaryTransmission($this->secundary_transmission_id),
            "transmission" => $this->getTransmission($this->transmission_id),
            "toepassings_gebied" => $this->getToepassingsGebied($this->toepassings_gebied_id),
            "users" => $this->getUsers($this->users_id),
            "vehicle_type" => $this->getVehicleType($this->vehicle_type_id),
            "merk" => $this->merk,
            "model" => $this->model,
            "gewicht" => $this->gewicht,
            "legend" => $this->legend,
            "distance_diagram_distance" => $this->distance_diagram_distance,
            "launch_rpm" => $this->launch_rpm,
            "jaar" => $this->jaar,
            "top_speed" => $this->top_speed,
            "extern_source" => $this->extern_source,
            "external_identifier" => $this->external_identifier,
            "updated" => $this->updated,
        ];
    }


    /* FK methods */

    public function getChassisList($sql = null)
    {
        $Chassis = new Chassis();
        $list = [];
        $list = $Chassis->lijst($sql);
        return $list;
    }

    public function getChassis($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getChassisId();
        }
        $Chassis = new Chassis($uid);
        if ($getObject) {
            return $Chassis;
        }
        return $Chassis->getValue();
    }

    public function getClutchList($sql = null)
    {
        $Clutch = new Clutch();
        $list = [];
        $list = $Clutch->lijst($sql);
        return $list;
    }

    public function getClutch($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getClutchId();
        }
        $Clutch = new Clutch($uid);
        if ($getObject) {
            return $Clutch;
        }
        return $Clutch->getValue();
    }

    public function getDriverList($sql = null)
    {
        $Driver = new Driver();
        $list = [];
        $list = $Driver->lijst($sql);
        return $list;
    }

    public function getDriver($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getDriverId();
        }
        $Driver = new Driver($uid);
        if ($getObject) {
            return $Driver;
        }
        return $Driver->getValue();
    }

    public function getEngineList($sql = null)
    {
        $Engine = new Engine();
        $list = [];
        $list = $Engine->lijst($sql);
        return $list;
    }

    public function getEngine($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getEngineId();
        }
        $Engine = new Engine($uid);
        if ($getObject) {
            return $Engine;
        }
        return $Engine->getValue();
    }

    public function getMerkenList($sql = null)
    {
        $Merken = new Merken();
        $list = [];
        $list = $Merken->lijst($sql);
        return $list;
    }

    public function getMerken($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getMerkenId();
        }
        $Merken = new Merken($uid);
        if ($getObject) {
            return $Merken;
        }
        return $Merken->getValue();
    }

    public function getRunList($sql = null)
    {
        $Run = new Run();
        $list = [];
        $list = $Run->lijst($sql);
        return $list;
    }

    public function getRun($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getRunId();
        }
        $Run = new Run($uid);
        if ($getObject) {
            return $Run;
        }
        return $Run->getValue();
    }

    public function getSecundaryTransmissionList($sql = null)
    {
        $SecundaryTransmission = new SecundaryTransmission();
        $list = [];
        $list = $SecundaryTransmission->lijst($sql);
        return $list;
    }

    public function getSecundaryTransmission($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getSecundaryTransmissionId();
        }
        $SecundaryTransmission = new SecundaryTransmission($uid);
        if ($getObject) {
            return $SecundaryTransmission;
        }
        return $SecundaryTransmission->getValue();
    }

    public function getTransmissionList($sql = null)
    {
        $Transmission = new Transmission();
        $list = [];
        $list = $Transmission->lijst($sql);
        return $list;
    }

    public function getTransmission($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getTransmissionId();
        }
        $Transmission = new Transmission($uid);
        if ($getObject) {
            return $Transmission;
        }
        return $Transmission->getValue();
    }

    public function getToepassingsGebiedList($sql = null)
    {
        $ToepassingsGebied = new ToepassingsGebied();
        $list = [];
        $list = $ToepassingsGebied->lijst($sql);
        return $list;
    }

    public function getToepassingsGebied($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getToepassingsGebiedId();
        }
        $ToepassingsGebied = new ToepassingsGebied($uid);
        if ($getObject) {
            return $ToepassingsGebied;
        }
        return $ToepassingsGebied->getValue();
    }

    public function getUsersList($sql = null)
    {
        $Users = new Users();
        $list = [];
        $list = $Users->lijst($sql);
        return $list;
    }

    public function getUsers($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getUsersId();
        }
        $Users = new Users($uid);
        if ($getObject) {
            return $Users;
        }
        return $Users->getValue();
    }

    public function getVehicleTypeList($sql = null)
    {
        $VehicleType = new VehicleType();
        $list = [];
        $list = $VehicleType->lijst($sql);
        return $list;
    }

    public function getVehicleType($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getVehicleTypeId();
        }
        $VehicleType = new VehicleType($uid);
        if ($getObject) {
            return $VehicleType;
        }
        return $VehicleType->getValue();
    }

}

