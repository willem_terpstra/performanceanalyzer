<?php

class _Run extends dbTable
{
    /* db table */
    protected $tableName = 'run';
    protected $pk = 'uid';
    protected $fieldsInfo = [
        'uid' => ['Type' => "int(10) unsigned", 'Null' => 'NO'],
        'correction_factor_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'users_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'vehicle_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'engine_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'visibility_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'naam' => ['Type' => "varchar(255)", 'Null' => 'YES'],
        'omschrijving' => ['Type' => "text", 'Null' => 'YES'],
        'datum_run' => ['Type' => "date", 'Null' => 'YES'],
        'tijd' => ['Type' => "time", 'Null' => 'YES'],
        'temperature' => ['Type' => "float", 'Null' => 'YES'],
        'humidity' => ['Type' => "float", 'Null' => 'YES'],
        'atm_pressure' => ['Type' => "float", 'Null' => 'YES'],
        'dyno_start_rpm' => ['Type' => "int(11)", 'Null' => 'YES'],
        'dyno_end_rpm' => ['Type' => "int(11)", 'Null' => 'YES'],
        'dyno_interval_rpm' => ['Type' => "int(11)", 'Null' => 'YES'],
        'filename' => ['Type' => "text", 'Null' => 'YES'],
        'imported_message' => ['Type' => "text", 'Null' => 'YES'],
        'image' => ['Type' => "varchar(1024)", 'Null' => 'YES'],
        'run_type' => ['Type' => "enum('hand_filled','generated','fos','dynojet')", 'Null' => 'YES'],
        'interpolation' => ['Type' => "int(11)", 'Null' => 'YES'],
        'max_hp_rpm' => ['Type' => "char(64)", 'Null' => 'YES'],
        'max_nm_rpm' => ['Type' => "char(64)", 'Null' => 'YES'],
        'session_id' => ['Type' => "varchar(64)", 'Null' => 'YES'],
        'session_tag' => ['Type' => "char(8)", 'Null' => 'YES'],
    ];

    /* modification map */
    protected $dbModified = [

        'uid' => false,
        'correction_factor_id' => false,
        'users_id' => false,
        'vehicle_id' => false,
        'engine_id' => false,
        'visibility_id' => false,
        'naam' => false,
        'omschrijving' => false,
        'datum_run' => false,
        'tijd' => false,
        'temperature' => false,
        'humidity' => false,
        'atm_pressure' => false,
        'dyno_start_rpm' => false,
        'dyno_end_rpm' => false,
        'dyno_interval_rpm' => false,
        'filename' => false,
        'imported_message' => false,
        'image' => false,
        'run_type' => false,
        'interpolation' => false,
        'max_hp_rpm' => false,
        'max_nm_rpm' => false,
        'session_id' => false,
        'session_tag' => false,];

    /* properties */
    protected $uid;
    protected $correction_factor_id;
    protected $users_id;
    protected $vehicle_id;
    protected $engine_id;
    protected $visibility_id;
    protected $naam;
    protected $omschrijving;
    protected $datum_run;
    protected $tijd;
    protected $temperature;
    protected $humidity;
    protected $atm_pressure;
    protected $dyno_start_rpm;
    protected $dyno_end_rpm;
    protected $dyno_interval_rpm;
    protected $filename;
    protected $imported_message;
    protected $image;
    protected $run_type;
    protected $interpolation;
    protected $max_hp_rpm;
    protected $max_nm_rpm;
    protected $session_id;
    protected $session_tag;

    /* soap declarations */
    public static $soapDeclarations = [
        'uid' => ['name' => 'uid', 'type' => 'xsd:string'],
        'correction_factor' => ['name' => 'correction_factor', 'type' => 'xsd:string'],
        'users' => ['name' => 'users', 'type' => 'xsd:string'],
        'vehicle' => ['name' => 'vehicle', 'type' => 'xsd:string'],
        'engine' => ['name' => 'engine', 'type' => 'xsd:string'],
        'visibility' => ['name' => 'visibility', 'type' => 'xsd:string'],
        'naam' => ['name' => 'naam', 'type' => 'xsd:string'],
        'omschrijving' => ['name' => 'omschrijving', 'type' => 'xsd:string'],
        'datum_run' => ['name' => 'datum_run', 'type' => 'xsd:string'],
        'tijd' => ['name' => 'tijd', 'type' => 'xsd:string'],
        'temperature' => ['name' => 'temperature', 'type' => 'xsd:string'],
        'humidity' => ['name' => 'humidity', 'type' => 'xsd:string'],
        'atm_pressure' => ['name' => 'atm_pressure', 'type' => 'xsd:string'],
        'dyno_start_rpm' => ['name' => 'dyno_start_rpm', 'type' => 'xsd:string'],
        'dyno_end_rpm' => ['name' => 'dyno_end_rpm', 'type' => 'xsd:string'],
        'dyno_interval_rpm' => ['name' => 'dyno_interval_rpm', 'type' => 'xsd:string'],
        'filename' => ['name' => 'filename', 'type' => 'xsd:string'],
        'imported_message' => ['name' => 'imported_message', 'type' => 'xsd:string'],
        'image' => ['name' => 'image', 'type' => 'xsd:string'],
        'run_type' => ['name' => 'run_type', 'type' => 'xsd:string'],
        'interpolation' => ['name' => 'interpolation', 'type' => 'xsd:string'],
        'max_hp_rpm' => ['name' => 'max_hp_rpm', 'type' => 'xsd:string'],
        'max_nm_rpm' => ['name' => 'max_nm_rpm', 'type' => 'xsd:string'],
        'session' => ['name' => 'session', 'type' => 'xsd:string'],
        'session_tag' => ['name' => 'session_tag', 'type' => 'xsd:string'],

    ];

    /* constructor */
    public function __construct($id = null)
    {

        if (!empty($id)) {
            $this->load($id);
        }

        parent::__construct($this->getTableName());
    }

    public function getClassName()
    {
        return "_Run";
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function getSoapOut()
    {
        return [
            "uid" => $this->uid,
            "correction_factor" => $this->getCorrectionFactor($this->correction_factor_id),
            "users" => $this->getUsers($this->users_id),
            "vehicle" => $this->getVehicle($this->vehicle_id),
            "engine" => $this->getEngine($this->engine_id),
            "visibility" => $this->getVisibility($this->visibility_id),
            "naam" => $this->naam,
            "omschrijving" => $this->omschrijving,
            "datum_run" => $this->datum_run,
            "tijd" => $this->tijd,
            "temperature" => $this->temperature,
            "humidity" => $this->humidity,
            "atm_pressure" => $this->atm_pressure,
            "dyno_start_rpm" => $this->dyno_start_rpm,
            "dyno_end_rpm" => $this->dyno_end_rpm,
            "dyno_interval_rpm" => $this->dyno_interval_rpm,
            "filename" => $this->filename,
            "imported_message" => $this->imported_message,
            "image" => $this->image,
            "run_type" => $this->run_type,
            "interpolation" => $this->interpolation,
            "max_hp_rpm" => $this->max_hp_rpm,
            "max_nm_rpm" => $this->max_nm_rpm,
            "session" => $this->getSession($this->session_id),
            "session_tag" => $this->session_tag,
        ];
    }


    /* FK methods */

    public function getCorrectionFactorList($sql = null)
    {
        $CorrectionFactor = new CorrectionFactor();
        $list = [];
        $list = $CorrectionFactor->lijst($sql);
        return $list;
    }

    public function getCorrectionFactor($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getCorrectionFactorId();
        }
        $CorrectionFactor = new CorrectionFactor($uid);
        if ($getObject) {
            return $CorrectionFactor;
        }
        return $CorrectionFactor->getValue();
    }

    public function getUsersList($sql = null)
    {
        $Users = new Users();
        $list = [];
        $list = $Users->lijst($sql);
        return $list;
    }

    public function getUsers($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getUsersId();
        }
        $Users = new Users($uid);
        if ($getObject) {
            return $Users;
        }
        return $Users->getValue();
    }

    public function getVisibilityList($sql = null)
    {
        $Visibility = new Visibility();
        $list = [];
        $list = $Visibility->lijst($sql);
        return $list;
    }

    public function getVisibility($uid = NULL, $getObject = false)
    {
        if (empty($uid)) {
            $uid = $this->getVisibilityId();
        }
        $Visibility = new Visibility($uid);
        if ($getObject) {
            return $Visibility;
        }
        return $Visibility->getValue();
    }

}

