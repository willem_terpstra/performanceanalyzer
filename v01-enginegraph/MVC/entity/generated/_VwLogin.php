<?php

class _VwLogin extends dbTable
{
    /* db table */
    protected $tableName = 'vw_login';
    protected $pk = '';
    protected $fieldsInfo = [
        'uid' => ['Type' => "int(10) unsigned", 'Null' => 'NO'],
        'userid' => ['Type' => "char(32)", 'Null' => 'YES'],
        'passw' => ['Type' => "char(32)", 'Null' => 'YES'],
        'auth_id' => ['Type' => "int(10) unsigned", 'Null' => 'YES'],
        'userid_md' => ['Type' => "varchar(32)", 'Null' => 'YES'],
        'passw_md' => ['Type' => "varchar(32)", 'Null' => 'YES'],
        'auth_naam' => ['Type' => "char(32)", 'Null' => 'YES'],
    ];

    /* modification map */
    protected $dbModified = [

        'uid' => false,
        'userid' => false,
        'passw' => false,
        'auth_id' => false,
        'userid_md' => false,
        'passw_md' => false,
        'auth_naam' => false,];

    /* properties */
    protected $uid;
    protected $userid;
    protected $passw;
    protected $auth_id;
    protected $userid_md;
    protected $passw_md;
    protected $auth_naam;

    /* soap declarations */
    public static $soapDeclarations = [
        'uid' => ['name' => 'uid', 'type' => 'xsd:string'],
        'userid' => ['name' => 'userid', 'type' => 'xsd:string'],
        'passw' => ['name' => 'passw', 'type' => 'xsd:string'],
        'auth' => ['name' => 'auth', 'type' => 'xsd:string'],
        'userid_md' => ['name' => 'userid_md', 'type' => 'xsd:string'],
        'passw_md' => ['name' => 'passw_md', 'type' => 'xsd:string'],
        'auth_naam' => ['name' => 'auth_naam', 'type' => 'xsd:string'],

    ];

    /* constructor */
    public function __construct($id = null)
    {
        $this->is_view = true;

        if (!empty($id)) {
            $this->load($id);
        }

        parent::__construct($this->getTableName());
    }

    public function getClassName()
    {
        return "_VwLogin";
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function getSoapOut()
    {
        return [
            "uid" => $this->uid,
            "userid" => $this->userid,
            "passw" => $this->passw,
            "auth" => $this->getAuth($this->auth_id),
            "userid_md" => $this->userid_md,
            "passw_md" => $this->passw_md,
            "auth_naam" => $this->auth_naam,
        ];
    }


    /* FK methods */

}

