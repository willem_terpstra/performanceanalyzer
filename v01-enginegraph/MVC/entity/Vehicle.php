<?php

/**
 * show off @method
 *
 * @method int borp() borp(int $int1, int $int2) multiply two integers
 */
class Vehicle extends _Vehicle implements DropdownAble
{

    const MAX_GEARS = 6;

    private $session_tag = NULL;
//	private $selectedRun = NULL;

    /**
     *
     * @var type Engine
     */
    private $engine;

    /**
     *
     * @var type Chassis
     */
    private $chassis;

    /**
     *
     * @var type Clutch
     */
    private $clutch;

    /**
     *
     * @var type SecundaryTransmission
     */
    private $secundary_transmission;

    /**
     *
     * @var type Transmission
     */
    private $transmission;

    /**
     *
     * @var type Driver
     */
    private $driver;

    /**
     * @var type Run
     */
    private $run;
    // child classes
    private $childs = [
        'engine' => 'Engine',
        'clutch' => 'Clutch',
        'chassis' => 'Chassis',
        'transmission' => 'Transmission',
        'secundary_transmission' => 'SecundaryTransmission',
        'driver' => 'Driver',
        'run' => 'Run'
    ];
    private $lineColors = ['red', 'green', 'blue', 'orange'];
    static private $lcCount = 0;

    function __construct($id = null)
    {
        parent::__construct($id);
        foreach ($this->childs as $key => $className) {
            $id = $key . '_id';
            if ($this->$id != NULL) {
                $this->$key = new $className($this->$id);
            }
        }
    }

//

    function __clone()
    {
        $this->setUid(NULL);
        $this->setUsersId(Authorization::getCurrentUser()->getUid());
        $this->setModel($this->getModel() . ' ' . Strings::show('copy'));
        foreach ($this->dbModified as &$property) {
            $property = true;
        }
    }

    public static function getChilds()
    {
        $vehicle = new Vehicle();
        return $vehicle->childs;
    }

    public function getChildObjects()
    {
        $childs = [];
        foreach ($this->childs as $k => $v) {
            if ($this->$k instanceof dbTable) {
                $childs[$k . '_id'] = $this->$k;
            }
        }
        return $childs;
    }

    public function getModifiedChildObjects()
    {
        $objects = [];
        foreach ($this->childs as $childName => $className) {
            if (!empty($this->$childName)) {
                $modifiedProps = $this->$childName->getModifiedPropertys();
                if (!empty($modifiedProps)) {
                    $objects[] = $this->$childName;
                }
            }
        }
        return $objects;
    }

    public function validate($post)
    {
        $result = parent::validate($post);
        foreach ($this->childs as $childName => $className) {
            if ($this->$childName instanceof $childName) {
                $b = $this->$childName->validate($post);
                if (!$b) {
                    $result = false;
                    $errors = $this->$childName->getMessages();
                    $this->setMessages($this->$childName->getMessages());
                }
            }
        }
        return $result;
    }

    public function clearMessages()
    {
        foreach ($this->childs as $childName => $className) {
            if ($this->$childName instanceof $childName) {
                $this->$childName->clearMessages();
            }
        }
        parent::clearMessages();
    }

    public function validateMerk($post)
    {
        if (array_key_exists('merk', $post) || array_key_exists('merken_id', $post)) {
            if (empty($post['merk'])) {
                if (empty($post['merken_id'])) {
                    $this->setMessages('brand_obliged');
                    return false;
                }
            }
        }
        return true;
    }

    public function initEngine($id)
    {
        $this->engine = new Engine($id);
        $this->engine_id = $id;
    }

    public function getEngine($uid = null, $getObject = false)
    {
        if (!($this->engine instanceof Engine)) {
            if (!empty($this->engine_id)) {
                $this->initEngine($this->engine_id);
            } else {
                return NULL;
            }
        }
        return $this->engine;
    }

    public function initTransmission($id)
    {
        $this->transmission = new Transmission($id);
        $this->transmission_id = $id;
    }

    public function initSecundaryTransmission($id)
    {
        if (empty($id)) {
            return;
        }
        $this->secundary_transmission = new SecundaryTransmission($id);
    }

    public function initChassis($id)
    {
        $this->chassis = new Chassis($id);
        $this->chassis_id = $id;
    }

    public function initClutch($id)
    {
        $this->clutch = new Clutch($id);
        $this->clutch_id = $id;
    }

    public function getClutch($uid = null, $getObject = false)
    {
        if (empty($this->clutch)) {
            return NULL;
        }
        return $this->clutch;
    }

    public function initDriver($id)
    {
        $this->driver = new Driver($id);
        $this->driver_id = $id;
    }

    public function getDriver($uid = null, $getObject = false)
    {
        if (empty($this->driver)) {
            if (!empty($this->driver_id)) {
                $this->initDriver($this->driver_id);
            } else {
                return NULL;
            }
        }
        return $this->driver;
    }

    public function initRun($param)
    {
        if ($param instanceof Run) {
            $this->run = $param;
            $this->run_id = $this->run->getUid();
        } else {
            $id = $param;
            $this->run = new Run($id);
            $this->run_id = $id;
        }
    }

    public function getDynoIntervalRpm()
    {
        if (!($this->run instanceof Run)) {
            if (!empty($this->run_id)) {
                $this->initRun($this->run_id);
            } else {
                return NULL;
            }
        }
        return $this->run->getDynoIntervalRpm();
    }

    public function getTransmission($uid = null, $getObject = false)
    {
        if (!($this->transmission instanceof Transmission)) {
            if (!empty($this->transmission_id)) {
                $this->initTransmission($this->transmission_id);
            } else {
                return NULL;
            }
        }
        return $this->transmission;
    }

    public function getSecundaryTransmission($uid = null, $getObject = false)
    {
        if (!($this->secundary_transmission instanceof SecundaryTransmission)) {
            if (!empty($this->secundary_transmission_id)) {
                $this->initSecundaryTransmission($this->secundary_transmission_id);
            } else {
                return NULL;
            }
        }
        return $this->secundary_transmission;
    }

    public function getUsers($uid = null, $getObject = false)
    {
        $Users = new Users($uid);
        return $Users->getValue();
    }

    public function getLayshaftReduction($gearNum)
    {
//		return 1;
        $directDriveGear = $this->transmission->getVersnDirectDrive();
        if ($gearNum == $directDriveGear) {
            return 1;
        }
        $layshaftReduction = $this->transmission->getLayshaftReduction();
        if (empty($layshaftReduction)) {
            return 1;
        } else {
            return $layshaftReduction;
        }
    }

    public function getAantalVersn()
    {
        if (empty($this->transmission)) {
            return NULL;
        }
        return $this->transmission->getAantalVersn();
    }

    public function getVerhPrimair()
    {
        if (empty($this->engine) || empty($this->transmission)) {
            return 1;
        }
        $engineSprocket = $this->engine->getEngineSprocketT();
        $clutchSprocket = $this->transmission->getInputSprocketT();

        if (!empty($engineSprocket) && !empty($clutchSprocket)) {
            return round($engineSprocket / $clutchSprocket, 4);
        }
        return 1;
    }

    public function getVerhSecundair()
    {
        if (empty($this->secundary_transmission)) {
            return NULL;
        }
        if ($this->secundary_transmission instanceof SecundaryTransmission) {
            $reduction = $this->secundary_transmission->calcReduction();

            return $reduction;
        }
        return 1;
    }

//    public function getDriverWeight($conversion = true)
//    {
//        if (empty($this->driver_id)) {
//            return 0;
//        }
//        if (empty($this->driver)) {
//            $this->initDriver($this->driver_id);
//        }
//        return $this->driver->getDriverWeight($conversion);
//    }

    public function getFrontalArea($conversion = true)
    {
        if (empty($this->chassis_id)) {
            return 0;
        }
        if (empty($this->chassis)) {
            $this->initChassis($this->chassis_id);
        }
        return $this->chassis->getFrontalArea($conversion);
    }

    public function getBsfc()
    {
        if ($this->engine instanceof Engine) {
            return $this->engine->getBsfc();
        }
        return NULL;
    }

    function __toString()
    {
        return $this->getMerk() . ' ' . $this->getModel();
    }

    public function searchProperty($property)
    {
        $func = 'get' . camelCase($property);
        if (method_exists($this, $func)) {
            return $this->$func(false);
        }
        if (property_exists($this, $property) || strpos($property, 'layshaft_reduction') !== false) {
            return $this->$func();
        }
        if (empty($this->engine)) {
            $this->initEngine($this->engine_id);
        }
        if (array_key_exists($property, $this->engine->getModifiedMap())) {
            return $this->engine->$func(false);
        }
        if (empty($this->transmission)) {
            $this->initTransmission($this->transmission_id);
        }
        if (array_key_exists($property, $this->transmission->getModifiedMap())) {
            return $this->transmission->$func(false);
        }
        if (empty($this->chassis)) {
            $this->initChassis($this->chassis_id);
        }
        if (array_key_exists($property, $this->chassis->getModifiedMap())) {
            return $this->chassis->$func(false);
        }
        if (empty($this->clutch)) {
            $this->initClutch($this->clutch_id);
        }
        if (array_key_exists($property, $this->clutch->getModifiedMap())) {
            return $this->clutch->$func(false);
        }
        if (empty($this->driver)) {
            $this->initDriver($this->driver_id);
        }
        if (array_key_exists($property, $this->driver->getModifiedMap())) {
            return $this->driver->$func(false);
        }


        $run = $this->getSelectedRun();
        if (empty($run)) {
            throw new VehicleException('NoRunSelected');
        }
        return $run->searchProperty($property, $this->engine->getShiftRpm());
    }

    function __call($name, $args)
    {
//		Klog::Loginfo(__CLASS__ . ' ' . $name . ' => ' . var_export($args, true));
        try {
            $result = parent::__call($name, $args);
            return $result;
        } catch (dbTableException $e) {
            if ($e->getCode() == dbTableException::PROPERTY_DOES_NOT_EXIST) {
                if (preg_match('/(?P<function>getMax)(?P<property>\w+)Rpm/', $name, $regs)) {
                    $property = strtolower($regs['property']);
                    $unit = Unit::$units[$property];
                    return $this->_getMaxAtRpm($unit);
                }
                if (preg_match('/(?P<function>getMax)(?P<property>\w+)/', $name, $regs)) {
                    $property = strtolower($regs['property']);
                    $run = $this->getSelectedRun();
                    if (!empty($run)) {
                        return $run->$name();
                    }
                }

                if (preg_match('/(?P<function>set)(?P<property>[\w|@]+)/', $name, $regs)) {
                    $p = explode('@', $regs['property']);
                    $property = $this->cc2u($p[0]);
                    $func = 'set' . $this->camelCase($property);
                    $table = NULL;
                    if (isset($p[1])) {
                        $table = $p[1];
                    }
                    foreach ($this->childs as $child => $className) {
                        if (!empty($table)) {
                            if ($table !== $child) {
                                continue;
                            }
                        }
                        try {
                            if (empty($this->$child)) {
                                $childId = $child . '_id';
                                $this->$child = new $className($this->$childId);
                            }
                            if ($child === $property) {
                                if (!($args[0] instanceof $className)) {
                                    throw new VehicleException('Invalid instance of: ' . $className);
                                }
                                $this->$child = $args[0];
                            }
                            if ($this->$child->hasProperty($property)) {
//Klog::LogInfo("Setting $property to ". $args[0]);
                                $this->$child->$func($args[0]);
                                return null;
                            }
                        } catch (dbTableException $e) {
                            if ($e->getCode() == dbTableException::PROPERTY_DOES_NOT_EXIST) {
                                // continue
                            } else {
                                // throw again
                                throw new dbTableException($e);
                            }
                        }
                    }
                }
                if (strpos($name, 'getLayshaftReduction') !== false) {
                    $gearNum = substr($name, -1);
                    return $this->getLayshaftReduction($gearNum);
                }
                if (preg_match('/(?P<function>get)(?P<property>[\w|@]+)/', $name, $regs)) {
                    $property = $regs['property'];
                    $table = NULL;
                    if (strpos($property, '@') !== false) {
                        $p = strpos($property, '@');
                        $table = substr($property, $p + 1);
                        $property = substr($property, 0, $p);
                    }
                    $f = 'get' . $property;
                    foreach ($this->childs as $child => $className) {
                        if (!empty($table)) {
                            if ($table != $child) {
                                continue;
                            }
                        }
                        try {
                            if (empty($this->$child)) {
                                $childId = $child . '_id';
                                $this->$child = new $className($this->$childId);
                            }
//							if ($this->$child->hasProperty($property)) {
                            return $this->$child->$f();
//							}
                        } catch (dbTableException $e) {
                            if ($e->getCode() == dbTableException::PROPERTY_DOES_NOT_EXIST) {
                                // continue
                            } else {
                                // throw again
                                throw new dbTableException($child . '_not_found', $e->getCode());
                            }
                        }
                    }
                    $run = $this->getSelectedRun();
                    if (!empty($run)) {
                        $value = $run->searchProperty(strtolower($property), $this->shift_rpm);
                        if ($value !== NULL) {
                            return $value;
                        }
                    }
                    throw new dbTableException($e);
                }
            }
        }
        return null;
    }

    public function setSelectedRun(Run $run)
    {
        $this->run_id = $run->getUid();
        $this->run = $run;
    }

    /**
     *
     * @return type Run
     */
    public function getSelectedRun()
    {
        if (empty($this->run)) {
            $runId = $this->getPreferredRunId();
            if (empty($runId)) {
                return NULL;
            }
            $this->run = new Run($runId);
        }
        return $this->run;
    }

//    public function getGewicht($conversion = true)
//    {
//        $w = NULL;
//        if ($conversion) {
//            $w = Unit::convert($this->gewicht, Unit::getDefaultUnit('weight'), Unit::getUnit('weight'));
//        } else {
//            $w = $this->gewicht;
//        }
//        return $w;
//    }

//    public function setGewicht($weight)
//    {
//        parent::setGewicht($weight * Unit::getFactor(Unit::getUnit('weight'), 'kg'));
//    }

    public function getRuns()
    {
        $runs = new Run();
        return $runs->lijst('SELECT * FROM run WHERE vehicle_id=?', [$this->getUid()]);
    }

    public function getNumRuns()
    {
        $sql = 'SELECT count(*) AS cnt FROM run WHERE vehicle_id=?';
        $rs = $this->execSql($sql, [$this->getUid()]);
        if ($this->numRows == 1) {
            $row = mysqli_fetch_assoc($rs);
            return $row['cnt'];
        }
        return 0;
    }

    public function getMaxRpm()
    {
        $runArr = [];
        $maxRpm = NULL;
        $run = $this->getSelectedRun();
        $runId = $run->getUid();
        if (empty($runId)) {
            return 0;
        }
        if (empty($run)) {
            throw new VehicleException('NoRunSelected');
        }
        $dyno = new Dyno();
        $maxRpm = $dyno->getField('SELECT MAX(rpm) FROM dyno WHERE run_id = ?', [$run->getUid()]);
        return $maxRpm;
    }

    private function getMaxNmRpm()
    {
        return $this->run->getMaxNmRpm();
    }

    private function getMaxHpRpm()
    {
        return $this->run->getMaxHpRpm();
    }

    /**
     * nm bij max_nm van geselecteerde run
     * @return int
     */
    public function getLaunchRpmNm()
    {
        // nearest value
        $launchRpm = $this->getLaunchRpm();
        $sql = "( SELECT nm, rpm FROM vw_dyno WHERE run_id= ? AND rpm >  $launchRpm ORDER BY rpm ASC  LIMIT 1 )
				UNION
				( SELECT nm, rpm FROM vw_dyno WHERE run_id= ? AND rpm <= $launchRpm ORDER BY rpm DESC LIMIT 1 )
				ORDER BY ABS(rpm - 50)";
        $uid = $this->getSelectedRun()->getUid();
        $rs = $this->execSql($sql, [
            $uid, $uid
        ]);
        $row = mysqli_fetch_assoc($rs);
        return $row['nm'];
    }

    /**
     * aantal toeren vanaf de startlijn
     * @return type int
     */
    public function getLaunchRpm()
    {
        if (empty($this->launch_rpm)) {
            $sql = 'SELECT nm, rpm FROM vw_dyno WHERE run_id = ? ORDER BY nm DESC LIMIT 1';
            $rs = $this->execSql($sql, [$this->getSelectedRun()->getUid()]);
            $row = mysqli_fetch_assoc($rs);
            $this->launch_rpm = $row['rpm'];
        }
        return $this->launch_rpm;
    }


    /**
     * Max power in user unit
     * @return type float
     */
    public function getMaxPower()
    {
        return $this->_getMaxAtRpm(Unit::$units['power']);
    }

    public function getMerk()
    {
        if (!empty($this->merken_id)) {
            $m = new Merken($this->merken_id);
            return $m->getNaam();
        }
        return null;
    }

    /**
     *
     * @return int,null
     */
    public function getPreferredRunId()
    {
        if (empty($this->run_id)) {
            $runs = $this->getRuns();
            foreach ($runs as $run) {
                return $run->getUid(); // select the first
            }
            return NULL;
        }
        return $this->run_id;
    }

    public function getDropdownDefaultValue()
    {
        return NULL;
    }

    public function getDropdownNullOption()
    {
        return NULL;
    }

    public function getIntValueMethod()
    {
        return 'getUid';
    }

    public function getValue()
    {
        return $this->getMerk() . ' ' . $this->getModel(); // .' '. $cc;
    }

    public function getLineColor()
    {
        if (self::$lcCount == 4) {
            self::$lcCount = 0;
        }
        return $this->lineColors[self::$lcCount++];
    }

    public function getSessionTag()
    {
        if (!empty($this->session_tag)) {
            return $this->session_tag;
        }
        return null;
    }

    public function setSessionTag($sessionTag)
    {
        $this->session_tag = $sessionTag;
    }

    private function conversionFunction(Vehicle $vehicle, $func)
    {
        if (method_exists($vehicle, $func)) {
            $r = new ReflectionMethod('Vehicle', $func);
            $params = $r->getParameters();
            foreach ($params as $param) {
                if ($param->getName() == 'conversion') {
                    return $vehicle->$func(true);
                }
                break; // only single parameter supported
            }
        }
        foreach ($this->getChildObjects() as $child) {
            if (method_exists($child, $func)) {
                $r = new ReflectionMethod($child->getClassName(), $func);
                $params = $r->getParameters();
                foreach ($params as $param) {
                    if ($param->getName() == 'conversion') {
//Klog::LogInfo($child->getClassName() .'::'. $func);
                        return $child->$func(true);
                    }
                    break; // only single parameter supported
                }
            }
        }
        return NULL;
    }

//    public function compare(Vehicle $compareTo, $compareFields)
//    {
////		$result = array('test' => array('TEST 1', 'TEST 2'));
//        $result = [];
//        $compareFields = array_keys($compareFields);
//        foreach ($compareFields as $compareField) {
//            $func = 'get' . camelCase($compareField);
//            $b = $compareTo->$func();
//            $a = $this->$func();
//            if($a instanceof dbTable && $b instanceof dbTable ) {
//                $result = array_merge($a->compare($b));
//                continue;
//            }
//            if (gettype($a) == 'double' && gettype($b) == 'double') {
//                $a = round($a, 2);
//                $b = round($b, 2);
//            }
////            if ($b === NULL) {
////                $b = $compareTo->$func();
////            }
//            if ($a !== $b) {
//                if ($unit = (string) $this->getFieldUnit($compareField)) {
//                    Unit::convert($unit, $a);
//                    Unit::convert($unit, $b);
//                    $unit = ' ' . $unit;
//                }
//                $result[$compareField] = [Strings::show($compareField), $a . $unit, '=>', $b . $unit];
//            }
//            $b = NULL;
//        }
//        return $result;
//    }

    /* system functions */

    public function getClassName()
    {
        return "Vehicle";
    }

    public function getUid()
    {
        $idName = $this->getPK();
        return $this->$idName;
    }

    public function setUid($id)
    {
        $idName = $this->getPK();
        $this->$idName = $id;
    }

    function __get($name)
    {
        if (strpos($name, '@') !== false) {
            $a = explode('@', $name);
            $table = $a[1];
            $name = $a[0];
        }
        foreach ($this->childs as $k => $child) {
            if (isset($table)) {
                if ($table != $k) {
                    continue;
                }
            }
            if (empty($this->$k)) {
                $id = $k . '_id';
                if (empty($this->$id)) {
                    $this->$k = new $child();
                } else {
                    $this->$k = new $child($this->$id);
                }
            }
            if ($this->$k->hasProperty($name)) {
                $f = 'get' . $this->camelCase($name);
                return $this->$k->$f();
            }
        }
        throw new dbTableException('PropertyNotFound', dbTableException::PROPERTY_DOES_NOT_EXIST);
    }

    function __set($name, $value)
    {
        foreach ($this->childs as $k => $child) {
            if (empty($this->$k)) {
                $id = $k . '_id';
                $this->$k = new $child($this->$id);
            }
            $table = NULL;
            $a = explode('@', $name);
            $fieldName = $a[0];
            if (isset($a[1])) {
                $table = $a[1];
            }
            if (!empty($table)) {
                if ($table != $k) {
                    continue;
                }
            }
            if ($this->$k->hasProperty($fieldName)) {
                $f = 'set' . $this->camelCase($fieldName);
                return $this->$k->$f($value);
            }
        }
        throw new dbTableException('PropertyNotFound: ' . $name, dbTableException::PROPERTY_DOES_NOT_EXIST);
    }

    /**
     *
     * @param int $uid
     * @param bool $getObject
     * @return Run
     */
    public function getRun($uid = NULL, $getObject = false)
    {
        if ($getObject) {
            if (empty($this->run)) {
                if (!empty($this->run_id)) {
                    $this->initRun($this->run_id);
                }
            }
            if (!empty($this->run)) {
                return $this->run;
            }
        }
        return parent::getRun();
    }

    public function getRunSummary()
    {
        if ($this->run instanceof Run) {
            $runView = new RunView();
            $runView->setRun($this->run);
        } else {
            $runView = new RunView($this->getPreferredRunId());
        }
        return $runView->summary();
    }

    public function getTransmissionSummary()
    {
        $transmissionView = new TransmissionView($this->getTransmissionId());
        return $transmissionView->summary();
    }

    public function getLegend()
    {
        $legend = $this->legend;
        if (empty($legend)) {
            return NULL;
        }
        $s = $this->$legend;
        return $s;
    }

//    public function getDistanceDiagramDistance($conversion = true)
//    {
//        if ($conversion) {
//            return Unit::convert($this->distance_diagram_distance, Unit::getDefaultUnit('distance'), Unit::getUnit('distance'));
//        } else {
//            return $this->distance_diagram_distance;
//        }
//    }

    public function getLayshaftReduction1()
    {
        return $this->getLayshaftReduction(1);
    }

    public function getLayshaftReduction2()
    {
        return $this->getLayshaftReduction(2);
    }

    public function getLayshaftReduction3()
    {
        return $this->getLayshaftReduction(3);
    }

    public function getLayshaftReduction4()
    {
        return $this->getLayshaftReduction(4);
    }

    public function getLayshaftReduction5()
    {
        return $this->getLayshaftReduction(5);
    }

    public function getLayshaftReduction6()
    {
        return $this->getLayshaftReduction(6);
    }

}

?>