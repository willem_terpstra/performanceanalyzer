<?php
if (array_key_exists('o', Get::$getVars) && Get::$getVars['o'] == 'html') {

} else {
    $selectRun = '';
    if (isset($runId)) {
        $selectRun = " and run_id=$runId ";
    }
    ?>
    <script type='text/javascript'>

        function fixToolTip() {
            var container = jQuery('#GoogleChart > div:last-child > div:last-child')[0];

            function setPosition(e) {
                var tooltipFields = jQuery('.google-visualization-tooltip-item');
                tooltipFields.each(function (index, item) {
                    var span = jQuery(item).find('span');
                    var text = span.text();
                    katex.render(text, item);
                    jQuery(this).attr("id", "MathDiv");
                });
            }

            if (typeof MutationObserver === 'function') {
                var observer = new MutationObserver(function (m) {
                    setPosition(m);
                });
                observer.observe(container, {
                    childList: true
                });
            } else if (document.addEventListener) {
                container.addEventListener('DOMNodeInserted', setPosition);
            } else {
                container.attachEvent('onDOMNodeInserted', setPosition);
            }
        }


        function drawVisualization(diagramId) {
            jQuery('#GoogleChart').html('');
            jQuery('#ajaxBusy').show();
            diagramId = typeof diagramId !== 'undefined' ? diagramId : <?= $record->getUid() ?>;
            if (typeof diagramId == 'object' || diagramId == "") {
                diagramId = <?= $record->getUid() ?>;
            }

            q = '/ajax/?action=chart_data&cpotv=<?= isset($compareToOtherVehicles) ? $compareToOtherVehicles : '' ?>&tq=select_*_from_diagram_where_diagram__id=' + diagramId + '_and_vehicle__id=<?= $vehicle->getUid() ?><?= $selectRun ?>_order_by_rpm_asc';
            var query = new google.visualization.Query(q);
            // Send the query with a callback function.
            query.send(handleQueryResponse);
        }

        function handleQueryResponse(response) {
            if (response.isError()) {
                alert('<?= Strings::show('error') ?>: ' + response.getMessage() + ' ' + response.getDetailedMessage());
                jQuery('#ajaxBusy').hide();
                return;
            }

            lcolors = [];
            data = response.getDataTable();
            var cols;
            jQuery.each(data, function (i, v) {
                if (v instanceof Array) {
                    cols = v;
                    return false;
                }
            });

            var xLabel, yLabel;
            jQuery.each(cols, function (index, value) {
                if (value['p'] != undefined) {
                    if (value['p']['color'] != undefined) {
                        lcolors.push(value['p']['color']);
                    }
                    if (value['p']['x_unit'] != undefined) {
                        xLabel = value['p']['x_unit'];
                    }
                    if (value['p']['y_unit'] != undefined) {
                        yLabel = value['p']['y_unit'];
                    }
                }
            });

            chart = new google.visualization.LineChart(document.getElementById('GoogleChart'));

            google.visualization.events.addListener(chart, 'ready', function () {

                jQuery.each(jQuery('text'), function (index, label) {
                    var labelText = jQuery(label).text();
                    if (labelText.match(/_|\^/)) {
                        labelText = labelText.replace(/_([^\{])|_\{([^\}]*)\}/g, '<tspan style="font-size: smaller;" baseline-shift="sub">$1$2</tspan>')
                        labelText = labelText.replace(/\^([^\{])|\^\{([^\}]*)\}/g, '<tspan style="font-size: smaller;" baseline-shift="super">$1$2</tspan>')
                        jQuery(label).html(labelText);
                    }
                });

                fixToolTip();
            });

            chart.draw(data, {
                colors: lcolors,
//                fontSize: 20,
                enableEvents: true,
                interpolateNulls: true,
                lineWidth: 1,
                width: jQuery('.eg_chart').width(),
                height: jQuery('.eg_chart').height(),
                chartArea: {
//                    left: '10%',
//                    top: '5%',
                    width: '90%',
                    height: '90%'
                },
                vAxis: {
                    title: yLabel,
                    logScale: false,
                    minValue: 0,
//                    baseline: 0,
                    gridlines: {
                        color: '#333', count: 10
                    },
                    baselineColor: 'red'
                }
                ,
                hAxis: {
                    gridlines: {
                        color: '#333', count: 10
                    },
                    minValue: 0,
                    baseline: 0,
                    title: xLabel,
                    logScale: false,
                    format: '#.##'
                }
                ,
//                legend: 'none',
                legend: {
                    position: 'in'
                },
//                focusTarget: 'category',
                tooltip: {
                    isHtml: true
                }
            })
            ;
            jQuery('#ajaxBusy').hide();
        }

        google.load('visualization', '1', {packages: ['corechart']});
        google.setOnLoadCallback(drawVisualization);

        var currentDiagramId = 0;


        function setSessVehicle(sessId, diagramId) {
            currentDiagramId = diagramId;

            jQuery('#diagramBtns .row .col ').children().each(function (ìndex, element) {
                    jQuery(element).removeClass('active');
                }
            );

            jQuery('#b' + diagramId).addClass('active');

            const sessVehData = jQuery('#frmSessVeh__sndb1').serialize();

            jQuery.post('/ajax/?action=set_vehicle&t=' + sessId, sessVehData, function (data) {
                updatePage(data);
                drawVisualization(diagramId);
            }).fail(function(response) {
                alert('Error: ' + response.responseText);
            });
        }

    </script>
    <?php
}

if (!empty($html)) {
    ?>
    <div id="editDialog"></div>
    <!--    <fieldset>-->
    <legend></legend>
    <div class="container-fluid">
        <div class="row">

            <div class="col"><?= $html ?></div>
        </div>
        <div class="row">
            <div class="col">
                <div id="errors"></div>
            </div>
        </div>
    </div>
    <!--    </fieldset>-->
    <?php
}
?>
<center>
    <div id="ajaxBusy"><img src="/img/ajax-loader.gif" style=""/></div>
</center>

<div id="GoogleChart"></div>
<div class="row">
    <div class="col-lg-12" id="sessVehResult" name="sessVehResult"></div>
</div>


