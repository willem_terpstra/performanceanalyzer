<?php
$sessionTag = '_sndb1';

if ($form) {
    ?>
    <form name="frmSessVeh_<?= $sessionTag ?>" id="frmSessVeh_<?= $sessionTag ?>" method="POST" action="Vehicle/update">
    <input type="hidden" name="tag" id="tag" value="<?= $sessionTag ?>"/>
    <?php
}
?>
    <div class="container" id="diagramBtns">
        <div class="row">
            <div class="col">
                <button type="button"
                        id="b<?= Diagram::POWER_TORQ; ?>"
                        class="btn btn-dark btn-block"
                        data-toggle="tooltip"
                        data-placement="top"
                        data-html="true"
                        data-delay='{
                            "show":"0",
                            "hide":"1000"
                        }'
                        title="<h3>Power torque diagram</h3>
								<p>The power torque diagram shows the power and torque throughout the rev range</p>
								<p>The performance can be modified in the form, under the 'Performance' label.
								After the update click this button again to regenerate the curve.</p>
								<p><a href='/help/power-torque-diagram-tutorial'>More help...</a></p>"
                        onclick="setSessVehicle('<?= $sessionTag; ?>', '<?= Diagram::POWER_TORQ; ?>'); "><?= Strings::show('power') ?></button>
            </div>

            <div class="col">
                <button type="button"
                        id="b<?= Diagram::ACCELERATION; ?>"
                        class="btn btn-dark btn-block active"
                        data-toggle="tooltip"
                        data-placement="top"
                        data-html="true"
                        data-delay='{
                            "show":"0",
                            "hide":"1000"
                        }'
                        title="<h3>Acceleration diagram</h3>
								<p>The acceleration diagram shows the acceleration of the motorcycle  in all the gears.</p>
								<p>On the Y-axis the acceleration is given (in m/s^2), And on the X-axis the speed is given in km/h.</p>
								<p>Change any of the parameters in the form on the right, then press this button to refresh the diagram.</p>
								<p><a href='/help/acceleration-diagram-tutorial'>More help...</a></p>"
                        onclick="setSessVehicle('<?= $sessionTag; ?>', '<?= Diagram::ACCELERATION; ?>'); "><?= Strings::show('acceleration') ?></button>
            </div>

            <div class="col">
                <button type="button"
                        id="b<?= Diagram::ACCELERATION_LEAD; ?>"
                        class="btn btn-dark btn-block"
                        data-toggle="tooltip"
                        data-placement="top"
                        data-html="true"
                        data-delay='{
                            "show":"0",
                            "hide":"1000"
                        }'
                        title="<h3>Lead diagram</h3
                                <p>The lead diagram shows the advantage or disadvantage of the current setup.</p>
                                <p>The horizontal line represents time in seconds, the vertical line shows the lead or lag in meters
                                compared to the base setup</p>
                <p><a href='/help/lead-diagram-tutorial'>More help...</a></p> "
                        onclick="setSessVehicle('<?= $sessionTag; ?>', '<?= Diagram::ACCELERATION_LEAD; ?>');
                                "><?= Strings::show('acceleration_lead') ?></button>
            </div>

            <div class="col">
                <button type="button"
                        id="b<?= Diagram::ACCELERATION_DISTANCE; ?>"
                        class="btn btn-dark btn-block"
                        data-toggle="tooltip"
                        data-placement="top"
                        data-html="true"
                        data-delay='{
                            "show":"0",
                            "hide":"1000"
                        }'
                        title="<h2>Quarter mile diagram</h2>
								<p>The quarter mile diagram simulates a sprint of a quarter mile.</p>
								<p> On the vertical axle, the meters are represented, and on the horizontal axle the seconds past. </p>
								<p><a href='/help/quarter-mile-diagram-tutorial'>More help...</a></p>"
                        onclick="setSessVehicle('<?= $sessionTag; ?>', '<?= Diagram::ACCELERATION_DISTANCE; ?>'); "><?= Strings::show('acceleration_400m') ?></button>
            </div>
        </div>
<!--        <div class="row">-->
<!--            <div class="col-lg-12" id="sessVehResult" name="sessVehResult"></div>-->
<!--        </div>-->
    </div>
<?php
if ($form) {
    ?>
    </form>
    <?php
}
?>