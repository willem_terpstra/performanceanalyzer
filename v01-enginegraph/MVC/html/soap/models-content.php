<div class="eg">
    <form name="frmSessVeh__sndb1" id="frmSessVeh__sndb1" method="POST" action="Vehicle/update">
        <input type="hidden" name="m_tag" id="tag" value="_sndb1"/>
        <?php
        $principe = NULL;
        if ($vehicle->principe == '4') {
            $principe = '4-stroke';
        } elseif ($vehicle->principe == '2') {
            $principe = '2-stroke';
        }

        $es = $vehicle->engine_sprocket_t;
        $cs = $vehicle->input_sprocket_t;
        if ($es == 1) {
            $a = getCogs($cs);
            $vehicle->engine_sprocket_t = $a[0];
            $vehicle->input_sprocket_t = $a[1];
        }

        $t1Label = 'front sprocket';
        $t2Label = 'rear sprocket';

        if (strpos($vehicle->secundary_drive_type, 'cardan') !== false) {
            $t1Label = 'crown';
            $t2Label = 'pinion';
        }
        $settings = [];
        $settings[] = 'Weight';
        $settings['driver'] = [
            'm' => '<input type="text" class="m" custom-unit="kg" size="4" name="m_driver[driver_weight]" id="driver_weight" value="' . $vehicle->driver_weight . '" />&nbsp;kg',
            'i' => '<input type="text" class="i" custom-unit="lb" size="4" name="i_driver[driver_weight]" id="driver_weight" value="' . round($vehicle->driver_weight * 2.2046, 2) . '" />&nbsp;lb'
        ];
        $settings['motorcycle'] = [
            'm' => '<input type="text" class="m" size="4" custom-unit="kg" name="m_gewicht" id="gewicht" value="' . $vehicle->gewicht . '" />&nbsp;kg',
            'i' => '<input type="text" class="i" size="4" custom-unit="lb" name="i_gewicht" id="gewicht" value="' . round($vehicle->gewicht * 2.2046, 2) . '" />&nbsp;lb'
        ];

        $settings[] = 'Performance';
        $settings['power(hp@rpm)'] = '<input type="text" size="9" name="m_max_hp_rpm" id="max_hp_rpm" value="' . $vehicle->max_hp_rpm . '" />';
        $settings['torque'] = [
            'm' => '<input type="text" class="m" custom-unit="nm" size="9" name="m_max_nm_rpm" id="max_nm_rpm" value="' . $vehicle->max_nm_rpm . '" />&nbsp;nm@rpm',
            'i' => '<input type="text" class="i" custom-unit="lbft" size="9" name="i_max_nm_rpm" id="max_nm_rpm" value="' . nmAtRpmTolbAtrpm($vehicle->max_nm_rpm) . '" />&nbsp;lbft@rpm',
        ];

        $settings[] = 'Primary transmission';
        $settings['crank sprocket'] = '<input type="text" size="4" name="m_engine_sprocket_t" id="engine_sprocket_t" value="' . $vehicle->engine_sprocket_t . '" />';
        $settings['clutch sprocket'] = '<input type="text" size="4" name="m_input_sprocket_t" id="input_sprocket_t" value="' . $vehicle->input_sprocket_t . '" />';

        $settings[] = 'Secundary transmission';
        $settings[$t1Label] = '<input type="text" size="4" name="m_final_t1" id="final_t1" value="' . $vehicle->final_t1 . '" />';
        $settings[$t2Label] = '<input type="text" size="4" name="m_final_t2" id="final_t2" value="' . $vehicle->final_t2 . '" />';

        $settings[] = 'Transmission';
        if (!empty($vehicle->verh_versn_1))
            $settings['first gear ratio'] = '<input type="checkbox" title="hide or show 1st gear" name="use_g1" checked="checked" value="1" />' . round(1 / $vehicle->verh_versn_1, 4);
        if (!empty($vehicle->verh_versn_2))
            $settings['second gear ratio'] = '<input type="checkbox" title="hide or show 2nd gear" name="use_g2" checked="checked" value="2" />' . round(1 / $vehicle->verh_versn_2, 4);
        if (!empty($vehicle->verh_versn_3))
            $settings['third gear ratio'] = '<input type="checkbox" title="hide or show 3rd gear" name="use_g3" checked="checked" value="3" />' . round(1 / $vehicle->verh_versn_3, 4);
        if (!empty($vehicle->verh_versn_4))
            $settings['fourth gear ratio'] = '<input type="checkbox" name="use_g4" title="hide or show 4th gear" checked="checked" value="4" />' . round(1 / $vehicle->verh_versn_4, 4);
        if (!empty($vehicle->verh_versn_5))
            $settings['fifth gear ratio'] = '<input type="checkbox" name="use_g5" title="hide or show 5th gear" checked="checked" value="5" />' . round(1 / $vehicle->verh_versn_5, 4);
        if (!empty($vehicle->verh_versn_6))
            $settings['sixth gear ratio'] = '<input type="checkbox" name="use_g6" title="hide or show 6th gear" checked="checked" value="6" />' . round(1 / $vehicle->verh_versn_6, 4);

        $settings['rear tyre'] =
            '<table class="table">
                  <thead>
                        <tr>
                            <th scope="col">mm</th>
                            <th scope="col">/</th>
                            <th scope="col">R</th>
                        </tr>
                  </thead>
                  <tbody>
                        <tr>
                            <td><input type="text" size="4" name="m_bandmaat_aangedr_wiel" id="bandmaat_aangedr_wiel" value="' . $vehicle->bandmaat_aangedr_wiel . '"/></td>
                            <td><input type="text" size="4" name="m_band_aspect_ratio" id="band_aspect_ratio" value="' . $vehicle->band_aspect_ratio . '"/> </td>
                            <td><input type="text" size="3" name="m_wielmaat_aangedr_wiel" id="wielmaat_aangedr_wiel" value="' . $vehicle->wielmaat_aangedr_wiel . '"/></td>
                        </tr>
                  </tbody>
             </table>';

        $settings[] = 'Aerodynamic data';
        $settings['cx-value'] = '<input type="text" size="4" name="m_cx" id="cx" value="' . $vehicle->cx . '" />';
        $settings['frontal area'] = [
            'm' => '<input type="text" class="m" size="4" custom-unit="m2"  name="m_frontal_area" id="frontal_area" value="' . $vehicle->frontal_area . '" />&nbsp;m2',
            'i' => '<input type="text" class="i" size="4" custom-unit="ft2" name="i_frontal_area" id="frontal_area" value="' . 10.7639 * $vehicle->frontal_area . '" />&nbsp;ft2'
        ];


        $o = [];
        foreach ($settings as $k => $v) {
            if ($v !== NULL && (is_array($v) || trim($v) != '')) {
                $k = ucfirst($k);
                if (is_array($v)) {
                    foreach ($v as $class => $str) {
                        $o[] = "<div class='row'><div class='col'><span class=\"eg_specs $class\" >$k:</span></div><div class='col'><span class=\"$class\" >$str</span></div></div>";
                    }
                } else {
                    if (is_numeric($k)) {
                        $o[] = "<div class='row'><div class='col' colspan=\"2\"><span class=\"eg_specs_head\" >$v</span></div></div>";
                    } else {
                        $o[] = "<div class='row'><div class='col'><span class=\"eg_specs\" >$k:</span></div><div class='col'>$v</div></div>";
                    }
                }
            }
        }

        $settings = implode("\n", $o);
        ?>
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-4">
                    <div class=" eg_fields">
                        <div id="settings">
                            <div class="container">

                                <?= $settings ?>
                            </div>
                        </div>
                        <br/>
                        <button type="button"
                                class="btn btn-dark btn-block"
                                data-toggle="tooltip"
                                data-placement="top"
                                data-html="true"
                                title="<h3>Go</h3>The 'Go' button refreshes the current diagram"
                                onclick="setSessVehicle('_sndb1', currentDiagramId); ">Go
                        </button>
                        <button type="button"
                                class="btn btn-dark btn-block"
                                data-toggle="tooltip"
                                data-placement="top"
                                data-html="true"
                                title="<h3>Set</h3>The 'Set' button makes your modified graph the one to be compared against"
                                onclick="setSessVehicle('_sndb2', currentDiagramId); ">Set
                        </button>
                    </div>
                </div>
                <div class="col-lg-8 eg_chart"><?= '[engine_graph vehicle="' . $vehicle->uid . '" no_form="1" ]' ?></div>
                <input type="hidden" name="uid" id="uid" value="<?= $vehicle->uid ?>"/>
            </div>
        </div>
    </form>
</div>
