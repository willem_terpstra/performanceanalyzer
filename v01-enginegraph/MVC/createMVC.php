<?php

/**
 *
 *
 * @version $Id$
 * @copyright 2007
 */

$root = $_SERVER['DOCUMENT_ROOT'] . "/";
date_default_timezone_set('Europe/Paris');
if (!$config = simplexml_load_file('config/config.xml')) {
    trigger_error('Error reading XML file', E_USER_ERROR);
}
set_time_limit(0);
// $subDir = $config->install_dir;
$subDir = '';
require_once "../autoload.php";

$conn = mysqli_connect('localhost', $config->mysql->user, $config->mysql->pwd);
mysqli_select_db($conn, $config->mysql->db);

dbTable::setConnection($conn);

class MVC
{
    const TEMPLATE_DIR = "MVC/template/";
    const GENERATED_DIR = "entity/generated/";
    const CLASS_DIR = "entity/";
    const LIB_DIR = "classes/lib/";
    const GENERATED_PREFIX = "_";

    const CONTROLLER_DIR = "controllers/";
    const VIEW_DIR = "views/";
    const GENERATED_VIEW_DIR = "views/generated/";
    const HTML_DIR = "html/";

    private $entityGenerate = true;
    private $controllerGenerate = true;
    private $viewGenerate = true;
    private $htmlGenerate = true;
    private $frontendGenerate = true;

    public $conn;

    public function main()
    {
        global $dir;
        global $subDir;
        global $config;
        $subDir = 'v01-enginegraph/';


        mysqli_select_db($this->conn, $config->mysql->db);

        $path = $_SERVER['DOCUMENT_ROOT'] . "/$subDir/" . self::TEMPLATE_DIR;

//        $sql = "SHOW TABLES";
//        $rs = mysqli_query($this->conn, $sql);
        $tables = [
            'chassis',
            'driver',
            'engine',
            'clutch',
        ];
//		$metaTableSettings = new MetaTableSettings();

        foreach($tables as $table_name) {
//        while ($row = mysqli_fetch_assoc($rs)) {
            $privates = '';
            $fillers = '';
            $getters = '';
            $viewSetters = '';
            $fkMethods = '';
            $modifiedMap = '';
            $soapDeclarations = '';
            $soapOut = '';
            $lijstRecords = '';
            $browseLijstRecords = '';
            $detailRecords = '';
            $browseDetailRecords = "";
            $metaTableSettingsAvailable = false;
            $this->entityGenerate = true;
            $this->controllerGenerate = false;
            $this->viewGenerate = false;
            $this->htmlGenerate = false;
            $this->frontendGenerate = false;


//            $table_name = $row['Tables_in_' . $config->mysql->db];
            $sql = "SHOW FIELDS FROM `$table_name`";

            if (strstr($table_name, 'status')) {
                $this->controllerGenerate = false;
                $this->viewGenerate = false;
                $this->htmlGenerate = false;
            }

//			$metaTableSettingsAvailable = $metaTableSettings->loadByProperty('target_table',$table_name);
            if ($metaTableSettingsAvailable) {
                echo 'meta_table_settings found for ' . $table_name . '<br/>';
                $this->entityGenerate = ($metaTableSettings->getCreateEntity() == '1');
                $this->controllerGenerate = ($metaTableSettings->getCreateController() == '1');
                $this->viewGenerate = ($metaTableSettings->getCreateView() == '1');
                $this->htmlGenerate = ($metaTableSettings->getCreateHtml() == '1');
                $this->frontendGenerate = ($metaTableSettings->getCreateFrontend() == '1');
                if (!$this->entityGenerate) {
                    $this->controllerGenerate = false;
                    $this->viewGenerate = false;
                    $this->htmlGenerate = false;
                    echo 'entity generate switched off for ' . $table_name . '<br/>';
                }
                if (!$this->controllerGenerate) {
                    $this->viewGenerate = false;
                    $this->htmlGenerate = false;
                    echo 'controller generate switched off for ' . $table_name . '<br/>';
                }
                if (!$this->viewGenerate) {
                    $this->htmlGenerate = false;
                    echo 'view generate switched off for ' . $table_name . '<br/>';
                }
                if (!$this->htmlGenerate) {
                    $this->htmlGenerate = false;
                    echo 'html generate switched off for ' . $table_name . '<br/>';
                }
            } else {
                echo 'no meta_table_settings for ' . $table_name . '<br/>';
            }
            $rsFields = mysqli_query($this->conn, $sql);

            $className = $this->camelCase($table_name);
            while ($fieldRow = mysqli_fetch_assoc($rsFields)) {
                $fieldName = $fieldRow['Field'];
                $camelCase = $this->camelCase($fieldName);

                $t = new TemplateEngine($path . "private.php");
                $t->assign('varName', $fieldName);
                $privates .= $t->execTemplate();

                $t = new TemplateEngine($path . "modifiedMap.php");
                $t->assign('varName', $fieldName);
                $modifiedMap .= $t->execTemplate();

                $t = new TemplateEngine($path . "soapDeclarations.php");
                $t->assign('varName', $fieldName);
                $soapDeclarations .= $t->execTemplate();

                $t = new TemplateEngine($path . "soapOut.php");
                $t->assign('varName', $fieldName);
                $soapOut .= $t->execTemplate();

                $t = new TemplateEngine($path . "fillers.php");
                $t->assign('varName', $fieldName);
                $fillers .= $t->execTemplate();

                $arr = explode("_", $fieldName);
                if (strtolower($arr[count($arr) - 1]) == "id" && count($arr) > 1) {
                    unset($arr[count($arr) - 1]);
                    $funcName = dbTable::camelCase($funcName = implode('_', $arr));
                    $referencedTable = dbTable::getReferencedTableName($config->mysql->db, $table_name, $fieldName);
                    if (empty($referencedTable)) {
                        echo "Warning: no foreign key defined for $table_name.$fieldName<br/>";
                    } else {
                        $referencedClass = dbTable::camelCase($referencedTable);
                        $t = new TemplateEngine($path . "fkMethods.php");
                        $t->assign('className', $referencedClass);
                        $t->assign('funcName', $funcName);
                        $t->assign('varName', $fieldName);
                        $fkMethods .= $t->execTemplate();
                        echo "Generated  $camelCase foreign key functions for $table_name.$fieldName => $referencedTable <br/>";
                    }
                }


                $t = new TemplateEngine($path . "viewSetters.php");
                $t->assign('className', $className);
                $t->assign('funcName', $camelCase);
                $t->assign('varName', $fieldName);
                $viewSetters .= $t->execTemplate();
            }

            // Data Model base class
            $t = new TemplateEngine($path . "generatedClassTemplate.php");
            $t->assign('conn', $this->conn);
            $t->assign('subDir', $subDir);
            $t->assign('baseClassDir', self::CLASS_DIR);
            $t->assign('libClassDir', self::LIB_DIR);
            $t->assign('table_name', $table_name);
            $t->assign('className', self::GENERATED_PREFIX . $className);
            $t->assign('modifiedMap', $modifiedMap);
            $t->assign('soapDeclarations', $soapDeclarations);
            $t->assign('soapOut', $soapOut);
            $t->assign('privates', $privates);
            $t->assign('fillers', $fillers);
            $t->assign('getters', $getters);
            $t->assign('fkMethods', $fkMethods);

            $content = $t->execTemplate();
            $generatedFilePath = self::GENERATED_DIR . self::GENERATED_PREFIX . $className . ".php";

            if ($this->entityGenerate) {
                $this->saveFile($generatedFilePath, $content);
            } else {
                echo "file $generatedFilePath NOT generated (see meta_table_settings)<br/>";
            }


            // Data model class
            $filePath = self::CLASS_DIR . $className . ".php";

            if (!file_exists($filePath)) {
                $t = new TemplateEngine($path . "classTemplate.php");
                $t->assign('includeFile', self::GENERATED_DIR . self::GENERATED_PREFIX . $className . ".php");
                $t->assign('extendClass', self::GENERATED_PREFIX . $className);
                $t->assign('className', $className);

                $content = $t->execTemplate();

                if ($this->entityGenerate) {
                    $this->saveFile($filePath, $content);
                } else {
                    echo "file $filePath NOT generated (see meta_table_settings)<br/>";
                }

            }

            // Controller
            $filePath = $_SERVER['DOCUMENT_ROOT'] . "/$subDir/MVC/" . self::CONTROLLER_DIR . $className . "Controller.php";
            if (!file_exists($filePath)) {
                $t = new TemplateEngine($_SERVER['DOCUMENT_ROOT'] . "/$subDir/" . self::TEMPLATE_DIR . "controllerTemplate.php");
                $t->assign('subDir', $subDir);
                $t->assign('className', $className);

                $content = $t->execTemplate();

                if ($this->entityGenerate && $this->controllerGenerate) {
                    $this->saveFile($filePath, $content);
                } else {
                    echo "file $filePath NOT generated (see meta_table_settings)<br/>";
                }
            }

            // Base view, generate always
            $filePath = $_SERVER['DOCUMENT_ROOT'] . "/$subDir/MVC/" . self::GENERATED_VIEW_DIR . self::GENERATED_PREFIX . $className . "View.php";
            $t = new TemplateEngine($_SERVER['DOCUMENT_ROOT'] . "/$subDir/" . self::TEMPLATE_DIR . "generatedViewTemplate.php");
            $t->assign('className', $className);
            $t->assign('setters', $viewSetters);
            $content = $t->execTemplate();

            if ($this->entityGenerate && $this->viewGenerate) {
                $this->saveFile($filePath, $content);
            } else {
                echo "file $filePath NOT generated (see meta_table_settings)<br/>";
            }

            // Extended view
            $filePath = $_SERVER['DOCUMENT_ROOT'] . "/$subDir/MVC/" . self::VIEW_DIR . $className . "View.php";
            if (!file_exists($filePath)) {
                $t = new TemplateEngine($_SERVER['DOCUMENT_ROOT'] . "/$subDir/" . self::TEMPLATE_DIR . "viewTemplate.php");
                $t->assign('className', $className);
                $content = $t->execTemplate();

                if ($this->entityGenerate && $this->viewGenerate) {
                    $this->saveFile($filePath, $content);
                } else {
                    echo "file $filePath NOT generated (see meta_table_settings)<br/>";
                }

            }

            // generate html late, otherwise you cannot use classes in meta html
            if ($this->htmlGenerate) {
                $rsFields = mysqli_query($this->conn, $sql);
                while ($fieldRow = mysqli_fetch_assoc($rsFields)) {
                    $fieldName = $fieldRow['Field'];
                    $fieldType = $fieldRow['Type'];
                    $camelCase = $this->camelCase($fieldName);

                    $t = new TemplateEngine($path . "lijstRecords.html");
                    $t->assign('className', $className);
                    $t->assign('funcName', $camelCase);
                    $t->assign('mode', 'edit');
                    $lijstRecords .= $t->execTemplate();

                    $t = new TemplateEngine($path . "lijstRecords.html");

                    $t->assign('className', $className);

                    $t->assign('funcName', $camelCase);

                    $t->assign('mode', 'browse');

                    $browseLijstRecords .= $t->execTemplate();


                    $t = new TemplateEngine($path . "detailRecords.php");
                    $t->assign('subDir', $subDir);
                    $t->assign('className', $className);
                    $t->assign('funcName', $camelCase);
                    $t->assign('varName', $fieldName);
                    $t->assign('fieldType', $fieldType);
                    $t->assign('includePath', self::CLASS_DIR);
                    $detailRecords .= $t->execTemplate();

                    $t = new TemplateEngine($path . "browseDetailRecords.php");
                    $t->assign('subDir', $subDir);
                    $t->assign('className', $className);
                    $t->assign('funcName', $camelCase);
                    $t->assign('varName', $fieldName);
                    $t->assign('fieldType', $fieldType);
                    $t->assign('includePath', self::CLASS_DIR);
                    $browseDetailRecords .= $t->execTemplate();

                }

                // Backend HTML
                $filePath = $_SERVER['DOCUMENT_ROOT'] . "/$subDir/MVC/" . self::HTML_DIR . $className . "Lijst.html";
                if (!file_exists($filePath)) {
                    $t = new TemplateEngine($_SERVER['DOCUMENT_ROOT'] . "/$subDir/" . self::TEMPLATE_DIR . "lijstTemplate.html");
                    $t->assign('className', $className);
                    $t->assign('lijstRecords', $lijstRecords);

                    $content = $t->execTemplate();
                    $this->saveFile($filePath, $content);

                }

                // Frontend HTML
                if ($this->frontendGenerate) {
                    $filePath = $_SERVER['DOCUMENT_ROOT'] . "/$subDir/MVC/" . self::HTML_DIR . 'browse' . $className . "Lijst.html";
//					if (!file_exists($filePath))
//					{	
                    $t = new TemplateEngine($_SERVER['DOCUMENT_ROOT'] . "/$subDir/" . self::TEMPLATE_DIR . "browseLijstTemplate.html");
                    $t->assign('className', $className);
                    $t->assign('lijstRecords', $browseLijstRecords);

                    $content = $t->execTemplate();
                    $this->saveFile($filePath, $content);
//					}
                } else {
                    echo "frontend generate switched off for $table_name (default for status tables)<br/>";
                }


                // Detail HTML
                $filePath = $_SERVER['DOCUMENT_ROOT'] . "/$subDir/MVC/" . self::HTML_DIR . $className . "Detail.html";
                if (!file_exists($filePath)) {
                    $t = new TemplateEngine($_SERVER['DOCUMENT_ROOT'] . "/$subDir/" . self::TEMPLATE_DIR . "detailTemplate.php");
                    $t->assign('subDir', $subDir);
                    $t->assign('className', $className);
                    $t->assign('detailRecords', $detailRecords);
                    $content = $t->execTemplate();

                    $this->saveFile($filePath, $content);


                }

                // Browse detail HTML
                $filePath = $_SERVER['DOCUMENT_ROOT'] . "/$subDir/MVC/" . self::HTML_DIR . 'browse' . $className . "Detail.html";
                if (!file_exists($filePath)) {
                    $t = new TemplateEngine($_SERVER['DOCUMENT_ROOT'] . "/$subDir/" . self::TEMPLATE_DIR . "browseDetailTemplate.php");
                    $t->assign('subDir', $subDir);
                    $t->assign('className', $className);
                    $t->assign('browseDetailRecords', $browseDetailRecords);
                    $content = $t->execTemplate();
                    $this->saveFile($filePath, $content);

                }
            } else {
                echo "HTML not generated for table: $table_name<br/>";
            }
        }
    }


    private function saveFile($filePath, $content)
    {
        $handle = fopen($filePath, "w");
        fwrite($handle, $content);
        fclose($handle);
        echo "file generated: $filePath<br/>";
    }

    private function camelCase($fieldName)
    {
        $arr = explode("_", $fieldName);
        $result = "";
        foreach ($arr as $part) {
            $result .= ucfirst($part);
        }
        $result = ucfirst($result);
        return $result;
    }

}

$c = new MVC();
$c->conn = $conn;
$c->main();

?>