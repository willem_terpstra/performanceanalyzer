<?php echo "<?php " ?>

class <?php echo $className; ?> extends dbTable    {
/* db table */
protected    $tableName = '<?php echo $table_name ?>';
protected    $pk = '<?php


$sql =
$rs = mysqli_query($conn, "SHOW INDEX FROM  $table_name");
while ($row = mysqli_fetch_array($rs)) {
    if ($row['Key_name'] == 'PRIMARY') {
        echo $row['Column_name'];
    }
}
?>';
protected $fieldsInfo = array(
<?php
$rs = mysqli_query($conn, "SHOW FULL COLUMNS FROM $table_name");
while ($row = mysqli_fetch_assoc($rs)) {
    ?>          '<?= $row['Field'] ?>' => [
                    'Type' => "<?= $row['Type'] ?>",
                    'Null' => '<?= $row['Null'] ?>',
    <?php
    if(!empty($row['Comment'])):
        $comment = json_decode($row['Comment']);
        if (false === $comment):
            throw new Exception('json_decode of comment failed: ' . var_export($row, true));
        endif;
        ?>
                'Extra' => <?= var_export((array)$comment, true) ?>
        <?php
    else:
        ?>
                'Extra' => []
        <?php

    endif;
    ?>

            ],
<?php }
?>
);

/* modification map */
protected  $dbModified = array (
<?php echo $modifiedMap; ?>
);

/* properties */
<?php echo $privates; ?>

/* soap declarations */
public static $soapDeclarations = array(
<?php echo $soapDeclarations; ?>
);

/* constructor */
public function __construct ($id = null)
{
<?php
if (substr(strtolower($table_name), 0, 3) == 'vw_') {
    ?>
    $this->is_view = true;
    <?php
}
?>

if ( !empty($id) )
{    $this->load($id);
}

parent::__construct($this->getTableName());
}

public function getClassName()
{    return "<?php echo $className ?>";
}

public function getTableName()
{    return $this->tableName;
}

public function getSoapOut()
{
return array(
<?php echo $soapOut; ?>
);
}


/* FK methods */
<?php echo $fkMethods; ?>

}

