public function get<?php echo $funcName; ?>List ($sql = null)    {
$<?php echo $className; ?> = new <?php echo $className; ?>();
$list = array();
$list = $<?php echo $className; ?>->lijst($sql);
return $list;
}

public function get<?php echo $funcName; ?> ($uid = NULL, $getObject = false)    {
if(empty($uid)) {
$uid = $this->get<?php echo $funcName; ?>Id();
}
$<?php echo $className; ?> = new <?php echo $className; ?>($uid);
if($getObject) {
return $<?php echo $className; ?>;
}
return $<?php echo $className; ?>->getValue();
}
