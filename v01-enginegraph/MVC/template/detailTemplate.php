<form
        id="frm<?php echo $className ?>"
        name="<?php echo $className ?>"
        method="POST"
        action="/<?php echo strtolower($className) ?>/update"
        class="niceform">
    <input type="hidden" id="act" name="act" value=""/>
    <!-- 			<input type="hidden" name="uid" value="<![CPHP[ echo $record->getUid(); ]CPHP]>"  /> -->
    <fieldset>
        <legend><?php echo $className ?></legend>
        <table>
            <![CPHP[
            if(isset($messages) && !empty($messages)) {
            echo '
            <div class="ui-state-error ui-corner-all">';
                foreach ( $messages as $message )
                { echo $message . '<br/>';
                }
                echo '
            </div>
            ';
            }
            ]CPHP]>
            <?php echo $detailRecords ?>
        </table>
    </fieldset>

    <button
            onclick="
                    frm = document.getElementById('frm<?php echo $className ?>');
                    el_act = document.getElementById('act');
                    el_act.value = 'UPDATE';
                    frm.submit();
                    "
            type="button">Save
    </button>
    <button
            onclick="
                    frm = document.getElementById('frm<?php echo $className ?>');
                    el_act = document.getElementById('act');
                    el_act.value = 'REMOVE';
                    frm.submit();
                    "
            type="button">Delete
    </button>
    <button
            onclick="
                    frm = document.getElementById('frm<?php echo $className ?>');
                    el_act = document.getElementById('act');
                    el_act.value = 'CANCEL';
                    frm.submit();
                    "
            type="button">Cancel
    </button>
</form>