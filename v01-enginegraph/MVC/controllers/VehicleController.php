<?php

class VehicleController extends Controller
{

    const FIND_DIAGRAMS = 'FIND_DIAGRAMS';

    private function ajaxCommand($view, $uid, $action)
    {
        global $request;
        $get = Get::$getVars;
        if (isset($request[5])) {
            if (is_numeric($request[5])) {
                $value = $request[5];
            }
        }
        switch ($action) {
        case 'setTransmission':
            if (isset($get['tag'])) {
                $cleanedSessionTag = self::cleanSessionTag(Get::$getVars['tag']);
                $vehicle = Sandbox::get($cleanedSessionTag);
                $transmission = new Transmission($value);
                $vehicle->setTransmissionId($transmission->getUid());
                $vehicle->setTransmission($transmission);
                Sandbox::set($vehicle);

                $transmissionView = new TransmissionView($value);
                $summary = $transmissionView->summary();
                $update = [];
                if (empty(Get::$getVars['frm_count'])) {
                    $update["frmSessVeh_$cleanedSessionTag div[id=transmissionInfo]"] = $summary;
                } else {
                    for ($i = 1; $i <= Get::$getVars['frm_count']; $i++) {
                        $update["frmSessVeh_$cleanedSessionTag" . "_$i div[id=transmissionInfo]"] = $summary;
                    }
                }
                $update['input_sprocket_t'] = $transmissionView->getInstance()->getInputSprocketT();
                return json_encode([
                    'error' => '',
                    'update' => $update,
                    'code' => 0
                ]);
            }
            return json_encode([
                'error' => Strings::show('tag_not_found'),
                'update' => [],
                'code' => 0
            ]);
        case 'getChassisTypes':
            $typeSelector = $get['target'];
            $merkId = $uid;
            $selectedEngineId = 0;
            return VehicleView::getChassisTypes($merkId, $selectedEngineId, $typeSelector);
        case 'getMotorTypes':
            $typeSelector = $get['target'];
            $merkId = $uid;
            $selectedEngineId = 0;
            return VehicleView::getMotorTypes($merkId, $selectedEngineId, $typeSelector);
        case 'getEngineRuns':
            $runSelector = $get['target'];
            $engineId = $uid;
            $selectedEngineId = 0;
            return VehicleView::getEngineRuns($engineId, $selected, $runSelector);
        case 'getRuns':
            $vehicleView = new VehicleView($uid);
            $html = $vehicleView->getRuns();
            if (Get::$getVars['o'] == 'json') {
                return json_encode([
                        'update' => [
                            'run_data' => $html,
                            'RunResult' => implode('<br/>', $vehicleView->getMessagesAsStrings()),
                        ]
                    ]
                );
            }
            return $vehicleView->getRuns();
        case 'selectPower':
            if (isset($get['v']) && !empty($get['v'])) {
                if (is_numeric($get['v'])) {
                    $vehicle = new Vehicle($get['v']);
                    $vehicle->setSessionTag(Get::$getVars['tag']);
                } else {
                    $vehicle = Sandbox::get($get['v']);
                }
            } else {
                $vehicle = new Vehicle();
            }
            return VehicleView::getPowerSelectbox($vehicle);
        case 'selectEngine':
            $t = new TemplateEngine('MVC/html/custom/frontend/scalar.html');
            $t->selectBox(Get::$getVars['elementName'], "VwEngineTuningRun", Get::$getVars['id'], 'onchange="setVehicleParameter(this);"');
            return $t->execTemplate();
        case 'selectTransmission':
            if (is_numeric($get['t'])) {
                return VehicleView::getTransmissionSelectBox($get['t'], $get['tag']);
            } else {
                throw new ControllerException('Invalid value for select Transmission: ' . $get['t']);
            }

        case 'setRun':
            if (isset(Get::$getVars['tag'])) {
                $cleanedSessionTag = self::cleanSessionTag(Get::$getVars['tag']);
                $vehicle = Sandbox::get($cleanedSessionTag);
                $vehicle->initRun(new Run($value));
                Sandbox::set($vehicle);

                $runView = new RunView($value);
                $summary = $runView->summary();
                $update = [];
                if (empty(Get::$getVars['frm_count'])) {
                    $update["frmSessVeh_$cleanedSessionTag div[id=runInfo]"] = $summary;
                } else {
                    for ($i = 1; $i <= Get::$getVars['frm_count']; $i++) {
                        $update["frmSessVeh_$cleanedSessionTag" . "_$i div[id=runInfo]"] = $summary;
                    }
                }
                $update['engine_sprocket_t'] = $runView->getInstance()->getEngine()->getEngineSprocketT();
                return json_encode([
                    'error' => '',
                    'update' => $update,
                    'code' => 0
                ]);
            }

            if (empty($value)) {
                return 'EmptyRunId';
            }

            $vehicle = new Vehicle($uid);

            $runId = $value;
            $vehicle->setRunId($runId);
            Ajax::setInstance('Vehicle', $vehicle);
            $vehicle->saveOrUpdate();
            $r = new Run($runId);

            return '<b>Run "' . $r->getNaam() . '"</b> ' . Strings::show('used_run');
        case 'setCmpRun':
            if (!isset($_SESSION['cmpRuns'])) {
                $_SESSION['cmpRuns'] = [];
            }
            if (in_array($value, $_SESSION['cmpRuns'])) {
                if ($get['v'] == 'false') {
                    foreach ($_SESSION['cmpRuns'] as $k => $c) {
                        if ($c == $value) {
                            unset($_SESSION['cmpRuns'][$k]);
                        }
                    }
                    return '<b>Run ' . $value . ' deselected for compare.';
                } elseif ($get['v'] == 'true') {
                    return '<b>Run ' . $value . ' was already selected for compare.';
                }
                return 'Unknown checkbox v: ' . $get['v'];
            } else {
                if ($get['v'] == 'true') {
                    $_SESSION['cmpRuns'][] = $value;
                }
            }
            if (count($_SESSION['cmpRuns']) == 1) {
                return '<b>Run ' . $value . ' selected for compare.';
            }
            if (count($_SESSION['cmpRuns']) > 1) {
                return '<b>Select a diagram to compare runs ' . implode(', ', $_SESSION['cmpRuns']);
            }
            return 'Selection of run ' . $value . ' to compare failed';
        case 'getGears':
            $browse = true;
            if (array_key_exists('e', Get::$getVars)) {
                if (!empty(Get::$getVars['e']) && Get::$getVars['e'] == 1) {
                    $browse = false;
                }
            }
            return $view->getGearTable($browse);
        case 'getTransmissionData':
            return $view->getTransmissionData();
        case 'setSessVeh':
            return self::setSessVeh(Get::$getVars['t'], Post::$postVars);

        case 'addSessVeh':
            $tag = self::cleanSessionTag(Get::$getVars['t']);
            $vehicle = new Vehicle($uid);
            Sandbox::set($vehicle);
            $diagram = new Diagram(Post::$postVars['diagram_id']);
            $t = new TemplateEngine('MVC/html/custom/frontend/diagram/diagram' . $diagram->getUsedClassName() . '.html');
            $t->assign('diagram', $diagram);
            $t->assign('vehicle', $vehicle);
            $html = $t->execTemplate();
            $close = new TemplateEngine('MVC/html/custom/frontend/diagram/close.php');
            $close->assign('sessionTag', $vehicle->getSessionTag());
            $close = $close->execTemplate();

            $result = [
                'update' => [
                    $tag => $html,
                ],
                'label' => $vehicle->getModel() . $close,
            ];
            return (json_encode($result));
        case 'removeSessVeh':
            Sandbox::remove(Get::$getVars['t']);
            return 'ok';
        case 'calcVolume':
            $vehicle = new Vehicle($uid);
            try {
                $vol = round($vehicle->getVolumePerCyl() * $vehicle->getAantalCyl(), 2);
            } catch (Exception $e) {
                return 0;
            }
            return $vol;
        case 'calcCompression':
            $vehicle = new Vehicle($uid);
            try {
                $comp = round($vehicle->getCompression(), 2);
            } catch (Exception $e) {
                return 0;
            }
            return $comp;
        case 'gearboxName':
            return 'gearboxname';
        case 'getHtml':
            $file = Get::$getVars['f'];
            $diagramId = (int)Get::$getVars['d'];
            if ($file == 'dropdownSandbox') {
                $tag = Authorization::generatePassword();
                $t = new TemplateEngine('MVC/html/custom/dropdownVehicle.php');
                $t->assign('tag', $tag);
                $t->assign('diagramId', $diagramId);
                $html = $t->execTemplate();
                $html = "<div id=\"$tag\" >$html</div>";
                return $html;
            }
            return '<h1>Error</h1>';
        default:
        }
        return 'Error in ajax command: ' . $action;
    }

    public function run($view, $uid, $action)
    {
        $post = Post::$postVars;

        global $request;
        if (isset($post['uid'])) {
            if (!empty($post['uid'])) {
                $uid = $post['uid'];
            }
        }
        if (is_string($view)) {
            if ($uid == '0') {
                $uid = NULL;
            }
            $view = new $view($uid);
        }

        // init child objects
        $childs = Vehicle::getChilds();
        if ($view instanceof VehicleView) {
            foreach ($childs as $childName => $className) {
                if (isset($post[$childName . '_id'])) {
                    if (!empty($post[$childName . '_id'])) {
                        $view->setChildObject($post[$childName . '_id'], $className);
                    }
                }
            }
        }
        $ajaxCommand = Ajax::getCommand();
        if (!empty($ajaxCommand)) {
            try {
                return $this->ajaxCommand($view, $uid, $ajaxCommand);
            } catch (Exception $e) {
                if (Controller::isJson()) {
                    return json_encode([
                        'update' => [
                            $view->getErrorDivId() => $e->getMessage()
                        ]
                    ]);
                } else {
                    return $e->getMessage();
                }
            }
        }

        if (!in_array($action, [
            Controller::ADD, Controller::DETAIL, Controller::SAVENEW, Controller::UPDATE
        ])
        ) {
            $found = false;
            if (!empty($uid)) {
                foreach (Sandbox::getAll() as $vehicle) {
                    if ($vehicle->getUid() == $uid) {
                        $found = true;
                        $view->setVehicle($vehicle);
                    } else {
                        Sandbox::remove($vehicle->getSessionTag());
                    }
                }
                if (!$found) {
                    $view->setVehicle(Sandbox::set($view->getVehicle()));
                }
            }
        }

        try {
            $content = parent::run($view, $uid, $action);
        } catch (dbTableException $e) {
            switch ($e->getCode()) {
            case dbTableException::LOAD_FAILED_EXCEPTION:
                header('HTTP/1.0 404 Not Found');
                // TODO: remove getMessage
                return Strings::show('vehicle_not_found') . ' ' . $e->getMessage();
            default:
                header('HTTP/1.0 404 Not Found');
                // TODO: remove getMessage
                return $e->getMessage();
            }
        } catch (ControllerException $e) {
            if ($e->getCode() == ControllerException::NO_VIEW) {
                header('HTTP/1.0 404 Not Found');
                return Strings::show('page_not_found');
            }
            if ($e->getCode() == ControllerException::ACTION_NOT_FOUND) {
                if ($action == self::FIND_DIAGRAMS) {
                    $content = $view->findDiagrams(Get::$getVars['diagram_id']);
                }
            }
        }

        if ($content == false) {
            if (isset($post['act'])) {
                switch ($post['act']) {

                default:
                    throw new Exception("action {$post['act']} not defined");
                }
            }
        }

        return $content;
    }

    private static function cleanSessionTag($tag)
    {
        if (preg_match('/(.*sndb\d+)_\d/', $tag, $regs)) {
            $tag = $regs[1];
        }
        return $tag;
    }

    public static function setSessVeh($tag, $settings, $errorStr = ''): array
    {
        $tag = self::cleanSessionTag($tag);
        $vehicle = Sandbox::get($tag);
        $orgVehicle = new Vehicle($settings['uid']);

        if ($vehicle instanceof Vehicle) {
            $vehicle->validate($settings);
            $errors = $vehicle->getMessages();
            $maxErrorLevel = $vehicle->getHighestErrorLevel();
            if ($maxErrorLevel <= Message::WARNING_LVL) {
//			if (empty($errors)) {			
                $vehicle->fill($settings);
//                $vehicle->convertUnits();
                $vehicle->clearMessages();
                Sandbox::set($vehicle);
                try {
                    $cmpResult = $orgVehicle->compare($vehicle, $settings);

                    $cmpResult = DiagramView::showDiff($cmpResult);

                    $t = new TemplateEngine('MVC/html/soap/bootstrap/info.html');
                    $t->assign('str', $cmpResult);
                    $cmpResult = $t->execTemplate();
                } catch (VehicleException $e) {
                    $errors[] = Strings::show($e->getMessage());
                }
            }
        } else {
            $errors[] = Strings::show('session_timed_out');
        }

        $eRes = [];
        foreach ($errors as $error) {
            if ($error instanceof Message) {
                $t = new TemplateEngine('MVC/html/soap/bootstrap/' . strtolower($error->getSeverity()) . '.html');
                $t->assign('str', $error->getSeverity() . ': ' . $error->getMessage());
                $eRes[] = $t->execTemplate();
            } elseif (is_string($error)) {
                $t = new TemplateEngine('MVC/html/soap/bootstrap/error.html');
                $t->assign('str', $error->getMessage());
                $eRes[] = $t->execTemplate();
            }
        }

        $result = [
            'update' => [
                'frmSessVeh_' . $tag . ' div[name=sessVehResult]' => [$cmpResult],
                'errors' => $eRes,
            ],
        ];

        return $result;
//        return json_encode($result, JSON_PRETTY_PRINT);
    }

}

?>