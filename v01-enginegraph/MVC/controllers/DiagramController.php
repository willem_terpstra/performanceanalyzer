<?php

class DiagramController extends Controller
{

    private static $selectedVehicle = NULL;
    private $soapRunIds = [];

    public static function getSelectedVehicle()
    {
        return self::$selectedVehicle;
    }

    private function ajaxCommand($view, $uid, $action)
    {
        global $request;
        $get = Get::$getVars;
        if (isset($request[5])) {
            if (is_numeric($request[5])) {
                $value = $request[5];
            }
        }
        switch ($action) {
        case 'selectVehicle':
            $tag = $get['tag'];
            return DiagramView::getVehicleSelectbox($tag, $uid);
        case 'getTypes':
            $typeSelector = $get['target'];
            $merkId = $get['merken_id'];
            return DiagramView::getVehicleTypes($merkId, $uid, $typeSelector);
        }
        return 'Error in ajax command: ' . $action;
    }

    public function selectGetvarRuns()
    {
        $runIds = explode(',', Get::$getVars['run_id']);
        return $runIds;
    }

    private function compare($diagramId)
    {
        $vehicle = NULL;
        $gd = [];
        $viewName = '';
        $diagram = new Diagram($diagramId);
        $name = $diagram->getName();
        $viewName = dbTable::camelCase($name) . 'DiagramView';
        $diagramView = new $viewName($diagramId);
        $gdArray = [];
        foreach (Sandbox::getAll() as $vehicle) {
            self::$selectedVehicle = $vehicle;
            if (isset(Get::$getVars['run_id'])) {
                if (!empty(Get::$getVars['run_id']) && is_numeric(Get::$getVars['run_id'])) {
                    $vehicle->setSelectedRun(new Run(Get::$getVars['run_id']));
                }
            }
            $diagramView->setVehicle($vehicle);
            $diagram = $diagramView->getInstance();
            $errors = $diagram->validateInput($vehicle);
            if (!empty($errors)) {
                // handled in interface
                continue;
            }
            $gd = $diagramView->collect($vehicle->getLineColor());
            $gdArray = array_merge($gdArray, $gd);
        }
        if (array_key_exists('o', Get::$getVars) && Get::$getVars['o'] == 'json') {
            return $diagramView->toJsonQuery($gdArray);
        }
        return null;
    }

    public function run($view, $uid, $action)
    {
        $post = Post::$postVars;
        $get = Get::$getVars;
        $errors = [];
        $ajaxCommand = Ajax::getCommand();
        if (!empty($ajaxCommand)) {
            return $this->ajaxCommand($view, $uid, $ajaxCommand);
        }
        if ($action == 'COMPARE' || $action == 'IMAGE') {
            if (array_key_exists('run_id', $get)) {
                if (is_numeric($get['run_id'])) {
                    Sandbox::removeAll();
                    Sandbox::set(new Vehicle($get['vehicle_id']));
                    $data = $this->compare($uid);
                    Sandbox::removeAll();
                    return $data;
                } else {
                    return NULL;
                }
            }
            return $this->compare($uid);
        }

        $vehicleIds = [];
        if (array_key_exists('vehicle_id', $get)) {
            if (is_numeric($get['vehicle_id'])) {
                $vehicleIds[] = $get['vehicle_id'];
            } else {
                if (strpos($get['vehicle_id'], ',') !== false) {
                    $vehicleIds = explode(',', $get['vehicle_id']);
                }
            }
        }
        if (!empty($this->soapRunIds)) {
            $runIds = $this->soapRunIds;
        }
        if (empty($vehicleIds)) {
            if (array_key_exists('run', Get::$getVars) || !empty($runIds)) {
                if (empty($runIds)) {
                    $runIds = Get::$getVars['run'];
                }
                $runs = [];
                Sandbox::removeAll();
                if (is_array($runIds)) {
                    foreach ($runIds as $runId) {
                        try {
                            $run = new Run($runId);
                        } catch (dbTableException $e) {
                            if ($e->getCode() == dbTableException::LOAD_FAILED_EXCEPTION) {
                                $errors[] = new Message('run_removed', Message::ERROR, 'Run');
                                continue;
                            }
                            throw new dbTableException($e->getMessage(), $e->getCode());
                        }
                        $vehicleIds[] = $run->getVehicleId();
                        $runs[] = $run;
                    }
                }
            }
        }

        $view = new $view($uid);
        if (!empty($errors)) {
            $view->setMessages($errors);
        }
        foreach ($vehicleIds as $k => $vehicleId) {
            if (!empty($vehicleId)) {
                foreach (Sandbox::getAll() as $vehicle) {
                    if ($vehicle->getUid() == $vehicleId) {
                        unset($vehicleIds[$k]);
                        if (isset($runs)) {
                            unset($runs[$k]);
                        }
                        break;
                    }
                }
            }
        }
        // reindex
        if (isset($vehicleIds) && is_array($vehicleIds)) {
            $vehicleIds = array_values($vehicleIds);
        }
        if (isset($runs) && is_array($runs)) {
            $runs = array_values($runs);
        }
        $first = true;
        $i = 0;
        foreach ($vehicleIds as $vehicleId) {
            $vehicle = new Vehicle($vehicleId);
            if (isset($runs) && is_array($runs) && array_key_exists($i, $runs)) {
                $vehicle->setSelectedRun($runs[$i]);
            }
            if ($first) {
                $view->setVehicle($vehicle);
                $first = false;
            }

            Sandbox::set($vehicle);
            $i++;
        }

        $content = parent::run($view, $uid, $action);
        if ($content == false) {
            if (isset($post['act'])) {
                switch ($post['act']) {
                default:
                    throw new Exception("action {$post['act']} not defined");
                }
            }
        }
        return $content;
    }

//	public function soap($diagramId, $runId) {
//		$graph = '';
////		$this->soapRunIds[] = $runId;
//		$view = new DiagramView($diagramId);
//		$interface = $view->browseDetail();
////		$interface = $this->run('diagramView', $diagramId, Controller::BROWSE);
////		$graph = $this->run('diagramView', $diagramId, Controller::IMAGE);
//		return array(
//			'interface' => $interface,
//			'graph' => $graph,
//		);
//	}
}

?>