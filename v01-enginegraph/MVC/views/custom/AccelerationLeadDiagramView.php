<?php

class AccelerationLeadDiagramView extends AccelerationDiagramView
{

    public function toJsonQuery($gd)
    {

        return parent::toJsonQuery($this->transform($gd));
    }

    private function transform($gd)
    {
        $numOfLines = count($gd);
        $numOfGears = $numOfLines / 2;
        $outputGd = [];
        $result1 = [];
        $result2 = [];
        for ($g = 0; $g < $numOfGears; $g++) {
            $gd[$g]->lerp();
            $result1 = array_merge($result1, $gd[$g]->getData());
            $gd[$g + $numOfGears]->lerp();
            $result2 = array_merge($result2, $gd[$g + $numOfGears]->getData());
        }
        $diff = [];
        for ($i = 0; $i < count($result1); $i++) {
            if (isset($result1[$i]) && isset($result2[$i]))
                $diff[$i] = $result1[$i] - $result2[$i];
        }
        $outputGd[0] = $gd[0];
        $outputGd[0]->setData($diff);
        return $outputGd;
    }

}

?>
