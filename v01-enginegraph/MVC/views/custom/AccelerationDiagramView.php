<?php

class AccelerationDiagramView extends DiagramView
{
//	function __construct($uid = NULL) {
//		parent::__construct(Diagram::ACCELERATION);
//	}

    public function getTitle($headTitle, $action = NULL)
    {
        $vehicle = $this->Diagram->getVehicle();
        if (empty($vehicle)) {
            if (!empty(Get::$getVars['vehicle_id'])) {
                $vehicleIds = explode(',', Get::$getVars['vehicle_id']);
                if (is_numeric($vehicleIds[0])) {
                    try {
                        $vehicle = new Vehicle($vehicleIds[0]);
                    } catch (dbTableException $e) {

                    }
                }
            }
        }
        if (!empty($vehicle)) {
            $s = [];
            $s[] = $vehicle->getMerk();
            $s[] = $vehicle->getModel();
            $s[] = Strings::show('AccelerationDiagram') . ' - ';

            $t = $vehicle->getTransmission();
            if (!empty($t)) {
                $s1 = $t->getName();
                if (!empty($s1)) {
                    $s[] = Strings::show('transmissie') . ':';
                    $s[] = $t->getName();
                }
            }

            $v = $vehicle->getMaxPower();
            if (!empty($v)) {
                $s[] = strtolower(Strings::show('power')) . ':';
                $s[] = str_replace(' ', '', $v);
            }

            $s = implode(' ', $s);
            $s = str_replace(['[copy]'], '', $s);
            return $s;
        }
        return '';
    }
}

?>
