<?php

class PowerTorqDiagramView extends TorqDiagramView
{

    public function hidePointLabels()
    {
        return ['nm', 'hp'];
    }

    public function getTitle($headTitle, $action = NULL)
    {
        $vehicle = $this->Diagram->getVehicle();
        if (empty($vehicle)) {
            if (!empty(Get::$getVars['vehicle_id'])) {
                $vehicleIds = explode(',', Get::$getVars['vehicle_id']);
                if (is_numeric($vehicleIds[0])) {
                    try {
                        $vehicle = new Vehicle($vehicleIds[0]);
                    } catch (dbTableException $e) {

                    }
                }
            }
        }
        if (!empty($vehicle)) {
            $s = [];
            $s[] = $vehicle->getMerk();
            $s[] = $vehicle->getModel();
            $s[] = Strings::show('PowerTorqDiagram') . ' - ';

            $v = $vehicle->getMaxPower();
            if (!empty($v)) {
                $s[] = strtolower(Strings::show('power')) . ':';
                $s[] = str_replace(' ', '', $v);
            }

            $s = implode(' ', $s);
            $s = str_replace(['[copy]'], '', $s);
            return $s;
        }
        return null;
    }

}

?>
