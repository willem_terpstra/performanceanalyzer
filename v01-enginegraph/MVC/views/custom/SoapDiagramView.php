<?php

class SoapDiagramView extends DiagramView
{

    private $enabled = [];

    public function setEnabled($enabled)
    {
        $enabled = explode(',', $enabled);
        $enabled = array_flip($enabled);
        foreach ($enabled as $field => $enable) {
            $this->enabled[$field] = 1;
        }
    }

    public function getHtml($sessionTag = NULL, $form = null)
    {
        if (empty($sessionTag)) {
            throw new DiagramException('SessionTagEmpty');
        } else {
            $vehicle = Sandbox::get($sessionTag);
//Klog::LogInfo(var_export($vehicle, true));
        }
        $errors = $this->Diagram->validateInput($vehicle);
        if (!empty($errors)) {
            $t = new TemplateEngine('MVC/html/custom/frontend/diagram/errors.html');
            $t->assign('vehicle', $vehicle);
            $t->assign('errors', $errors);
            return $t->execTemplate();
        }
        $t = new TemplateEngine('MVC/html/soap/frontend/diagram/diagram' . $this->Diagram->getUsedClassName() . '.php');
        $t->assign('diagram', $this->Diagram);
        $t->assign('vehicle', $vehicle);
        $t->assign('form', $form);
        $t->assign('enabled', $this->enabled);
        $html = $t->execTemplate();
        return $html;
    }

}

?>
