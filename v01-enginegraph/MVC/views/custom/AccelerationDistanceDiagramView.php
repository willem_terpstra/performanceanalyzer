<?php

class AccelerationDistanceDiagramView extends AccelerationDiagramView
{

    public function toJsonQuery($gd)
    {
        return parent::toJsonQuery($this->transform($gd));
    }

    private function transform($gd)
    {
        $numOfVehicles = count(Sandbox::getAll());
        $numOfLines = count($gd);
        $numOfGears = $numOfLines / $numOfVehicles;
        $u = NULL;
        $distance = 402.336;
        foreach ($gd as $g) {
            if ($g->getYUnit() != 'm') {
//				$u = new Unit();
//				$u = $u->loadByNaam($g->getYUnit());
                $f = Unit::getFactor('m', $g->getYUnit());
                $distance = round(402.336 * $f, 0);
            }
            $data = $g->getData();
            $newData = [];
            if (!empty($data) && max($data) > $distance && min($data) < $distance) {
                $prevValue = NULL;
                $prevKey = NULL;
                foreach ($data as $key => $value) {
                    if ($prevValue && $prevValue < $distance && $value > $distance) {
                        $xHigh = $key;
                        $yHigh = $value;
                        $y = $distance;
                        $xLow = $prevKey;
                        $yLow = $prevValue;
                        $xd = $xHigh - $xLow;
                        $yd = $yHigh - $yLow;
                        $d = $y - $yLow;
                        $r = $d / $yd;
                        $x = $xLow + ($xd * $r);
                        $newData[(string)$x] = $y;
                        break;
                    } else {
                        $newData[$key] = $value;
                    }
                    $prevValue = $value;
                    $prevKey = $key;
                }
            }
            if ($newData) {
                $g->setData($newData);
            }
        }
        return $gd;
    }
}

?>
