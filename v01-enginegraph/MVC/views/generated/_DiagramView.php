<?php

class _DiagramView extends View
{
    /**
     *
     * @var type Diagram
     */

    protected $Diagram;
    private $redirectAfter = NULL;

    function __construct($uid = NULL)
    {
        $this->Diagram = new Diagram($uid);
    }

    public function getInstance()
    {
        return $this->Diagram;
    }

    public function getMessages()
    {
        return $this->Diagram->getMessages();
    }

    public function setMessages($messages)
    {
        $this->Diagram->setMessages($messages);
    }

    public function getDiagram()
    {
        return $this->Diagram;
    }

    public function lijst($sql = null)
    {
        $Diagram = new Diagram();
        $Diagram = $Diagram->lijst($sql);
        $t = new TemplateEngine("DiagramLijst.html");
        $t->assign("Diagram", $Diagram);
        return $t->execTemplate();
    }

    public function browseLijst($sql = null)
    {
        $Diagram = new Diagram();
        $Diagram = $Diagram->lijst($sql);
        $t = new TemplateEngine("browseDiagramLijst.html");
        $t->assign("Diagram", $Diagram);
        return $t->execTemplate();
    }

    public function detail()
    {
        $t = new TemplateEngine("DiagramDetail.html");
        $t->assign('messages', $this->getMessages());
        $t->assign("record", $this->Diagram);

        return $t->execTemplate();
    }

    public function browseDetail()
    {
        $t = new TemplateEngine("browseDiagramDetail.html");
        $t->assign("record", $this->Diagram);

        return $t->execTemplate();
    }

    public function validate()
    {
        $post = Post::$postVars;
        $r = $this->Diagram->validate($post);
        $m = $this->Diagram->getMessages();
        if (empty($m)) {
            return true;
        }
        return false;
    }

    public function save()
    {
        $post = Post::$postVars;

        if (array_key_exists('uid', $post)) {
            $this->Diagram->setUid($post['uid']);
        }

        if (array_key_exists('is_active', $post)) {
            $this->Diagram->setIsActive($post['is_active']);
        }

        if (array_key_exists('x_unit_group_id', $post)) {
            $this->Diagram->setXUnitGroupId($post['x_unit_group_id']);
        }

        if (array_key_exists('y_unit_group_id', $post)) {
            $this->Diagram->setYUnitGroupId($post['y_unit_group_id']);
        }

        if (array_key_exists('name', $post)) {
            $this->Diagram->setName($post['name']);
        }

        if (array_key_exists('description', $post)) {
            $this->Diagram->setDescription($post['description']);
        }

        if (array_key_exists('sort_order', $post)) {
            $this->Diagram->setSortOrder($post['sort_order']);
        }

        if (array_key_exists('collect_result', $post)) {
            $this->Diagram->setCollectResult($post['collect_result']);
        }


        try {
            $this->Diagram->saveOrUpdate();
        } catch (dbTableException $e) {
            $this->Diagram->setMessage($e->getMessage());
            return $this->detail();
        }
        return $this->quit();

    }

    public function quit()
    {
        if ($this->redirectAfter == NULL) {
            header("location: /Diagram/browseLijst");
            exit;
        }
        header("location: {$this->redirectAfter}");
        exit;
    }

    protected function setRedirectAfter($url)
    {
        $this->redirectAfter = $url;
    }

    public function remove()
    {
        $this->Diagram->remove();
        return $this->quit();
    }


}
