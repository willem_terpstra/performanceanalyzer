<?php

class _DriverView extends View {
/**
*
* @var type Driver
*/

protected $Driver;
private    $redirectAfter = NULL;

function __construct($uid = NULL)    {
$this->Driver = new Driver($uid);
}

public function getInstance() {
return $this->Driver;
}

public function getMessages() {
return $this->Driver->getMessages();
}

public function setMessages($messages) {
$this->Driver->setMessages($messages);
}

public function getDriver () {
return $this->Driver;
}

public function lijst($sql = null)    {
$Driver = new Driver();
$Driver = $Driver->lijst($sql);
$t = new TemplateEngine("DriverLijst.html");
$t->assign("Driver",$Driver);
return $t->execTemplate();
}

public function browseLijst($sql = null)
{    $Driver = new Driver();
$Driver = $Driver->lijst($sql);
$t = new TemplateEngine("browseDriverLijst.html");
$t->assign("Driver",$Driver);
return $t->execTemplate();
}

public function detail()
{    $t = new TemplateEngine("DriverDetail.html");
$t->assign('messages', $this->getMessages());
$t->assign("record",$this->Driver);

return $t->execTemplate();
}

public function browseDetail()
{    $t = new TemplateEngine("browseDriverDetail.html");
$t->assign("record",$this->Driver);

return $t->execTemplate();
}

public function validate() {
$post = Post::$postVars;
$r = $this->Driver->validate($post);
$m = $this->Driver->getMessages();
if(empty($m)) {
return true;
}
return false;
}

public function save()
{
$post = Post::$postVars;

if(array_key_exists('uid', $post)) {
$this->Driver->setUid ($post['uid']);
} 

if(array_key_exists('users_id', $post)) {
$this->Driver->setUsersId ($post['users_id']);
} 

if(array_key_exists('vehicle_id', $post)) {
$this->Driver->setVehicleId ($post['vehicle_id']);
} 

if(array_key_exists('driver_name', $post)) {
$this->Driver->setDriverName ($post['driver_name']);
} 

if(array_key_exists('driver_weight', $post)) {
$this->Driver->setDriverWeight ($post['driver_weight']);
} 


try {
$this->Driver->saveOrUpdate();
} catch (dbTableException $e) {
$this->Driver->setMessage($e->getMessage());
return $this->detail();
}
return $this->quit();

}

public function quit() {
if($this->redirectAfter == NULL ) {
header ("location: /Driver/browseLijst" );
exit;
}
header ("location: {$this->redirectAfter}");
exit;
}

protected function setRedirectAfter($url) {
$this->redirectAfter = $url;
}

public function remove()
{    $this->Driver->remove();
return $this->quit();
}


}
