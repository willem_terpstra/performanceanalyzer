<?php

class DiagramView extends _DiagramView
{

    private $r = NULL;
    private $diagramR = NULL;
    private $legend = [];
    private $description = NULL;
    private $diagramYUnit = NULL;
    private $diagramXUnit = NULL;
    private $userYUnit = NULL;
    private $userXUnit = NULL;
    private static $frmCount = 0;

    const DETAIL_DATA = 1;
    const DETAIL_R = 2;

    /**
     * DiagramView constructor.
     * @param null $uid
     */
    public function __construct($uid = NULL)
    {
        $d = new Diagram($uid);
        if (empty($uid)) {
            $this->Diagram = $d;
            return;
        }
        $n = $d->getName();
        $cc = camelCase($n);
        $cc .= 'Diagram';
        $this->Diagram = new $cc($uid);
        $diagramR = new DiagramR();
        $diagramR = $diagramR->lijst('SELECT * FROM diagram_r WHERE diagram_id= ? and inactive=?', [$uid, 0]);
        $this->diagramR = [];
        foreach ($diagramR as $dr) {
            $this->diagramR[] = $dr;
        }
    }

    public function setR($r)
    {
        $this->r = $r;
    }

    public function setLegend($tag, $legend)
    {
        $this->legend[$tag] = $legend;
    }

    public function setDescription($desc)
    {
        $this->description = $desc;
    }

    public function getVehicle()
    {
        return $this->Diagram->getVehicle();
    }

    public function setVehicle(Vehicle $vehicle)
    {
        $this->Diagram->setVehicle($vehicle);
    }

    public static function getVehicleSelectbox($tag, $diagramId)
    {
        $sql = "SELECT * FROM vw_vehicle_diagram where diagram_id=$diagramId";

        $diagramVehicles = new VwVehicleDiagram();
        $merkenLijst = [];
        $first = NULL;
        foreach ($diagramVehicles->lijst($sql) as $instance) {
            if (empty($first)) {
                $first = $instance->getMerkenId();
            }
            $merkenLijst[$instance->getMerkenId()] = $instance->getNaam();
        }
        $t = new TemplateEngine('MVC/html/custom/frontend/diagram/view/vehicleSelectbox/selectmerkbox.php');
        $merkSelector = 'modal_merken_id';
        $t->assign('id', $merkSelector);
        $t->assign('options', $merkenLijst);

        $merken = $t->execTemplate();

        $typeSelector = 'modal_engine_id';
        $types = self::getVehicleTypes($first, $diagramId, $typeSelector);

        $t = new TemplateEngine('MVC/html/custom/frontend/diagram/view/vehicleSelectbox/main.html');
        $t->assign('tag', $tag);
        $t->assign('diagramId', $diagramId);
        $t->assign('merkSelector', $merkSelector);
        $t->assign('merken', $merken);
        $t->assign('typeSelector', $typeSelector);
        $t->assign('types', $types);
        return $t->execTemplate();
    }

    public static function getVehicleTypes($merkId, $diagramId, $typeSelector)
    {
        $sql = "SELECT * FROM vw_vehicle_diagram WHERE (diagram_id =$diagramId AND merken_id =$merkId)";
        $vehicles = new VwVehicleDiagram();
        $opt = [];
        foreach ($vehicles->lijst($sql) as $vehicle) {
            $v = new Vehicle($vehicle->getVehicleId());
            $opt[$vehicle->getVehicleId()] = $v->getModel() . ' ' . $v->getMaxPowerRpm() . ' ' . $v->getMaxTorqRpm();
        }
        $t = new TemplateEngine('MVC/html/custom/frontend/diagram/view/vehicleSelectbox/selecttypebox.php');
        $t->assign('id', $typeSelector);
        $t->assign('options', $opt);
        return $t->execTemplate();
    }

    private function collectData(R $r)
    {
        $result = [];

        $vehicle = $this->getVehicle();
        if (!$this->Diagram->useFormula($r, $vehicle)) {
            return false;
        }

        $result = $r->getResult($vehicle);
        if ($result === false) {
            return false;
        }

        // converteer eindresultaat naar juiste unit
        // unit van r
        $yUnit = $r->getUnit(NULL, true);
        // unit van diagram
        $diagramYUnit = $yUnit->getUnitGroup();
        $diagramUserYUnit = Unit::$units[$diagramYUnit];

        $this->userYUnit = $diagramUserYUnit;
        $this->diagramYUnit = $yUnit->getNaam();
//        if ($this->userYUnit != $this->diagramYUnit) {
//            $yUnit = $yUnit->loadByNaam($this->userYUnit);
//            // convert
//            $yFactor = Unit::getFactor($this->diagramYUnit, $this->userYUnit);
//        } else {
//            $yFactor = 1;
//        }

        $xUnit = new Unit();
        $xUnit = $xUnit->loadByNaam($result['xUnit']);
        $xUnitGroup = $xUnit->getUnitGroup();
        $this->userXUnit = Unit::$units[$xUnitGroup];
        $this->diagramXUnit = $xUnit->getNaam();
//        if ($this->userXUnit != $this->diagramXUnit) {
//            $xUnit = $xUnit->loadByNaam($this->userXUnit);
//            // convert
//            $xFactor = Unit::getFactor($this->diagramXUnit, $this->userXUnit);
//        } else {
//            $xFactor = 1;
//        }

//        if (($xFactor != 1) || ($yFactor != 1)) {
//            $convertedData = [];
//            $convertedMisc = [];
//
//            $i = 0;
//            foreach ($result['data'] as $x => $y) {
//                $convertedData[(string)($x * $xFactor)] = $y * $yFactor;
//                $i++;
//            }
//            foreach ($convertedMisc as &$misc) {
//                $misc = array_values($misc);
//            }
//            $result['data'] = $convertedData;
//        }
        return $result;
    }

    public function collect($lineColor = NULL)
    {
        global $collect_result;
        global $collected_result;
        $errors = [];
        $processedErrors = [];
        $graphDescriptors = [];
        $userUnits = Unit::getUnits();
        $xUnitName = $userUnits[$this->Diagram->getXUnitGroup(NULL, true)->getName()];
        $yUnitGroup = $this->Diagram->getYUnitGroup(NULL, true);
        $yUnitGroupName = $yUnitGroup->getName();
        $yUnitName = $userUnits[$yUnitGroup->getName()];

        $diagramRArr = $this->diagramR;
        if (!is_array($diagramRArr)) {
            $diagramRArr = [0 => $this->diagramR];
        }

        $run = $this->getVehicle()->getSelectedRun();
        foreach ($diagramRArr as $diagramR) {
            $r = new R($diagramR->getRId());

            $this->setLegend('r_naam', $r->getName());
            $this->setLegend('run_value', $run->getValue());
            $this->setLegend('v_legend', $this->getVehicle()->getLegend());
            try {
                $collect_result = explode(',', $diagramR->getCollectResult());
                $collected_result = [];
                $data = $this->collectData($r);
                if ($data === false) {
                    continue;
                }
            } catch (RException $e) {
                if ($e->getCode() == RException::PARSE_FORMULA_FAILED) {
                    $params = $e->getParams();
                    $errors[] = $params[RException::PARSE_FORMULA_FAILED];
                    $errors[] = $e->getMessage();
                } elseif ($this->Diagram->validateException($e->getParams())) {  // vraag het betreffend diagram
                    // ignore
                    continue;
                } else {
                    $errors[] = $e->getMessage();
                }
            } catch (Exception $e) {
                $errors[] = $e->getMessage();
            }

            $gd = new GraphDescriptor();
            if (!empty($errors)) {
                if (empty($processedErrors)) {
                    $newErrors = array_unique($errors);
                } else {
                    $newErrors = array_diff($errors, $processedErrors);
                }
                if (empty($newErrors)) {
                    continue;
                }
                $gd->setMessages($newErrors);
                $graphDescriptors[] = $gd;

                foreach ($newErrors as $newError) {
                    $processedErrors[] = $newError;
                }
                continue;
            }
            $gd->setData($data['data']);
            $gd->setMiscData($collected_result);
            $gd->setYUnitName($this->Diagram->getName() . ' ' . $this->userYUnit);
            $gd->setLineColor($lineColor);

            $legends = [];
            $legend = $this->getVehicle()->getLegend();
            if (!empty($legend)) {
                $legends[] = $legend;
            }
            $gd->setLegend(implode(' ', $legends));
            $gd->setDescription($this->description);
            $yUnitGroupRName = $r->getUnit(NULL, true)->getUnitGroup(NULL, true)->getName();
            if ($yUnitGroupRName != $yUnitGroupName) {
                $gd->setYUnit($userUnits[$yUnitGroupRName]);
            } else {
                $gd->setYUnit($yUnitName);
            }
            $gd->setXUnit($xUnitName);

            $gd->setTitle(Strings::show($this->Diagram->getDescription()));
            $graphDescriptors[] = $gd;
        }
        return $graphDescriptors;
    }

    public function browseDetail($sessionTag = NULL, $form = true)
    {
        global $layout;
        $vehicle = NULL;
        if (!empty($sessionTag) && !is_numeric($sessionTag)) {
            $vehicle = Sandbox::get($sessionTag);
            if ($vehicle == NULL) {
                throw new DiagramException('Vehicle ' . $sessionTag . ' not found in sandbox');
            }
        } elseif (!array_key_exists('vehicle_id', Get::$getVars)) {
            foreach (Sandbox::getAll() as $sandbox) {
                $vehicle = $sandbox;
                break;
            }
            if (empty($vehicle)) {
                throw new DiagramException('NoVehicleInGet', DiagramException::NO_VEHICLE);
            }
        } else {
            $vehicle = new Vehicle(Get::$getVars['vehicle_id']);
        }

        $t = new TemplateEngine('MVC/html/custom/browseDiagramDetail.php');
        $t->assign('record', $this->Diagram);
        try {
            $xUnitGroup = $this->Diagram->getXUnitGroup();
            $xUnit = Unit::$units[$xUnitGroup];
//            $xUnit = Unit::getUnit($xUnitGroup);
            $xAxisTitle = Strings::show($xUnitGroup) . ' ' . $xUnit;

            $yUnitGroup = $this->Diagram->getYUnitGroup();
            $yUnit = Unit::$units[$yUnitGroup];
            $yAxisTitle = Strings::show($yUnitGroup) . ' ' . $yUnit;
        } catch (UnitException $e) {
            return 'Unit for ' . $e->getMessage() . ' not defined in your units. Go to <a href="/Registreren/" ><b>Registreren</b></a> to do so.';
        }

        $t->assign('vehicle', $vehicle);
        $t->assign('messages', $this->getMessagesAsStrings());
        $t->assign('html', $this->getHtml($sessionTag, $form));
        $t->assign('xAxisTitle', $xAxisTitle);
        $t->assign('yAxisTitle', $yAxisTitle);
        $t->assign('x', $layout->diagram->x);
        $t->assign('y', $layout->diagram->y);
        $t->assign('chartArea', $layout->diagram->chartArea);
        return $t->execTemplate();
    }

    public function browseDetailByVehicle(Vehicle $vehicle)
    {
        global $layout;
        $t = new TemplateEngine('MVC/html/custom/browseDiagramByVehicleDetail.php');

        $links = [];
        $vehicleDiagrams = new VehicleDiagram();
        foreach ($vehicleDiagrams->lijst('SELECT * FROM vehicle_diagram WHERE vehicle_id=' . $vehicle->getUid()) as $vehicleDiagram) {
            $diagramView = new DiagramView($vehicleDiagram->getDiagramId());
            if ($diagramView->getDiagram()->getIsActive()) {
                $links[] = ['label' => $diagramView->getDiagram()->getName(), 'data' => $diagramView->getHtmlByVehicle($vehicle)];
            }
        }
        $tabs = new TemplateEngine('MVC/html/custom/frontend/tabs.php');
        $tabs->assign('tabs', $links);
        $html = $tabs->execTemplate();
        $t->assign('vehicle', $vehicle);
        $t->assign('html', $html);
        $t->assign('frmCount', self::$frmCount);
        $t->assign('x', $layout->diagram->x);
        $t->assign('y', $layout->diagram->y);
        $t->assign('chartArea', $layout->diagram->chartArea);
        return $t->execTemplate();
    }

    public static function showDiff($compareResult)
    {
        $t = new TemplateEngine('MVC/html/custom/bootstrap/table.html');
        $t->assign('showLabels', false);
        $t->assign('data', $compareResult);
        return $t->execTemplate();
    }

    public function detail()
    {
        $rUids = [];
        $data = [];
        $diagramR = new VwDiagramR();
        $sql = "SELECT * FROM vw_diagram_r WHERE diagram_id=" . $this->Diagram->getUid();
        $diagramR = $diagramR->lijst($sql); // loadByDiagramName($this->Diagram->getName());

        $t = new TemplateEngine('MVC/html/custom/VwDiagramRLijst.html');
        $t->assign('VwDiagramR', $diagramR);
        $data = $t->execTemplate();


        $tabs = [self::DETAIL_DATA, self::DETAIL_R];

        $links = [];
        foreach ($tabs as $tab) {
            switch ($tab) {
            case self::DETAIL_DATA:
                $links[] = ['label' => Strings::show('diagram_gegevens'), 'data' => parent::detail()];
                break;
            case self::DETAIL_R:
                $links[] = ['label' => Strings::show('formules'), 'data' => $data];
                break;
            }
        }

        $t = new TemplateEngine('MVC/html/custom/frontend/tabs.php');
        $t->assign('tabs', $links);
        return $t->execTemplate();
    }

    public function getHtml($sessionTag = NULL, $form = null)
    {
        $links = [];
        foreach (Sandbox::getAll() as $vehicle) {
            $close = new TemplateEngine('MVC/html/custom/frontend/diagram/close.php');
            $close->assign('sessionTag', $vehicle->getSessionTag());
            $close = $close->execTemplate();
            $errors = $this->Diagram->validateInput($vehicle);
            if (!empty($errors)) {
                $t = new TemplateEngine('MVC/html/custom/frontend/diagram/errors.html');
                $t->assign('vehicle', $vehicle);
                $t->assign('errors', $errors);

                $links[] = [
                    'label' => $vehicle,
                    'data' => $t->execTemplate(),
                    'close' => $close];
                continue;
            }
            $t = new TemplateEngine('MVC/html/custom/frontend/diagram/diagram' . $this->Diagram->getUsedClassName() . '.html');
            $t->assign('diagram', $this->Diagram);
            $t->assign('vehicle', $vehicle);
            $html = $t->execTemplate();
            $links[] = [
                'label' => $vehicle,
                'data' => $html,
                'close' => $close];
        }
        $tag = Authorization::generatePassword();
        $t = new TemplateEngine('MVC/html/custom/dropdownVehicle.php');
        $t->assign('tag', $tag);
        $t->assign('diagramId', $this->Diagram->getUid());
        $html = $t->execTemplate();
        $html = "<div id=\"$tag\" >$html</div>";

        $links[] = ['label' => '+', 'data' => $html];
        $t = new TemplateEngine('MVC/html/custom/frontend/tabs.php');
        $t->assign('tabsName', 'tabsSandbox');
        $t->assign('tabs', $links);
        return $t->execTemplate();
    }

    public function getHtmlByVehicle(Vehicle $vehicle)
    {
        $errors = $this->Diagram->validateInput($vehicle);
        if (!empty($errors)) {
            $t = new TemplateEngine('MVC/html/custom/frontend/diagram/errors.html');
            $t->assign('vehicle', $vehicle);
            $t->assign('errors', $errors);
            $html = ['label' => $vehicle, 'data' => $t->execTemplate()];
        } else {
            $t = new TemplateEngine('MVC/html/custom/frontend/diagram/diagram' . $this->Diagram->getUsedClassName() . '.html');
            $t->assign('diagram', $this->Diagram);
            $t->assign('frmCount', ++self::$frmCount);
            $t->assign('vehicle', $vehicle);
            $html = $t->execTemplate();
        }
        return $html;
    }

    public function summary()
    {
        $t = new TemplateEngine('MVC/html/custom/frontend/diagram/diagramSummary.html');
        $t->assign('record', $this->Diagram);
        return $t->execTemplate();
    }

    public function getMetaDescription($tags, $action = null)
    {
        $vehicle = $this->Diagram->getVehicle();
        $s[] = $this->getTitle($tags);
        if ($vehicle instanceof Vehicle) {
            $r = $vehicle->getSelectedRun();
            if ($r instanceof Run) {
                $s[] = trim(strip_tags($r->getNaam() . $r->getOmschrijving() . $r->getImportedMessage()));
            }
        }
        return implode('. ', $s);
    }

    public static function getFrmCount()
    {
        return self::$frmCount;
    }

    public function toJsonQuery($gd)
    {
        global $soap;
        if ($soap) {
            $reqId = '%reqId%';
        } else {
            $reqId = $this->getReqId();
        }
        $errors = [];
        foreach ($gd as $g) {
            $errors = array_merge($errors, $g->getMessages());
        }

        if (!empty($errors)) {
            $this->toJsonError($errors);
        }


        $json = [
            'version' => 0.6,
            'reqId' => $reqId,
            'status' => 'ok',
            'table' => [
                'cols' => [
                ],
                'rows' => [
                ]
            ]
        ];

        if ($reqId === NULL) {
            unset($json['reqId']);
        }

        $xArray = [];
        $gCount = 0;
        foreach ($gd as $g) {

            if (empty($json['table']['cols'])) {
                $xUnit = $g->getXUnit();
                $json['table']['cols'] = [
                    [
                        'id' => $xUnit,
                        'label' => $xUnit,
                        'type' => 'number',
//						'p' => array('color' => $g->getLineColor())
                    ]
                ];
            }
            $json['table']['cols'][] =
                [
                    'id' => $g->getRName(),
//						'label' => $g->getLegend(),
                'type' => 'number',
                'p' => [
                    'color' => $g->getLineColor(),
                    'x_unit' => $xUnit,
                    'y_unit' => $g->getYUnit()
                ]
            ];
            $xArray = $xArray + $g->getData();
            $gCount++;
        }
        $n = [];
        foreach ($xArray as $x => $y) {
            $n[$x] = (string)$y;
        }
        $xArray = $n;
        ksort($xArray, SORT_NUMERIC);

        foreach ($xArray as $x => $y) {
            if ($_SESSION['units'] === 'imperial')
            {
                Unit::convert($xUnit, $x);
            }

            $json['table']['rows'][] = [
                'c' => [
                    [
                        'v' => (float)$x,
                        'f' => str_replace(' ', '\hspace{1 mm}', round($x, 2) . ' ' . $xUnit)
                    ]
                ]
            ];
        }
        unset($x);

        // hier hebben we alle x-waarden,
        // nu de bijbehorende y waarden toevoegen
        unset($g);
        foreach ($gd as $g) {
            $yUnit = $g->getYUnit();
            $yArray = $g->getData();
            $miscData = $g->getMiscData();
            $m = [];
            $hiddenPointLabels = $this->hidePointLabels();
            foreach ($miscData as $name => $misc) {
                if (in_array($name, $hiddenPointLabels)) {
                    continue;
                }
                if (is_string($misc) && substr($misc, 0, 1) == '[') {
                    $misc = trim(str_replace(['[', ']'], '', $misc));
                    $m[$name] = explode(' ', trim($misc));
                } else {
                    $i = count($misc['data']) - count($yArray);
                    if ($i >= 0) {
                        $m[$name] = [];
//                        $factor = 1;
                        $usedUnit = Unit::$units[$misc['unit_group']];
//                        if (isset($misc['unit_group'])) {
//                            $usedUnit = Unit::getUnit($misc['unit_group']);
//                            $factor = Unit::convert(1, Unit::getDefaultUnit($misc['unit_group']), $usedUnit);
//                        }
                        if (!empty($usedUnit)) {
                            $usedUnit = ' ' . $usedUnit;
                        }

                        foreach ($yArray as $key => $value) {
                            $m[$name][$key] = round($misc['data'][$i], 2) . ' ' . $usedUnit;
                            $i++;
                        }
                    }
                }
//				echo $m[$name][count($m[$name])] . '<br/>';
                if (is_array($m[$name])) {
                    if (count($yArray) && count($yArray) == count($m[$name])) {
                        $m[$name] = array_combine(array_keys($yArray), $m[$name]);
                    }
                }
            }

            $miscData = $m;
            $rowNr = 0;

            foreach ($xArray as $x => $d) {
                if (isset($yArray[$x])) {
                    $txt = [];

                    foreach ($miscData as $name => $misc) {
                        if (isset($misc[$x])) {
                            $txt[] = "\n" . Strings::show($name) . ': ' . $misc[$x];
                        }
                    }

                    $yValue = $yArray[$x];
                    if ($_SESSION['units'] === 'imperial')
                    {
                        Unit::convert($yUnit, $yValue);
                    }

                    $json['table']['rows'][$rowNr]['c'][] = [
                        'v' => (float)round($yValue, 2),
                        'f' => str_replace(' ', '\hspace{1 mm}', (string)round($yValue, 2) . ' ' . $yUnit . implode(",", $txt))
                    ];
                } else {
                    $json['table']['rows'][$rowNr]['c'][] = ['v' => NULL];
                }
                $rowNr++;
            }
        }
        return 'google.visualization.Query.setResponse(' . json_encode($json, JSON_PRETTY_PRINT) . ');';
    }

    private function toJsonError($errors)
    {
        global $soap;
        if ($soap) {
            $reqId = '%reqId%';
        } else {
            $reqId = $this->getReqId();
        }
        if (!is_array($errors)) {
            $errors = (array)$errors;
        }
        if (empty($errors)) {
            return;
        }

        $json = [
            'version' => 0.6,
            'reqId' => $reqId,
            'status' => 'error',
            'errors' => [
                [
                    'reason' => 'other',
                    'message' => implode(', ', $errors)
                ]
            ]
        ];
        if ($reqId === NULL) {
            unset($json['reqId']);
        }
        echo 'google.visualization.Query.setResponse(' . json_encode($json, JSON_PRETTY_PRINT) . ');';
        exit;
    }

    private function getReqId()
    {
        $reqId = NULL;
        if (array_key_exists('tqx', Get::$getVars)) {
            $tqx = explode(';', Get::$getVars['tqx']);
            foreach ($tqx as $t) {
                if (preg_match('/reqId:(\d+)/', $t, $regs)) {
                    $reqId = $regs[1];
                }
            }
        }
        return $reqId;
    }

    public function hidePointLabels()
    {
        return [];
    }

}
