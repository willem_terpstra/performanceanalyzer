<?php
global $dir;
if (empty($_SERVER['DOCUMENT_ROOT'])) {
    $dir = getcwd();
    $dir = str_replace('\MVC', '', $dir);
}
else
{
    $dir = $_SERVER['DOCUMENT_ROOT'] . '/v01-enginegraph';
}
//	if(isset($_GET['XDEBUG_SESSION_START'])) {
//	    $dir .= '';
//	}
global $filePaths;

$filePaths = [
    'AccelerationDiagram' => "{$dir}/MVC/entity/custom",
    'AccelerationTimeDiagram' => "{$dir}/MVC/entity/custom",
    'Advertenties' => "{$dir}/MVC/entity",
    'AdvertentiesModellen' => "{$dir}/MVC/entity",
    'Auth' => "{$dir}/MVC/entity",
    'Ajax' => "{$dir}/MVC/classes/lib",
    'Authorization' => "{$dir}/MVC/classes/lib",
    'Cache' => "{$dir}/MVC/entity",
    'Chassis' => "{$dir}/MVC/entity",
    'ClassAuth' => "{$dir}/MVC/entity",
    'ClassNames' => "{$dir}/MVC/entity",
    'Clutch' => "{$dir}/MVC/entity",
    'Controller' => "{$dir}/MVC/classes/lib",
    'CorrectionFactor' => "{$dir}/MVC/entity",
    'dbTable' => "{$dir}/MVC/classes/lib",
    'dbTableException' => "{$dir}/MVC/classes/Exceptions",
    'Diagram' => "{$dir}/MVC/entity",
    'DiagramController' => "{$dir}/MVC/controllers",
    'DiagramR' => "{$dir}/MVC/entity",
    'DiagramView' => "{$dir}/MVC/views",
    'Drag' => "{$dir}/MVC/entity",
    'Driver' => "{$dir}/MVC/entity",
    'Dyno' => "{$dir}/MVC/entity",
    'DynoInnodb' => "{$dir}/MVC/entity",
    'DropdownAble' => "{$dir}/MVC/interfaces",
    'Engine' => "{$dir}/MVC/entity",
    'Get' => "{$dir}/MVC/classes/lib",
    'GraphDescriptor' => "{$dir}/MVC/classes/lib",
    'Klogger' => "{$dir}/MVC/classes/lib",
    'Languages' => "{$dir}/MVC/entity",
    'MetaTableSettings' => "{$dir}/MVC/entity",
    'PhpR' => "{$dir}/MVC/classes/lib",
    'Post' => "{$dir}/MVC/classes/lib",
    'R' => "{$dir}/MVC/entity",
    'Rcalc' => "{$dir}/MVC/interfaces",
    'Run' => "{$dir}/MVC/entity",
    'Sandbox' => "{$dir}/MVC/classes/lib",
    'SecundaryTransmission' => "{$dir}/MVC/entity",
    'Strings' => "{$dir}/MVC/entity",
    'TemplateEngine' => "{$dir}/MVC/classes/lib",
    'Transmission' => "{$dir}/MVC/entity",
    'Unit' => "{$dir}/MVC/entity",
    'UnitGroup' => "{$dir}/MVC/entity",
    'Users' => "{$dir}/MVC/entity",
    'Vehicle' => "{$dir}/MVC/entity",
    'VehicleDiagram' => "{$dir}/MVC/entity",
    'VehicleException' => "{$dir}/MVC/classes/lib/exceptions",
    'View' => "{$dir}/MVC/classes/lib",
    'VwLogin' => "{$dir}/MVC/entity",
    'VwDyno' => "{$dir}/MVC/entity",
    '_Advertenties' => "{$dir}/MVC/entity/generated",
    '_AdvertentiesModellen' => "{$dir}/MVC/entity/generated",
    '_Auth' => "{$dir}/MVC/entity/generated",
    '_Cache' => "{$dir}/MVC/entity/generated",
    '_Chassis' => "{$dir}/MVC/entity/generated",
    '_ClassAuth' => "{$dir}/MVC/entity/generated",
    '_ClassNames' => "{$dir}/MVC/entity/generated",
    '_Clutch' => "{$dir}/MVC/entity/generated",
    '_CorrectionFactor' => "{$dir}/MVC/entity/generated",
    '_Diagram' => "{$dir}/MVC/entity/generated",
    '_DiagramR' => "{$dir}/MVC/entity/generated",
    '_DiagramView' => "{$dir}/MVC/views/generated",
    '_Drag' => "{$dir}/MVC/entity/generated",
    '_Driver' => "{$dir}/MVC/entity/generated",
    '_Dyno' => "{$dir}/MVC/entity/generated",
    '_DynoInnodb' => "{$dir}/MVC/entity/generated",
    '_Engine' => "{$dir}/MVC/entity/generated",
    '_Languages' => "{$dir}/MVC/entity/generated",
    '_MetaTableSettings' => "{$dir}/MVC/entity/generated",
    '_R' => "{$dir}/MVC/entity/generated",
    '_Run' => "{$dir}/MVC/entity/generated",
    '_SecundaryTransmission' => "{$dir}/MVC/entity/generated",
    '_Strings' => "{$dir}/MVC/entity/generated",
    '_Transmission' => "{$dir}/MVC/entity/generated",
    '_Unit' => "{$dir}/MVC/entity/generated",
    '_UnitGroup' => "{$dir}/MVC/entity/generated",
    '_Users' => "{$dir}/MVC/entity/generated",
    '_Vehicle' => "{$dir}/MVC/entity/generated",
    '_VehicleDiagram' => "{$dir}/MVC/entity/generated",
    '_VwDyno' => "{$dir}/MVC/entity/generated",
    '_VwLogin' => "{$dir}/MVC/entity/generated",

];


$includePaths = [
    "{$dir}/MVC/classes",
    "{$dir}/MVC/classes/lib",
//		"{$dir}/MVC/classes/traits",
    "{$dir}/MVC/interfaces",
#		"{$dir}/MVC/classes/required_entitys",
//		"{$dir}/gsuggest",
//		"{$dir}/MVC",
    "$dir/MVC/html/custom",
    "{$dir}/MVC/entity",
    "{$dir}/MVC/entity/generated",
    "{$dir}/MVC/entity/custom",
    "{$dir}/MVC/views",
    "{$dir}/MVC/views/generated",
    "{$dir}/MVC/views/custom",
    "{$dir}/MVC/html",

    "{$dir}/MVC/controllers",
    "{$dir}/MVC/classes/Exceptions",
    "{$dir}/MVC/controllers/custom",

];

foreach ($includePaths as $includePath) {    # $includePath = str_replace('/','\\',$includePath);
    set_include_path($includePath . PATH_SEPARATOR . get_include_path());
}

function autoload($className)
{
    global $filePaths;
    if (isset($filePaths[$className])) {
        require $filePaths[$className] . '/' . $className . '.php';
    } else {
 echo "'$className' =>\n";
        require $className . '.php';
    }
}

spl_autoload_register('autoload');
?>