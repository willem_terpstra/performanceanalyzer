<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
define ('WP_USE_EXT_MYSQL', false);

/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'password');

/** MySQL hostname */
define('DB_HOST', 'wordpress');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'axaJ#DVUfTJ}Dn+N8oIBok@j6Bty5icTANZq9AU?2h!+fj^$T!h!PYLL7m/ZpxY?');
define('SECURE_AUTH_KEY',  '~X[ xl2,[T%SQm^Yg+|[6^ddB<1TyUNAHYp>#|5GWo~P (17Z^ :--cE$m+i:l+T');
define('LOGGED_IN_KEY',    '117Q1bt7J<fKx@eMz$^s& 4?*&@ex3ug!{M2BpuHTpvF<|KK::@3d;Punvv1GG{3');
define('NONCE_KEY',        '?,zn7RwL^mdKg74XNg4UJ.neQWjT|1-0-+Rc9Pudt,74Xn6Fh-FtY`#fFS!H/nxT');
define('AUTH_SALT',        'R.Joh_9.O)9n%#EZ>UO6Om >))5n!HX]tg<z[@QFf<JUa71P=,Q)gaUtxbOPrK^n');
define('SECURE_AUTH_SALT', 'P`xh9!^V5T_F+`_/=4~|4q_~oYY!_6w]?7%hU&@y)w+Sw+X)*(l$>@(r1,;B3]^y');
define('LOGGED_IN_SALT',   'TcX4`3UC;wPT%`V0|BHw zA?8$Df5|S|,B`W)sVNf]?nc[&n1L[@JTv)sSA.=b!R');
define('NONCE_SALT',       'Lg}^?S.6@gE+O,-@$SR}S;s[>dY`T1,w9T1mnYs|AT%=kpLJ8trPKm;Gf_=kK|QY');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

//-WP Tuner Plugin by MrPete------------
//--------------------------------------
$wpTunerStart = microtime();					// get start time as early as we can
if ( function_exists( 'getrusage' ) ) { $wpTunerStartCPU = getrusage(); }
//@include_once(dirname(__FILE__).'/wp-content/plugins/wptuner/wptunertop.php'); // fire up WPTuner
//--------------------------------------
//-END WP Tuner Plugin------------------
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
if (!defined('WP_POST_REVISIONS'))
{
    define('WP_POST_REVISIONS', false);
}
