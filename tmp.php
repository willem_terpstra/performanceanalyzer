<?php			$model_list = get_the_term_list($post->ID, 'model', 'Model: ', ', ', '');
			$year_list = get_the_term_list($post->ID, 'year', 'Year: ', ', ', '');
			$taxo_text = '';
			if (!empty($model_list)) {
				$taxo_text .= "$model_list<br />\n";
			}
			if (!empty($year_list)) {
				$taxo_text .= "$year_list";
			}
			if (!empty($taxo_text)) {
				?>  
				<div class="entry-utility">  
					<?php
					echo $taxo_text;
					?>
				</div>
				<?php
			}
?>